<?php return array (
  'domain' => NULL,
  'plural-forms' => 'nplurals=2; plural=(n != 1);',
  'messages' => 
  array (
    '' => 
    array (
      '' => 
      array (
        0 => 'Project-Id-Version: Daekin
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2020-05-20 14:18+0200
PO-Revision-Date: 2021-05-06 17:26+0200
Language: es
X-Generator: Poedit 2.4.3
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: cms/php
X-Poedit-SearchPath-2: assets/js
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: alerts
X-Poedit-SearchPathExcluded-2: lang
X-Poedit-SearchPathExcluded-3: uploads
X-Poedit-SearchPathExcluded-4: assets/css
X-Poedit-SearchPathExcluded-5: assets/fonts
X-Poedit-SearchPathExcluded-6: assets/ico
X-Poedit-SearchPathExcluded-7: assets/img
X-Poedit-SearchPathExcluded-8: assets/less
X-Poedit-SearchPathExcluded-9: cms/alerts
X-Poedit-SearchPathExcluded-10: cms/assets
X-Poedit-SearchPathExcluded-11: cms/lang
X-Poedit-SearchPathExcluded-12: cms/logs
X-Poedit-SearchPathExcluded-13: cms/site
X-Poedit-SearchPathExcluded-14: cms/uploads
X-Poedit-SearchPathExcluded-15: backup
X-Poedit-SearchPathExcluded-16: assets/js
',
      ),
      'BACKGROUND' => 
      array (
        0 => 'CONTEXTO',
      ),
      'THE CHALLENGE' => 
      array (
        0 => 'RETO',
      ),
      'THE PROJECT' => 
      array (
        0 => 'PROYECTO',
      ),
      'ACTIVITIES AND OUTCOMES' => 
      array (
        0 => 'ACTIVIDADES Y RESULTADOS',
      ),
      'NEWS' => 
      array (
        0 => 'NOTICIAS',
      ),
      'Downloads' => 
      array (
        0 => 'Descargas',
      ),
      'Download' => 
      array (
        0 => 'Descargar',
      ),
      'Contact' => 
      array (
        0 => 'Contacto',
      ),
      '<span class=\'blue\'>SECURE DIGITAL BUSINESS</span> COLLABORATION MAINTAINING DATA SOVEREIGNTY' => 
      array (
        0 => 'COLABORACIÓN <span class=\'blue\'>DIGITAL SEGURA ENTRE EMPRESAS </span> SIN PÉRDIDA DE LA SOBERANÍA DEL DATO',
      ),
      'DAEKIN\'S CHALLENGE' => 
      array (
        0 => 'EL RETO DE DAEKIN',
      ),
      'DAEKIN (Datuak Konpartitzeko Ekimena) is a Type I ELKARTEK project, funded by the Basque Government\'s 2020 call.' => 
      array (
        0 => 'DAEKIN (Datuak Konpartitzeko Ekimena) es un proyecto ELKARTEK de tipo I subvencionado por el Gobierno Vasco en la convocatoria 2020.',
      ),
      'Project funded by the Basque Government\'s Department for  Economic Development, Sustainability and the Environment (ELKARTEK Programme).' => 
      array (
        0 => 'Proyecto financiado por el Departamento de Desarrollo Económico, Sostenibilidad y Medio Ambiente del Gobierno Vasco (Programa ELKARTEK)',
      ),
      'DEPARTMENT FOR ECONOMIC DEVELOPMENT, SUSTAINABILITY AND THE ENVIRONMENT ELKARTEK 2020 PROGRAMME.' => 
      array (
        0 => 'DAEKIN ES UN PROYECTO FINANCIADO POR EL PROGRAMA ELKARTEK 2020 DEL DEPARTAMENTO DE DESARROLLO ECONÓMICO, MEDIO AMBIENTE Y SOSTENIBILIDAD DEL GOBIERNO VASCO.',
      ),
      'THE PROJECT FOCUSES ON RESEARCH INTO METHODS, TECHNIQUES AND TECHNOLOGIES FOR SECURE DIGITAL DATA SHARING.' => 
      array (
        0 => 'EL PROYECTO SE CENTRA EN LA INVESTIGACIÓN DE METODOLOGÍAS, TÉCNICAS Y TECNOLOGÍAS PARA LA COMPARTICIÓN DIGITAL SEGURA DE LOS DATOS.',
      ),
      'Its main aim is to <strong>investigate the technological advances needed to define a reference architecture for a data sharing platform</strong>, more specifically for the energy sector.' => 
      array (
        0 => 'Su objetivo principal es <strong>investigar los avances tecnológicos necesarios para la definición de una arquitectura de referencia de una plataforma de compartición de datos</strong>, en específico para el sector energético.',
      ),
      'the project' => 
      array (
        0 => 'El Proyecto',
      ),
      'Keep up to date with developments in the project.' => 
      array (
        0 => 'Mantente al día con información actualizada sobre el proyecto.',
      ),
      'Learn more' => 
      array (
        0 => 'Saber más',
      ),
      'Activity' => 
      array (
        0 => 'Actividad',
      ),
      'OUTCOME' => 
      array (
        0 => 'RESULTADO',
      ),
      'See all' => 
      array (
        0 => 'Ver todas',
      ),
      'THE CONSORTIUM' => 
      array (
        0 => 'EL CONSORCIO',
      ),
      'The following participants are involved in this project' => 
      array (
        0 => 'En este proyecto convergemos diversos agentes',
      ),
      'Technology Centre - Coordinator' => 
      array (
        0 => 'Centro tecnológico – Coordinador',
      ),
      'Technology Centre' => 
      array (
        0 => 'Centro tecnológico',
      ),
      'Business R&D unit' => 
      array (
        0 => 'Unidad de I+D empresarial',
      ),
      'Supply/demand broker' => 
      array (
        0 => 'Agente intermediación oferta/demanda',
      ),
      'CONTACT DAEKIN' => 
      array (
        0 => 'PONTE EN CONTACTO CON DAEKIN',
      ),
      'Contact us for further information or if you wish to collaborate with us or send us your feedback.' => 
      array (
        0 => 'Escríbenos si quieres más información, colaborar o darnos tu opinión.',
      ),
      'Background' => 
      array (
        0 => 'Contexto',
      ),
      'GAIA-X AND THE EU DATA SPACE ENERGY' => 
      array (
        0 => 'GAIA-X Y EL EU DATA SPACE ENERGY',
      ),
      'In late 2019, the German government launched the GAIA-X project, aimed at creating <strong>an open, federated data infrastructure</strong>, based on European values and consisting of components and services to access, store, exchange and use data, following a set of predefined rules. Seven European governments and 22 businesses from France and Germany have joined the project, which aims to create an ecosystem of developers, suppliers and users of digital products and services, based on digital sovereignty and used as a key foundation for growth in Europe, using digital innovation and developing new business models.' => 
      array (
        0 => 'A finales de 2019 el Gobierno de Alemania lanzó la iniciativa “GAIA-X” para la creación de <strong>una infraestructura de datos abierta y federada</strong>, basada en valores europeos, consistente en componentes y servicios que hagan posible el acceso a datos, así como su almacenamiento, intercambio y uso según reglas predefinidas. Siete gobiernos europeos y 22 empresas (alemanas y francesas) se han ido sumando a esta iniciativa, que pretende crear un ecosistema de desarrolladores, proveedores y usuarios de productos y servicios digitales, sobre la base de la soberanía digital, que sirva como cimiento clave para el crecimiento de Europa a través de innovaciones digitales y del desarrollo de nuevos modelos de negocio.',
      ),
      'The EU Energy Data Space, headed by leading European power companies, was launched at the 2020 GAIA-X Summit.' => 
      array (
        0 => 'En la cumbre de GAIA-X celebrada en 2020 se presentó el proyecto “EU Data Space Energy”, liderado por algunas de las principales compañías energéticas europeas.',
      ),
      'IDSA' => 
      array (
        0 => 'ASOCIACIÓN IDSA',
      ),
      'IDSA is a European organisation that defines and promotes a <strong>digital architecture for the secure exchange of data between businesses</strong>, allowing data suppliers to maintain control and sovereignty over shared data at all times.' => 
      array (
        0 => 'IDSA es una organización europea que define y promueve el desarrollo de una <strong>arquitectura digital para el intercambio seguro de datos entre empresas</strong>, de forma que los proveedores de datos mantengan en todo momento el control y la soberanía de los datos compartidos.',
      ),
      'IDSA architecture is intended to <strong>establish a global standard of basic conditions for data and interface governance</strong> to be used as the basis for the promotion and certification of a wide array of software solutions, digital services and new business models. IDSA has more than 100 active members from 20 countries, including the Technology Centres involved in DAEKIN (Tecnalia, Ikerlan, Tekniker).' => 
      array (
        0 => 'La arquitectura IDSA pretende <strong>establecer un estándar internacional de condiciones básicas de gobernanza del dato e interfaces</strong>. Con ello pretende constituir la base para impulsar y certificar una amplia variedad de soluciones software, servicios digitales y nuevos modelos de negocio. Entre los más de 100 miembros activos de IDSA de más de 20 países se encuentran los Centros Tecnológicos que participan en DAEKIN (Tecnalia, Ikerlan, Tekniker).',
      ),
      'REFERENCE MODEL' => 
      array (
        0 => 'MODELO DE REFERENCIA',
      ),
      'Establishes a global standard.' => 
      array (
        0 => 'Establece un estándar internacional.',
      ),
      'Guarantees basic data and interface management conditions.' => 
      array (
        0 => 'Garantiza las condiciones básicas de gobernanza del dato e interfaces.',
      ),
      '100+ active members in 20+ countries.' => 
      array (
        0 => '+ 100 miembros activos de +20 países.',
      ),
      'Promotes and certifies software solutions, digital services and new business models.' => 
      array (
        0 => 'Impulsa y certifica soluciones software, servicios digitales y nuevos modelos de negocio.',
      ),
      'Participation of Tecnalia, Tekniker and Ikerlan.' => 
      array (
        0 => 'Participación de Tecnalia, Tekniker, Ikerlan.',
      ),
      'RESEARCH KEYS' => 
      array (
        0 => 'CLAVES DE INVESTIGACIÓN',
      ),
      'ONLINE EVENTS' => 
      array (
        0 => 'EVENTOS EN RED',
      ),
      'CERTIFICATION MEASURES' => 
      array (
        0 => 'MEDIDAS DE CERTIFICACIÓN',
      ),
      'EFINING AND IMPLEMENTING STANDARDS' => 
      array (
        0 => 'DEFINICIÓN E IMPLEMENTACIÓN DE ESTÁNDARS',
      ),
      'USER-DRIVEN PROJECTS' => 
      array (
        0 => 'PROYECTOS IMPULSADOS POR EL USUARIO',
      ),
      'NEW BUSINESS MODELS' => 
      array (
        0 => 'NUEVOS MODELOS DE NEGOCIO',
      ),
      'DEVELOPING ARCHITECTURES' => 
      array (
        0 => 'DESARROLLO DE ARQUITECTURAS',
      ),
      'RESEARCH-INDUSTRY EXCHANGE' => 
      array (
        0 => 'INTERCAMBIO ENTRE INVESTIGACIÓN E INDUSTRIA',
      ),
      '“SENSING & REMOTE MONITORING IN MARINE RENEWABLE ENERGY” INTERREGIONAL PILOT ACTION' => 
      array (
        0 => '“SENSING & REMOTE MONITORING IN MARINE RENEWABLE ENERGY” INTERREGIONAL PILOT ACTION',
      ),
      'Over 2018 and 2019, the Basque Energy Cluster headed the interregional pilot data-sharing action <strong>“Sensing & Remote Monitoring in Marine Renewable Energy facilities”</strong> (S&RM in MRE), by means of a contact with DG REGIO, as part of the <strong>Marine Renewable Energy (MRE) partnership</strong>, a network of 16 regions led by the Basque Country and Scotland.' => 
      array (
        0 => 'A lo largo de 2018 y 2019 el Cluster de Energía lideró la iniciativa piloto de colaboración interregional <strong>“Sensing & Remote Monitoring in Marine Renewable Energy facilities”</strong> (S&RM in MRE), a través de un contrato de la DG REGIO, en el marco de la red <strong>"Marine Renewable Energy (MRE) partnership"</strong>, con 16 regiones co-lideradas por el País Vasco y Escocia.',
      ),
      'The partnership defined the “business case” to develop <strong>a digital platform that would enable the owners of active wind farm data</strong> (wind farm operators and wind turbine manufacturers) <strong>to share data with businesses along the wind power value chain</strong>, including component manufacturers, digital businesses, research organisations, etc.' => 
      array (
        0 => 'A través de esta colaboración se definió el “caso de negocio” para el desarrollo de <strong>una plataforma digital que permita que las empresas propietarias de datos de parques eólicos en operación</strong> (operadores de parques eólicos y fabricantes de aerogeneradores) <strong>puedan compartir sus datos con empresas de la cadena de valor</strong>, fabricantes de componentes, empresas digitales, entidades de investigación, ...',
      ),
      'OFFSHORE WIND POWER DATA DIGITAL PLATFORM' => 
      array (
        0 => 'PLATAFORMA DIGITAL DE DATOS EN EÓLICA OFFSHORE',
      ),
      'The proposed platform responded to a series of data governance and sovereignty challenges, guaranteeing data privacy and security by means of implementing technology solutions based on <strong>IDSA reference architecture</strong>.' => 
      array (
        0 => 'La plataforma propuesta respondía a los retos de gobernanza y soberanía del dato garantizando la privacidad y seguridad de los datos, a través de la implementación de soluciones tecnológicas <strong>basadas en la arquitectura de referencia de IDSA</strong>.',
      ),
      'BEGINNINGS (2018)' => 
      array (
        0 => 'ORIGEN (2018)',
      ),
      'Initiative led by the Basque Energy Cluster.' => 
      array (
        0 => 'Iniciativa liderada por el Cluster de Energía.',
      ),
      'Detection and remote monitoring in renewable marine energy.' => 
      array (
        0 => 'Proyecto de detección y monitorización remota en las energías renovables marinas.',
      ),
      '16 regions, led by the Basque Country and Scotland.' => 
      array (
        0 => '16 regiones colideradas por el País Vasco y escocia.',
      ),
      'PILOT ACTION' => 
      array (
        0 => 'ACCIÓN PILOTO',
      ),
      'Defining a business case.' => 
      array (
        0 => 'Definición del “Caso de negocio”.',
      ),
      'Responds to data governance and sovereignty challenges, guaranteeing privacy and security.' => 
      array (
        0 => 'Responde a retos de gobernanza y soberanía del dato garantizando la privacidad y seguridad.',
      ),
      'Implements technology solutions based on IDSA reference architecture.' => 
      array (
        0 => 'Implementa de soluciones tecnológicas basadas en la arquitectura de referencia de IDSA.',
      ),
      'LEARN MORE ABOUT THE CHALLENGE' => 
      array (
        0 => 'CONOCE EL RETO',
      ),
      'Elkartek faces a range of challenges surrounding data, access, security, sovereignty and more.' => 
      array (
        0 => 'Elkartek se enfrenta a diferentes desafíos en torno al dato, acceso, seguridad, soberanía...',
      ),
      'The Challenge' => 
      array (
        0 => 'El Reto',
      ),
      'DAEKIN\'S CHALLENGE IS <strong>TO DEVELOP A VIABLE PLATFORM</strong> FOR <strong>SECURE, ANONYMOUS DATA EXCHANGE</strong> USING BUSINESS MODELS THAT ALLOW <span class=\'magenta\'>DATA OWNERS</span> AND <span class=\'magenta\'>DATA USERS</span> TO TAKE PART.' => 
      array (
        0 => 'El reto de DAEKIN reside en el <strong>desarrollo factible</strong> de una  plataforma que permita el <strong>intercambio de datos de manera segura, anónimizada</strong> y con modelos de negocio que permitan la participación de los <span class=\'magenta\'>“Data owners”</span> o propietarios de los datos y <span class=\'magenta\'>“Data Users”</span> o usuarios de los datos.',
      ),
      'Data owners' => 
      array (
        0 => 'Propietarios de datos',
      ),
      'Data users' => 
      array (
        0 => 'Usuarios de datos',
      ),
      'OPERATION' => 
      array (
        0 => 'FUNCIONAMIENTO',
      ),
      'Data transfer without IDS' => 
      array (
        0 => 'Comunicaciones de datos sin IDS',
      ),
      'Data transfer with IDS' => 
      array (
        0 => 'Comunicaciones de datos con IDS',
      ),
      'User limits' => 
      array (
        0 => 'Limitaciones de usuarios',
      ),
      'IDSA ECOSYSTEM' => 
      array (
        0 => 'ECOSISTEMA IDSA',
      ),
      'The platform is based on the IDSA ecosystem. IDSA is the European association that promotes and certifies software solutions, digital platforms and business models, creating a global standard of basic conditions for data and interface governance.' => 
      array (
        0 => 'La plataforma está basada en el ecosistema IDSA, la Asociación Europea que impulsa y certifica soluciones de software, plataformas digitales y diversos modelos de negocio, creando un estándar internacional de condiciones básicas para la gobernanza de datos e interfaces.',
      ),
      'SECURITY GUARANTEE' => 
      array (
        0 => 'GARANTÍA DE SEGURIDAD',
      ),
      'Includes concept tests on the main components.' => 
      array (
        0 => 'Incluye pruebas de concepto de los principales componentes.',
      ),
      'Guarantees traceability, security and governability.' => 
      array (
        0 => 'Garantiza la trazabilidad, seguridad y gobernabilidad.',
      ),
      'Allows secure exchange of power data, allowing both users and suppliers to take part, maintaining control and sovereignty over the data.' => 
      array (
        0 => 'Trabaja el intercambio seguro de datos de energía, permitiendo que participen los usuarios y los proveedores, quienes mantienen el control y la soberanía de los datos.',
      ),
      'PROJECT AIM' => 
      array (
        0 => 'OBJETIVO DEL PROYECTO',
      ),
      '<strong>To investigate the technological advances required to define a data sharing platform reference architecture</strong> based on the IDSA ecosystem, including concept testing of its main components, and to create an environment in which data services can be developed with guarantees of traceability, security and governability, particularly for the <span class=\'magenta\'><strong>power sector</strong>.</span>' => 
      array (
        0 => '<strong>Investigar en los avances tecnológicos necesarios</strong> para la definición de <strong>una arquitectura de referencia de una plataforma de compartición de datos</strong>, basada en el ecosistema IDSA, incluyendo pruebas de concepto de sus principales componentes, y para favorecer el desarrollo de servicios en torno a los datos con garantías de trazabilidad, seguridad y gobernabilidad, en específico para el <span class=\'magenta\'><strong>sector energético</strong>.</span>',
      ),
      'OFFSHORE WIND PLATFORM' => 
      array (
        0 => 'PLATAFORMA EÓLICA OFFSHORE',
      ),
      'This architecture will make it possible to implement a digital data platform on offshore wind energy. Allied to the huge traction of the local value chain, this will raise the Basque Country to a leadership position within the framework of the European Data Strategy in relation to the power sector.' => 
      array (
        0 => 'Esta arquitectura posibilitará la implantación de una Plataforma Digital de datos en Eólica Offshore, que impulsada por la gran capacidad de tracción de la cadena de valor en el País Vasco, permitirá elevar a nuestra Comunidad a una posición de liderazgo en el marco de la Estrategia Europea del Dato en relación al sector energético.',
      ),
      'CONNECTORS & INTERFACES' => 
      array (
        0 => 'CONECTORES & INTERFACES',
      ),
      'Owners developing power assets' => 
      array (
        0 => 'Propietarios desarrolladores de activos energéticos',
      ),
      'OEMs (Tier 1), EPC subcontractors, O&M services' => 
      array (
        0 => 'OEMs (Tier 1), contratistas EPC, servicios de OM',
      ),
      'DATA<br />PROVIDER' => 
      array (
        0 => 'PROVEEDORES<br />DE DATOS',
      ),
      'Data Owners' => 
      array (
        0 => 'Propietarios de Datos',
      ),
      'Data Users' => 
      array (
        0 => 'Usuarios de Datos',
      ),
      'Systems and components manufacturers (Tiers 2 and 3), Engineering firms' => 
      array (
        0 => 'Fabricantes de sistemas y componentes (Tier 2 and 3), Ingenierías',
      ),
      'ICT businesses: software, sensors, communications, Big Data, data analysis, AI, etc.' => 
      array (
        0 => 'Empresas de TICs: desarrolladoras de softwares, sensores, comunicaciones, Big Data, análisis de datos, IA...',
      ),
      'Academia: research centres, universities, test labs, etc.' => 
      array (
        0 => 'Académico: centros de investigación, universidades, laboratorios de prueba, etc.',
      ),
      'LEARN MORE ABOUT THE PROJECT' => 
      array (
        0 => 'CONOCE EL PROYECTO',
      ),
      'See the project phases and the role played by each participant' => 
      array (
        0 => 'Descubre las fases del proyecto y el roll de cada participante',
      ),
      'WORK PACKAGES' => 
      array (
        0 => 'PAQUETES DE TRABAJO',
      ),
      'LEARN MORE ABOUT DAEKIN\'S WORK PACKAGES AND HOW WORK IS STRUCTURED ALONG THE PROJECT.' => 
      array (
        0 => 'CONOZCA LOS DIFERENTES PAQUETES DE TRABAJO DE DAEKIN Y CÓMO ESTÁ ESTRUCTURADO EL TRABAJO DENTRO DEL PROYECTO.',
      ),
      'PROJECT LEADER' => 
      array (
        0 => 'PROYECTO LIDERADO POR',
      ),
      'CONSORTIUM' => 
      array (
        0 => 'Consorcio',
      ),
      'El proyecto está liderado por el centro tecnológico <a href=\'https://www.tecnalia.com/\' target=\'_blank\' class=\'black\'>TECNALIA</a>, que además coordinará las actividades en torno al enfoque IDS en cuanto a la compartición del dato con soberanía, y a la aplicabilidad de los distintos enfoques en el caso de uso offshore. Otro de los centros tecnológicos, <a href=\'https://www.ikerlan.es/\' target=\'_blank\' class=\'black\'>IKERLAN</a>, estará a cargo tanto de los requisitos y el diseño de la arquitectura de regencia como del enfoque Datalake. <a href=\'https://www.tekniker.es/\' target=\'_blank\' class=\'black\'>TEKNIKER</a>, el tercer y último centro tecnológico, lidera la tarea relacionada con el Edge Computing.' => 
      array (
        0 => 'El proyecto está liderado por el centro tecnológico <a href=\'https://www.tecnalia.com/\' target=\'_blank\' class=\'black\'>TECNALIA</a>, que además coordinará las actividades en torno al enfoque IDS en cuanto a la compartición del dato con soberanía, y a la aplicabilidad de los distintos enfoques en el caso de uso offshore. Otro de los centros tecnológicos, <a href=\'https://www.ikerlan.es/\' target=\'_blank\' class=\'black\'>IKERLAN</a>, estará a cargo tanto de los requisitos y el diseño de la arquitectura de regencia como del enfoque Datalake. <a href=\'https://www.tekniker.es/\' target=\'_blank\' class=\'black\'>TEKNIKER</a>, el tercer y último centro tecnológico, lidera la tarea relacionada con el Edge Computing.',
      ),
      'En cuanto a las unidades de I+D empresariales hay que destacar las dos participantes en el proyecto: <a href=\'https://hispavistalabs.com/\' target=\'_blank\' class=\'black\'>HISPAVISTA LABS</a> y <a href=\'https://www.glual.com/es/ingenieria/glual_innova.html\' target=\'_blank\' class=\'black\'>GLUAL INNOVA</a>. Esta última es líder del PT responsable del análisis de modelos de compartición y entornos de colaboración. Completa el consorcio la <a href=\'http://www.clusterenergia.com/\' target=\'_blank\' class=\'black\'>ASOCIACIÓN CLUSTER DE ENERGIA</a> como agente de intermediación oferta/demanda. Este será el responsable de las actividades de comunicación y difusión de <span class=\'workMagenta\'><strong>DAEKIN</strong>.</span>' => 
      array (
        0 => 'En cuanto a las unidades de I+D empresariales hay que destacar las dos participantes en el proyecto: <a href=\'https://hispavistalabs.com/\' target=\'_blank\' class=\'black\'>HISPAVISTA LABS</a> y <a href=\'https://www.glual.com/es/ingenieria/glual_innova.html\' target=\'_blank\' class=\'black\'>GLUAL INNOVA</a>. Esta última es líder del PT responsable del análisis de modelos de compartición y entornos de colaboración. Completa el consorcio la <a href=\'http://www.clusterenergia.com/\' target=\'_blank\' class=\'black\'>ASOCIACIÓN CLUSTER DE ENERGIA</a> como agente de intermediación oferta/demanda. Este será el responsable de las actividades de comunicación y difusión de <span class=\'workMagenta\'><strong>DAEKIN</strong>.</span>',
      ),
      'WP1. Analysis of sharing models and teamwork environments' => 
      array (
        0 => 'PT1. Análisis de modelos de compartición y entornos de colaboración',
      ),
      'WP1. ANALYSIS OF SHARING MODELS AND TEAMWORK ENVIRONMENTS' => 
      array (
        0 => 'PT1. ANÁLISIS DE MODELOS DE COMPARTICIÓN Y ENTORNOS DE COLABORACIÓN',
      ),
      'WP LEADER' => 
      array (
        0 => 'LÍDER PT',
      ),
      'This Work Package conducts a general analysis on the implications of sharing and data teamworking in business models, defining no specific domains.' => 
      array (
        0 => 'En este Paquete de Trabajo se analiza de forma general sin especificar dominios concretos, la implicación de la compartición y la colaboración en torno a los datos en los modelos de negocio.',
      ),
      'This activity consists of:' => 
      array (
        0 => 'En esta actividad:',
      ),
      'General analysis.' => 
      array (
        0 => 'Se realiza un análisis generalista.',
      ),
      'Analysis of the specific wind power use case, determining the requirements and specifications needed for subsequent work packages (WP2, WP3, WP4, WP5).' => 
      array (
        0 => 'Se realiza un análisis del caso de uso concreto de la eólica para determinar los requisitos y especificaciones que serán necesarios en paquetes de trabajo posteriores como son: PT2, PT3, PT4, y PT5.',
      ),
      'Finally, an analysis of current methods and cases that support the roll-out of the models.' => 
      array (
        0 => 'Finalmente se analizará las metodologías y casuísticas actuales existentes que apoyan la puesta en marcha de estos modelos.',
      ),
      'New forms of data collaboration and business models are analysed.' => 
      array (
        0 => 'Se analizan nuevas formas de colaboración de datos y modelos de negocio.',
      ),
      'It is focussed on a case study.' => 
      array (
        0 => 'Se orienta a un caso de estudio.',
      ),
      'OFFSHORE WIND POWER' => 
      array (
        0 => 'EÓLICA OFFSHORE',
      ),
      'The methods put into place are analysed.' => 
      array (
        0 => 'Se analizan metodologías previamente implementadas.',
      ),
      '<span class=\'workBlue\'>WP2.</span> Reference architecture requirements and design' => 
      array (
        0 => '<span class=\'workBlue\'>PT2.</span> Requisitos y diseño de arquitectura de referencia',
      ),
      'WP2. REFERENCE ARCHITECTURE REQUIREMENTS AND DESIGN' => 
      array (
        0 => 'PT2. REQUISITOS Y DISEÑO DE ARQUITECTURA DE REFERENCIA',
      ),
      'Work Package 2 will establish the design of an architecture which will make it possible to define the bases for research into data sharing with no loss of sovereignty, and concept testing for a series of development empowerment tools. The aim of this work package is to provide the basis architecture for WP3, WP4 and WP5, and gather the results obtained in a final architecture.' => 
      array (
        0 => 'El Paquete de Trabajo 2 busca establecer el diseño de una arquitectura que permita definir las bases para la investigación en compartición de datos sin pérdida de soberanía y pruebas de concepto para una serie de herramientas habilitadoras de aplicaciones. El objetivo de este paquete de trabajo consiste en servir de arquitectura base para PT3, PT4, PT5, y aunar los resultados obtenidos en una arquitectura final.',
      ),
      'Objectives' => 
      array (
        0 => 'Objetivos',
      ),
      'To define the high-level architecture.' => 
      array (
        0 => 'Definir la arquitectura de alto nivel.',
      ),
      'To spell out the design for the reference architecture.' => 
      array (
        0 => 'Detallar el diseño de la arquitectura de referencia.',
      ),
      'To identify the tools and techniques required to verify the architecture and assess the Key Performance Indicators (KPI) that will be used in subsequent implementations of the architecture.' => 
      array (
        0 => 'Identificar el conjunto de herramientas y técnicas necesarias para la verificación de la arquitectura y evaluación de KPIs (Key Performance Indicators) que serán usados para posteriores implantaciones de la arquitectura.',
      ),
      'Requirements' => 
      array (
        0 => 'Requisitos',
      ),
      'Takes on the requirements for the models analysed in WP1.' => 
      array (
        0 => 'Se recogen los requisitos para los diferentes modelos estudiados en el PT1.',
      ),
      'Architecture design' => 
      array (
        0 => 'Diseño arquitectura',
      ),
      'The range of currently proposed design focuses, approaches and patterns are analysed:' => 
      array (
        0 => 'Se analizan diferentes enfoques, aproximaciones o patrones de diseño actualmente propuestos:',
      ),
      'Centralised repositories' => 
      array (
        0 => 'La centralización de repositorios',
      ),
      'WP3' => 
      array (
        0 => 'PT3',
      ),
      'Architecture proposed in IDS' => 
      array (
        0 => 'La arquitectura propuesta en IDS',
      ),
      'WP4' => 
      array (
        0 => 'PT4',
      ),
      'Secure distributed processing' => 
      array (
        0 => 'El procesamiento distribuido seguro',
      ),
      'WP5' => 
      array (
        0 => 'PT5',
      ),
      'Verification' => 
      array (
        0 => 'Verificación',
      ),
      'Indicators and risks are established, to be monitored and measured during the implementation of the architecture, providing a verification methodology.' => 
      array (
        0 => 'Se establecen indicadores y riesgos que puedan ser monitorizados o medidos a lo largo de la implementación de la arquitectura y de esta forma aportar una metodología para su verificación.',
      ),
      '<span class=\'workOrange\'>WP3.</span> Data value and governance - datalake approach' => 
      array (
        0 => '<span class=\'workOrange\'>PT3.</span> Valor y gobernanza del dato – enfoque datalake',
      ),
      'WP3. DATA VALUE AND GOVERNANCE - DATALAKE APPROACH' => 
      array (
        0 => 'PT3. VALOR Y GOBERNANZA DEL DATO – ENFOQUE DATALAKE',
      ),
      'Work Package 3 aims to analyse the storage needs of data and metadata, both at infrastructure level and at data auditing and governance level. The aim of this work package is to design and develop a model to govern data and metadata obtained from a range of sources throughout their entire lifespan.' => 
      array (
        0 => 'Este Paquete de Trabajo 3 busca analizar las necesidades de almacenamiento de los datos y los correspondientes metadatos tanto a nivel de infraestructura como a nivel de auditoría y gobierno de los datos. El objetivo de este paquete de trabajo consiste en diseñar y desarrollar un módulo que permita gobernar los datos y metadatos procedentes de diferentes fuentes a lo largo del ciclo de vida completo de los datos.',
      ),
      'It will also help to control dataset versions in order to construct incremental analytical data, and foster knowledge extraction by means of machine learning techniques based on metadata.' => 
      array (
        0 => 'De igual manera, deberá asistir en el control de versiones de los datasets para la construcción de datos analíticos incrementales, así como habilitará la extracción de conocimiento mediante técnicas de machine learning en base a los metadatos.',
      ),
      'Main aims:' => 
      array (
        0 => 'Objetivos principales:',
      ),
      'To define data storage requirements.' => 
      array (
        0 => 'Definir los requisitos de almacenamiento de los datos.',
      ),
      'To define data security and governance requirements.' => 
      array (
        0 => 'Definir los requisitos de seguridad y gobierno de los datos.',
      ),
      'To identify the technology needed for the efficient management of data and metadata throughout their lifespan.' => 
      array (
        0 => 'Identificar las tecnologías necesarias para gestionar de manera eficiente los datos y metadatos a lo largo de su ciclo de vida.',
      ),
      'To develop a module entrusted with versioning datasets to generate analytical models.' => 
      array (
        0 => 'Desarrollar un módulo encargado del versionado de datasets para la generación de modelos analíticos.',
      ),
      'To develop a module to extract knowledge from metadata.' => 
      array (
        0 => 'Desarrollar un módulo que permita la extracción de conocimiento en base a los metadatos.',
      ),
      'DATALAKE APPROACH' => 
      array (
        0 => 'EFECTO DATALAKE',
      ),
      'DIFFERENT SOURCES OF DATA' => 
      array (
        0 => 'DISTINTAS FUENTES DE DATOSEFECTO DATALAKE',
      ),
      'DATA ARE STORED DIRECTLY, UNCLASSIFIED' => 
      array (
        0 => 'LOS DATOS SE ALMACENAN DIRECTAMENTE, SIN CLASIFICAR',
      ),
      'DATA ARE SELECTED AND CLASSIFIED AS NEEDED' => 
      array (
        0 => 'LOS DATOS SE SELECCIONAN Y CLASIFICAN EN EL MOMENTO DE NECESITARLOS',
      ),
      '<span class=\'workGreen\'>WP4.</span> Data sharing with sovereignty - IDS approach' => 
      array (
        0 => '<span class=\'workGreen\'>PT4.</span> Compartición del dato con soberanía – enfoque IDS',
      ),
      'WP4. DATA SHARING WITH SOVEREIGNTY - IDS APPROACH' => 
      array (
        0 => 'PT4. COMPARTICIÓN DEL DATO CON SOBERANÍA – ENFOQUE IDS',
      ),
      'Work Package 4 seeks to analyse assisted data sharing by means of use policies, forestalling against sovereignty loss.' => 
      array (
        0 => 'En este Paquete de Trabajo 4 se busca analizar el enfoque de la compartición de datos asistida mediante políticas de uso, que eviten la pérdida de soberanía.',
      ),
      'Activities:' => 
      array (
        0 => 'Actividades:',
      ),
      'The first activity will be research into the said use policies, with two main aims:' => 
      array (
        0 => 'La primera actividad que se realizará es la investigación en torno a dichas políticas de uso, con dos objetivos principales:',
      ),
      'To study the definition possibilities of the use policies and possible languages, focussing on designing a final use policy definition language.' => 
      array (
        0 => 'El estudio de las posibilidades de definición de dichas políticas de uso y los posibles lenguajes, centrando la investigación en el diseño de un lenguaje final de definición de políticas de uso.',
      ),
      'To design a solution capable of controlling the use of data for the policies defined in the previous task, i.e. complying with use policies for distributed platforms.' => 
      array (
        0 => 'Diseñar una solución que sea capaz de controlar el uso de los datos para las políticas definidas en la tarea anterior, es decir el cumplimiento de las políticas de uso para plataformas distribuidas.',
      ),
      'Another important task of this work package is to review the design of the components of the IDS architecture, with the aim of adapting them, improving them or detecting new component designs not taken into account heretofore, and which are defined for offshore wind power.' => 
      array (
        0 => 'Además, otra de las principales actividades de este paquete de trabajo es la revisión del diseño de los diferentes componentes de la arquitectura IDS, con el fin de adaptarlos, mejorarlos o detectar nuevos diseños de componentes hasta ahora no considerados, y definidos para el contexto de la eólica offshore.',
      ),
      'Finally, the package will aim to integrate and validate new components.' => 
      array (
        0 => 'Finalmente, se tratará de integrar y validar los nuevos componentes.',
      ),
      'COMPONENT INTEGRATION AND VALIDATION' => 
      array (
        0 => 'INTEGRACIÓN Y VALIDACIÓN DE COMPONENTES',
      ),
      '<strong>Data sharing policies</strong> are defined.' => 
      array (
        0 => 'Se definen las <strong>políticas de compartición</strong> de datos.',
      ),
      'A solution capable of <strong>controlling data use</strong> for the defined policies is designed.' => 
      array (
        0 => 'Se diseña una solución capaz de <strong>controlar el uso de datos</strong> para las políticas definidas.',
      ),
      'The <strong>characteristics and limitations</strong> of the <strong>IDS</strong> architecture are studied.' => 
      array (
        0 => 'Se estudian las <strong>características y limitaciones</strong> de la arquitectura <strong>IDS</strong>."',
      ),
      'A methodology is defined to allow for <strong>integration and validation of IDS components</strong> defined in advance, checking that they comply with the defined design and requirements.' => 
      array (
        0 => 'Se define una metodología que permita la <strong>integración y validación de los componentes IDS</strong> previamente desarrollados, comprobando que cumplen con el diseño y los requisitos previamente definidos.',
      ),
      'The end result is <strong>adapting the architecture proposed by IDS</strong>, improving it to cover the requirements specified in WP2, and giving it a much more detailed design than currently supported by the original.' => 
      array (
        0 => 'El resultado final es una <strong>adaptación de la arquitectura propuesta por IDS</strong>, mejorada para cubrir los requisitos especificados en el PT2 y con un nivel de detalle en su diseño mucho mayor del que actualmente aporta la original.',
      ),
      'END RESULT' => 
      array (
        0 => 'RESULTADO FINAL',
      ),
      'A reference design of the IDS architecture based on concept tests on the basic components and studying its suitability for the <strong>offshore use case</strong>.' => 
      array (
        0 => 'Se lleva a cabo un diseño de referencia de la arquitectura IDS basado en pruebas de concepto de los componentes básicos y se estudiará su adecuación al <strong>caso de uso offshore</strong>.',
      ),
      'DATASETs transferred from the supplier to the user' => 
      array (
        0 => 'DATASETs transferidos del proveedor al usuario',
      ),
      'METADATA description of DATASETS/supplier/user' => 
      array (
        0 => 'METADATA descripción de DATASETs/proveedor/usuario',
      ),
      'APPLICATION for specific data manipulation' => 
      array (
        0 => 'APLICACION para manipulación específica de datos',
      ),
      'Data exchange ACTIVE' => 
      array (
        0 => 'Intercambio de datos ACTIVO',
      ),
      'Data exchange INACTIVE' => 
      array (
        0 => 'Intercambio de datos INACTIVO',
      ),
      'Metadata exchange' => 
      array (
        0 => 'Intercambio de metadatos',
      ),
      'Application download' => 
      array (
        0 => 'Descarga de aplicaciones',
      ),
      'Scroll Up' => 
      array (
        0 => 'Subir',
      ),
      '<span class=\'workPink\'>WP5.</span> Edge computing distributed processing' => 
      array (
        0 => '<span class=\'workPink\'>PT5.</span> Procesamiento distribuido egde computing',
      ),
      'WP5. EDGE COMPUTING DISTRIBUTED PROCESSING' => 
      array (
        0 => 'PT5. PROCESAMIENTO DISTRIBUIDO EGDE COMPUTING',
      ),
      'Work Package 5 will analyse <strong>two possibilities for enabling data from the Edge environment towards the outside</strong>: distributed and federated focus. On the one hand, in the distributed approach, this will allow for the validation of a secure, reliable data exchange demo, and, on the other, in the federated approach, the implementation of FML and Blockchain techniques.' => 
      array (
        0 => 'En el Paquete de Trabajo 5 se analizarán <strong>dos posibilidades a la hora de habilitar los datos desde el entorno Edge a entidades externas</strong>: enfoque distribuido y federado. De esta manera, permitirá validar, por un lado, una demostración de intercambio de datos segura y confiable basada en el enfoque distribuido y, por otro lado, la implementación de técnicas de FML y Blockchain en el enfoque federado.',
      ),
      'DISTRIBUTED APPROACH' => 
      array (
        0 => 'ENFOQUE DISTRIBUIDO',
      ),
      'The aim is to analyse the characteristics and limits of the Edge environment and the scope of IDS approaches with regard to connectors. Based on the findings, it will seek to implement and validate data exchange using an IDS connector on the Edge. Finally, a data sovereignty solution will be implemented and validated, to guarantee secure, reliable data sharing.' => 
      array (
        0 => 'Se busca analizar las características y limitaciones del entorno Edge, así como el alcance de las aproximaciones de IDS en cuanto a conectores. En base a los resultados obtenidos, se busca implementar y validar el intercambio de datos a partir de un conector IDS en el Edge. Finalmente, se implementará y validará una solución de soberanía de los datos que garantice la compartición de datos segura y confiable.',
      ),
      'FEDERATED APPROACH' => 
      array (
        0 => 'ENFOQUE FEDERADO',
      ),
      'The aim is to analyse and ascertain the viability of implementing FML, Blockchain and cryptographic techniques in use cases where data sharing is not viable but agents wish to work together to obtain greater insight, without compromising their respective data.' => 
      array (
        0 => 'Se busca analizar y demostrar la viabilidad que tiene la implementación de técnicas de FML, Blockchain y criptografía para aquellos casos de uso donde la compartición de datos no sea viable, pero se desee colaborar entre diferentes actores de cara a obtener mejores insights sin comprometer la información de cada parte.',
      ),
      'EDGE ENVIRONMENT' => 
      array (
        0 => 'ENTORNO EDGE',
      ),
      'EDGE COMPUTING' => 
      array (
        0 => 'EDGE COMPUTING',
      ),
      'Edge computing is a paradigm of distributed computing that <strong>transmits computing and data storage to where they are needed</strong>, improving response times and saving bandwidth.' => 
      array (
        0 => 'Es un paradigma de computación distribuida que <strong>acerca computación y almacenamiento de datos a la ubicación donde se necesita</strong>, para mejorar los tiempos de respuesta y ahorrar ancho de banda.',
      ),
      'Data sharing in <strong>Edge environments</strong> involves <strong>challenges around integrating</strong> data from different sources and sharing them with other agents to create new business models.' => 
      array (
        0 => 'La compartición de datos en <strong>Edge environments</strong> presenta <strong>desafíos referentes a la integración</strong> de los datos de diferentes fuentes y la compartición de estos con otras entidades para la creación de nuevos modelos de negocio.',
      ),
      '<span class=\'workYellow\'>WP6.</span> Reference architecture requirements and design' => 
      array (
        0 => '<span class=\'workYellow\'>PT6.</span> Requisitos y diseño de arquitectura de referencia',
      ),
      'WP6. REFERENCE ARCHITECTURE REQUIREMENTS AND DESIGN' => 
      array (
        0 => 'PT6. REQUISITOS Y DISEÑO DE ARQUITECTURA DE REFERENCIA',
      ),
      'Work Package 6 is a cross-cutting package where the developments from the previous work packages are put towards defining the reference architecture for a specific use case in offshore wind power.' => 
      array (
        0 => 'El Paquete de Trabajo 6 es un paquete transversal donde se materializan los desarrollos de los Paquetes de Trabajo anteriores en una particularización de la arquitectura de referencia para un caso de uso concreto del dominio de la eólica offshore.',
      ),
      'Ultimate goal:' => 
      array (
        0 => 'Objetivo final:',
      ),
      'The ultimate goal is to use the platform as a <strong>data sharing</strong> environment among businesses along the offshore wind power value chain, becoming a <strong>benchmark at European level</strong> with the capacity to integrate into the GAIA-X single data space being promoted by the European Commission.' => 
      array (
        0 => 'El objetivo final es que esta plataforma sirva como entorno de <strong>compartición de datos</strong> entre empresas de la cadena de valor de eólica offshore y se convierta en un <strong>referente a nivel europeo</strong> con capacidad de integrarse dentro del espacio único de datos (GAIA-X) que está impulsando la Comisión Europea.',
      ),
      'The WP goes in greater depth into the previous analysis of the <strong>requirements</strong> of offshore wind power, undertaking <strong>a more detailed study</strong>.' => 
      array (
        0 => 'Se profundiza en el análisis de <strong>requisitos</strong> del caso de eólica offshore ya realizado y se lleva a cabo un <strong>estudio más detallado</strong>.',
      ),
      'The requirements are integrated with the <strong>technologies developed</strong> by WP2, WP3, WP4 and WP5.' => 
      array (
        0 => 'Se integran dichos requisitos con los <strong>desarrollos tecnológicos</strong> de los PT2, PT3, PT4 y PT5.',
      ),
      'A design document is developed, specifying the components required for the specific reference architecture to be implemented in a <span class=\'data d-inline workYellow\'>REAL PLATFORM.</span>' => 
      array (
        0 => 'Se desarrolla un documento de diseño con especificación de los componentes necesarios para que la particularización de la arquitectura de referencia sea implementada en una <span class=\'data d-inline workYellow\'>PLATAFORMA REAL.</span>',
      ),
      '<span class=\'workDarkBlue\'>WP7.</span> Communication and dissemination' => 
      array (
        0 => '<span class=\'workDarkBlue\'>PT7.</span> Comunicación y difusión',
      ),
      'WP7. COMMUNICATION AND DISSEMINATION' => 
      array (
        0 => 'PT7. COMUNICACIÓN Y DIFUSIÓN',
      ),
      'The main aim of Work Package 7 is to define and roll out the strategy for communicating the developments and outcomes of the project, maximising its visibility with regard to identified stakeholders and driving the transfer of the knowledge and technologies developed towards energy value chain businesses.' => 
      array (
        0 => 'El objetivo principal del Paquete de Trabajo 7 es la definición y puesta en marcha de la estrategia de comunicación para de los avances y resultados del proyecto, a fin de maximizar su visibilidad por parte de los grupos de interés identificados y de impulsar la transferencia del conocimiento y las tecnologías desarrolladas a empresas de las cadenas de valor energéticas.',
      ),
      'ACTIVITIES' => 
      array (
        0 => 'ACTIVIDADES',
      ),
      'IDENTITY' => 
      array (
        0 => 'IDENTIDAD',
      ),
      'Creating a <strong>corporate identity</strong> for the project.' => 
      array (
        0 => 'Creación de la <strong>identidad corporativa</strong> del proyecto.',
      ),
      'APPLICATIONS' => 
      array (
        0 => 'APLICACIONES',
      ),
      'Developing a <strong>website</strong> and <strong>leaflets</strong>.' => 
      array (
        0 => 'Desarrollo de una <strong>página web</strong> y de <strong>dípticos</strong>.',
      ),
      'COMMUNICATION' => 
      array (
        0 => 'COMUNICACIÓN',
      ),
      'Creating <strong>press release</strong> and <strong>articles</strong> for local and international media.' => 
      array (
        0 => 'Elaboración de <strong>notas de prensa</strong> y <strong>artículos</strong> en medios locales e internacionales.',
      ),
      'Creating a new Work Group:' => 
      array (
        0 => 'Creación del nuevo Grupo de Trabajo:',
      ),
      'Digitalising wind turbine components.' => 
      array (
        0 => 'Digitalización de componentes de aerogeneradores.',
      ),
      'DISSEMINATION' => 
      array (
        0 => 'DIFUSIÓN',
      ),
      'Presence at international <strong>fairs and events</strong>.' => 
      array (
        0 => 'Participación en <strong>ferias y eventos</strong> de carácter internacional.',
      ),
      'Organising <strong>sector workshops</strong> and <strong>seminars relating to</strong> the project.' => 
      array (
        0 => 'Organización de <strong>workshops sectoriales</strong> y <strong>jornadas vinculadas</strong> al proyecto.',
      ),
      'Carrying out <strong>presentations</strong> at <strong>technical conferences</strong>.' => 
      array (
        0 => 'Presentación de <strong>ponencias</strong> en <strong>conferencias técnicas</strong>.',
      ),
      'Creating a new European-level data-sharing community:' => 
      array (
        0 => 'Creación de una nueva comunidad de compartición de datos a nivel europeo:',
      ),
      'Energy Community' => 
      array (
        0 => 'Energy Community',
      ),
      'Planned presence at international fairs and events' => 
      array (
        0 => 'Participaciones en ferias y eventos de carácter internacional previstas',
      ),
      '<span class=\'data\'>Wind Europe Copenhagen 2021</span>Group stand at fair.' => 
      array (
        0 => '<span class=\'data\'>Wind Europe Copenhagen 2021</span>Presencia en feria con stand agrupado.',
      ),
      '<span class=\'data\'>EES/Intersolar Munich 2021</span>Own stand at fair.' => 
      array (
        0 => '<span class=\'data\'>EES/Intersolar Munich 2021</span>Presencia en feria con stand propio.',
      ),
      '<span class=\'data\'>EnLit Europe 2021</span>Own stand at fair.' => 
      array (
        0 => '<span class=\'data\'>EnLit Europe 2021</span>Presencia en feria con stand agrupado.',
      ),
      'Wind Turbine Data Sharing Work Group' => 
      array (
        0 => 'Grupo de Trabajo de Compartición de Datos de Aerogeneradores',
      ),
      'Aims of Work Group:' => 
      array (
        0 => 'Objetivos del Grupo de Trabajo:',
      ),
      'To facilitate wind turbine component manufacturers (segment 3) access to the data required to operate wind farms, so that they can obtain value from them and improve their competitive offer.' => 
      array (
        0 => 'Facilitar a los fabricantes de componentes de aerogeneradores (segmento 3) el acceso a los datos necesarios en operación de los parques eólicos, de forma que puedan obtener valor de los mismos y mejorar su oferta competitiva.',
      ),
      'To compile, process and analyse the requirements and specifications of the data requested by wind turbine component manufacturers (Data Users).' => 
      array (
        0 => 'Recopilar los requisitos y especificaciones de los datos demandados por fabricantes de componentes de aerogeneradores (“Data users”) para su procesado y análisis.',
      ),
      'To identify the conditions and requirements of Data Owners (segments 1 & 2) for data sharing (data privacy, security, governance, compensation...) and offer solutions.' => 
      array (
        0 => 'Identificar las condiciones y requisitos de las empresas propietarias de datos (“Data owners”, segmentos 1 y 2) para su compartición (privacidad, seguridad, gobernanza, retribución del dato…) y proponer respuestas a los mismos.',
      ),
      'To contribute to the Elkartek DAEKIN project in defining and validating a digital platform for sharing data from wind turbines in operational wind farms, offering confidence in data privacy and security and providing easy, systematic access to quality data to businesses all along the value chain.' => 
      array (
        0 => 'Contribuir al proyecto Elkartek DAEKIN en la especificación y validación de una plataforma digital para la compartición de datos procedentes de aerogeneradores en parques eólicos en operación, que proporcione confianza en la privacidad y seguridad de los datos y facilite el acceso fácil y sistemático a datos de calidad por parte de todas las empresas de la cadena de valor.',
      ),
      'To identify collaborative projects for joint data use and analysis among companies in the wind value chain.' => 
      array (
        0 => 'Identificar proyectos en colaboración para el uso y análisis de datos conjunto entre empresas de la cadena de valor eólica.',
      ),
      'EXPECTED RESULTS' => 
      array (
        0 => 'RESULTADOS PREVISTOS',
      ),
      'Specifications for the data requested by component manufacturers (Data users): quantity, frequency, quality, taxonomy, format, etc.' => 
      array (
        0 => 'Documento de especificaciones de datos demandados por los fabricantes de componentes (“Data users”): cantidad, frecuencia, calidad, taxonomía, formato...',
      ),
      'Agreements with Data owners (wind farm operators, wind turbine manufacturers) for the sharing (cession/sale) of wind turbine data at component and/or subsystem level.' => 
      array (
        0 => 'Acuerdos con empresas “propietarias de datos” (“Data owners”: operadores de parques eólicos, fabricantes de aerogeneradores) para la compartición (cesión/venta) de datos de aerogeneradores a nivel de componentes y/o subsistemas.',
      ),
      'Comparison and review of the specifications and architecture of the digital platform to be defined and developed as part of the DAEKIN project.' => 
      array (
        0 => 'Contraste y revisión de las especificaciones y arquitectura de la plataforma digital a especificar y desarrollar en el proyecto DAEKIN.',
      ),
      'Validation of the demo / minimum viable product of the digital platform.' => 
      array (
        0 => 'Validación del demostrador / Producto Mínimo Viable de la plataforma digital.',
      ),
      'Proposed collaborative pilot projects on using and sharing accessible data.' => 
      array (
        0 => 'Propuestas de proyectos piloto en colaboración de utilización y análisis de los datos accesibles.',
      ),
      'MANUFACTURERS' => 
      array (
        0 => 'EMPRESAS FABRICANTES',
      ),
      'DIGITALISATION<br />COMPANIES' => 
      array (
        0 => 'EMPRESAS<br />DIGITALIZACIÓN',
      ),
      'BASQUE NETWORK<br />AGENTS' => 
      array (
        0 => 'AGENTES DE LA<br />RED VASCA',
      ),
      'WORKING GROUP ACTIVITIES' => 
      array (
        0 => 'ACTIVIDADES DEL GRUPO DE TRABAJO',
      ),
      'Launch meeting of the Wind Turbine Data Sharing Work Group.' => 
      array (
        0 => 'Reunión de lanzamiento del Grupo de Trabajo de Compartición de Datos de Aerogeneradores.',
      ),
      'Bilateral meetings to compile the specifications of the data demanded by component manufacturers.' => 
      array (
        0 => 'Recopilación de las especificaciones de los datos demandados por los fabricantes de componentes mediante reuniones bilaterales.',
      ),
      '<strong>Filter</strong>:' => 
      array (
        0 => '<strong>Filtrar</strong>:',
      ),
      'See the latest news and developments surrounding Daekin' => 
      array (
        0 => 'Descubre la evolución y las últimas informaciones sobre Daekin',
      ),
      'ACTIVITIES AND<br />OUTCOMES' => 
      array (
        0 => 'ACTIVIDADES Y<br />RESULTADOS',
      ),
      'All' => 
      array (
        0 => 'Todo',
      ),
      'Activities' => 
      array (
        0 => 'Actividades',
      ),
      'Outcomes' => 
      array (
        0 => 'Resultados',
      ),
      'PREVIOUS' => 
      array (
        0 => 'ANTERIOR',
      ),
      'NEXT' => 
      array (
        0 => 'SIGUIENTE',
      ),
      'DAEKIN launched at Digitalisation Technology Forum' => 
      array (
        0 => 'Presentación de DAEKIN en el Foro Tecnológico de Digitalización',
      ),
      'SECTOR NEWS' => 
      array (
        0 => 'NOTICIAS DEL SECTOR',
      ),
      'Energy Cluster Association' => 
      array (
        0 => 'Asociación del Cluster de Energía',
      ),
      'Project funded by the Department of Economic Development and Infrastructure of the Basque Government (HAZITEK programme) and the European Regional Development Fund (ERDF)' => 
      array (
        0 => 'Proyecto financiado por el Departamento de Desarrollo Económico e Infraestructuras del Gobierno Vasco (Programa HAZITEK) y el Fondo Europeo de Desarrollo Regional (FEDER)',
      ),
      'Read more' => 
      array (
        0 => 'Leer más',
      ),
      'Back to' => 
      array (
        0 => 'Regresar a',
      ),
      'News' => 
      array (
        0 => 'Noticias',
      ),
      'Keep up to date with our latest news.' => 
      array (
        0 => 'Mantente al día con nuestras últimas noticias.',
      ),
    ),
  ),
);