<?php return array (
  'domain' => NULL,
  'plural-forms' => 'nplurals=2; plural=(n != 1);',
  'messages' => 
  array (
    '' => 
    array (
      '' => 
      array (
        0 => 'Project-Id-Version: Daekin
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2020-02-27 12:26+0100
PO-Revision-Date: 2021-05-06 08:56+0200
Language: en
X-Generator: Poedit 2.4.3
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: cms/php
X-Poedit-SearchPath-2: assets/js
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: alerts
X-Poedit-SearchPathExcluded-2: lang
X-Poedit-SearchPathExcluded-3: uploads
X-Poedit-SearchPathExcluded-4: assets/css
X-Poedit-SearchPathExcluded-5: assets/fonts
X-Poedit-SearchPathExcluded-6: assets/ico
X-Poedit-SearchPathExcluded-7: assets/img
X-Poedit-SearchPathExcluded-8: assets/less
X-Poedit-SearchPathExcluded-9: cms/alerts
X-Poedit-SearchPathExcluded-10: cms/assets
X-Poedit-SearchPathExcluded-11: cms/lang
X-Poedit-SearchPathExcluded-12: cms/logs
X-Poedit-SearchPathExcluded-13: cms/site
X-Poedit-SearchPathExcluded-14: cms/uploads
X-Poedit-SearchPathExcluded-15: backup
',
      ),
      'BACKGROUND' => 
      array (
        0 => '',
      ),
      'THE CHALLENGE' => 
      array (
        0 => '',
      ),
      'THE PROJECT' => 
      array (
        0 => '',
      ),
      'ACTIVITIES AND OUTCOMES' => 
      array (
        0 => '',
      ),
      'NEWS' => 
      array (
        0 => '',
      ),
      'Downloads' => 
      array (
        0 => '',
      ),
      'Contact' => 
      array (
        0 => '',
      ),
      '<span class=\'blue\'>SECURE DIGITAL BUSINESS</span> COLLABORATION MAINTAINING DATA SOVEREIGNTY' => 
      array (
        0 => '',
      ),
      'DAEKIN\'S CHALLENGE' => 
      array (
        0 => '',
      ),
      'DAEKIN (Datuak Konpartitzeko Ekimena) is a Type I ELKARTEK project, funded by the Basque Government\'s 2020 call.' => 
      array (
        0 => '',
      ),
      'Project funded by the Basque Government\'s Department for  Economic Development, Sustainability and the Environment (ELKARTEK Programme).' => 
      array (
        0 => '',
      ),
      'DEPARTMENT FOR ECONOMIC DEVELOPMENT, SUSTAINABILITY AND THE ENVIRONMENT ELKARTEK 2020 PROGRAMME.' => 
      array (
        0 => '',
      ),
      'THE PROJECT FOCUSES ON RESEARCH INTO METHODS, TECHNIQUES AND TECHNOLOGIES FOR SECURE DIGITAL DATA SHARING.' => 
      array (
        0 => '',
      ),
      'Its main aim is to <strong>investigate the technological advances needed to define a reference architecture for a data sharing platform</strong>, more specifically for the energy sector.' => 
      array (
        0 => '',
      ),
      'Keep up to date with developments in the project.' => 
      array (
        0 => '',
      ),
      'Learn more' => 
      array (
        0 => '',
      ),
      'Outcome' => 
      array (
        0 => '',
      ),
      'See all' => 
      array (
        0 => '',
      ),
      'THE CONSORTIUM' => 
      array (
        0 => '',
      ),
      'The following participants are involved in this project' => 
      array (
        0 => '',
      ),
      'Technology Centre - Coordinator' => 
      array (
        0 => '',
      ),
      'Business R&D unit' => 
      array (
        0 => '',
      ),
      'Supply/demand broker' => 
      array (
        0 => '',
      ),
      'CONTACT DAEKIN' => 
      array (
        0 => '',
      ),
      'Contact us for further information or if you wish to collaborate with us or send us your feedback.' => 
      array (
        0 => '',
      ),
      'GAIA-X AND THE EU DATA SPACE ENERGY' => 
      array (
        0 => '',
      ),
      'In late 2019, the German government launched the GAIA-X project, aimed at creating <strong>an open, federated data infrastructure</strong>, based on European values and consisting of components and services to access, store, exchange and use" "data, following a set of predefined rules. Seven European governments and 22 businesses from France and Germany have joined the project, which aims to create an ecosystem of developers, suppliers and users of digital products and services, based on digital sovereignty and used as a key foundation for growth in Europe, using digital innovation and developing new business models.' => 
      array (
        0 => '',
      ),
      'The EU Energy Data Space, headed by leading European power companies, was launched at the 2020 GAIA-X Summit.' => 
      array (
        0 => '',
      ),
      'IDSA is a European organisation that defines and promotes a <strong>digital architecture for the secure exchange of data between businesses</strong>, allowing data suppliers to maintain control and sovereignty over shared data at all times.' => 
      array (
        0 => '',
      ),
      'IDSA architecture is intended to <strong>establish a global standard of basic conditions for data and interface governance</strong>. to be used as the basis for the promotion and certification of a wide array of software solutions, digital services and new business models. IDSA has more than 100 active members from 20 countries, including the Technology Centres involved in DAEKIN (Tecnalia, Ikerlan, Tekniker).' => 
      array (
        0 => '',
      ),
      'REFERENCE MODEL' => 
      array (
        0 => '',
      ),
      'Establishes a global standard.' => 
      array (
        0 => '',
      ),
      'Guarantees basic data and interface management conditions.' => 
      array (
        0 => '',
      ),
      '100+ active members in 20+ countries.' => 
      array (
        0 => '',
      ),
      'Promotes and certifies software solutions, digital services and new business models.' => 
      array (
        0 => '',
      ),
      'Participation of Tecnalia, Tekniker and Ikerlan.' => 
      array (
        0 => '',
      ),
      'RESEARCH KEYS' => 
      array (
        0 => '',
      ),
      'ONLINE EVENTS' => 
      array (
        0 => '',
      ),
      'CERTIFICATION MEASURES' => 
      array (
        0 => '',
      ),
      'EFINING AND IMPLEMENTING STANDARDS' => 
      array (
        0 => '',
      ),
      'USER-DRIVEN PROJECTS' => 
      array (
        0 => '',
      ),
      'NEW BUSINESS MODELS' => 
      array (
        0 => '',
      ),
      'DEVELOPING ARCHITECTURES' => 
      array (
        0 => '',
      ),
      'RESEARCH-INDUSTRY EXCHANGE' => 
      array (
        0 => '',
      ),
      '“SENSING & REMOTE MONITORING IN MARINE RENEWABLE ENERGY” INTERREGIONAL PILOT ACTION' => 
      array (
        0 => '',
      ),
      'Over 2018 and 2019, the Basque Energy Cluster headed the interregional pilot data-sharing action <strong>“Sensing & Remote Monitoring in Marine Renewable Energy facilities”</strong> (S&RM in MRE), by means of a contact with DG REGIO, as part of the <strong>Marine Renewable Energy (MRE) partnership</strong>, a network of 16 regions led by the Basque Country and Scotland.The partnership defined the “business case” to develop <strong>a digital platform that would enable the owners of active wind farm data</strong> (wind farm operators and wind turbine manufacturers) <strong>to share data with businesses along the wind power value chain</strong>, including component manufacturers, digital businesses, research organisations, etc.' => 
      array (
        0 => '',
      ),
      'OFFSHORE WIND POWER DATA DIGITAL PLATFORM' => 
      array (
        0 => '',
      ),
      'The proposed platform responded to a series of data governance and sovereignty challenges, guaranteeing data privacy and security by means of implementing technology solutions based on <strong>IDSA reference architecture</strong>.' => 
      array (
        0 => '',
      ),
      'BEGINNINGS (2018)' => 
      array (
        0 => '',
      ),
      'Initiative led by the Basque Energy Cluster.' => 
      array (
        0 => '',
      ),
      'Detection and remote monitoring in renewable marine energy.' => 
      array (
        0 => '',
      ),
      '16 regions, led by the Basque Country and Scotland.' => 
      array (
        0 => '',
      ),
      'PILOT ACTION' => 
      array (
        0 => '',
      ),
      'Defining a business case.' => 
      array (
        0 => '',
      ),
      'Responds to data governance and sovereignty challenges, guaranteeing privacy and security.' => 
      array (
        0 => '',
      ),
      'Implements technology solutions based on IDSA reference architecture.' => 
      array (
        0 => '',
      ),
      'LEARN MORE ABOUT THE CHALLENGE' => 
      array (
        0 => '',
      ),
      'Elkartek faces a range of challenges surrounding data, access, security, sovereignty and more.' => 
      array (
        0 => '',
      ),
      'The Challenge' => 
      array (
        0 => '',
      ),
      'El reto de DAEKIN reside en el <strong>desarrollo factible</strong> de una  plataforma que permita el <strong>intercambio de datos de manera segura, anónimizada</strong> y con modelos de negocio que permitan la participación de los <span class=\'magenta\'>“Data owners”</span> o propietarios de los datos y <span class=\'magenta\'>“Data Users”</span> o usuarios de los datos.' => 
      array (
        0 => '',
      ),
      'Data owners' => 
      array (
        0 => '',
      ),
      'Data users' => 
      array (
        0 => '',
      ),
      'OPERATION' => 
      array (
        0 => '',
      ),
      'Data transfer without IDS' => 
      array (
        0 => '',
      ),
      'Data transfer with IDS' => 
      array (
        0 => '',
      ),
      'User limits' => 
      array (
        0 => '',
      ),
      'IDSA ECOSYSTEM' => 
      array (
        0 => '',
      ),
      'The platform is based on the IDSA ecosystem. IDSA is the European association that promotes and certifies software solutions, digital platforms and business models, creating a global standard of basic conditions for data and interface governance.' => 
      array (
        0 => '',
      ),
      'SECURITY GUARANTEE' => 
      array (
        0 => '',
      ),
      'Includes concept tests on the main components.Guarantees traceability, security and governability.Allows secure exchange of power data, allowing both users and suppliers to take part, maintaining control and sovereignty over the data.' => 
      array (
        0 => '',
      ),
      'PROJECT AIM' => 
      array (
        0 => '',
      ),
      '<strong>To investigate the technological advances required to define a data sharing platform reference architecture</strong> based on the IDSA ecosystem, including concept testing of its main components, and to create an environment in which data services can be developed with guarantees of traceability, security and governability, particularly for the <span class=\'magenta\'><strong>power sector</strong>.</span>' => 
      array (
        0 => '',
      ),
      'OFFSHORE WIND PLATFORM' => 
      array (
        0 => '',
      ),
      'This architecture will make it possible to implement a digital data platform on offshore wind energy. Allied to the huge traction of the local value chain, this will raise the Basque Country to a leadership position within the framework of the European Data Strategy in relation to the power sector.' => 
      array (
        0 => '',
      ),
      'CONNECTORS & INTERFACES' => 
      array (
        0 => '',
      ),
      'Owners developing power assets' => 
      array (
        0 => '',
      ),
      'OEMs (Tier 1), EPC subcontractors, O&M services' => 
      array (
        0 => '',
      ),
      'DATA PROVIDER' => 
      array (
        0 => '',
      ),
      'Systems and components manufacturers (Tiers 2 and 3), Engineering firms' => 
      array (
        0 => '',
      ),
      'ICT businesses: software, sensors, communications, Big Data, data analysis, AI, etc.' => 
      array (
        0 => '',
      ),
      'Academia: research centres, universities, test labs, etc.' => 
      array (
        0 => '',
      ),
      'LEARN MORE ABOUT THE PROJECT' => 
      array (
        0 => '',
      ),
      'See the project phases and the role played by each participant' => 
      array (
        0 => '',
      ),
      'WORK PACKAGES' => 
      array (
        0 => '',
      ),
      'LEARN MORE ABOUT DAEKIN\'S WORK PACKAGES AND HOW WORK IS STRUCTURED ALONG THE PROJECT' => 
      array (
        0 => '',
      ),
      'PROJECT LEADER' => 
      array (
        0 => '',
      ),
      'WP1. Analysis of sharing models and teamwork environments' => 
      array (
        0 => '',
      ),
      'WP LEADER' => 
      array (
        0 => '',
      ),
      'This Work Package conducts a general analysis on the implications of sharing and data teamworking in business models, defining no specific domains.' => 
      array (
        0 => '',
      ),
      'This activity consists of:' => 
      array (
        0 => '',
      ),
      'General analysis' => 
      array (
        0 => '',
      ),
      'Analysis of the specific wind power use case, determining the requirements and specifications needed for subsequent work packages (WP2, WP3, WP4, WP5).' => 
      array (
        0 => '',
      ),
      'Finally, an analysis of current methods and cases that support the roll-out of the models.' => 
      array (
        0 => '',
      ),
      'New forms of data collaboration and business models are analysed' => 
      array (
        0 => '',
      ),
      'It is focussed on a case study' => 
      array (
        0 => '',
      ),
      'OFFSHORE WIND POWER' => 
      array (
        0 => '',
      ),
      'The methods put into place are analysed.' => 
      array (
        0 => '',
      ),
      '<span class=\'workBlue\'>WP2.</span> Reference architecture requirements and design' => 
      array (
        0 => '',
      ),
      'WP2. REFERENCE ARCHITECTURE REQUIREMENTS AND DESIGN' => 
      array (
        0 => '',
      ),
      'Work Package 2 will establish the design of an architecture which will make it possible to define the bases for research into data sharing with no loss of sovereignty, and concept testing for a series of development empowerment tools. The aim of this work package is to provide the basis architecture for WP3, WP4 and WP5, and gather the results obtained in a final architecture' => 
      array (
        0 => '',
      ),
      'Objectives' => 
      array (
        0 => '',
      ),
      'To define the high-level architecture.' => 
      array (
        0 => '',
      ),
      'To spell out the design for the reference architecture.' => 
      array (
        0 => '',
      ),
      'To identify the tools and techniques required to verify the architecture and assess the Key Performance Indicators (KPI) that will be used in subsequent implementations of the architecture.' => 
      array (
        0 => '',
      ),
      'Requirements' => 
      array (
        0 => '',
      ),
      'Takes on the requirements for the models analysed in WP1.' => 
      array (
        0 => '',
      ),
      'Architecture design' => 
      array (
        0 => '',
      ),
      'The range of currently proposed design focuses, approaches and patterns are analysed:' => 
      array (
        0 => '',
      ),
      'Centralised repositories' => 
      array (
        0 => '',
      ),
      'WP3' => 
      array (
        0 => '',
      ),
      'Architecture proposed in IDS' => 
      array (
        0 => '',
      ),
      'WP4' => 
      array (
        0 => '',
      ),
      'Secure distributed processing' => 
      array (
        0 => '',
      ),
      'WP5' => 
      array (
        0 => '',
      ),
      'Verification' => 
      array (
        0 => '',
      ),
      'Indicators and risks are established, to be monitored and measured during the implementation of the architecture, providing a verification methodology.' => 
      array (
        0 => '',
      ),
      '<span class=\'workOrange\'>WP3.</span> Data value and governance - datalake approach' => 
      array (
        0 => '',
      ),
      'WP3. DATA VALUE AND GOVERNANCE - DATALAKE APPROACH' => 
      array (
        0 => '',
      ),
      'Work Package 3 aims to analyse the storage needs of data and metadata, both at infrastructure level and at data auditing and governance level. The aim of this work package is to design and develop a model to govern data and metadata obtained from a range of sources throughout their entire lifespan.' => 
      array (
        0 => '',
      ),
      'It will also help to control dataset versions in order to construct incremental analytical data, and foster knowledge extraction by means of machine learning techniques based on metadata.' => 
      array (
        0 => '',
      ),
      'Main aims' => 
      array (
        0 => '',
      ),
      'To define data storage requirements' => 
      array (
        0 => '',
      ),
      'To define data security and governance requirements.' => 
      array (
        0 => '',
      ),
      'To identify the technology needed for the efficient management of data and metadata throughout their lifespan.' => 
      array (
        0 => '',
      ),
      'To develop a module entrusted with versioning datasets to generate analytical models.' => 
      array (
        0 => '',
      ),
      'To develop a module to extract knowledge from metadata.' => 
      array (
        0 => '',
      ),
      'DIFFERENT SOURCES OF DATA' => 
      array (
        0 => '',
      ),
      'DATA ARE STORED DIRECTLY, UNCLASSIFIED' => 
      array (
        0 => '',
      ),
      'DATA ARE SELECTED AND CLASSIFIED AS NEEDED' => 
      array (
        0 => '',
      ),
      '<span class=\'workGreen\'>WP4.</span> Data sharing with sovereignty - IDS approach' => 
      array (
        0 => '',
      ),
      'WP4. DATA SHARING WITH SOVEREIGNTY - IDS APPROACH' => 
      array (
        0 => '',
      ),
      'Work Package 4 seeks to analyse assisted data sharing by means of use policies, forestalling against sovereignty loss.' => 
      array (
        0 => '',
      ),
      'Activities:' => 
      array (
        0 => '',
      ),
      'The first activity will be research into the said use policies, with two main aims:' => 
      array (
        0 => '',
      ),
      'To study the definition possibilities of the use policies and possible languages, focussing on designing a final use policy definition language.' => 
      array (
        0 => '',
      ),
      'To design a solution capable of controlling the use of data for the policies defined in the previous task, i.e. complying with use policies for distributed platforms.' => 
      array (
        0 => '',
      ),
      'Another important task of this work package is to review the design of the components of the IDS architecture, with the aim of adapting them, improving them or detecting new component designs not taken into account heretofore, and which are defined for offshore wind power.' => 
      array (
        0 => '',
      ),
      'Finally, the package will aim to integrate and validate new components.' => 
      array (
        0 => '',
      ),
      'COMPONENT INTEGRATION AND VALIDATION' => 
      array (
        0 => '',
      ),
      '<strong>Data sharing policies</strong> are defined' => 
      array (
        0 => '',
      ),
      'A solution capable of <strong>controlling data use</strong> for the defined policies is designed.' => 
      array (
        0 => '',
      ),
      'The <strong>characteristics and limitations</strong> of the <strong>IDS</strong> architecture are studied.' => 
      array (
        0 => '',
      ),
      'A methodology is defined to allow for <strong>integration and validation of IDS components</strong> defined in advance, checking that they comply with the defined design and requirements.' => 
      array (
        0 => '',
      ),
      'The end result is <strong>adapting the architecture proposed by IDS</strong>, improving it to cover the requirements specified in WP2, and giving it a much more detailed design than currently supported by the original.' => 
      array (
        0 => '',
      ),
      'END RESULT' => 
      array (
        0 => '',
      ),
      'A reference design of the IDS architecture based on concept tests on the basic components and studying its suitability for the <strong>offshore use case</strong>.' => 
      array (
        0 => '',
      ),
      'DATASETs transferred from the supplier to the user' => 
      array (
        0 => '',
      ),
      'METADATA description of DATASETS/supplier/user' => 
      array (
        0 => '',
      ),
      'APPLICATION for specific data manipulation' => 
      array (
        0 => '',
      ),
      'Data exchange ACTIVE' => 
      array (
        0 => '',
      ),
      'Data exchange INACTIVE' => 
      array (
        0 => '',
      ),
      'Metadata exchange' => 
      array (
        0 => '',
      ),
      'Application download' => 
      array (
        0 => '',
      ),
      'Scroll Up' => 
      array (
        0 => '',
      ),
      '<span class=\'workPink\'>WP5.</span> Edge computing distributed processing' => 
      array (
        0 => '',
      ),
      'WP5. EDGE COMPUTING DISTRIBUTED PROCESSING' => 
      array (
        0 => '',
      ),
      'Work Package 5 will analyse <strong>two possibilities for enabling data from the Edge environment towards the outside</strong>: distributed and federated focus. On the one hand, in the distributed approach, this will allow for the validation of a secure, reliable data exchange demo, and, on the other, in the federated approach, the implementation of FML and Blockchain techniques.' => 
      array (
        0 => '',
      ),
      'DISTRIBUTED APPROACH' => 
      array (
        0 => '',
      ),
      'The aim is to analyse the characteristics and limits of the Edge environment and the scope of IDS approaches with regard to connectors. Based on the findings, it will seek to implement and validate data exchange using an IDS connector on the Edge. Finally, a data sovereignty solution will be implemented and validated, to guarantee secure, reliable data sharing.' => 
      array (
        0 => '',
      ),
      'FEDERATED APPROACH' => 
      array (
        0 => '',
      ),
      'The aim is to analyse and ascertain the viability of implementing FML, Blockchain and cryptographic techniques in use cases where data sharing is not viable but agents wish to work together to obtain greater insight, without compromising their respective data.' => 
      array (
        0 => '',
      ),
      'EDGE ENVIRONMENT' => 
      array (
        0 => '',
      ),
      'EDGE COMPUTING' => 
      array (
        0 => '',
      ),
      'Edge computing is a paradigm of distributed computing that <strong>transmits computing and data storage to where they are needed</strong>, improving response times and saving bandwidth.' => 
      array (
        0 => '',
      ),
      'Data sharing in <strong>Edge environments</strong> involves <strong>challenges around integrating</strong> data from different sources and sharing them with other agents to create new business models.' => 
      array (
        0 => '',
      ),
      '<span class=\'workYellow\'>WP6.</span> Reference architecture requirements and design' => 
      array (
        0 => '',
      ),
      'WP6. REFERENCE ARCHITECTURE REQUIREMENTS AND DESIGN' => 
      array (
        0 => '',
      ),
      'Work Package 6 is a cross-cutting package where the developments from the previous work packages are put towards defining the reference architecture for a specific use case in offshore wind power.' => 
      array (
        0 => '',
      ),
      'Ultimate goal:' => 
      array (
        0 => '',
      ),
      'The ultimate goal is to use the platform as a <strong>data sharing</strong> environment among businesses along the offshore wind power value chain, becoming a <strong>benchmark at European level</strong> with the capacity to integrate into the GAIA-X single data space being promoted by the European Commission.' => 
      array (
        0 => '',
      ),
      'The WP goes in greater depth into the previous analysis of the <strong>requirements</strong> of offshore wind power, undertaking <strong>a more detailed study</strong>.' => 
      array (
        0 => '',
      ),
      'The requirements are integrated with the <strong>technologies developed</strong> by WP2, WP3, WP4 and WP5.' => 
      array (
        0 => '',
      ),
      'A design document is developed, specifying the components required for the specific reference architecture to be implemented in a <span class=\'data d-inline workYellow\'>REAL PLATFORM.</span>' => 
      array (
        0 => '',
      ),
      '<span class=\'workDarkBlue\'>WP7.</span> Communication and dissemination' => 
      array (
        0 => '',
      ),
      'WP7. COMMUNICATION AND DISSEMINATION' => 
      array (
        0 => '',
      ),
      'The main aim of Work Package 7 is to define and roll out the strategy for communicating the developments and outcomes of the project, maximising its visibility with regard to identified stakeholders and driving the transfer of the knowledge and technologies developed towards energy value chain businesses.' => 
      array (
        0 => '',
      ),
      'ACTIVITIES' => 
      array (
        0 => '',
      ),
      'IDENTITY' => 
      array (
        0 => '',
      ),
      'Creating a <strong>corporate identity</strong> for the project.' => 
      array (
        0 => '',
      ),
      'APPLICATIONS' => 
      array (
        0 => '',
      ),
      'Developing a <strong>website</strong> and <strong>leaflets</strong>.' => 
      array (
        0 => '',
      ),
      'COMMUNICATION' => 
      array (
        0 => '',
      ),
      'Creating <strong>press release</strong> and <strong>articles</strong> for local and international media.' => 
      array (
        0 => '',
      ),
      'Creating a new Work Group:' => 
      array (
        0 => '',
      ),
      'Digitalising wind turbine components.' => 
      array (
        0 => '',
      ),
      'DISSEMINATION' => 
      array (
        0 => '',
      ),
      'Presence at international <strong>fairs and events</strong>.' => 
      array (
        0 => '',
      ),
      'Organising <strong>sector workshops</strong> and <strong>seminars relating to</strong> the project.' => 
      array (
        0 => '',
      ),
      'Carrying out <strong>presentations</strong> at <strong>technical conferences</strong>.' => 
      array (
        0 => '',
      ),
      'Creating a new European-level data-sharing community:' => 
      array (
        0 => '',
      ),
      'Energy Community' => 
      array (
        0 => '',
      ),
      'Planned presence at international fairs and events' => 
      array (
        0 => '',
      ),
      '<span class=\'data\'>Wind Europe Copenhagen 2021</span>Group stand at fair.' => 
      array (
        0 => '',
      ),
      '<span class=\'data\'>EES/Intersolar Munich 2021</span>Own stand at fair.' => 
      array (
        0 => '',
      ),
      '<span class=\'data\'>EnLit Europe 2021</span>Own stand at fair.' => 
      array (
        0 => '',
      ),
      'Wind Turbine Data Sharing Work Group' => 
      array (
        0 => '',
      ),
      'Aims of Work Group:' => 
      array (
        0 => '',
      ),
      'To facilitate wind turbine component manufacturers (segment 3) access to the data required to operate wind farms, so that they can obtain value from them and improve their competitive offer.' => 
      array (
        0 => '',
      ),
      'To compile, process and analyse the requirements and specifications of the data requested by wind turbine component manufacturers (Data Users).' => 
      array (
        0 => '',
      ),
      'To identify the conditions and requirements of Data Owners (segments 1 & 2) for data sharing (data privacy, security, governance, compensation...) and offer solutions.' => 
      array (
        0 => '',
      ),
      'To contribute to the Elkartek DAEKIN project in defining and validating a digital platform for sharing data from wind turbines in operational wind farms, offering confidence in data privacy and security and providing easy, systematic access to quality data to businesses all along the value chain.' => 
      array (
        0 => '',
      ),
      'To identify collaborative projects for joint data use and analysis among companies in the wind value chain.' => 
      array (
        0 => '',
      ),
      'EXPECTED RESULTS' => 
      array (
        0 => '',
      ),
      'Specifications for the data requested by component manufacturers (Data users): quantity, frequency, quality, taxonomy, format, etc.' => 
      array (
        0 => '',
      ),
      'Agreements with Data owners (wind farm operators, wind turbine manufacturers) for the sharing (cession/sale) of wind turbine data at component and/or subsystem level.' => 
      array (
        0 => '',
      ),
      'Comparison and review of the specifications and architecture of the digital platform to be defined and developed as part of the DAEKIN project.' => 
      array (
        0 => '',
      ),
      'Validation of the demo / minimum viable product of the digital platform.' => 
      array (
        0 => '',
      ),
      'Proposed collaborative pilot projects on using and sharing accessible data.' => 
      array (
        0 => '',
      ),
      'MANUFACTURERS' => 
      array (
        0 => '',
      ),
      'DIGITALISATION<br />COMPANIES' => 
      array (
        0 => '',
      ),
      'BASQUE NETWORK<br />AGENTS' => 
      array (
        0 => '',
      ),
      'WORKING GROUP ACTIVITIES' => 
      array (
        0 => '',
      ),
      'Launch meeting of the Wind Turbine Data Sharing Work Group.' => 
      array (
        0 => '',
      ),
      'Bilateral meetings to compile the specifications of the data demanded by component manufacturers.' => 
      array (
        0 => '',
      ),
      '<strong>Filter</strong>:' => 
      array (
        0 => '',
      ),
      'See the latest news and developments surrounding Daekin' => 
      array (
        0 => '',
      ),
      'ACTIVITIES ANDS<br />OUTCOMES' => 
      array (
        0 => '',
      ),
      'All' => 
      array (
        0 => '',
      ),
      'Activities' => 
      array (
        0 => '',
      ),
      'Outcomes' => 
      array (
        0 => '',
      ),
      'PREVIOUS' => 
      array (
        0 => '',
      ),
      'NEXT' => 
      array (
        0 => '',
      ),
      'DAEKIN launched at Digitalisation Technology Forum' => 
      array (
        0 => '',
      ),
      'SECTOR NEWS' => 
      array (
        0 => '',
      ),
      'Energy Cluster Association' => 
      array (
        0 => '',
      ),
      'Read more' => 
      array (
        0 => '',
      ),
      'Back to' => 
      array (
        0 => '',
      ),
      'News' => 
      array (
        0 => '',
      ),
      'Keep up to date with our latest news.' => 
      array (
        0 => '',
      ),
    ),
  ),
);