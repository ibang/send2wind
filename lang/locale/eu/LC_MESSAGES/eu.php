<?php return array (
  'domain' => NULL,
  'plural-forms' => 'nplurals=2; plural=(n != 1);',
  'messages' => 
  array (
    '' => 
    array (
      '' => 
      array (
        0 => 'Project-Id-Version: Daekin
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2020-05-20 14:18+0200
PO-Revision-Date: 2020-05-20 14:22+0200
Language: eu_ES
X-Generator: Poedit 2.3.1
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: cms/php
X-Poedit-SearchPath-2: assets/js
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: alerts
X-Poedit-SearchPathExcluded-2: lang
X-Poedit-SearchPathExcluded-3: uploads
X-Poedit-SearchPathExcluded-4: assets/css
X-Poedit-SearchPathExcluded-5: assets/fonts
X-Poedit-SearchPathExcluded-6: assets/ico
X-Poedit-SearchPathExcluded-7: assets/img
X-Poedit-SearchPathExcluded-8: assets/less
X-Poedit-SearchPathExcluded-9: cms/alerts
X-Poedit-SearchPathExcluded-10: cms/assets
X-Poedit-SearchPathExcluded-11: cms/lang
X-Poedit-SearchPathExcluded-12: cms/logs
X-Poedit-SearchPathExcluded-13: cms/site
X-Poedit-SearchPathExcluded-14: cms/uploads
X-Poedit-SearchPathExcluded-15: backup
X-Poedit-SearchPathExcluded-16: assets/js/tools/sanitizer.js
',
      ),
      'BACKGROUND' => 
      array (
        0 => 'TESTUINGURUA',
      ),
      'THE CHALLENGE' => 
      array (
        0 => 'ERRONKA',
      ),
      'THE PROJECT' => 
      array (
        0 => 'PROIEKTUA',
      ),
      'ACTIVITIES AND OUTCOMES' => 
      array (
        0 => 'JARDUERAK ETA EMAITZAK',
      ),
      'NEWS' => 
      array (
        0 => 'BERRIAK',
      ),
      'Downloads' => 
      array (
        0 => 'DESKARGAK',
      ),
      'Download' => 
      array (
        0 => 'Deskargatu',
      ),
      'Contact' => 
      array (
        0 => 'KONTAKTUA',
      ),
      '<span class=\'blue\'>SECURE DIGITAL BUSINESS</span> COLLABORATION MAINTAINING DATA SOVEREIGNTY' => 
      array (
        0 => 'ENPRESEN ARTEKO <span class=\'blue\'>LANKIDETZA DIGITAL SEGURUA</span> DATUAREN SUBIRANOTASUNA GALDU GABE',
      ),
      'DAEKIN\'S CHALLENGE' => 
      array (
        0 => 'DAEKIN-EN ERRONKA',
      ),
      'DAEKIN (Datuak Konpartitzeko Ekimena) is a Type I ELKARTEK project, funded by the Basque Government\'s 2020 call.' => 
      array (
        0 => 'DAEKIN (Datuak Konpartitzeko Ekimena) Eusko Jaurlaritzak 2020ko deialdian diruz lagundutako I. motako ELKARTEK proiektua da.',
      ),
      'Project funded by the Basque Government\'s Department for  Economic Development, Sustainability and the Environment (ELKARTEK Programme).' => 
      array (
        0 => 'Eusko Jaurlaritzako Ekonomiaren Garapen, Jasangarritasun eta Ingurumen Sailak finantzatutako proiektua (ELKARTEK programa).',
      ),
      'DEPARTMENT FOR ECONOMIC DEVELOPMENT, SUSTAINABILITY AND THE ENVIRONMENT ELKARTEK 2020 PROGRAMME.' => 
      array (
        0 => 'DAEKIN EUSKO JAURLARITZAKO EKONOMIAREN GARAPEN, INGURUMEN ETA JASANGARRITASUN SAILAREN 2020KO ELKARTEK PROGRAMAK FINANTZATUTAKO PROIEKTUA DA.',
      ),
      'THE PROJECT FOCUSES ON RESEARCH INTO METHODS, TECHNIQUES AND TECHNOLOGIES FOR SECURE DIGITAL DATA SHARING.' => 
      array (
        0 => 'DATUEN PARTEKATZE DIGITAL SEGURURAKO METODOLOGIAK, TEKNIKAK ETA TEKNOLOGIAK IKERTZEA DA PROIEKTUAREN ARDATZA.',
      ),
      'Its main aim is to <strong>investigate the technological advances needed to define a reference architecture for a data sharing platform</strong>, more specifically for the energy sector.' => 
      array (
        0 => 'Honen helburu nagusia <strong>datuak partekatzeko plataformaren erreferentziako arkitektura definitzeko beharrezkoak diren aurrerapen teknologikoak ikertzea da</strong>, bereziki energia-sektorearentzat.',
      ),
      'the project' => 
      array (
        0 => 'PROIEKTUA',
      ),
      'Keep up to date with developments in the project.' => 
      array (
        0 => 'Egunean egon, proiektuari buruzko informazio eguneratuarekin.',
      ),
      'Learn more' => 
      array (
        0 => 'Gehiago ezagutu',
      ),
      'Activity' => 
      array (
        0 => 'JARDUERA',
      ),
      'OUTCOME' => 
      array (
        0 => 'EMAITZA',
      ),
      'See all' => 
      array (
        0 => 'Ikusi guztiak',
      ),
      'THE CONSORTIUM' => 
      array (
        0 => 'PARTZUERGOA',
      ),
      'The following participants are involved in this project' => 
      array (
        0 => 'Hainbat eragilek bat egingo dugu proiektu honetan',
      ),
      'Technology Centre - Coordinator' => 
      array (
        0 => 'Zentro teknologikoa – Koordinatzailea',
      ),
      'Technology Centre' => 
      array (
        0 => 'Zentro teknologikoa',
      ),
      'Business R&D unit' => 
      array (
        0 => 'Enpresen I+G unitatea',
      ),
      'Supply/demand broker' => 
      array (
        0 => 'Eskaintza eta eskariaren arteko bitartekaritza-eragilea',
      ),
      'CONTACT DAEKIN' => 
      array (
        0 => 'HARREMANETAN JARRI DAEKIN-EKIN',
      ),
      'Contact us for further information or if you wish to collaborate with us or send us your feedback.' => 
      array (
        0 => 'Idatzi informazio gehiago lortzeko, laguntzeko edo iritzia emateko.',
      ),
      'Background' => 
      array (
        0 => 'Testuingurua',
      ),
      'GAIA-X AND THE EU DATA SPACE ENERGY' => 
      array (
        0 => 'GAIA-X ETA EU DATA SPACE ENERGY',
      ),
      'In late 2019, the German government launched the GAIA-X project, aimed at creating <strong>an open, federated data infrastructure</strong>, based on European values and consisting of components and services to access, store, exchange and use data, following a set of predefined rules. Seven European governments and 22 businesses from France and Germany have joined the project, which aims to create an ecosystem of developers, suppliers and users of digital products and services, based on digital sovereignty and used as a key foundation for growth in Europe, using digital innovation and developing new business models.' => 
      array (
        0 => '2019aren amaieran, Alemaniako Gobernuak "GAIA-X" deritzon ekimena abiarazi zuen, Europako balioetan oinarritutako <strong>datu-azpiegitura ireki eta federatua</strong>, sortzeko. Honen osagai eta zerbitzuak dira datuak eskuratu, biltegiratu, trukatu eta erabiltzeko aukera emango dutenak, aldez aurretik zehaztutako arauei jarraiki. Europako zazpi gobernu eta 22 enpresa (alemanak eta frantsesak) atxikiz joan zaizkio subirasotasun digitalean oinarritutako produktu eta zerbitzu digitalen garatzaile, hornitzaile eta erabiltzaileen ekosistema sortzea xede duen ekimen honi, Europaren hazkundea berrikuntza digitalen eta negozio-eredu berrien bitartez bultzatzeko giltzarrizko oinarria izan dadin.',
      ),
      'The EU Energy Data Space, headed by leading European power companies, was launched at the 2020 GAIA-X Summit.' => 
      array (
        0 => '2020an antolatutako GAIA-X ekimenari buruzko goi-bileran, Europako energia-konpainia nagusietako batzuk buru dituen "EU Data Space Energy" proiektua aurkeztu zen.',
      ),
      'IDSA' => 
      array (
        0 => 'IDSA ELKARTEA',
      ),
      'IDSA is a European organisation that defines and promotes a <strong>digital architecture for the secure exchange of data between businesses</strong>, allowing data suppliers to maintain control and sovereignty over shared data at all times.' => 
      array (
        0 => 'IDSA <strong>enpresen arteko datu-truke segururako arkitektura digitalaren</strong>, garapena zehaztu eta sustatzen duen europar erakundea da, datuen hornitzaileek uneoro partekatutako datuen kontrolari eta subiranotasunari eutsi diezaieten.',
      ),
      'IDSA architecture is intended to <strong>establish a global standard of basic conditions for data and interface governance</strong> to be used as the basis for the promotion and certification of a wide array of software solutions, digital services and new business models. IDSA has more than 100 active members from 20 countries, including the Technology Centres involved in DAEKIN (Tecnalia, Ikerlan, Tekniker).' => 
      array (
        0 => 'IDSA arkitekturaren bidez, <strong>datu eta interfazeen oinarrizko gobernantza-baldintzen nazioarteko estandarra</strong> ezarri nahi da. Horren laguntzaz, askotariko software-soluzio, zerbitzu digital eta negozio-ereduak bultzatu eta ziurtatzeko oinarria eratu nahi da. IDSAk dituen 20tik gora herrialdetako 100 kide aktiboen artean, DAEKINen parte hartzen duten zentro teknologikoak daude (Tecnalia, Ikerlan, Tekniker).',
      ),
      'REFERENCE MODEL' => 
      array (
        0 => 'ERREFERENTZIAZKO EREDUA',
      ),
      'Establishes a global standard.' => 
      array (
        0 => 'Nazioarteko estandarra ezartzen du.',
      ),
      'Guarantees basic data and interface management conditions.' => 
      array (
        0 => 'Datu eta interfazeen oinarrizko gobernantza-baldintzak bermatzen ditu.',
      ),
      '100+ active members in 20+ countries.' => 
      array (
        0 => '20tik gora herrialdetako 100 kide aktibo baino gehiago.',
      ),
      'Promotes and certifies software solutions, digital services and new business models.' => 
      array (
        0 => 'Software-soluzioak, zerbitzu digitalak eta negozio-eredu berriak bultzatu eta ziurtatzen ditu.',
      ),
      'Participation of Tecnalia, Tekniker and Ikerlan.' => 
      array (
        0 => 'Tecnalia, Tekniker eta Ikerlanen parte-hartzea.',
      ),
      'RESEARCH KEYS' => 
      array (
        0 => 'IKERKETAREN GAKOAK',
      ),
      'ONLINE EVENTS' => 
      array (
        0 => 'SAREKO EKITALDIAK',
      ),
      'CERTIFICATION MEASURES' => 
      array (
        0 => 'ZIURTAPEN-NEURRIAK',
      ),
      'EFINING AND IMPLEMENTING STANDARDS' => 
      array (
        0 => 'ESTANDARREN DEFINIZIOA ETA INPLEMENTAZIOA',
      ),
      'USER-DRIVEN PROJECTS' => 
      array (
        0 => 'ERABILTZAILEAK BULTZATUTAKO PROIEKTUAK',
      ),
      'NEW BUSINESS MODELS' => 
      array (
        0 => 'NEGOZIO-EREDU BERRIAK',
      ),
      'DEVELOPING ARCHITECTURES' => 
      array (
        0 => 'ARKITEKTUREN GARAPENA',
      ),
      'RESEARCH-INDUSTRY EXCHANGE' => 
      array (
        0 => 'IKERKETAREN ETA INDUSTRIAREN ARTEKO TRUKEA',
      ),
      '“SENSING & REMOTE MONITORING IN MARINE RENEWABLE ENERGY” INTERREGIONAL PILOT ACTION' => 
      array (
        0 => '“SENSING & REMOTE MONITORING IN MARINE RENEWABLE ENERGY” INTERREGIONAL PILOT ACTION',
      ),
      'Over 2018 and 2019, the Basque Energy Cluster headed the interregional pilot data-sharing action <strong>“Sensing & Remote Monitoring in Marine Renewable Energy facilities”</strong> (S&RM in MRE), by means of a contact with DG REGIO, as part of the <strong>Marine Renewable Energy (MRE) partnership</strong>, a network of 16 regions led by the Basque Country and Scotland.' => 
      array (
        0 => '2018an eta 2019an, Energia Klusterrak <strong>“Sensing & Remote Monitoring in Marine Renewable Energy facilities”</strong> (S&RM in MRE) deritzon eskualdearteko lankidetzarako ekimen pilotua lideratu zuen <strong>"Marine Renewable Energy (MRE) partnership"</strong>, sarearen barruan, DG REGIOren kontratu baten bidez eta Euskadi eta Eskozia buru zituzten 16 eskualderen parte-hartzeaz.',
      ),
      'The partnership defined the “business case” to develop <strong>a digital platform that would enable the owners of active wind farm data</strong> (wind farm operators and wind turbine manufacturers) <strong>to share data with businesses along the wind power value chain</strong>, including component manufacturers, digital businesses, research organisations, etc.' => 
      array (
        0 => 'Lankidetza honen bitartez, <strong>eragiketan dauden parke eolikoetako datuen jabe diren enpresei</strong> (parke eolikoetako operadoreak eta aerosorgailuen fabrikatzaileak) <strong>datuak balio-kateko enpresekin</strong>, (osagaien fabrikatzaileak, enpresa digitalak, ikerketa-erakundeak...) <strong>partekatzeko aukera emango dien plataforma digitala</strong> garatzeko “negozioaren kasua” definitu zen.',
      ),
      'OFFSHORE WIND POWER DATA DIGITAL PLATFORM' => 
      array (
        0 => 'DATUEN PLATAFORMA DIGITALA OFFSHORE EOLIKOAN',
      ),
      'The proposed platform responded to a series of data governance and sovereignty challenges, guaranteeing data privacy and security by means of implementing technology solutions based on <strong>IDSA reference architecture</strong>.' => 
      array (
        0 => 'Proposatutako plataformak erantzuna ematen zien datuaren gobernantza eta subiranotasunaren arloko erronkei, datuen pribatutasuna eta segurtasuna bermatuz, <strong>IDSAren erreferentziazko arkitekturan oinarritutako</strong> soluzio teknologikoen inplementazioaren bitartez.',
      ),
      'BEGINNINGS (2018)' => 
      array (
        0 => 'JATORRIA (2018)',
      ),
      'Initiative led by the Basque Energy Cluster.' => 
      array (
        0 => 'Energia Klusterra buru duen ekimena.',
      ),
      'Detection and remote monitoring in renewable marine energy.' => 
      array (
        0 => 'Itsasoko energia berriztagarriak urrunetik antzeman eta monitorizatzeko proiektua.',
      ),
      '16 regions, led by the Basque Country and Scotland.' => 
      array (
        0 => 'Euskadi eta Eskozia buru dituzten 16 eskualderen partaidetza.',
      ),
      'PILOT ACTION' => 
      array (
        0 => 'EKINTZA PILOTUA',
      ),
      'Defining a business case.' => 
      array (
        0 => '“Negozioaren kasua” definitzea.',
      ),
      'Responds to data governance and sovereignty challenges, guaranteeing privacy and security.' => 
      array (
        0 => 'Datuaren gobernantza eta subiranotasunaren arloko erronkei erantzutea, pribatutasuna eta segurtasuna bermatuz.',
      ),
      'Implements technology solutions based on IDSA reference architecture.' => 
      array (
        0 => '<strong>IDSAren erreferentziazko arkitekturan oinarritutako</strong> soluzio teknologikoen inplementazioa.',
      ),
      'LEARN MORE ABOUT THE CHALLENGE' => 
      array (
        0 => 'ERRONKA EZAGUTU',
      ),
      'Elkartek faces a range of challenges surrounding data, access, security, sovereignty and more.' => 
      array (
        0 => 'Elkartek-ek hainbat erronka ditu datuaren, sarbidearen, segurtasunaren, subiranotasunaren eta abarren inguruan.',
      ),
      'The Challenge' => 
      array (
        0 => 'Erronka',
      ),
      'DAEKIN\'S CHALLENGE IS <strong>TO DEVELOP A VIABLE PLATFORM</strong> FOR <strong>SECURE, ANONYMOUS DATA EXCHANGE</strong> USING BUSINESS MODELS THAT ALLOW <span class=\'magenta\'>DATA OWNERS</span> AND <span class=\'magenta\'>DATA USERS</span> TO TAKE PART.' => 
      array (
        0 => '<strong>DATUAK MODU SEGURU ETA ANONIMIZATUAN</strong> ETA <span class=\'magenta\'>“DATA OWNER”</span> EDO DATU-JABEEN ZEIN <span class=\'magenta\'>“DATA USER”</span> EDO DATU-ERABILTZAILEEN PARTE-HARTZEA BIDERATZEKO MODUKO NEGOZIO-EREDUEN ARABERA TRUKATZEA AHALBIDETUKO DUEN PLATAFORMA EGINGARRIA GARATZEA DA DAEKIN-EN ERRONKA.',
      ),
      'Data owners' => 
      array (
        0 => 'Datuen jabeak',
      ),
      'Data users' => 
      array (
        0 => 'Datuen erabiltzaileak',
      ),
      'OPERATION' => 
      array (
        0 => 'FUNTZIONAMENDUA',
      ),
      'Data transfer without IDS' => 
      array (
        0 => 'Datu-komunikazioak IDS gabe',
      ),
      'Data transfer with IDS' => 
      array (
        0 => 'Datu-komunikazioak IDSrekin',
      ),
      'User limits' => 
      array (
        0 => 'Datu-komunikazioak IDSrekin',
      ),
      'IDSA ECOSYSTEM' => 
      array (
        0 => 'IDSA EKOSISTEMA',
      ),
      'The platform is based on the IDSA ecosystem. IDSA is the European association that promotes and certifies software solutions, digital platforms and business models, creating a global standard of basic conditions for data and interface governance.' => 
      array (
        0 => 'Plataformak IDSAren ekosistema du oinarri, izan ere, europar elkarte honek software-soluzioak, plataforma digitalak eta hainbat negozio-eredu bultzatu eta ziurtatzen ditu, datu eta interfazeen gobernantzarako oinarrizko baldintzen nazioarteko estandarra sortuz.',
      ),
      'SECURITY GUARANTEE' => 
      array (
        0 => 'SEGURTASUN BERMEA',
      ),
      'Includes concept tests on the main components.' => 
      array (
        0 => 'Osagai nagusien kontzeptu-probak barne hartzen ditu.',
      ),
      'Guarantees traceability, security and governability.' => 
      array (
        0 => 'Trazabilitatea, segurtasuna eta gobernagarritasuna bermatzen ditu.',
      ),
      'Allows secure exchange of power data, allowing both users and suppliers to take part, maintaining control and sovereignty over the data.' => 
      array (
        0 => 'Energia arloko datuen truke segurua lantzen dihardu, erabiltzaileei eta hornitzaileei parte hartzeko aukera emanez, datuen kontrolari eta subiranotasunari euts diezaieten.',
      ),
      'PROJECT AIM' => 
      array (
        0 => 'PROIEKTUAREN HELBURUA',
      ),
      '<strong>To investigate the technological advances required to define a data sharing platform reference architecture</strong> based on the IDSA ecosystem, including concept testing of its main components, and to create an environment in which data services can be developed with guarantees of traceability, security and governability, particularly for the <span class=\'magenta\'><strong>power sector</strong>.</span>' => 
      array (
        0 => 'IDSA ekosisteman oinarritutako <strong>datuak partekatzeko plataforma baten erreferentziazko arkitektura</strong> definitzeko <strong>beharrezkoak diren aurrerapen teknologikoak ikertzea</strong>, osagai nagusien kontzeptu-probak barne, eta datuen inguruko zerbitzuen garapena erraztea trazabilitatea, segurtasuna eta gobernagarritasuna bermatuz, bereziki <span class=\'magenta\'><strong>energia-sektorearentzat</strong>.</span>',
      ),
      'OFFSHORE WIND PLATFORM' => 
      array (
        0 => 'OFFSHORE PLATAFORMA EOLIKOA',
      ),
      'This architecture will make it possible to implement a digital data platform on offshore wind energy. Allied to the huge traction of the local value chain, this will raise the Basque Country to a leadership position within the framework of the European Data Strategy in relation to the power sector.' => 
      array (
        0 => 'Arkitektura honi esker, Offshore Eolikoan datu-plataforma digitala ezarri ahal izango da, izan ere, Euskadiko balio-katearen trakzio-gaitasun handiak bultzatuta, gure Erkidegoa lidergo-posizioetara jasotzeko aukera erraztuko du, energia-sektoreari dagokion Datuaren Europako Estrategiaren barruan.',
      ),
      'CONNECTORS & INTERFACES' => 
      array (
        0 => 'CONNECTORS & INTERFACES',
      ),
      'Owners developing power assets' => 
      array (
        0 => 'Energia-aktiboak garatzen dituzten jabeak',
      ),
      'OEMs (Tier 1), EPC subcontractors, O&M services' => 
      array (
        0 => 'OEMs (Tier 1), EPC kontratistak, OM zerbitzuak',
      ),
      'DATA<br />PROVIDER' => 
      array (
        0 => 'DATA<br />PROVIDER',
      ),
      'Data Owners' => 
      array (
        0 => 'Data Owners',
      ),
      'Data Users' => 
      array (
        0 => 'Data Users',
      ),
      'Systems and components manufacturers (Tiers 2 and 3), Engineering firms' => 
      array (
        0 => 'Sistemen eta osagaien fabrikatzaileak (Tier 2 and 3), Ingeniaritzak',
      ),
      'ICT businesses: software, sensors, communications, Big Data, data analysis, AI, etc.' => 
      array (
        0 => 'IKT enpresak: softwareen garatzaileak, sentsoreak, komunikazioak, Big Data, datuen analisia, AI...',
      ),
      'Academia: research centres, universities, test labs, etc.' => 
      array (
        0 => 'Akademikoa: ikerketa-zentroak, unibertsitateak, proba-laborategiak, etab.',
      ),
      'LEARN MORE ABOUT THE PROJECT' => 
      array (
        0 => 'PROIEKTUA EZAGUTU',
      ),
      'See the project phases and the role played by each participant' => 
      array (
        0 => 'Ezagutu proiektuaren faseak eta partaide bakoitzaren eginkizuna',
      ),
      'WORK PACKAGES' => 
      array (
        0 => 'LAN PAKETEAK',
      ),
      'LEARN MORE ABOUT DAEKIN\'S WORK PACKAGES AND HOW WORK IS STRUCTURED ALONG THE PROJECT.' => 
      array (
        0 => 'EZAGUTU DAEKIN-EN LAN-PAKETEAK ETA NOLA DAGOEN ANTOLATUTA LANA PROIEKTUAREN BARRUAN.',
      ),
      'PROJECT LEADER' => 
      array (
        0 => 'PROIEKTUAREN LIDERRA',
      ),
      'CONSORTIUM' => 
      array (
        0 => 'PARTZUERGOA',
      ),
      'El proyecto está liderado por el centro tecnológico <a href=\'https://www.tecnalia.com/\' target=\'_blank\' class=\'black\'>TECNALIA</a>, que además coordinará las actividades en torno al enfoque IDS en cuanto a la compartición del dato con soberanía, y a la aplicabilidad de los distintos enfoques en el caso de uso offshore. Otro de los centros tecnológicos, <a href=\'https://www.ikerlan.es/\' target=\'_blank\' class=\'black\'>IKERLAN</a>, estará a cargo tanto de los requisitos y el diseño de la arquitectura de regencia como del enfoque Datalake. <a href=\'https://www.tekniker.es/\' target=\'_blank\' class=\'black\'>TEKNIKER</a>, el tercer y último centro tecnológico, lidera la tarea relacionada con el Edge Computing.' => 
      array (
        0 => '<a href=\'https://www.tecnalia.com/\' target=\'_blank\' class=\'black\'>TECNALIA</a>, zentro teknologikoak, proiektua lideratzeaz gain, IDS ikuspegiaren inguruko jarduerak koordinatuko ditu subiranotasuna duen datua partekatzeari eta offshore erabileren kasuan ikuspegi desberdinak aplikatzeari dagokienez. Beste zentro teknologikoetako bat, <a href=\'https://www.ikerlan.es/\' target=\'_blank\' class=\'black\'>IKERLAN</a>, erreferentziazko arkitekturaren betekizunez eta diseinuaz nahiz Datalake ikuspegiaz arduratuko da. Hirugarren eta azken zentro teknologikoa, <a href=\'https://www.tekniker.es/\' target=\'_blank\' class=\'black\'>TEKNIKER</a>,Edge Computing-en inguruko zereginaren buru izango da.',
      ),
      'En cuanto a las unidades de I+D empresariales hay que destacar las dos participantes en el proyecto: <a href=\'https://hispavistalabs.com/\' target=\'_blank\' class=\'black\'>HISPAVISTA LABS</a> y <a href=\'https://www.glual.com/es/ingenieria/glual_innova.html\' target=\'_blank\' class=\'black\'>GLUAL INNOVA</a>. Esta última es líder del PT responsable del análisis de modelos de compartición y entornos de colaboración. Completa el consorcio la <a href=\'http://www.clusterenergia.com/\' target=\'_blank\' class=\'black\'>ASOCIACIÓN CLUSTER DE ENERGIA</a> como agente de intermediación oferta/demanda. Este será el responsable de las actividades de comunicación y difusión de <span class=\'workMagenta\'><strong>DAEKIN</strong>.</span>' => 
      array (
        0 => 'Enpresetako I+G unitateei dagokienez, proiektuan parte hartu duten bi hauek nabarmendu behar dira: <a href=\'https://hispavistalabs.com/\' target=\'_blank\' class=\'black\'>HISPAVISTA LABS</a> eta <a href=\'https://www.glual.com/es/ingenieria/glual_innova.html\' target=\'_blank\' class=\'black\'>GLUAL INNOVA</a>. Azken hau da partekatze-ereduak eta lankidetza-inguruneak aztertzeaz arduratzen den LTaren burua. Partzuergoa <a href=\'http://www.clusterenergia.com/\' target=\'_blank\' class=\'black\'>ENERGIA KLUSTERRAREKIN</a> osatzen da, eskaintza eta eskariaren arteko bitartekaritza-eragile gisa <span class=\'workMagenta\'><strong>DAEKIN</strong></span> proiektuari buruzko komunikazio eta zabalkundearen arloko jarduerez arduratuko da.',
      ),
      'WP1. Analysis of sharing models and teamwork environments' => 
      array (
        0 => '1.LP. Partekatze-ereduen eta lankidetza-inguruneen azterketa',
      ),
      'WP1. ANALYSIS OF SHARING MODELS AND TEAMWORK ENVIRONMENTS' => 
      array (
        0 => '1.LP. PARTEKATZE-EREDUEN ETA LANKIDETZA-INGURUNEEN AZTERKETA',
      ),
      'WP LEADER' => 
      array (
        0 => 'LP-KO LIDERRA',
      ),
      'This Work Package conducts a general analysis on the implications of sharing and data teamworking in business models, defining no specific domains.' => 
      array (
        0 => 'Lan Pakete honetan oro har aztertuko da, arlorik zehaztu gabe, partekatzeak eta lankidetzak negozio-ereduetako datuen inguruan duten inplikazioa.',
      ),
      'This activity consists of:' => 
      array (
        0 => 'Jarduera honetan:',
      ),
      'General analysis.' => 
      array (
        0 => 'Analisi orokorra egingo da.',
      ),
      'Analysis of the specific wind power use case, determining the requirements and specifications needed for subsequent work packages (WP2, WP3, WP4, WP5).' => 
      array (
        0 => 'Eolikoaren erabilera zehatz baten kasua aztertuko da, ondorengo lan-paketeetan (2.LP, 3.LP, 4.LP eta 5.LP) beharrezkoak izango diren betekizunak eta zehaztapenak finkatzeko.',
      ),
      'Finally, an analysis of current methods and cases that support the roll-out of the models.' => 
      array (
        0 => 'Azkenik, eredu horiek abian jartzea bultzatzen duten egungo metodologiak eta kasuistikak aztertuko dira.',
      ),
      'New forms of data collaboration and business models are analysed.' => 
      array (
        0 => 'Datuen arloko lankidetza eta negozio-eredu berriak aztertuko dira.',
      ),
      'It is focussed on a case study.' => 
      array (
        0 => 'Azterketa-kasu batera bideratuko da.',
      ),
      'OFFSHORE WIND POWER' => 
      array (
        0 => 'OFFSHORE EOLIKOA',
      ),
      'The methods put into place are analysed.' => 
      array (
        0 => 'Aldez aurretik inplementatutako metodologiak aztertuko dira.',
      ),
      '<span class=\'workBlue\'>WP2.</span> Reference architecture requirements and design' => 
      array (
        0 => '<span class=\'workBlue\'>2.LP.</span> Erreferentziazko arkitekturaren betekizunak eta diseinua',
      ),
      'WP2. REFERENCE ARCHITECTURE REQUIREMENTS AND DESIGN' => 
      array (
        0 => '2.LP. ERREFERENTZIAZKO ARKITEKTURAREN BETEKIZUNAK ETA DISEINUA',
      ),
      'Work Package 2 will establish the design of an architecture which will make it possible to define the bases for research into data sharing with no loss of sovereignty, and concept testing for a series of development empowerment tools. The aim of this work package is to provide the basis architecture for WP3, WP4 and WP5, and gather the results obtained in a final architecture.' => 
      array (
        0 => '2. Lan Paketearen helburua da subiranotasunaren galerarik gabeko datuen partekatzeari buruzko ikerketarako oinarriak, eta aplikazioak gaitzeko zenbait tresnaren kontzeptu-probak definitzea ahalbidetuko duen arkitekturaren diseinua ezartzea. Lan-pakete honen bidez, 3.LP, 3.LP eta 5.LPrako oinarrizko arkitektura antolatu eta amaierako arkitekturan lortutako emaitzak bateratuko dira.',
      ),
      'Objectives:' => 
      array (
        0 => 'Helburuak:',
      ),
      'To define the high-level architecture.' => 
      array (
        0 => 'Goi-mailako arkitektura definitzea.',
      ),
      'To spell out the design for the reference architecture.' => 
      array (
        0 => 'Erreferentziako arkitekturaren diseinua zehaztea.',
      ),
      'To identify the tools and techniques required to verify the architecture and assess the Key Performance Indicators (KPI) that will be used in subsequent implementations of the architecture.' => 
      array (
        0 => 'Arkitektura egiaztatu eta KPI (Key Performance Indicators) ebaluatzeko beharrezkoak diren tresnen eta tekniken multzoa identifikatzea.',
      ),
      'Requirements' => 
      array (
        0 => 'Betekizunak',
      ),
      'Takes on the requirements for the models analysed in WP1.' => 
      array (
        0 => '1.LPan aztertutako ereduetarako baldintzak jaso dira.',
      ),
      'Architecture design' => 
      array (
        0 => 'Arkitekturaren diseinua',
      ),
      'The range of currently proposed design focuses, approaches and patterns are analysed:' => 
      array (
        0 => 'Egun proposatutako ikuspegiak, hurbilketak edo diseinu-ereduak aztertuko dira:',
      ),
      'Centralised repositories' => 
      array (
        0 => 'Errepositorioen zentralizazioa',
      ),
      'WP3' => 
      array (
        0 => '3.LP',
      ),
      'Architecture proposed in IDS' => 
      array (
        0 => 'IDSan proposatutako arkitektura',
      ),
      'WP4' => 
      array (
        0 => '4.LP',
      ),
      'Secure distributed processing' => 
      array (
        0 => 'Prozesamendu banatu segurua',
      ),
      'WP5' => 
      array (
        0 => '5.LP',
      ),
      'Verification' => 
      array (
        0 => 'Egiaztapena',
      ),
      'Indicators and risks are established, to be monitored and measured during the implementation of the architecture, providing a verification methodology.' => 
      array (
        0 => 'Arkitektura inplementatzean monitorizatu edo neur daitezkeen adierazleak eta arriskuak ezarriko dira, horiek egiaztatzeko metodologia finkatuz.',
      ),
      '<span class=\'workOrange\'>WP3.</span> Data value and governance - datalake approach' => 
      array (
        0 => '<span class=\'workOrange\'>3.LP.</span> Datuaren balioa eta gobernantza – datalake ikuspegia',
      ),
      'WP3. DATA VALUE AND GOVERNANCE - DATALAKE APPROACH' => 
      array (
        0 => '3.LP. DATUAREN BALIOA ETA GOBERNANTZA – DATALAKE IKUSPEGIA',
      ),
      'Work Package 3 aims to analyse the storage needs of data and metadata, both at infrastructure level and at data auditing and governance level. The aim of this work package is to design and develop a model to govern data and metadata obtained from a range of sources throughout their entire lifespan.' => 
      array (
        0 => '3. Lan Pakete honen helburua datuak eta dagozkien metadatuak biltegiratzeko beharrak aztertzea da, azpiegiturari zein datuen auditoretza eta gobernuari erreparatuta. Lan-pakete honen bidez, hainbat iturritatik bildutako datuak eta metadatuak bizi-ziklo osoan gobernatzen lagunduko duen modulua diseinatu eta garatuko da.',
      ),
      'It will also help to control dataset versions in order to construct incremental analytical data, and foster knowledge extraction by means of machine learning techniques based on metadata.' => 
      array (
        0 => 'Era berean, dataseten bertsioak kontrolatzen lagundu beharko du datu analitiko inkrementalak eraikitzeko, eta ezagutza ateratzeko gaitasuna izan beharko du machine learning tekniken bidez, metadatuetan oinarrituta.',
      ),
      'Main aims:' => 
      array (
        0 => 'Helburu nagusiak:',
      ),
      'To define data storage requirements.' => 
      array (
        0 => 'Datuak biltegiratzeko betekizunak zehaztea.',
      ),
      'To define data security and governance requirements.' => 
      array (
        0 => 'Datuen segurtasun eta gobernuari buruzko betekizunak zehaztea.',
      ),
      'To identify the technology needed for the efficient management of data and metadata throughout their lifespan.' => 
      array (
        0 => 'Datuak eta metadatuak bizi-ziklo osoan eraginkortasunez kudeatzeko behar diren teknologiak identifikatzea.',
      ),
      'To develop a module entrusted with versioning datasets to generate analytical models.' => 
      array (
        0 => 'Eredu analitikoak sortzeko datasetak bertsionatzeaz arduratuko den modulua garatzea.',
      ),
      'To develop a module to extract knowledge from metadata.' => 
      array (
        0 => 'Metadatuetan oinarritutako ezagutza ateratzeko aukera emango duen modulua garatzea.',
      ),
      'DATALAKE APPROACH' => 
      array (
        0 => 'DATALAKE EFEKTUA',
      ),
      'DIFFERENT SOURCES OF DATA' => 
      array (
        0 => 'DATU-ITURRI DESBERDINAK',
      ),
      'DATA ARE STORED DIRECTLY, UNCLASSIFIED' => 
      array (
        0 => 'DATUAK ZUZENEAN BILTEGIRATZEN DIRA, SAILKATU GABE',
      ),
      'DATA ARE SELECTED AND CLASSIFIED AS NEEDED' => 
      array (
        0 => 'DATUAK BEHAR DIREN UNEAN HAUTATU ETA SAILKATZEN DIRA',
      ),
      '<span class=\'workGreen\'>WP4.</span> Data sharing with sovereignty - IDS approach' => 
      array (
        0 => '<span class=\'workGreen\'>4.LP.</span> Subiranotasuna duen datua partekatzea – IDS ikuspegia',
      ),
      'WP4. DATA SHARING WITH SOVEREIGNTY - IDS APPROACH' => 
      array (
        0 => '4.LP. SUBIRANOTASUNA DUEN DATUA PARTEKATZEA – IDS IKUSPEGIA',
      ),
      'Work Package 4 seeks to analyse assisted data sharing by means of use policies, forestalling against sovereignty loss.' => 
      array (
        0 => '4. Lan Pakete honen helburua datuen partekatze lagunduaren ikuspegia aztertzea da, subiranotasunaren galera saihestuko duten erabilera-politiken bidez.',
      ),
      'Activities:' => 
      array (
        0 => 'Jarduerak:',
      ),
      'The first activity will be research into the said use policies, with two main aims:' => 
      array (
        0 => 'Egin beharreko lehenengo jarduera erabilera-politikei buruzko ikerketa izango da, bi helburu nagusi lortzeko:',
      ),
      'To study the definition possibilities of the use policies and possible languages, focussing on designing a final use policy definition language.' => 
      array (
        0 => 'Erabilera-politika horiek eta balizko hizkuntzak zehazteko aukerak aztertzea, erabilera-politikak definitzeko amaierako hizkuntzaren diseinua ikerketaren ardatz bihurtuz.',
      ),
      'To design a solution capable of controlling the use of data for the policies defined in the previous task, i.e. complying with use policies for distributed platforms.' => 
      array (
        0 => 'Aurreko eginkizunean zehaztutako politiketan datuen erabilera kontrolatzen lagun dezakeen soluzioa diseinatzea, hau da, plataforma banatuetan erabilera-politikak betetzea.',
      ),
      'Another important task of this work package is to review the design of the components of the IDS architecture, with the aim of adapting them, improving them or detecting new component designs not taken into account heretofore, and which are defined for offshore wind power.' => 
      array (
        0 => 'Gainera, lan-pakete honen beste jarduera nagusietako bat IDS arkitekturaren osagaien diseinua berrikustea izango da, osagai horiek egokitu eta hobetzeko, edo eolikoaren testuingururako definitu eta orain arte aintzat hartu ez diren osagaien diseinu berriak detektatzeko.',
      ),
      'Finally, the package will aim to integrate and validate new components.' => 
      array (
        0 => 'Azkenik, osagai berriak integratzen eta baliozkotzen saiatuko da.',
      ),
      'COMPONENT INTEGRATION AND VALIDATION' => 
      array (
        0 => 'OSAGAIEN INTEGRAZIOA ETA BALIOZKOTZEA',
      ),
      '<strong>Data sharing policies</strong> are defined.' => 
      array (
        0 => 'Datuak <strong>partekatzeko politikak</strong> definituko dira.',
      ),
      'A solution capable of <strong>controlling data use</strong> for the defined policies is designed.' => 
      array (
        0 => '<strong>Datuen erabilera kontrolatzeko</strong> gai den soluzioa diseinatuko da zehaztutako politiketarako.',
      ),
      'The <strong>characteristics and limitations</strong> of the <strong>IDS</strong> architecture are studied.' => 
      array (
        0 => '<strong>IDS</strong> arkitekturaren <strong>ezaugarriak eta mugapenak</strong> aztertuko dira."',
      ),
      'A methodology is defined to allow for <strong>integration and validation of IDS components</strong> defined in advance, checking that they comply with the defined design and requirements.' => 
      array (
        0 => 'Aldez aurretik garatutako <strong>IDS osagaiak integratu eta baliozkotzeko</strong> metodologia definituko, aldez aurretik zehaztutako diseinua eta baldintzak betetzen dituztela egiaztatuz.',
      ),
      'The end result is <strong>adapting the architecture proposed by IDS</strong>, improving it to cover the requirements specified in WP2, and giving it a much more detailed design than currently supported by the original.' => 
      array (
        0 => 'Azken emaitza <strong>IDS bidez proposatutako arkitekturaren egokitzapena</strong> izango da, 2. LPan zehaztutako baldintzak betetzeko hobetua eta jatorrizkoak baino zehaztasun handiagoko diseinua duena.',
      ),
      'END RESULT' => 
      array (
        0 => 'AZKEN EMAITZA',
      ),
      'A reference design of the IDS architecture based on concept tests on the basic components and studying its suitability for the <strong>offshore use case</strong>.' => 
      array (
        0 => 'IDS arkitekturaren erreferentziazko diseinua egin da, oinarrizko osagaien kontzeptu-probetan oinarrituta, eta <strong>offshore erabileraren kasura</strong> egokitzen ote den aztertuko da.',
      ),
      'DATASETs transferred from the supplier to the user' => 
      array (
        0 => 'Hornitzaileak erabiltzaileari transferitutako DATASETak',
      ),
      'METADATA description of DATASETS/supplier/user' => 
      array (
        0 => 'METADATA hornitzailea/erabiltzailea DATASETen deskribapena',
      ),
      'APPLICATION for specific data manipulation' => 
      array (
        0 => 'Datuen manipulazio espezifikorako APLIKAZIOA',
      ),
      'Data exchange ACTIVE' => 
      array (
        0 => 'Datu-truke AKTIBOA',
      ),
      'Data exchange INACTIVE' => 
      array (
        0 => 'Datu-truke INAKTIBOA',
      ),
      'Metadata exchange' => 
      array (
        0 => 'Metadatuen trukea',
      ),
      'Application download' => 
      array (
        0 => 'Aplikazioen deskarga',
      ),
      'Scroll Up' => 
      array (
        0 => 'IGO',
      ),
      '<span class=\'workPink\'>WP5.</span> Edge computing distributed processing' => 
      array (
        0 => '<span class=\'workPink\'>5.LP.</span> Egde computing prozesamendu banatua',
      ),
      'WP5. EDGE COMPUTING DISTRIBUTED PROCESSING' => 
      array (
        0 => '5.LP. EGDE COMPUTING PROZESAMENDU BANATUA',
      ),
      'Work Package 5 will analyse <strong>two possibilities for enabling data from the Edge environment towards the outside</strong>: distributed and federated focus. On the one hand, in the distributed approach, this will allow for the validation of a secure, reliable data exchange demo, and, on the other, in the federated approach, the implementation of FML and Blockchain techniques.' => 
      array (
        0 => '5. Lan Paketearen <strong>datuak Edge ingurunetik kanpoko erakundeei egokitzeko dauden bi aukera aztertuko dira</strong>: ikuspegi banatua eta ikuspegi federatua. Honela, ikuspegi banatuan oinarritutako datu-truke seguru eta fidagarriaren erakustaldia baliozkotu ahal izango da, baita FML eta Blockchain tekniken inplementazioa ere, ikuspegi federatuan.',
      ),
      'DISTRIBUTED APPROACH' => 
      array (
        0 => 'IKUSPEGI BANATUA',
      ),
      'The aim is to analyse the characteristics and limits of the Edge environment and the scope of IDS approaches with regard to connectors. Based on the findings, it will seek to implement and validate data exchange using an IDS connector on the Edge. Finally, a data sovereignty solution will be implemented and validated, to guarantee secure, reliable data sharing.' => 
      array (
        0 => 'Edge ingurunearen ezaugarriak eta mugapenak aztertzea du helburu, baita IDS hurbilketen irismena ere, konektoreei dagokienez. Lortutako emaitzen arabera, Edge inguruneko IDS konektore batetik abiatuta egindako datu-trukea inplementatu eta baliozkotu nahi da. Azkenik, datuen partekatze seguru eta fidagarria bermatuko duen datuen subiranotasunari buruzko soluzio bat inplementatu eta baliozkotuko da.',
      ),
      'FEDERATED APPROACH' => 
      array (
        0 => 'IKUSPEGI FEDERATUA',
      ),
      'The aim is to analyse and ascertain the viability of implementing FML, Blockchain and cryptographic techniques in use cases where data sharing is not viable but agents wish to work together to obtain greater insight, without compromising their respective data.' => 
      array (
        0 => 'FML, Blockchain eta kriptografia tekniken inplementazioaren bideragarritasuna aztertu eta frogatzea du helburu, datuak partekatzea bideragarria ez den kasuetarako, eta hainbat eragileren arteko lankidetza sustatu nahi da insight hobeak lortzeko, baina alde bakoitzaren informazioa arriskuan jarri gabe.',
      ),
      'EDGE ENVIRONMENT' => 
      array (
        0 => 'EDGE INGURUNEA',
      ),
      'EDGE COMPUTING' => 
      array (
        0 => 'EDGE COMPUTING',
      ),
      'Edge computing is a paradigm of distributed computing that <strong>transmits computing and data storage to where they are needed</strong>, improving response times and saving bandwidth.' => 
      array (
        0 => 'Konputazio banatuaren paradigma honen bitartez, <strong>datuen konputazioa eta biltegiratzea hurbiltzen dira beharrezkoak dituzten kokapenetara</strong>, erantzun-denborak hobetu eta banda-zabalera aurrezteko.',
      ),
      'Data sharing in <strong>Edge environments</strong> involves <strong>challenges around integrating</strong> data from different sources and sharing them with other agents to create new business models.' => 
      array (
        0 => '<strong>Edge inguruneetan</strong>, datuak partekatzeak zenbait <strong>erronka</strong> ditu hainbat iturritatik bildutako datuak <strong>integratzeari</strong> eta beste erakunde batzuekin partekatzeari dagokienez, negozio-eredu berriak sortu ahal izateko.',
      ),
      '<span class=\'workYellow\'>WP6.</span> Reference architecture requirements and design' => 
      array (
        0 => '<span class=\'workYellow\'>6.LP.</span> Erreferentziazko arkitekturaren diseinua eta eskakizunak',
      ),
      'WP6. REFERENCE ARCHITECTURE REQUIREMENTS AND DESIGN' => 
      array (
        0 => '6.LP. ERREFERENTZIAZKO ARKITEKTURAREN DISEINUA ETA ESKAKIZUNAK',
      ),
      'Work Package 6 is a cross-cutting package where the developments from the previous work packages are put towards defining the reference architecture for a specific use case in offshore wind power.' => 
      array (
        0 => '6. Lan Paketea, offshore eolikoari dagokion kasu jakin baterako erreferentziazko arkitekturaren zehaztapenaren bitartez, aurreko lan-paketeen garapenak gauzarazten dituen zeharkako lan-paketea da.',
      ),
      'Ultimate goal:' => 
      array (
        0 => 'Azken helburua:',
      ),
      'The ultimate goal is to use the platform as a <strong>data sharing</strong> environment among businesses along the offshore wind power value chain, becoming a <strong>benchmark at European level</strong> with the capacity to integrate into the GAIA-X single data space being promoted by the European Commission.' => 
      array (
        0 => 'Offshore eolikoaren balio-kateko enpresei <strong>datuak</strong> elkarren artean <strong>partekatzeko</strong> ingurunea eskaini eta <strong>Europa mailako erreferente</strong> bihurtzea da plataforma honen azken helburua, Europako Batzordea sustatzen ari den datu-esparru bateratuan (GAIA-X) integratzeko aukerarekin.',
      ),
      'The WP goes in greater depth into the previous analysis of the <strong>requirements</strong> of offshore wind power, undertaking <strong>a more detailed study</strong>.' => 
      array (
        0 => 'Dagoeneko eginda dagoen offshore eolikoaren kasuaren <strong>betekizunen</strong> azterketan sakondu eta azterketa zehatzagoa egin da.',
      ),
      'The requirements are integrated with the <strong>technologies developed</strong> by WP2, WP3, WP4 and WP5.' => 
      array (
        0 => 'Betekizun horiek 2LP, 3LP, 4LP eta 5LPen <strong>garapen teknologikoekin</strong> integratu dira.',
      ),
      'A design document is developed, specifying the components required for the specific reference architecture to be implemented in a <span class=\'data d-inline workYellow\'>REAL PLATFORM.</span>' => 
      array (
        0 => 'Diseinuari buruzko dokumentua garatu da, eta bertan, beharrezko osagaiak zehaztu dira erreferentziazko arkitekturaren berezitasuna <span class=\'data d-inline workYellow\'>PLATAFORMA ERREAL</span> batean inplementatu ahal izateko.',
      ),
      '<span class=\'workDarkBlue\'>WP7.</span> Communication and dissemination' => 
      array (
        0 => '<span class=\'workDarkBlue\'>7.LP.</span> Komunikazioa eta zabalkundean',
      ),
      'WP7. COMMUNICATION AND DISSEMINATION' => 
      array (
        0 => '7.LP. KOMUNIKAZIOA ETA ZABALKUNDEA',
      ),
      'The main aim of Work Package 7 is to define and roll out the strategy for communicating the developments and outcomes of the project, maximising its visibility with regard to identified stakeholders and driving the transfer of the knowledge and technologies developed towards energy value chain businesses.' => 
      array (
        0 => '7. Lan Paketearen helburu nagusia proiektuaren aurrerapen eta emaitzei buruzko komunikazio-estrategia zehaztu eta abian jartzea da, identifikatutako intereseko taldeetan ahalik eta ikusgaitasunik handiena lortzeko, eta ezagutzaren eta teknologia garatuen transferentzia bultzatzeko energiaren arloko balio-kateetako enpresen mesedetan.',
      ),
      'ACTIVITIES' => 
      array (
        0 => 'JARDUERAK',
      ),
      'IDENTITY' => 
      array (
        0 => 'NORTASUNA',
      ),
      'Creating a <strong>corporate identity</strong> for the project.' => 
      array (
        0 => 'Proiektuaren <strong>nortasun korporatiboa</strong> sortzea.',
      ),
      'APPLICATIONS' => 
      array (
        0 => 'APLIKAZIOAK',
      ),
      'Developing a <strong>website</strong> and <strong>leaflets</strong>.' => 
      array (
        0 => '<strong>Webgunea</strong> eta <strong>diptikoak</strong> garatzea.',
      ),
      'COMMUNICATION' => 
      array (
        0 => 'KOMUNIKAZIOA',
      ),
      'Creating <strong>press release</strong> and <strong>articles</strong> for local and international media.' => 
      array (
        0 => '<strong>Prentsa-oharrak</strong> landu eta tokiko zein nazioarteko komunikabideetan <strong>artikuluak</strong> argitaratzea.',
      ),
      'Creating a new Work Group:' => 
      array (
        0 => 'Lan-Talde berriaren sorrera:',
      ),
      'Digitalising wind turbine components.' => 
      array (
        0 => 'Aerosorgailuetako osagaien digitalizazioa.',
      ),
      'DISSEMINATION' => 
      array (
        0 => 'ZABALKUNDEA',
      ),
      'Presence at international <strong>fairs and events</strong>.' => 
      array (
        0 => 'Nazioarteko <strong>azoka eta ekitaldietan</strong> parte hartzea.',
      ),
      'Organising <strong>sector workshops</strong> and <strong>seminars relating to</strong> the project.' => 
      array (
        0 => 'Proiektuari lotutako <strong>workshop sektorialak</strong> eta <strong>jardunaldiak antolatzea</strong>.',
      ),
      'Carrying out <strong>presentations</strong> at <strong>technical conferences</strong>.' => 
      array (
        0 => '<strong>Hitzaldi teknikoetan txostenak</strong> aurkeztea.',
      ),
      'Creating a new European-level data-sharing community:' => 
      array (
        0 => 'Datuak partekatzeko Europa mailako komunitate berri baten sorrera:',
      ),
      'Energy Community' => 
      array (
        0 => 'Energy Community',
      ),
      'Planned presence at international fairs and events' => 
      array (
        0 => 'Aurreikusitako nazioarteko azoka eta ekitaldietan parte hartzea',
      ),
      '<span class=\'data\'>Wind Europe Copenhagen 2021</span>Group stand at fair.' => 
      array (
        0 => '<span class=\'data\'>Wind Europe Copenhagen 2021</span>Stand taldekatuaren bidezko parte-hartzea.',
      ),
      '<span class=\'data\'>EES/Intersolar Munich 2021</span>Own stand at fair.' => 
      array (
        0 => '<span class=\'data\'>EES/Intersolar Munich 2021</span>Stand propioaren bidezko parte-hartzea.',
      ),
      '<span class=\'data\'>EnLit Europe 2021</span>Own stand at fair.' => 
      array (
        0 => '<span class=\'data\'>EnLit Europe 2021</span>Stand taldekatuaren bidezko parte-hartzea.',
      ),
      'Wind Turbine Data Sharing Work Group' => 
      array (
        0 => 'Aerosorgailuen Datuak Partekatzeari buruzko Lan-Taldea',
      ),
      'Aims of Work Group:' => 
      array (
        0 => 'Lan-Taldearen helburuak:',
      ),
      'To facilitate wind turbine component manufacturers (segment 3) access to the data required to operate wind farms, so that they can obtain value from them and improve their competitive offer.' => 
      array (
        0 => 'Aerosorgailuetako osagaien fabrikatzaileei (3. segmentua) parke eolikoetako eragiketa-lanetan beharrezkoak diren datuak eskuratzeko aukera erraztea, horietatik balioa lortu eta lehiatzeko eskaintza hobetu ahal dezaten.',
      ),
      'To compile, process and analyse the requirements and specifications of the data requested by wind turbine component manufacturers (Data Users).' => 
      array (
        0 => 'Aerosorgailuetako osagaien fabrikatzaileek (“Data users”) eskatutako datuen betekizunak eta zehaztapenak biltzea prozesatu eta aztertu ahal izateko.',
      ),
      'To identify the conditions and requirements of Data Owners (segments 1 & 2) for data sharing (data privacy, security, governance, compensation...) and offer solutions.' => 
      array (
        0 => '“Datuen jabeak” diren enpresek (“Data owners”, 1. eta 2. segmentuak)  datuak partekatzeko ezarri dituzten baldintzak eta betekizunak (pribatutasuna, segurtasuna, gobernantza, datuen ordainsaria) zehaztu eta hauei eman beharreko erantzunak proposatzea.',
      ),
      'To contribute to the Elkartek DAEKIN project in defining and validating a digital platform for sharing data from wind turbines in operational wind farms, offering confidence in data privacy and security and providing easy, systematic access to quality data to businesses all along the value chain.' => 
      array (
        0 => 'DAEKIN Elkartek proiektuari eragiketan dauden parke eolikoetako aerosorgailuetatik ateratako datuak partekatzeko plataforma digitala zehazten eta baliozkotzen laguntzea, datuen pribatutasun eta segurtasunari buruzko konfiantza areagotzeko eta balio-kateko enpresa guztiei kalitatezko datuak modu erraz eta sistematikoan eskuratzeko aukera emateko.',
      ),
      'To identify collaborative projects for joint data use and analysis among companies in the wind value chain.' => 
      array (
        0 => 'Balio-kate eolikoko enpresen artean datuak batera erabili eta aztertzeko lankidetza bidezko -proiektuak identifikatzea.',
      ),
      'EXPECTED RESULTS' => 
      array (
        0 => 'AURREIKUSITAKO EMAITZAK',
      ),
      'Specifications for the data requested by component manufacturers (Data users): quantity, frequency, quality, taxonomy, format, etc.' => 
      array (
        0 => 'Osagaien fabrikatzaileek eskatutako datuen zehaztapenen agiria (“Data users”): kopurua, maiztasuna, kalitatea, taxonomia, formatua...',
      ),
      'Agreements with Data owners (wind farm operators, wind turbine manufacturers) for the sharing (cession/sale) of wind turbine data at component and/or subsystem level.' => 
      array (
        0 => '“Datuen jabeak” diren enpresekiko akordioak (“Data owners”: parke eolikoetako operadoreak, aerosorgailuen fabrikatzaileak) aerosorgailuen datuak osagai edota azpisistema mailan partekatu (laga/saldu) ahal izateko.',
      ),
      'Comparison and review of the specifications and architecture of the digital platform to be defined and developed as part of the DAEKIN project.' => 
      array (
        0 => 'DAEKIN proiektuan zehaztu eta garatu beharreko plataforma digitalaren arkitektura eta zehaztapenak kontrastatu eta berrikustea.',
      ),
      'Validation of the demo / minimum viable product of the digital platform.' => 
      array (
        0 => 'Plataforma digitalaren Gutxieneko Produktu Bideragarria deritzon frogatzailea baliozkotzea.',
      ),
      'Proposed collaborative pilot projects on using and sharing accessible data.' => 
      array (
        0 => 'Eskuragarri dauden datuak erabili eta aztertzeko lankidetza bidezko proiektu pilotuen proposamenak.',
      ),
      'MANUFACTURERS' => 
      array (
        0 => 'ENPRESAK<br />FABRIKATZAILEAK',
      ),
      'DIGITALISATION<br />COMPANIES' => 
      array (
        0 => 'ENPRESAK<br />DIGITALIZAZIOA',
      ),
      'BASQUE NETWORK<br />AGENTS' => 
      array (
        0 => 'EUSKAL SAREKO<br />ERAGILEAK',
      ),
      'WORKING GROUP ACTIVITIES' => 
      array (
        0 => 'LAN TALDEKO JARDUERAK',
      ),
      'Launch meeting of the Wind Turbine Data Sharing Work Group.' => 
      array (
        0 => 'Aerosorgailuen Datuak Partekatzeari buruzko Lan-Taldea abiarazteko bilera.',
      ),
      'Bilateral meetings to compile the specifications of the data demanded by component manufacturers.' => 
      array (
        0 => 'Osagaien fabrikatzaileek eskatutako datuen zehaztapenen bilketa, aldebiko bileren bitartez.',
      ),
      '<strong>Filter</strong>:' => 
      array (
        0 => '<strong>Iragazi</strong>:',
      ),
      'See the latest news and developments surrounding Daekin' => 
      array (
        0 => 'Ezagutu Daekini buruzko azken albisteak eta bilakaera',
      ),
      'ACTIVITIES AND<br />OUTCOMES' => 
      array (
        0 => 'JARDUERAK ETA<br />EMAITZAK',
      ),
      'All' => 
      array (
        0 => 'Dena',
      ),
      'Activities' => 
      array (
        0 => 'Jarduerak',
      ),
      'Outcomes' => 
      array (
        0 => 'Emaitzak',
      ),
      'PREVIOUS' => 
      array (
        0 => 'AURREKOA',
      ),
      'NEXT' => 
      array (
        0 => 'HURRENGOA',
      ),
      'DAEKIN launched at Digitalisation Technology Forum' => 
      array (
        0 => 'DAEKINen aurkezpena Digitalizazioaren Foro Teknologikoan',
      ),
      'SECTOR NEWS' => 
      array (
        0 => 'SEKTOREKO BERRIAK',
      ),
      'Energy Cluster Association' => 
      array (
        0 => 'Energia Kluster Elkartea',
      ),
      'Project funded by the Department of Economic Development and Infrastructure of the Basque Government (HAZITEK programme) and the European Regional Development Fund (ERDF)' => 
      array (
        0 => 'Eusko Jaurlaritzako Ekonomiaren Garapen eta Azpiegitura Sailak (HAZITEK programa) eta Eskualde Garapeneko Europako Funtsak (FEDER) finantzatutako proiektua',
      ),
      'Read more' => 
      array (
        0 => 'Irakurri gehiago',
      ),
      'Back to' => 
      array (
        0 => 'Itzuli',
      ),
      'News' => 
      array (
        0 => 'Berrira',
      ),
      'Keep up to date with our latest news.' => 
      array (
        0 => 'Jarrai ezazu gure azken berrien berri.',
      ),
    ),
  ),
);