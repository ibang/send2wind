﻿Antes de trabajar en cualquier entorno (Desarrollo, Pruebas o Online) hay que crear una serie de enlaces simbólicos a tres archivos (.htaccess de home y de cms y en el config.php)

Una opción es hacerlo desde la terminal, como explicamos más abajo, y otra ejecutar el script correspondiente al sistema operativo utilizado para crear de forma automática los enlaces:

createWindowsLinks.bat para windows mediante doble clic

createMacLinks.command para mac mediante doble clic (tiene que tener permisos de ejecución a través del terminal >chmod +x createMacLinks.command )

createLinuxLinks.sh para linux o mac vía terminal ( >sh createLinuxLinks.sh y tiene que tener permisos de ejecución. >chmod +x createLinuxLinks.sh)


IDIOMAS:

	Hay dos métodos para gestionar las traducciones:
		1 - Método XML
			Se obtienen las traducciones desde un fichero XML alojado en la carpeta .lang/{idioma}.xml a través de la variable $txt->{RUTA}...->
			
		2 - Fichero PO
			Se obtienen las traducciones a través de ficheros PO/MO alojados en la carpeta .lang/locale/{idioma}/LC_MESSAGES/{fichero}.mo/po.
			Para obtener la traducción, se escribe el comando _("{ID TEXTO A TRADUCIR}")
			Primero se crea un archivo {fichero}.po vacío en la carpeta y se establecen las propiedades con el programa poedit:
				- PATH base para hacer las búsquedas de textos a traducir.
				- PATHs a excluir de la búsqueda de textos a traducir.
				- Generar búsqueda de textos desde las fuentes.
				- Reiniciar apache para evitar cacheo.
			En el fichero init.php se carga el método de idioma elegido (PO o XML)
		
		3 - Establecer Método de idioma a utilizar (XML, PO o ambos)
			En el fichero de config.php, configurar el método de traducción.
			ATENCIÓN: Para los textos ESTÁTICOS funcionan muy bien los ficheros PO, pero para dinámicos, como las URLS, 
			no hay una forma eficiente de hacerlo con PO, yo usuaría en este caso XML o BASE DE DATOS

Desarrollo:

.htaccess

		- Eliminar .htaccess si existe
		- Crear enlace simbólico "Como administrador" del archivo htaccess_local.txt al archivo virtual .htaccess
		- WINDOWS: mklink .htaccess htaccess_local.txt
		- LINUX / MAC: ln -s htaccess_local.txt .htaccess

robots.txt
		
		- Eliminar robots.txt si existe
		- Hacer una copia del fichero robots_desarrollo.txt en el archivo robots.txt
		
php/config.php

		- Eliminar config.php si existe
		- Crear enlace simbólico "Como administrador" del archivo config_local.php al archivo virtual config.php
		- WINDOWS: mklink config.php config_local.php
		- LINUX / MAC: ln -s config_local.php config.php
		
Pruebas:

.htaccess

		- Eliminar .htaccess si existe
		- Crear enlace simbólico "Como administrador" del archivo htaccess_pruebas.txt al archivo virtual .htaccess
		- WINDOWS: mklink .htaccess htaccess_pruebas.txt
		- LINUX / MAC: ln -s htaccess_pruebas.txt .htaccess

robots.txt
		
		- Eliminar robots.txt si existe
		- Hacer una copia del fichero robots_pruebas.txt en el archivo robots.txt
		
php/config.php

		- Eliminar config.php si existe
		- Crear enlace simbólico "Como administrador" del archivo config_pruebas.php al archivo virtual config.php
		- WINDOWS: mklink config.php config_pruebas.php
		- LINUX / MAC: ln -s config_pruebas.php config.php

Online:

.htaccess

		- Eliminar .htaccess si existe
		- Crear enlace simbólico "Como administrador" del archivo htaccess_online.txt al archivo virtual .htaccess
		- WINDOWS: mklink .haccess htaccess_online.txt
		- LINUX / MAC: ln -s htaccess_online.txt .htaccess

robots.txt
		
		- Eliminar robots.txt si existe
		- Hacer una copia del fichero robots_online.txt en el archivo robots.txt
		
php/config.php

		- Eliminar config.php si existe
		- Crear enlace simbólico "Como administrador" del archivo config_online.php al archivo virtual config.php
		- WINDOWS: mklink config.php config_online.php
		- LINUX / MAC: ln -s config_online.php config.php
					
sitemap.xml

		- Generar sitemap.xml

.gitignore

		- Borrar archivo .gitignore
		
README.txt

		- Borrar archivo README.txt
		
Sincronizar carpetas que no están en el repositorio