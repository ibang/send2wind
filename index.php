<?
require_once("php/preinit.php");
$idiomaDefecto = $db->get_default_lang();
$availableLocalizationsCodeList = $db->get_active_localizations_code_list();
$langsCodeList = $db->get_active_langs_array();

$strList = '';
if (!empty($langsCodeList)){ //primero los idiomas por si no encuentra ninguno coincidente para que coja uno de estos como los más prioritarios
	$strList.=($strList?','. implode(',', $langsCodeList):implode(',', $langsCodeList));
}
if (!empty($availableLocalizationsCodeList)){ //después los disponibles en la BD
	$strList.=($strList?','. implode(',', $availableLocalizationsCodeList):implode(',', $availableLocalizationsCodeList));
}
$strList = implode(',', array_unique(explode(',', $strList))); // remove duplicates

$available = parseLanguageList($strList);
session_name(COOKIENAME);
@session_start();
$prehome = false; // No dejamos que cambie de país (geolocalización desactivada)
$es_chrome = false;

$langURL = '';
$langURLReal = '';
$domainLanguage = HOSTDOMAIN;
$domainProtocol = BASE_PROTOCOL;
$languageList = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])?$_SERVER['HTTP_ACCEPT_LANGUAGE']:null);
$userAgent = (isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'');
if (empty($_SESSION['lang-location']) ){
	//iniciamos la sesión
	if (strpos($userAgent, 'Chrome') !== false){
		// User agent is Google Chrome
		$es_chrome = true;
	}
	$accepted = parseLanguageList($languageList, $es_chrome);
		
	$matches = findMatches($accepted, $available);
	
	if (!empty($matches)){
		$idiArr = reset($matches);
		$idiomaDefecto = $idiArr[0];
	}
	
	// GEOIP DETECTION
	if (GEOIP_ACTIVE=='1'){
		//addToLog("GEO IP DETECTION: START");
		$ipCountryIsoCode = false;
		$ipCountryIsoCode = getIPcountryIsoCode();
		//addToLog("GEO IP DETECTION: END");

		// GEOIP VS BROWSER LOCATION 
		// miramos si coincide el browser detected a través del HTTP_USER_AGENT con el IPcountry
		if ($ipCountryIsoCode and !preg_match("/-".$ipCountryIsoCode."/", $idiomaDefecto)){
			$currentURL = $db->get_url_from_localization_code($idiomaDefecto);
			$currentDefaultLan = substr($currentURL , 0,2);
			if ($db->get_lang_from_localization_url($currentDefaultLan.'-'.$ipCountryIsoCode)){
				$idiomaDefecto = $db->get_code_from_localization_url($currentDefaultLan.'-'.$ipCountryIsoCode);
			}
			else{ //miro si hay alguno del país
				$available_langs = $db->get_available_languages_for_country_iso($ipCountryIsoCode);
				if (!empty($available_langs)){
					$idiomaDefecto = $available_langs[0]['lang_code'].'-'.$ipCountryIsoCode;
				}
			}
		}
	}
	//
	
	$_SESSION['lang-location'] = $idiomaDefecto;
	$prehome = false;
}
elseif(!empty($_SESSION['lang-url']) AND (!isset($_GET['reset']) OR $_GET['reset']!='1')){ //cuando alguien accede a posta a la home metiendo la url, le redireccionamos donde debe ir con la sessión
	$prehome = false;
	$langURL = $_SESSION['lang-url']; //cojo la URL de navegación de la sesión, porque la hemos perdido de la url
	if ($domainLanguage = Constants::getDomainLanguage($langURL)){
		$langURLReal = '';
		$domainProtocol = Constants::getProtocol($domainLanguage);
	}
	else{
		$langURLReal = $langURL;
		$domainLanguage = HOSTDOMAIN;
		$domainProtocol = BASE_PROTOCOL;
	}
}
$langLocation = $_SESSION['lang-location'];
$query_string = (!empty($_SERVER['QUERY_STRING']) AND !isset($_GET['reset']))?'?'.$_SERVER['QUERY_STRING']:'';

if (!$langURL){
	$langURL = $db->get_url_from_localization_code($langLocation);
	if ($domainLanguage = Constants::getDomainLanguage($langURL)){
		$langURLReal = '';
		$domainProtocol = Constants::getProtocol($domainLanguage);
	}
	else{
		$langURLReal = $langURL;
		$domainLanguage = HOSTDOMAIN;
		$domainProtocol = BASE_PROTOCOL;
	}
	$_SESSION['lang-url'] = $langURL;
}

if (!$prehome){
	//addToLog("REDIRECT INDEX.PHP: ".".$domainProtocol."://".rtrim($domainLanguage.$URL_ROOT.$langURLReal,"/")."/".$query_string);
	//header( $_SERVER["SERVER_PROTOCOL"]." 301 Moved Permanently" );
	header('Link: <'.$domainProtocol.'://'.rtrim($domainLanguage.$URL_ROOT,'/').'/>; rel="alternate"; hreflang="x-default"');
	header( $_SERVER["SERVER_PROTOCOL"]." 302 Found" );
	header("Location: ".$domainProtocol."://".rtrim($domainLanguage.$URL_ROOT.$langURLReal,"/")."/".$query_string);
	exit();     
}
else{
	$PRE = '';
	include("site/prehome.php");
	unset($_SESSION['lang-location']);
	unset($_SESSION['lang-url']);
	session_write_close();
}
exit();
?>