<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Challenge - DAEKIN");
$description=__("Daekin's challenge is to develop a viable platform for secure, anonymous data exchange");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="nosotros" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
            <div class="row w-100 no-gutters">
              <div class="col-md-12">
                <figure class="figure mb-sm-0">
                  <img src="<?=$URL_ROOT?>assets/img/challenge/hero.jpg" class="">
                </figure>
                <div class="text-box pr-2">
                  <h1 class="pl-1 pl-lg-4"><?=__("THE CHALLENGE");?></h1>
                  <p class="scroll data"></p>
                </div>
              </div>
            </div>
            <div class="banda-hero mt-0 pt-2 pb-2 pl-2 pr-2 pl-md-4 pr-md-4">
              <div class="row">
                <div class="col-lg-6 align-self-center">
                  <p><?=__("DAEKIN (Datuak Konpartitzeko Ekimena) is a Type I ELKARTEK project, funded by the Basque Government's 2020 call.");?></p>
                </div>
                <div class="col-lg-2 text-center">
                  <img src="<?=$URL_ROOT?>assets/img/all/logo-gobierno-vasco.png" alt="<?=__("Eusko Jaurlaritza - Gobierno Vasco");?>">
                </div>
                <div class="col-lg-3 align-self-center">
                  <p class="small"><?=__("Project funded by the Basque Government's Department for  Economic Development, Sustainability and the Environment (ELKARTEK Programme).");?></p>
                </div>
              </div>
            </div>
          </article>
      </div>
    </div>
    <div class="full-container">
      <div class="container">

        <article class="mt-2 pl-1 pr-1 pl-md-3 pr-md-1">
          <div class="row bloques show-on-scroll">
            <div class="col-md-5 ml-md-2 mt-3">
                <p class="introDestacado"><?=__("DAEKIN'S CHALLENGE IS <strong>TO DEVELOP A VIABLE PLATFORM</strong> FOR <strong>SECURE, ANONYMOUS DATA EXCHANGE</strong> USING BUSINESS MODELS THAT ALLOW <span class='magenta'>DATA OWNERS</span> AND <span class='magenta'>DATA USERS</span> TO TAKE PART.");?></p>
            </div>
            <div class="col-md-6 pt-md-4 pb-md-4 pt-4 pb-4 ml-md-0 ml-3" style="background: url(<?=$URL_ROOT?>assets/img/challenge/bg-intro.jpg) no-repeat center top / 50%;">
              <div class="row align-self-center text-center ml-2 mt-2">
                <div class="col-4 bg-white shadow align-self-center pl-md-1 pr-md-1 pt-md-1 pb-md-0 mr-1">
                  <p class="text-center pt-1 pt-lg-0"><?=__("Data owners");?></p>
                </div>
                <div class="col-2 bg-white shadow text-center align-self-center pl-md-1 pr-md-1 pt-md-1 pb-md-1 mr-1">
                  <img src="<?=$URL_ROOT?>assets/img/all/double-arrow.svg" alt="">
                </div>
                <div class="col-4 bg-white shadow align-self-center pl-md-1 pr-md-1 pt-md-1 pb-md-0">
                  <p class="text-center pt-1 pt-lg-0"><?=__("Data users");?></p>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article id="challenge" class="mt-4 pl-1 pr-1 pl-md-3 pr-md-3 bloques show-on-scroll">
          <h2 class=""><?=__("OPERATION");?></h2>
          <div class="row text-center">
              <img class="graph w-100" src="<?=$URL_ROOT?>assets/img/challenge/industrial-data-space.svg" title="" />
          </div>
          <div class="row leyenda">
                <p><?=__("Data transfer without IDS");?></p>
                <p><?=__("Data transfer with IDS");?></p>
                <p><?=__("Data transfer with IDS");?></p>
                <p><?=__("Data users");?></p>
                <p><?=__("User limits");?></p>
          </div>
          <div class="row leyenda">
            <div class="col-md-5 ml-md-1">
              <div class="bg-white shadow p-2 mb-2 mt-2 pt-2">
                <h3 class="data"><?=__("IDSA ECOSYSTEM");?></h3>
                <p class="no-item"><?=__("The platform is based on the IDSA ecosystem. IDSA is the European association that promotes and certifies software solutions, digital platforms and business models, creating a global standard of basic conditions for data and interface governance.");?></p>
              </div>
            </div>
            <div class="col-md-6 ml-md-1">
              <div class="bg-white shadow p-2 mb-2 mt-2 pt-2">
                <h3 class="data"><?=__("SECURITY GUARANTEE");?></h3>
                <ul>
                  <li><?=__("Includes concept tests on the main components.");?></li>
                  <li><?=__("Guarantees traceability, security and governability.");?></li>
                  <li><?=__("Allows secure exchange of power data, allowing both users and suppliers to take part, maintaining control and sovereignty over the data.");?></li>
                </ul>
              </div>
            </div>
          </div>
        </article>

      </div>
    </div>

    <div class="container mt-3 mt-md-5 pb-4 bloques show-on-scroll">
      <article class="row">
          <div class="col-md-6 pl-md-2 pr-md-5">
            <h2><?=__("PROJECT AIM");?></h2>
            <p><?=__("<strong>To investigate the technological advances required to define a data sharing platform reference architecture</strong> based on the IDSA ecosystem, including concept testing of its main components, and to create an environment in which data services can be developed with guarantees of traceability, security and governability, particularly for the <span class='magenta'><strong>power sector</strong>.</span>");?></p>
          </div>
          <div class="col-md-6 pl-md-2">
            <figure class="text-right">
              <img src="<?=$URL_ROOT?>assets/img/challenge/foto-objetivo.png" alt="">
            </figure>
          </div>
          <div class="col-md-12 bg-magenta shadow mt-md-3 pt-3 pl-4 pr-4 pb-3 ml-md-2 mr-md-2">
            <h3 class="introDestacado"><?=__("OFFSHORE WIND PLATFORM");?></h3>
            <p class="columnas"><?=__("This architecture will make it possible to implement a digital data platform on offshore wind energy. Allied to the huge traction of the local value chain, this will raise the Basque Country to a leadership position within the framework of the European Data Strategy in relation to the power sector.");?></p>
          </div>
      </article>
    </div>
    <div class="full-container bg-gray pt-4 pb-4">
      <div class="container">
          <h3 class="text-center introDestacado"><?=__("OFFSHORE WIND POWER DATA DIGITAL PLATFORM");?></h3>
          <div class="row mt-1 mt-sm-4 mt-lg-4 align-self-start">
            <div class="fadeInDown col-md-4 mt-4 owners text-center">
              <div class="bg-corporate2 shadow max-width-2 mb-2 pt-2 pl-2 pr-2 pb-1 text-center align-self-center">
                <p class="data text-white"><?=__("CONNECTORS & INTERFACES");?></p>
              </div>
              <figure>
                <img class="rotate-vertical-right pb-3" src="<?=$URL_ROOT?>assets/img/all/arrow.svg">
                <img class="rotate-vertical-left pt-3" src="<?=$URL_ROOT?>assets/img/all/arrow.svg">
              </figure>
              <figure class="flex-row text-center align-self-center">
                <img src="<?=$URL_ROOT?>assets/img/challenge/icono-data-source-lines.svg" />
                <img class="ml-md-2 mr-md-2 mb-3" src="<?=$URL_ROOT?>assets/img/challenge/icono-dots.svg" width="30" />
                <img src="<?=$URL_ROOT?>assets/img/challenge/icono-data-source-lines.svg" />
              </figure>
              <figure>
                <img src="<?=$URL_ROOT?>assets/img/challenge/iconos-energias.svg" />
              </figure>
                <div class="data-owners">
                  <a class="btn-data-owners shadow" href="#"><?=__("Data Owners");?></a>
                  <div class="data-owners1 text-center">
                    <p class="pt-md-1"><?=__("Owners developing power assets");?></p>
                  </div>
                  <div class="data-owners2 text-center">
                    <p class="pt-md-1"><?=__("OEMs (Tier 1), EPC subcontractors, O&M services");?></p>
                  </div>
                </div>
            </div>
            <div class="col-md-4 text-center fadeInDown">
                <div class="cloud-data">
                  <div class="bg-white-round shadow">
                    <p><?=__("DATA<br />PROVIDER");?></p>
                  </div>
                </div>
            </div>
            <div class="fadeInDown col-md-4 mt-4 users text-left d-flex flex-column">
              <div class="bg-corporate2 shadow max-width-2 mb-2 pt-2 pl-2 pr-2 pb-1 text-center">
                <p class="data text-white"><?=__("Conectors & interfaces");?></p>
              </div>
              <figure class="text-center">
                <img src="<?=$URL_ROOT?>assets/img/challenge/icono-laptops.svg">
              </figure>
                <div class="data-users">
                  <a class="btn-data-users shadow" href="#"><?=__("Data Users");?></a>
                  <div class="data-users1 text-center">
                    <p class="pt-md-1 align-self-center"><?=__("Systems and components manufacturers (Tiers 2 and 3), Engineering firms");?></p>
                  </div>
                  <div class="data-users2 text-center">
                    <p class="pt-md-1 align-self-center"><?=__("ICT businesses: software, sensors, communications, Big Data, data analysis, AI, etc.");?></p>
                  </div>
                  <div class="data-users3 text-center">
                    <p class="pt-md-1 align-self-center"><?=__("Academia: research centres, universities, test labs, etc.");?></p>
                  </div>
                </div>
            </div>
          </div>
      </div>
    </div>
      <div class="container">
      <article class="banner mt-5 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/challenge/banner.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("LEARN MORE ABOUT THE PROJECT");?></h3>
          <p class="text-white"><?=__("See the project phases and the role played by each participant");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/" class="btn btn-corporate1 shadow"><?=__("THE PROJECT");?></a>
      </article>
    </div>
  </main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>