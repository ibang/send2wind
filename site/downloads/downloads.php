<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
//addToLog('SERVER DESCARGAS: '.print_r($_SERVER, true));
require_once("../../site/php/inc-downloads.php");
$title=__("Downloads | Daekin");
$description=__("Here you will find all the documentation about Daekin.");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
  $js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="descargas" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?" ".$langURL:"")?> interior">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<main>
  <div class="full-container">

      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/downloads/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=$txt->downloads->title?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>

  <div class="container">
    <div class="row">
      <div class="col-md-6 mt-3 mb-2">
        <p><?=$txt->downloads->message?></p>
      </div>
      <div class="col-md-6 mt-3 mb-2">
        <div class="buscador text-right d-flex justify-content-end">
          <form action="<?=$URL_ROOT_BASE?>/downloads/" method="post" id="form-search" name="form-search" >
            <fieldset>
              <div class="form-inline">
                <input class="form-control mb-0 mb-md-2 mr-1 inputBuscador" id="inlineFormInput" placeholder="<?=$txt->downloads->searchdoc?>" name="downloadSearch" value="<?=$downloadSearch;?>" ><button type="submit" class="btn btn-corporate1 mb-2 pt-1 pb-1"><?=$txt->downloads->search?></button>
    			
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>

    <!-- Listado descargas -->

    <div class="row">
      <div class="col-md shadow bg-gray p-2 mb-2 mr-2">
        <div class="producto pt-1">
          <p class="small text-uppercase"><?=__("Downloads");?></p>
          <p class="mb-1"><?=__("Lorem ipsum dolor sit amet");?></p>
          <p class="small mb-2"><?=__("PDF - 217KB");?></p>
          <div class="product-list-actions">
            <a class="btn-descarga" href="<?=$URL_ROOT_BASE?>/downloads/<?=$current['slug'];?>.<?=$current['fileformat1'];?>" role="button"><?=__("Download");?></a>
          </div>
        </div>
      </div>

      <div class="col-md shadow bg-gray p-2 mb-2 mr-2">
        <div class="producto pt-1">
          <p class="small text-uppercase"><?=__("Downloads");?></p>
          <p class="mb-1"><?=__("Lorem ipsum dolor sit amet");?></p>
          <p class="small mb-2"><?=__("PDF - 217KB");?></p>
          <div class="product-list-actions">
            <a class="btn-descarga" href="<?=$URL_ROOT_BASE?>/downloads/<?=$current['slug'];?>.<?=$current['fileformat1'];?>" role="button"><?=__("Download");?></a>
          </div>
        </div>
      </div>

      <div class="col-md shadow bg-gray p-2 mb-2 mr-2">
        <div class="producto pt-1">
          <p class="small text-uppercase"><?=__("Downloads");?></p>
          <p class="mb-1"><?=__("Lorem ipsum dolor sit amet");?></p>
          <p class="small mb-2"><?=__("PDF - 217KB");?></p>
          <div class="product-list-actions">
            <a class="btn-descarga" href="<?=$URL_ROOT_BASE?>/downloads/<?=$current['slug'];?>.<?=$current['fileformat1'];?>" role="button"><?=__("Download");?></a>
          </div>
        </div>
      </div>

      <div class="col-md shadow bg-gray p-2 mb-2 mr-2">
        <div class="producto pt-1">
          <p class="small text-uppercase"><?=__("Downloads");?></p>
          <p class="mb-1"><?=__("Lorem ipsum dolor sit amet");?></p>
          <p class="small mb-2"><?=__("PDF - 217KB");?></p>
          <div class="product-list-actions">
            <a class="btn-descarga" href="<?=$URL_ROOT_BASE?>/downloads/<?=$current['slug'];?>.<?=$current['fileformat1'];?>" role="button"><?=__("Download");?></a>
          </div>
        </div>
      </div>

      <div class="col-md shadow bg-gray p-2 mb-2 mr-2">
        <div class="producto pt-1">
          <p class="small text-uppercase"><?=__("Downloads");?></p>
          <p class="mb-1"><?=__("Lorem ipsum dolor sit amet");?></p>
          <p class="small mb-2"><?=__("PDF - 217KB");?></p>
          <div class="product-list-actions">
            <a class="btn-descarga" href="<?=$URL_ROOT_BASE?>/downloads/<?=$current['slug'];?>.<?=$current['fileformat1'];?>" role="button"><?=__("Download");?></a>
          </div>
        </div>
      </div>
    </div>

    <!-- / Listado descargas -->

<?if (!empty($donwloads_list)){?>	
    <div class="product-list mb-2">
		<div class="row">
		<?$icn=0;foreach ($donwloads_list as $current){$icn++;?>
			<div class="col-md shadow bg-gray p-2 mb-2  mr-2">
			  <div class="producto pt-1">
				<p class="small text-uppercase"><?=$current['category'];?></p>
				<h3 class="mb-1"><?=$current['headline'];?></h3>
				<p class="text-uppercase mb-2"><?=$current['fileformat1'];?> - <?=formatSizeUnits($current['filesize1']);?></p>
				<div class="product-list-actions">
				  <a class="btn btn-corporate1 arrow-down" href="<?=$URL_ROOT_BASE?>/downloads/<?=$current['slug'];?>.<?=$current['fileformat1'];?>" role="button"><?=__("Download");?></a>
				</div>
			  </div>
			</div>
			<?if (!($icn%4) AND ($icn < $front_downloads_length)){?>
		</div><!-- /row -->
		<div class="row">
			<?}?>
		<?}?>
		</div><!-- /row -->
    </div><!-- /product-list -->
<?}?>
  </div><!-- /container -->

</main>
    
	
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>