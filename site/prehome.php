<?
$error = 'prehome';
if (!isset($PRE)){
	$PRE = '../';
}
require_once($PRE."php/init.php");
$title=$txt->home->title;
$description=$txt->home->description;
$keywords=$txt->home->keywords;
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html id="prehome" class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html id="prehome" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html id="prehome" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html id="prehome" class="no-js" lang="<?=$language?>"><!--<![endif]-->
<head>
<?require("{$DOC_ROOT}site/includes/widget-head-tag-manager.php")?>
<script>
  window['GoogleAnalyticsObject'] = 'ga';
  window['ga'] = window['ga'] || function() {
    (window['ga'].q = window['ga'].q || []).push(arguments)
  };
</script>	
<meta charset="utf-8">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-mobile-web-app-title" content="<?=$txt->logo?>">

<meta name="robots" content="noindex,follow" />

<!--<meta name="google-site-verification" content="xxx" />-->
	
<title><?=$title?></title>
<meta name="description" content="<?=$description?>" />
<meta name="keywords" content="<?=$keywords?>" />

<link rel="shortcut icon" href="<?=$URL_ROOT?>assets/ico/favicon.ico" />
<link rel="apple-touch-icon" href="<?=$URL_ROOT?>assets/ico/apple-touch-icon.png" />

<link rel="author" href="<?=$URL_ROOT?>humans.txt" />

<link rel="alternate" href="<?=HTTPHOST.$URL_ROOT;?>change-country/" hreflang="x-default" />

<? if (LESSORCSS=='CSS'){?>
<link href="<?=$URL_ROOT?>assets/css/screen.css?v=<?=CFGCURRENTVERSION;?>" rel="stylesheet">
<? }?>

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="<?=$URL_ROOT?>assets/js/modernizr.min.js"></script>

<? if (LESSORCSS=='LESS'){?>
<link rel="stylesheet/less" href="<?=$URL_ROOT?>assets/less/bootstrap.less" type="text/css" />
<script>less = { env: 'development' };</script>
<script src="<?=$URL_ROOT?>assets/js/less.min.js"></script>
<? }?>
</head>

<body>
<?require("{$DOC_ROOT}site/includes/widget-body-tag-manager.php")?>
<header>
	<div class="navbar navbar-default full-container clearfix" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<h1 id="logo" class="navbar-brand"><a href="<?=$URL_ROOT_BASE?>/"><span><?=$txt->logo?></span></a></h1>
			</div>
		</div>
	</div>
</header>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="item">
					<div class="col-md-3">
						<h2>Africa</h2>
						<ul>
							<li><a href="<?=$URL_ROOT?>en-ao/" rel="nofollow"><?=$db->getCountryName('ao');?></a></li>
							<li><a href="<?=$URL_ROOT?>fr-dz/" rel="nofollow"><?=$db->getCountryName('dz');?></a></li>
							<li><?=$db->getCountryName('cm');?> <a class="language" href="<?=$URL_ROOT?>en-cm/" rel="nofollow">en</a> <a class="language" href="<?=$URL_ROOT?>fr-cm/" rel="nofollow">fr</a></li>
							<li><a href="<?=$URL_ROOT?>en-eg/" rel="nofollow"><?=$db->getCountryName('eg');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-gq/" rel="nofollow"><?=$db->getCountryName('gq');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-gh/" rel="nofollow"><?=$db->getCountryName('gh');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-ke/" rel="nofollow"><?=$db->getCountryName('ke');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-ly/" rel="nofollow"><?=$db->getCountryName('ly');?></a></li>
							<li><?=$db->getCountryName('mu');?> <a class="language" href="<?=$URL_ROOT?>en-mu/" rel="nofollow">en</a> <a class="language" href="<?=$URL_ROOT?>fr-mu/" rel="nofollow">fr</a></li>
							<li><a href="<?=$URL_ROOT?>fr-ma/" rel="nofollow"><?=$db->getCountryName('ma');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-za/" rel="nofollow"><?=$db->getCountryName('za');?></a></li>
							<li><a href="<?=$URL_ROOT?>fr-tn/" rel="nofollow"><?=$db->getCountryName('tn');?></a></li>
						</ul>
						<h2>Asia</h2>
						<ul>
							<li><a href="<?=$URL_ROOT?>en-in/" rel="nofollow"><?=$db->getCountryName('in');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-id/" rel="nofollow"><?=$db->getCountryName('id');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-my/" rel="nofollow"><?=$db->getCountryName('my');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-mm/" rel="nofollow"><?=$db->getCountryName('mm');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-ph/" rel="nofollow"><?=$db->getCountryName('ph');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-sg/" rel="nofollow"><?=$db->getCountryName('sg');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-kr/" rel="nofollow"><?=$db->getCountryName('kr');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-th/" rel="nofollow"><?=$db->getCountryName('th');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-tw/" rel="nofollow"><?=$db->getCountryName('tw');?></a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h2>America</h2>
						<ul>
							<li><a href="<?=$URL_ROOT?>es-ar/" rel="nofollow"><?=$db->getCountryName('ar');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-bb/" rel="nofollow"><?=$db->getCountryName('bb');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-br/" rel="nofollow"><?=$db->getCountryName('br');?></a></li>
							<li><?=$db->getCountryName('ca');?> <a class="language" href="<?=$URL_ROOT?>en-ca/" rel="nofollow">en</a> <a class="language" href="<?=$URL_ROOT?>fr-ca/" rel="nofollow">fr</a></li>
							<li><a href="<?=$URL_ROOT?>es-cl/" rel="nofollow"><?=$db->getCountryName('cl');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-co/" rel="nofollow"><?=$db->getCountryName('co');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-cr/" rel="nofollow"><?=$db->getCountryName('cr');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-cu/" rel="nofollow"><?=$db->getCountryName('cu');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-ec/" rel="nofollow"><?=$db->getCountryName('ec');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-sv/" rel="nofollow"><?=$db->getCountryName('sv');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-gt/" rel="nofollow"><?=$db->getCountryName('gt');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-hn/" rel="nofollow"><?=$db->getCountryName('hn');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-mx/" rel="nofollow"><?=$db->getCountryName('mx');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-pa/" rel="nofollow"><?=$db->getCountryName('pa');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-pe/" rel="nofollow"><?=$db->getCountryName('pe');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-uy/" rel="nofollow"><?=$db->getCountryName('uy');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-us/" rel="nofollow"><?=$db->getCountryName('us');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-ve/" rel="nofollow"><?=$db->getCountryName('ve');?></a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h2>Europe</h2>
						<ul>
							<li><a href="<?=$URL_ROOT?>es-ad/" rel="nofollow"><?=$db->getCountryName('ad');?></a></li>
							<li><?=$db->getCountryName('be');?> <a class="language" href="<?=$URL_ROOT?>en-be/" rel="nofollow">en</a> <a class="language" href="<?=$URL_ROOT?>fr-be/" rel="nofollow">fr</a></li>
							<li><a href="<?=$URL_ROOT?>en-bg/" rel="nofollow"><?=$db->getCountryName('bg');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-de/" rel="nofollow"><?=$db->getCountryName('de');?></a></li>
							<li><a href="<?=$URL_ROOT?>es-es/" rel="nofollow"><?=$db->getCountryName('es');?></a></li>
							<li><a href="<?=$URL_ROOT?>fr-fr/" rel="nofollow"><?=$db->getCountryName('fr');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-gr/" rel="nofollow"><?=$db->getCountryName('gr');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-ie/" rel="nofollow"><?=$db->getCountryName('ie');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-il/" rel="nofollow"><?=$db->getCountryName('il');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-it/" rel="nofollow"><?=$db->getCountryName('it');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-kz/" rel="nofollow"><?=$db->getCountryName('kz');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-nl/" rel="nofollow"><?=$db->getCountryName('nl');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-no/" rel="nofollow"><?=$db->getCountryName('no');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-pl/" rel="nofollow"><?=$db->getCountryName('pl');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-pt/" rel="nofollow"><?=$db->getCountryName('pt');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-ro/" rel="nofollow"><?=$db->getCountryName('ro');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-sk/" rel="nofollow"><?=$db->getCountryName('sk');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-tr/" rel="nofollow"><?=$db->getCountryName('tr');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-ua/" rel="nofollow"><?=$db->getCountryName('ua');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-gb/" rel="nofollow"><?=$db->getCountryName('gb');?></a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h2>Oceania</h2>
						<ul>
							<li><a href="<?=$URL_ROOT?>en-au/" rel="nofollow"><?=$db->getCountryName('au');?></a></li>
						</ul>
						<h2>Middle East</h2>
						<ul>
							<li><a href="<?=$URL_ROOT?>en-iq/" rel="nofollow"><?=$db->getCountryName('iq');?></a></li>
							<li><?=$db->getCountryName('lb');?> <a class="language" href="<?=$URL_ROOT?>en-lb/" rel="nofollow">en</a> <a class="language" href="<?=$URL_ROOT?>fr-lb/" rel="nofollow">fr</a></li>
							<li><a href="<?=$URL_ROOT?>en-om/" rel="nofollow"><?=$db->getCountryName('om');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-qa/" rel="nofollow"><?=$db->getCountryName('qa');?></a></li>
							<li><a href="<?=$URL_ROOT?>en-sa/" rel="nofollow"><?=$db->getCountryName('sa');?></a></li>
							<li><?=$db->getCountryName('sy');?> <a class="language" href="<?=$URL_ROOT?>en-sy/" rel="nofollow">en</a> <a class="language" href="<?=$URL_ROOT?>fr-sy/" rel="nofollow">fr</a></li>
							<li><a href="<?=$URL_ROOT?>en-ae/" rel="nofollow"><?=$db->getCountryName('ae');?></a></li>
						</ul>
						<h2>Others</h2>
						<ul>
							<li><a href="<?=$URL_ROOT?>en/">English</a></li>
							<li><a href="<?=$URL_ROOT?>es/">Español</a></li>
							<li><a href="<?=$URL_ROOT?>fr/">Français</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="container contact">
		<div class="row">
			<div class="col-md-12">
				<p class="copy"><?=sprintf($txt->footer->copy, date('Y'));?></p>
			</div>
		</div>
	</div>
</footer>
<script src="<?=$URL_ROOT?>assets/js/jquery-2.1.3.min.js"></script>
<script src="<?=$URL_ROOT?>assets/js/bootstrap.min.js"></script>
<script type='application/ld+json'>
{
  "@context" : "http://schema.org", 
  "@type" : "LocalBusiness",
  "url" : "<?=MAINHOST;?>",
  "name" : "Daekin",
  "logo" : "<?=MAINHOST;?>/assets/img/all/logo-daekin.png",
  "sameAs" : [ "https://www.linkedin.com/company/xxxx",
      "https://www.youtube.com/channel/xxxx"],
  "telephone" : "(+34) XXX XXX XXX",
  "email" : "info@daekin.es",
  "address" :  { 
    "@type" : "PostalAddress",
    "streetAddress" : "Ctra. Madrid-Irún, Km 415",
    "addressLocality" : "Idiazabal",
    "addressRegion" : "Gipuzkoa",
    "postalCode" : "20213",
    "addressCountry" : "Spain"
  } 
}
</script>
</body>
</html>