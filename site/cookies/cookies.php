<?
require_once("../../php/init.php");
$title=$txt->cookies->title;
$description=$txt->cookies->description;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="cookies" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior soloTexto">
<?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
	<div class="container mt-2 mt-lg-5">
		<section class="info">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<article class="infoProducto">
						<h1><?=$txt->cookies->h1?></h1>
						<?=$txt->cookies->text?>
					</article>
				</div>
			</div>
		</section>
	</div>
</main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>