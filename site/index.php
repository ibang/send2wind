<?
require_once("../php/init.php");
require_once("../site/php/inc-functions.php");
require_once("../site/php/inc-index.php");
$title=_("Home - SEND2WIND");
$description=_("SEND2WIND development of innovative and integral solutions for foundations, towers and auxiliary systems of high-power offshore wind turbines");
$js[]="ekko-lightbox.min.js";
$js[]="lightbox.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="home" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
<?require("{$DOC_ROOT}site/includes/header.php")?>
    <main role="main">
      <div class="full-container">
        <div class="container">
          <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure mb-sm-0">
                    <img src="<?=$URL_ROOT?>assets/img/home/hero.jpg" class="">
                    <?php /*<figcaption class="figure-caption"><?=_("source: HINE");?></figcaption>*/?>
                  </figure>
                  <div class="text-box pr-1">
                    <h1 class="pl-1 pl-lg-5"><strong>SEND2WIND</strong><br />Energia Datuak Konpartitzeko Ekimena<span class="separador"><?=_("<span class='blue'>SECURE DIGITAL BUSINESS</span> COLLABORATION MAINTAINING DATA SOVEREIGNTY");?></span></h1>
                    <p class="contact"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->challenge->url?>/" class=" pl-1 pl-md-5"><?=_("SEND2WIND'S CHALLENGE");?></a></p>
                    <p class="scroll data"><?=_("Scroll");?></p>
                  </div>
                </div>
            </div>
            <div class="banda-hero mt-0 pt-2 pb-2 pl-2 pr-2 pl-md-4 pr-md-4">
              <div class="row">
                <div class="col-lg-6 align-self-center">
                  <p><?=_("SEND2WIND (Datuak Konpartitzeko Ekimena) is a Type I ELKARTEK project, funded by the Basque Government's 2020 call.");?></p>
                </div>
                <div class="col-lg-2 text-center">
                  <img src="<?=$URL_ROOT?>assets/img/all/logo-gobierno-vasco.png" alt="<?=_("Eusko Jaurlaritza - Gobierno Vasco");?>">
                </div>
                <div class="col-lg-3 align-self-center">
                  <p class="small"><?=_("Project funded by the Basque Government's Department for  Economic Development, Sustainability and the Environment (ELKARTEK Programme).");?></p>
                </div>
              </div>
            </div>
          </article>
      </div>
    </div>

    <div class="full-container bloques show-on-scroll">
      <div class="container pt-3 pb-3 pt-md-4 pt-lg-5 pb-md-3">
        <article class="pl-1 pr-1 pl-md-3 pr-md-3">
          <div class="row">
            <div class="col-lg-6 pl-md-3 align-self-center">
              <h2 class=""><?=_("THE PROJECT");?></h2>
              <p class="introDestacado p-0"><?=_("DEPARTMENT FOR ECONOMIC DEVELOPMENT, SUSTAINABILITY AND THE ENVIRONMENT ELKARTEK 2020 PROGRAMME.");?></p>
           </div>
           <div class="col-lg-6 pl-0 pr-0 text-right">
              <img src="<?=$URL_ROOT?>assets/img/home/foto-proyecto.jpg" class="relative-index-5" />
           </div>
          </div>
          <div class="row bg-corporate1 margin-minus-6">
            <div class="col-lg-6 pl-2 pt-3 pl-md-3 pb-md-3 col-sm-12">
              <p class="introDestacado text-white"><?=_("THE PROJECT FOCUSES ON RESEARCH INTO METHODS, TECHNIQUES AND TECHNOLOGIES FOR SECURE DIGITAL DATA SHARING.");?></p>
              <p class="text-white mt-2 intro"><?=_("Its main aim is to <strong>investigate the technological advances needed to define a reference architecture for a data sharing platform</strong>, more specifically for the energy sector.");?></p>
            </div>
            <div class="col-lg-6 col-sm-12 text-right align-self-center">
              <div class="mt-2 mb-2 mt-md-2 mt-lg-4">
                <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/" class="btn btn-corporate1 shadow"><?=_("the project");?></a>
              </div>
            </div>
          </div>
        </article>
      </div>
    </div>



    <div id="activities-results" class="container pb-md-5 pl-md-3 pr-md-3 bloques show-on-scroll">

      <div class="pl-3 pl-md-5 pr-md-5 pt-md-4">
       <h2 class=""><?=_("ACTIVITIES AND OUTCOMES");?></h2>
       <p><?=_("Keep up to date with developments in the project.");?></p>
      </div>
      <div class="container">
        <div class="row slider">

          <div class="col-md-12 pb-3 pt-2">
            <div class="card">
              <img class="" src="<?=$URL_ROOT?>assets/img/news/img-actividad01.jpg" alt="">
              <div class="card-body">
                <h4 class="card-title"><?=_("Activity");?></h4>
                <p class="card-text"><?=_("El Cluster de Energía pone en marcha el Grupo de Trabajo de Compartición de Datos de Aerogeneradores");?></p>
                <a class="btn btn-corporate3"><?=_("Learn more");?></a>
              </div>
            </div>
          </div>

          <div class="col-md-12 pb-3 pt-2">
            <div class="card">
              <img class="" src="<?=$URL_ROOT?>assets/img/news/img-resultado01.jpg" alt="">
              <div class="card-body">
                <h4 class="card-title"><?=_("OUTCOME");?></h4>
                <p class="card-text"><?=_("Participación de 6 fabricantes de componentes del GT de Datos en las reuniones");?></p>
                <a class="btn btn-corporate3"><?=_("Learn more");?></a>
              </div>
            </div>
          </div>

          <div class="col-md-12 pb-3 pt-2">
            <div class="card">
              <img class="" src="<?=$URL_ROOT?>assets/img/news/img-actividad03.jpg" alt="">
              <div class="card-body">
                <h4 class="card-title"><?=_("Activity");?></h4>
                <p class="card-text"><?=_("Aprobación del Plan de Comunicación del proyecto SEND2WIND por parte del consorcio");?></p>
                <a class="btn btn-corporate3"><?=_("Learn more");?></a>
              </div>
            </div>
          </div>

          <div class="col-md-12 pb-3 pt-2">
            <div class="card">
              <img class="" src="<?=$URL_ROOT?>assets/img/news/img-actividad02.jpg" alt="">
              <div class="card-body">
                <h4 class="card-title"><?=_("Activity");?></h4>
                <p class="card-text"><?=_("Presentación de SEND2WIND en el Foro Tecnológico de Digitalización");?></p>
                <a class="btn btn-corporate3"><?=_("Learn more");?></a>
              </div>
            </div>
          </div>
        </div>
            <p class="mt-4 mb-3 text-center"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/" class="btn btn-corporate1 shadow"><?=_("See all");?></a></p>
      </div>
    </div>


    <div class="full-container bg-contenido-1">
          <div class="container pt-5 bloques show-on-scroll">
            <article class="project pb-1 pl-1 pr-1 pb-md-3 pl-md-3 pr-md-3">
              <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-4 ml-sm-2 ml-md-3 ml-lg-0 mb-4">
                  <h2><?=_("THE CONSORTIUM");?></h2>
                  <p><?=_("The following participants are involved in this project");?></p>
                  <a href="<?=$URL_ROOT_BASE?>/<?=$txt->consortium->url?>/" class="btn btn-corporate1 shadow"><?=_("THE CONSORTIUM");?></a>
                </div>
                <div class="col-lg-8">
                  <ul class="listIcos list-unstyled flex-wrap">
                    <li class="col-12 col-sm-12 col-md-6"><a href="https://www.tecnalia.com" target="_blank" title="Tecnalia" class="logo-box"><img src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg" width="220"><span><?=_("Technology Centre - Coordinator");?></span></a></li>
                    <li class="col-12 col-sm-12 col-md-6"><a href="https://www.glual.com/" target="_blank" title="Glual Hydraulics" class="logo-box"><img src="<?=$URL_ROOT?>assets/img/all/logo-glual-hidraulica.svg" class=""><span><?=_("Business R&D unit");?></span></a></li>
                    <li class="col-12 col-sm-12 col-md-6"><a href="https://www.ikerlan.es/" target="_blank" title="Ikerlan" class="logo-box"><img src="<?=$URL_ROOT?>assets/img/all/logo-ikerlan-brta.svg"><span><?=_("Technology Centre");?></span></a></li>
                    <li class="col-12 col-sm-12 col-md-6"><a href="https://hispavistalabs.com/" target="_blank" title="Hispavista Labs" class="logo-box"><img src="<?=$URL_ROOT?>assets/img/all/logo-hispavista-labs.png"><span><?=_("Business R&D unit");?></span></a></li>
                    <li class="col-12 col-sm-12 col-md-6"><a href="https://www.tekniker.es/es" target="_blank" title="Tekniker" class="logo-box"><img src="<?=$URL_ROOT?>assets/img/all/logo-tekniker.svg"><span><?=_("Technology Centre");?></span></a></li>
                    <li class="col-12 col-sm-12 col-md-6 mnt-1"><a href="http://www.clusterenergia.com/" target="_blank" title="Tekniker" class="logo-box"><img src="<?=$URL_ROOT?>assets/img/all/logo-cluster-energia.svg"><span><?=_("Supply/demand broker");?></span></a></li>
                  </ul>
                </div>
              </div>
            </article>
          </div>
    </div>


    <div class="container">
      <article class="banner mt-5 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/home/bg-contact.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=_("CONTACT SEND2WIND");?></h3>
          <p class="text-white"><?=_("Contact us for further information or if you wish to collaborate with us or send us your feedback.");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->contacto->url?>/" class="btn btn-corporate1 shadow"><?=_("Contact");?></a>
      </article>
    </div>
</main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>