<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-news.php");
$title=__("Noticias sobre")." ".$category_news[0]['category'];
$description= __("Toda la información sobre")." ".$category_news[0]['category'];
$keywords=$category_news[0]['category'];
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="news" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="full-container box">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<ol class="breadcrumb hidden-xs">
					<li><a href="<?=$URL_ROOT_BASE?>/"><?=__("Home");?></a></li>
					<li><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/"><?=__("News");?></a></li>
					<li class="active"><?=__("Category");?>: <strong><?=$category_news["0"]["category"];?></strong></li>
				</ol>
			</div>
			<div class="col-md-3">
				<p class="back text-right hidden-xs"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/"> <i class="fa fa-angle-left"></i> <?=__("Back to");?> <?=__("News");?></a></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h1><?=__("News about");?> <?=$category_news["0"]["category"];?></h1>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<?if(!empty($category_news)){?>
	<div class="row">
		<?foreach ($category_news as $ind=>$article){?>
		<div class="col-sm-6">
			<article class="item-news">
				<div class="object">
					<?if ($article["image1"]){?>
					<p class="photo"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/<?=$article["slug"]?>/"><img class="img-responsive" src="<?=$URL_ROOT?>uploads/news/<?=$article["image1"]?>" alt="<?=($article["alt-image1"]?htmlspecialchars(strip_tags($article["alt-image1"])):str_replace("\"","'", $article["headline"]))?>" /></a></p>
					<?}?>
					<div class="content">
						<p class="date"><?=parsedate($article["date"],$language);?></p>
						<h2 class="title"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/<?=$article["slug"]?>/"><?=$article["headline"]?></a></h2>
					</div>
				</div>
				<p class="summary hidden-xs"><?=$article["intro"]?> <a class="link" href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/<?=$article["slug"]?>/"><?=__("Leer más");?> <i class="fa fa-angle-right"></i></a></p>
			</article>
		</div>
			<?if(($ind%2)==1 AND $ind!=$lastArticleIndex){?>
	</div>
	<div class="row">
				<?}?>
			<?}?>
	</div>
	<?}?>
	<? if ($actual_page>1 or $actual_page<$total_pages){?>
	<div class="row">
		<div class="col-md-12">
			<nav>
				<ul class="pager">
					<? if ($actual_page>1){?><li class="previous"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/<?=$txt->news->category->url?>/<?=$article["category_slug"];?>/<?if (($actual_page-1)>1){echo ($actual_page-1)."/";};?>" role="button"><i class="fa fa-angle-left"></i> <?=__("Previous");?></a></li><?}?>
					<? if ($actual_page<$total_pages){?><li class="next"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/<?=$txt->news->category->url?>/<?=$article["category_slug"];?>/<?=($actual_page+1);?>/" role="button"><?=__("Next");?> <i class="fa fa-angle-right"></i></a></li><?}?>
				</ul>
			</nav>
		</div>
	</div>
	<?}?>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>