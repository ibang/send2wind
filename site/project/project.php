<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Project - DAEKIN");
$description=__("9 Basque companies have decided to join forces in the DAEKIN project to co-ordinate and find synergies in the development of new offshore solutions");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="project" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure mb-sm-0">
                    <img src="<?=$URL_ROOT?>assets/img/project/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("THE PROJECT");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
            <div class="banda-hero mt-0 pt-2 pb-2 pl-2 pr-2 pl-md-4 pr-md-4">
              <div class="row">
                <div class="col-lg-6 align-self-center">
                  <p><?=__("DAEKIN (Datuak Konpartitzeko Ekimena) is a Type I ELKARTEK project, funded by the Basque Government's 2020 call.");?></p>
                </div>
                <div class="col-lg-2 text-center">
                  <img src="<?=$URL_ROOT?>assets/img/all/logo-gobierno-vasco.png" alt="<?=__("Eusko Jaurlaritza - Gobierno Vasco");?>">
                </div>
                <div class="col-lg-3 align-self-center">
                  <p class="small"><?=__("Project funded by the Basque Government's Department for  Economic Development, Sustainability and the Environment (ELKARTEK Programme).");?></p>
                </div>
              </div>
            </div>
          </article>
      </div>
    </div>

    <!-- PAQUETES DE TRABAJO -->

    <div class="full-container">
      <div class="container">
        <article id="wp" class="mt-4 pl-1 pr-1 pl-md-3 pr-md-3">
          <div class="row bloques show-on-scroll">
            <div class="col-md-6 mb-4">
              <h2><?=__("WORK PACKAGES");?></h2>
              <p class="introDestacado pr-1 pr-lg-3"><?=__("LEARN MORE ABOUT DAEKIN'S WORK PACKAGES AND HOW WORK IS STRUCTURED ALONG THE PROJECT.");?></p>
            </div>
            <div class="col-md-12 col-lg-6 pl-0 pl-lg-4 pr-lg-4 text-right">
              <div class="bg-gray p-2 pl-md-3 text-left">
                <p class="data"><?=__("PROJECT LEADER");?></p>
                <figure>
                  <img src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg" width="250">
                </figure>
              </div>
            </div>
          </div>
          <div class="packageChart mt-3 bloques show-on-scroll flex-column">
            <div class="row order-1">
              <div class="col-md-6 offset-md-3 mr-ml">
                <a href="#wp1" class="scroll">
                  <div class="row bg-white shadow position-relative pt-1 text-center arrowDownAfter border pt-lg-2 pt-2 border-dark work-packs">
                      <img class="text-right logos col-lg-3 ml-2" src="<?=$URL_ROOT?>assets/img/all/logo-glual-hidraulica.svg" alt="Glual" width="92" height="32" />
                      <div class="col-lg-9 pl-2 pt-2 pr-2 pb-1 text-left">
                        <p><?=__("WP1. Analysis of sharing models and teamwork environments");?></p>
                      </div>
                  </div>
                </a>
              </div> <!-- /col-md-12 -->
            </div>
            <div class="row line1">
              <div class="col-md order-4">
                <a href="#wp4" class="scroll alig-self-center">
                  <div class="bg-white shadow arrow-right position-relative pl-2 pb-1 mb-2 mb-sm-0 pt-2 h-100 border border-workGreen work-packs">
                      <img class="ml-2" src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg" alt="Tecnalia" />
                      <div class="text-left pl-2 pt-2">
                        <p><?=__("<span class='workGreen'>WP4.</span> Data sharing with sovereignty - IDS approach");?></p>
                      </div>
                  </div>
                </a>
              </div> <!-- /col-md-3 -->
              <div class="col-md mr-ml mt-0 order-2">
                <a href="#wp2" class="scroll">
                  <div class="row bg-white shadow arrow-horizontal position-relative p-2 mt-2 mt-md-0 mb-2 mb-sm-0 pt-1 text-center border border-workBlue work-packs">
                      <img class="col-lg-3 text-right logos" src="<?=$URL_ROOT?>assets/img/all/logo-ikerlan-brta.svg" alt="Ikerlan" />
                      <div class="col-lg-12 text-left pl-lg-2 pt-2">
                        <p><?=__("<span class='workBlue'>WP2.</span> Reference architecture requirements and design");?></p>
                      </div>
                  </div>
                </a>
                <a href="#wp3" class="scroll">
                  <div class="row bg-white shadow arrow-horizontal arrow-vertical position-relative p-2 mt-2 mt-md-2 mb-2 mb-sm-0 pt-1 border border-workOrange work-packs">
                    <img class="col-lg-3 text-right logos" src="<?=$URL_ROOT?>assets/img/all/logo-ikerlan-brta.svg" alt="Ikerlan" />
                    <div class="col-lg-12 text-left pl-2 pt-2">
                      <p><?=__("<span class='workOrange'>WP3.</span> Data value and governance - datalake approach");?></p>
                    </div>
                  </div>
                </a>
              </div> <!-- /col-md-4 -->
              <div class="col-md w-100 order-5">
                <div class="bg-white shadow position-relative pl-2 mb-2 mb-sm-0 pt-2 h-100 arrow-mdTop border border-workPink work-packs">
                  <a href="#wp5" class="scroll">
                    <img class="" src="<?=$URL_ROOT?>assets/img/all/logo-tekniker.svg" alt="Tekniker" />
                    <div class="text-left p-2">
                      <p><?=__("<span class='workPink'>WP5.</span> Edge computing distributed processing");?></p>
                    </div>
                  </a>
                </div>
              </div> <!-- /col-md-3 -->
            </div><!-- /row -->
            <div class="row">
              <div class="col-lg-6 offset-md-3 mr-ml">
                <a href="#wp6" class="scroll">
                  <div class="row bg-white shadow position-relative mb-2 text-center arrowDownBefore pl-2 pb-1 pt-1 border border-workYellow work-packs">
                      <img class="col-lg-2 text-right ml-1 pt-lg-2 ml-lg-0 pb-lg-0" src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg" alt="Tecnalia" />
                      <div class="col-lg-9 pl-2 pt-2 pr-lg-2 text-left">
                        <p><?=__("<span class='workYellow'>WP6.</span> Reference architecture requirements and design");?></p>
                      </div>
                  </div>
                </a>
              </div> <!-- /col-md-12 -->
            </div><!-- /row -->
            <div class="row mt-md-2">
              <div class="col-lg-8 offset-md-2 mr-ml">
                <a href="#wp7" class="scroll work-packs">
                  <div class="row bg-white shadow position-relative pl-2 pb-1 pt-1 text-center alig-self-center border border-workDarkBlue work-packs align-items-center">
                      <img class="col-lg-3 text-right pb-0 pt-lg-2 pl-lg-2 pb-lg-2" src="<?=$URL_ROOT?>assets/img/all/logo-cluster-energia.svg" alt="Cluster Energía" />
                      <div class="col-lg-9 pl-2 pt-2 pr-lg-2 text-left">
                        <p><?=__("<span class='workDarkBlue'>WP7.</span> Communication and dissemination");?></p>
                      </div>
                  </div>
                </a>
              </div> <!-- /col-md-12 -->
            </div>
          </div>
        </article>
      </div>
    </div>

    <!-- CONSORCIO -->

    <div class="container pt-3 pb-3 pb-md-4 mt-3 mt-md-5 bloques show-on-scroll">
      <h2><?=__("CONSORTIUM");?></h2>
      <div class="columnas">
        <p><?=__("El proyecto está liderado por el centro tecnológico <a href='https://www.tecnalia.com/' target='_blank' class='black'>TECNALIA</a>, que además coordinará las actividades en torno al enfoque IDS en cuanto a la compartición del dato con soberanía, y a la aplicabilidad de los distintos enfoques en el caso de uso offshore. Otro de los centros tecnológicos, <a href='https://www.ikerlan.es/' target='_blank' class='black'>IKERLAN</a>, estará a cargo tanto de los requisitos y el diseño de la arquitectura de regencia como del enfoque Datalake. <a href='https://www.tekniker.es/' target='_blank' class='black'>TEKNIKER</a>, el tercer y último centro tecnológico, lidera la tarea relacionada con el Edge Computing.");?></p>
        <p><?=__("En cuanto a las unidades de I+D empresariales hay que destacar las dos participantes en el proyecto: <a href='https://hispavistalabs.com/' target='_blank' class='black'>HISPAVISTA LABS</a> y <a href='https://www.glual.com/es/ingenieria/glual_innova.html' target='_blank' class='black'>GLUAL INNOVA</a>. Esta última es líder del PT responsable del análisis de modelos de compartición y entornos de colaboración. Completa el consorcio la <a href='http://www.clusterenergia.com/' target='_blank' class='black'>ASOCIACIÓN CLUSTER DE ENERGIA</a> como agente de intermediación oferta/demanda. Este será el responsable de las actividades de comunicación y difusión de <span class='workMagenta'><strong>DAEKIN</strong>.</span>");?></p>
      </div>

      <div class="bg-contenido-3 pt-3 pt-md-4 pb-3 pb-md-4 mt-3">

        <div class="row d-flex justify-content-center">
          <div class="col-md-6 col-lg-2">
            <div class="text-center">
              <a href="https://www.tecnalia.com" target="_blank" title="Tecnalia" class="logo-box logo-box-small"><img src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg"><span><?=__("Technology Centre");?></span></a>
            </div>
          </div>
          <div class="col-md-6 col-lg-2">
            <div class="text-center">
              <a href="https://www.glual.com/" target="_blank" title="Glual Hydraulics" class="logo-box logo-box-small"><img src="<?=$URL_ROOT?>assets/img/all/logo-glual-hidraulica.svg" class=""><span><?=__("Business R&D unit");?></span></a>
            </div>
          </div>
        </div>

        <div class="row d-flex justify-content-center mn-6">
          <div class="col-md-6 col-lg-5">
            <div class="text-center">
              <a href="https://www.ikerlan.es/" target="_blank" title="Ikerlan" class="logo-box logo-box-small ml-0 mr-lg-4"><img src="<?=$URL_ROOT?>assets/img/all/logo-ikerlan-brta.svg"><span><?=__("Technology Centre");?></span></a>
            </div>
          </div>
          <div class="col-md-6 col-lg-5">
            <div class="text-center">
              <a href="https://hispavistalabs.com/" target="_blank" title="Hispavista Labs" class="logo-box logo-box-small ml-0 ml-lg-4"><img src="<?=$URL_ROOT?>assets/img/all/logo-hispavista-labs.png"><span><?=__("Business R&D unit");?></span></a>
            </div>
          </div>
        </div>

        <div class="row d-flex justify-content-center">
          <div class="col-md-6 col-lg-4">
            <div class="text-center">
              <a href="https://www.tekniker.es/es" target="_blank" title="Tekniker" class="logo-box logo-box-small mr-0 mr-lg-4"><img src="<?=$URL_ROOT?>assets/img/all/logo-tekniker.svg"><span><?=__("Technology Centre");?></span></a>
            </div>
          </div>


          <div class="col-md-6 col-lg-4">
            <div class="text-center">
              <a href="" target="_blank" title="Daekin" class="logo-box logo-box-small transparent mr-0 mr-lg-4 pb-lg-3"><img src="<?=$URL_ROOT?>assets/img/project/logo-daekin-round.png"></a>
            </div>
          </div>

          <div class="col-md-6 col-lg-4">
            <div class="text-center">
              <a href="http://www.clusterenergia.com/" target="_blank" title="Cluster Energía" class="logo-box logo-box-small ml-0 ml-lg-4"><img src="<?=$URL_ROOT?>assets/img/all/logo-cluster-energia.svg"><span><?=__("Brokerage agent");?></span></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="wp1"></div>

    <div class="full-container bg-gray pt-3 pt-md-4 pb-3 pb-md-4 mt-3 mt-md-5 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container pt-3">
        <h3 class="work mb-3 col-10 col-lg-7"><?=__("WP1. ANALYSIS OF SHARING MODELS AND TEAMWORK ENVIRONMENTS");?></h3>
        <div class="row">
          <div class="col-md-3">
            <h4 class="data"><?=__("WP LEADER");?>: GLUAL INNOVA</h4>
            <a href="https://www.glual.com/" target="_blank"><img class="mb-2 multiply grow" src="<?=$URL_ROOT?>assets/img/project/logo-glual.png" alt="Glual" /></a>
            <span class="small-2"><?=__("Contact");?></span>
            <p><?=__("Aitor Aguirre");?><br /><a href="tel:0034943157015">943 157 015</a><br /><a href="mailto:aguirre@glual.com">aguirre@glual.com</a></p>
          </div>
          <div class="col-md-7 offset-md-1">
            <p><?=__("This Work Package conducts a general analysis on the implications of sharing and data teamworking in business models, defining no specific domains.");?></p>
            <h5 class="pt-1"><?=__("This activity consists of:");?></h5>
            <ol class="listNumeric listBlack">
              <li><?=__("General analysis.");?></li>
              <li><?=__("Analysis of the specific wind power use case, determining the requirements and specifications needed for subsequent work packages (WP2, WP3, WP4, WP5).");?></li>
              <li><?=__("Finally, an analysis of current methods and cases that support the roll-out of the models.");?></li>
            </ol>
          </div>
        </div><!-- /.row -->
        <div class="row d-flex justify-content-center">
            <div class="col-12 col-lg-6 pt-4 pb-4 pt-lg-5 pb-lg-5 bg-ball-1 mr-negative-2">
              <p class="text-right pr-1 pl-1 mr-0 pl-lg-4 pr-lg-5 mr-lg-5"><?=__("New forms of data collaboration and business models are analysed.");?></p>
            </div>
            <div class="col-12 col-lg-6 pt-3 pb-4 pt-lg-5 pb-lg-5 bg-ball-2">
              <p class="text-center pr-0 pr-lg-4 pl-0 ml-0-0 pl-lg-5 ml-lg-5 arrow-bottom-vert position-relative"><?=__("It is focussed on a case study.");?></p>
              <p class="text-center pr-0 pt-2 pr-lg-4 pl-0 ml-0-0 pl-lg-5 ml-lg-5 pt-lg-3 position-relative"><?=__("OFFSHORE WIND POWER");?></p>
            </div>
            <div class="col-12 col-lg-6 pt-0 bg-ball-3 pt-lg-4 pb-lg-5 ml-negative-3 mt-negative-3">
              <p class="text-right pr-0 pl-0 pt-3 pl-lg-4 pr-lg-5 mr-lg-5 mt-lg-1"><?=__("The methods put into place are analysed.");?></p>
            </div>
          </div>
          <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>

    <div id="wp2"></div>
    
    <div class="container pt-lg-5 mt-lg-5 bloques show-on-scroll">
      <h3 class="work mb-3 col-10 col-lg-7 workBlue"><?=__("WP2. REFERENCE ARCHITECTURE REQUIREMENTS AND DESIGN");?></h3>
      <div class="row">
        <div class="col-md-3">
          <h4 class="data"><?=__("WP LEADER");?>: IKERLAN</h4>
          <a href="https://www.ikerlan.es/" target="_blank"><img class="mb-2 grow" src="<?=$URL_ROOT?>assets/img/all/logo-ikerlan.svg" alt="Ikerlan" width="130" /></a>
          <span class="small-2"><?=__("Contact");?></span>
          <p><?=__("Aizea Lojo");?><br /><a href="tel:0034943712400">943 712 400</a><br /><a href="mailto:alojo@ikerlan.es">alojo@ikerlan.es</a></p>
        </div>
        <div class="col-md-7 offset-md-1">
          <p><?=__("Work Package 2 will establish the design of an architecture which will make it possible to define the bases for research into data sharing with no loss of sovereignty, and concept testing for a series of development empowerment tools. The aim of this work package is to provide the basis architecture for WP3, WP4 and WP5, and gather the results obtained in a final architecture.");?></p>
          <h5 class="pt-1 workBlue"><?=__("Objectives:");?></h5>
          <ol class="list-arrow-workBlue pl-0">
            <li><?=__("To define the high-level architecture.");?></li>
            <li><?=__("To spell out the design for the reference architecture.");?></li>
            <li><?=__("To identify the tools and techniques required to verify the architecture and assess the Key Performance Indicators (KPI) that will be used in subsequent implementations of the architecture.");?></li>
          </ol>

          <ol class="listNumericBlue listBlue">
            <li>
              <h6 class="mb-1"><?=__("Requirements");?></h6>
              <p><?=__("Takes on the requirements for the models analysed in WP1.");?></p>
            </li>
            <li>
              <h6 class="mb-1"><?=__("Architecture design");?></h6>
              <p><?=__("The range of currently proposed design focuses, approaches and patterns are analysed:");?></p>

              <ul>
                <li><?=__("Centralised repositories");?><a class="arrowYellow" href="#wp3"><?=__("WP3");?></a></li>
                <li><?=__("Architecture proposed in IDS");?><a class="arrowGreen" href="#wp4"><?=__("WP4");?></a></li>
                <li><?=__("Secure distributed processing");?><a class="arrowPink" href="#wp5"><?=__("WP5");?></a></li>
              </ul>
            </li>
            <li>
              <h6 class="mb-1"><?=__("Verification");?></h6>
              <p><?=__("Indicators and risks are established, to be monitored and measured during the implementation of the architecture, providing a verification methodology.");?></p>
            </li>
          </ol>
        </div>
      </div><!-- /.row -->
    <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
    </div>


    <div id="wp3"></div>


    <div class="full-container bg-gray mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 workOrange"><?=__("WP3. DATA VALUE AND GOVERNANCE - DATALAKE APPROACH");?></h3>
        <div class="row">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: IKERLAN</h4>
              <a href="https://www.ikerlan.es/" target="_blank"><img class="mb-2 grow" src="<?=$URL_ROOT?>assets/img/all/logo-ikerlan.svg" alt="Ikerlan" width="130" /></a>
              <span class="small-2"><?=__("Contact");?></span>
              <p><?=__("Aizea Lojo");?><br /><a href="tel:0034943712400">943 712 400</a><br /><a href="mailto:alojo@ikerlan.es">alojo@ikerlan.es</a></p>
            </div>
            <div class="col-md-7 offset-md-1">
              <p><?=__("Work Package 3 aims to analyse the storage needs of data and metadata, both at infrastructure level and at data auditing and governance level. The aim of this work package is to design and develop a model to govern data and metadata obtained from a range of sources throughout their entire lifespan.");?></p>
              <p><?=__("It will also help to control dataset versions in order to construct incremental analytical data, and foster knowledge extraction by means of machine learning techniques based on metadata.");?></p>
              <h5 class="pt-1 workOrange"><?=__("Main aims:");?></h5>
              <ol class="list-arrow-workOrange pl-0">
                <li><?=__("To define data storage requirements.");?></li>
                <li><?=__("To define data security and governance requirements.");?></li>
                <li><?=__("To identify the technology needed for the efficient management of data and metadata throughout their lifespan.");?></li>
                <li><?=__("To develop a module entrusted with versioning datasets to generate analytical models.");?></li>
                <li><?=__("To develop a module to extract knowledge from metadata.");?></li>
              </ol>
            </div>
          </div><!-- /.row -->

          <div class="row mt-4">
            <div class="col-md-3">
              <p class="vertical workOrange mt-2"><?=__("DATALAKE APPROACH");?></p>
            </div>
            <div class="row">
              <figure class="col">
                <img src="<?=$URL_ROOT?>assets/img/project/grafico-wp3.svg" width="243" class="mt-2 mb-2 rounded" />
              </figure>
              <ul class="col listGraficoOrange mt-3">
                <li><span class="data"><?=__("DIFFERENT SOURCES OF DATA");?></span></li>
                <li><span class="data"><?=__("DATA ARE STORED DIRECTLY, UNCLASSIFIED");?></span></li>
                <li class="mt-lg-5"><span class="data"><?=__("DATA ARE SELECTED AND CLASSIFIED AS NEEDED");?></span></li>
              </ul>
            </div>
          </div><!-- /.row -->
        <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
      </div>
    </div>


    <div id="wp4"></div>


    <div class="full-container mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 workGreen"><?=__("WP4. DATA SHARING WITH SOVEREIGNTY - IDS APPROACH");?></h3>
          <div class="row">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: TECNALIA</h4>
              <a href="https://www.tecnalia.com/" target="_blank"><img class="mb-2 grow" src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg" alt="Tecnalia" /></a>
              <span class="small-2"><?=__("Contact");?></span>
              <p><?=__("Ana Isabel Torre Bastida");?><br /><a href="tel:0034902760000">902 760 000</a><br /><a href="mailto:isabel.torre@tecnalia.com">isabel.torre@tecnalia.com</a></p>
            </div>
            <div class="col-md-7 offset-md-1">
              <p><?=__("Work Package 4 seeks to analyse assisted data sharing by means of use policies, forestalling against sovereignty loss.");?></p>
              <h5 class="pt-1 workGreen"><?=__("Activities:");?></h5>
              <ol class="listNumericGreen pl-0">
                <li><?=__("The first activity will be research into the said use policies, with two main aims:");?>
                  <ul>
                    <li><?=__("To study the definition possibilities of the use policies and possible languages, focussing on designing a final use policy definition language.");?></li>
                    <li><?=__("To design a solution capable of controlling the use of data for the policies defined in the previous task, i.e. complying with use policies for distributed platforms.");?></li>
                  </ul>
                </li>
                <li><?=__("Another important task of this work package is to review the design of the components of the IDS architecture, with the aim of adapting them, improving them or detecting new component designs not taken into account heretofore, and which are defined for offshore wind power.");?></li>
                <li><?=__("Finally, the package will aim to integrate and validate new components.");?></li>
              </ol>
            </div>
          </div><!-- /.row -->
          <div class="container mt-4">
            <h4 class="data"><?=__("COMPONENT INTEGRATION AND VALIDATION");?></h4>
            <div class="row d-flex align-items-center mb-lg-0">
              <div class="col-md-12 col-lg-2 mr-lg-1 mb-1 position-relative">
                  <p class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowGreenRightAfter arrow-margen"><?=__("<strong>Data sharing policies</strong> are defined.");?></p>
              </div>
              <div class="col-lg-3 pr-lg-2 mb-1 position-relative">
                  <p class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowGreenRightAfter"><?=__("A solution capable of <strong>controlling data use</strong> for the defined policies is designed.");?></p>
              </div>
              <div class="col-lg-3 pr-lg-2 mb-1 position-relative">
                  <p class="bg-white shadow pt-2 pb-2 pl-3 pr-2 arrowGreenRightAfter arrowGreenBottomBefore"><?=__("The <strong>characteristics and limitations</strong> of the <strong>IDS</strong> architecture are studied.");?></p>
              </div>
              <div class="col-lg-3">
                  <p class="bg-white shadow pt-2 pb-2 pl-3 pr-3 mb-1"><?=__("A methodology is defined to allow for <strong>integration and validation of IDS components</strong> defined in advance, checking that they comply with the defined design and requirements.");?></p>
              </div>
            </div><!-- /.row -->
          </div>

          <div class="container">
            <div class="row d-flex align-items-center mt-negative-2">
              <div class="col-lg-3 mt-1 mb-1 position-relative">
                  <p class="bg-white shadow pt-2 pb-2 pl-3 pr-3"><?=__("The end result is <strong>adapting the architecture proposed by IDS</strong>, improving it to cover the requirements specified in WP2, and giving it a much more detailed design than currently supported by the original.");?></p>
              </div>
              <h4 class="data pl-1 pl-lg-2 pr-lg-2"><?=__("END RESULT");?></h4>
              <div class="col-lg-3 position-relative">
                  <p class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowGreenLeftBefore"><?=__("A reference design of the IDS architecture based on concept tests on the basic components and studying its suitability for the <strong>offshore use case</strong>.");?></p>
              </div>
            </div><!-- /.row -->
          </div>

          <div class="container mt-4">
            <div class="row">
              <div class="col-lg-3 order-1">
                <figure class="d-flex align-items-center">
                  <img src="<?=$URL_ROOT?>assets/img/all/ico-data.svg" alt="" width="35" />
                  <figcaption class="small pl-1"><?=__("DATASETs transferred from the supplier to the user");?></figcaption>
                </figure>

                <figure class="d-flex align-items-center">
                  <img src="<?=$URL_ROOT?>assets/img/all/ico-meta.svg" alt="" width="35" />
                  <figcaption class="small pl-1"><?=__("METADATA description of DATASETS/supplier/user");?></figcaption>
                </figure>

                <figure class="d-flex align-items-center">
                  <img src="<?=$URL_ROOT?>assets/img/all/ico-app.svg" alt="" width="35" />
                  <figcaption class="small pl-1"><?=__("APPLICATION for specific data manipulation");?></figcaption>
                </figure>

                <figure class="d-flex align-items-center">
                  <img src="<?=$URL_ROOT?>assets/img/project/arrow-datos-activos.svg" alt="" width="35" />
                  <figcaption class="small pl-1"><?=__("Data exchange ACTIVE");?></figcaption>
                </figure>

                <figure class="d-flex align-items-center">
                  <img src="<?=$URL_ROOT?>assets/img/project/arrow-datos-no-activos.svg" alt="" width="35" />
                  <figcaption class="small pl-1"><?=__("Data exchange INACTIVE");?></figcaption>
                </figure>

                <figure class="d-flex align-items-center">
                  <img src="<?=$URL_ROOT?>assets/img/project/arrow-meta-datos.svg" alt="" width="35" />
                  <figcaption class="small pl-1"><?=__("Metadata exchange");?></figcaption>
                </figure>

                <figure class="d-flex align-items-center">
                  <img src="<?=$URL_ROOT?>assets/img/project/arrow-descarga-aplicaciones.svg" alt="" width="35" />
                  <figcaption class="small pl-1"><?=__("Application download");?></figcaption>
                </figure>
              </div>
              <div class="col-lg-6 offset-md-2 order-2">
                <figure class="grafico">
                  <img class="mb-2" src="<?=$URL_ROOT?>assets/img/project/grafico-wp4.svg" alt="" />
                </figure>

              </div>
            </div><!-- /.row -->
          </div>
          <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>


    <div id="wp5"></div>


    <div class="full-container bg-gray mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 workPink"><?=__("WP5. EDGE COMPUTING DISTRIBUTED PROCESSING");?></h3>
          <div class="row">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: TEKNIKER</h4>
              <a href="https://www.tekniker.es/" target="_blank"><img class="mb-2 grow" src="<?=$URL_ROOT?>assets/img/all/logo-tekniker-2.svg" alt="Tekniker" width="140" /></a>
              <span class="small-2"><?=__("Contact");?></span>
              <p><?=__("Aitor Arnaiz");?><br /><a href="tel:0034943206744">943 206 744</a><br /><a href="mailto:aitor.arnaiz@tekniker.es">aitor.arnaiz@tekniker.es</a></p>
            </div>
            <div class="col-md-7 offset-md-1">
              <p><?=__("Work Package 5 will analyse <strong>two possibilities for enabling data from the Edge environment towards the outside</strong>: distributed and federated focus. On the one hand, in the distributed approach, this will allow for the validation of a secure, reliable data exchange demo, and, on the other, in the federated approach, the implementation of FML and Blockchain techniques.");?></p>
              <div class="d-flex">
                <div class="col-lg-6 pl-0 pr-lg-3">
                  <h5 class="pt-1 pl-0 upper-case workPink"><?=__("DISTRIBUTED APPROACH");?></h5>
                  <p class="p-300"><?=__("The aim is to analyse the characteristics and limits of the Edge environment and the scope of IDS approaches with regard to connectors. Based on the findings, it will seek to implement and validate data exchange using an IDS connector on the Edge. Finally, a data sovereignty solution will be implemented and validated, to guarantee secure, reliable data sharing.");?></p>
                </div>
                <div class="col-lg-6 pl-0">
                  <h5 class="pt-1 upper-case workPink"><?=__("FEDERATED APPROACH");?></h5>
                  <p class="p-300"><?=__("The aim is to analyse and ascertain the viability of implementing FML, Blockchain and cryptographic techniques in use cases where data sharing is not viable but agents wish to work together to obtain greater insight, without compromising their respective data.");?></p>
                </div>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="container mt-4">
            <h5 class="upper-case workPink"><?=__("EDGE ENVIRONMENT");?></h4>
            <div class="row d-column align-items-center mb-lg-2">
              <div class="col-lg-4 mt-1 mb-1">
                <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3">
                  <h6 class="data"><?=__("EDGE COMPUTING");?></h6>
                  <p><?=__("Edge computing is a paradigm of distributed computing that <strong>transmits computing and data storage to where they are needed</strong>, improving response times and saving bandwidth.");?></p>
                </div>
                <div class="bg-white shadow mt-2 pt-2 pb-2 pl-3 pr-3">
                  <p><?=__("Data sharing in <strong>Edge environments</strong> involves <strong>challenges around integrating</strong> data from different sources and sharing them with other agents to create new business models.");?></p>
                </div>
              </div>
              <div class="col-lg-5 offset-md-2 pb-lg-2">
                <figure class="grafico">
                  <img class="mb-2" src="<?=$URL_ROOT?>assets/img/project/grafico-wp5.svg" alt="" />
                </figure>
              </div>
            </div><!-- /.row -->
          </div>
          <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>


    <div id="wp6"></div>


    <div class="full-container mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 workYellow"><?=__("WP6. REFERENCE ARCHITECTURE REQUIREMENTS AND DESIGN");?></h3>
          <div class="row">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: TECNALIA</h4>
              <a href="https://www.tecnalia.com/" target="_blank"><img class="mb-2 grow" src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg" alt="Tecnalia" /></a>
              <span class="small-2"><?=__("Contact");?></span>
              <p><?=__("Ana Isabel Torre Bastida");?><br /><a href="tel:0034902760000">902 760 000</a><br /><a href="mailto:isabel.torre@tecnalia.com">isabel.torre@tecnalia.com</a></p>
            </div>
            <div class="col-md-7 offset-md-1">
              <p><?=__("Work Package 6 is a cross-cutting package where the developments from the previous work packages are put towards defining the reference architecture for a specific use case in offshore wind power.");?></p>
                <h5 class="pt-1 pl-0 workYellow"><?=__("Ultimate goal:");?></h5>
                <p class="p-arrow"><?=__("The ultimate goal is to use the platform as a <strong>data sharing</strong> environment among businesses along the offshore wind power value chain, becoming a <strong>benchmark at European level</strong> with the capacity to integrate into the GAIA-X single data space being promoted by the European Commission.");?></p>
            </div>
          </div><!-- /.row -->
          <div class="container mt-4">
            <div class="row d-column align-items-center mb-lg-2">
              <div class="col-lg-4 mt-1 mb-1">
                <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowYellowRightAfter mr-lg-4">
                  <p><?=__("The WP goes in greater depth into the previous analysis of the <strong>requirements</strong> of offshore wind power, undertaking <strong>a more detailed study</strong>.");?></p>
                </div>
              </div>
              <div class="col-lg-4 mt-1 mb-1">
                <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowYellowRightAfter mr-lg-4">
                  <p><?=__("The requirements are integrated with the <strong>technologies developed</strong> by WP2, WP3, WP4 and WP5.");?></p>
                </div>
              </div>
              <div class="col-lg-4 mt-1 mb-1">
                <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowYellowBottomBefore">
                  <p><?=__("A design document is developed, specifying the components required for the specific reference architecture to be implemented in a <span class='data d-inline workYellow'>REAL PLATFORM.</span>");?></p>
                </div>
              </div>
              <div class="col-lg-4 mt-1 mb-1 offset-md-8">
                <div class="bg-yellow shadow pt-2 pb-2 pl-3 pr-3 mt-lg-4">
                  <p class="data"><?=__("OFFSHORE WIND POWER DATA DIGITAL PLATFORM");?></p>
                </div>
              </div>
            </div><!-- /.row -->
          </div>
          <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>


    <div id="wp7"></div>


    <div class="full-container bg-gray mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 workDarkBlue"><?=__("WP7. COMMUNICATION AND DISSEMINATION");?></h3>
          <div class="row">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: CLUSTER ENERGÍA</h4>
              <a href="http://www.clusterenergia.com/" target="_blank_"><img class="mb-2 grow" src="<?=$URL_ROOT?>assets/img/all/logo-cluster-energia.svg" alt="Clúster Energía - Basque Energy Cluster" width="190" /></a>
              <span class="small-2"><?=__("Contact");?></span>
              <p><?=__("José Ignacio Hormaeche");?><br /><a href="tel:00349442402114">944 240 211</a><br /><a href="mailto:jihormaeche@clusterenergia.com">jihormaeche@clusterenergia.com</a></p>
            </div>
            <div class="col-md-7 offset-md-1">
              <p><?=__("The main aim of Work Package 7 is to define and roll out the strategy for communicating the developments and outcomes of the project, maximising its visibility with regard to identified stakeholders and driving the transfer of the knowledge and technologies developed towards energy value chain businesses.");?></p>
            </div>
          </div><!-- /.row -->
          <div class="container mt-4">
            <h5 class="upper-case workDarkBlue"><?=__("ACTIVITIES");?></h5>
            <div class="row d-column align-items-center mb-lg-2">
              <div class="col-lg-3 mt-1 mb-1">
                <a href="#" class="grow contrast-1">
                  <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowDarkBlueRightAfter mr-lg-2 grow">
                    <h6 class="data workDarkBlue"><?=__("IDENTITY");?></h6>
                    <p><?=__("Creating a <strong>corporate identity</strong> for the project.");?></p>
                  </div>
                </a>
              </div>
              <div class="col-lg-3 mt-1 mb-1">
                <a href="#" class="grow contrast-1">
                  <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowDarkBlueRightAfter mr-lg-2 grow">
                    <h6 class="data workDarkBlue"><?=__("APPLICATIONS");?></h6>
                    <p><?=__("Developing a <strong>website</strong> and <strong>leaflets</strong>.");?></p>
                  </div>
                </a>
              </div>
              <div class="col-lg-3 mt-1 mb-1">
                <a href="#" class="grow contrast-1">
                  <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowDarkBlueBottomAfter mr-lg-2 grow">
                    <h6 class="data workDarkBlue"><?=__("COMMUNICATION");?></h6>
                    <p><?=__("Creating <strong>press release</strong> and <strong>articles</strong> for local and international media.");?></p>
                  </div>
                </a>
              </div>
              <div class="col-lg-3 mt-1 mb-1">
                <a href="#grupo_trabajo" class="grow scroll">
                  <div class="bg-darkblue shadow pt-2 pb-2 pl-3 pr-3 arrowDarkBlueBottomNone mr-lg-2 grow">
                    <p class="text-white"><?=__("Creating a new Work Group:");?></p>
                    <p class="data text-white"><?=__("Digitalising wind turbine components.");?></p>
                  </div>
                </a>
              </div>
            </div><!-- /.row -->

            <div class="row d-column align-items-center mb-lg-2">
              <div class="col-lg-3 mt-1 mb-1">
                <a href="#ferias" class="grow contrast-1 scroll">
                  <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowDarkBlueTopBefore mr-lg-2 grow">
                    <h6 class="data workDarkBlue"><?=__("DISSEMINATION");?></h6>
                    <p><?=__("Presence at international <strong>fairs and events</strong>.");?></p>
                  </div>
                </a>
              </div>
              <div class="col-lg-3 mt-1 mb-1">
                <a href="#" class="grow contrast-1 scroll">
                  <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowDarkBlueLeftBefore mr-lg-2 grow">
                    <h6 class="data workDarkBlue"><?=__("DISSEMINATION");?></h6>
                    <p><?=__("Organising <strong>sector workshops</strong> and <strong>seminars relating to</strong> the project.");?></p>
                  </div>
                </a>
              </div>
              <div class="col-lg-3 mt-1 mb-1">
                <a href="#" class="grow contrast-1">
                  <div class="bg-white shadow pt-2 pb-2 pl-3 pr-3 arrowDarkBlueBottomNone arrowDarkBlueLeftBefore mr-lg-2 grow">
                    <h6 class="data workDarkBlue"><?=__("DISSEMINATION");?></h6>
                    <p><?=__("Carrying out <strong>presentations</strong> at <strong>technical conferences</strong>.");?></p>
                  </div>
              </a>
              </div>
              <div class="col-lg-3 mt-1 mb-1">
                <a href="#" class="grow">
                  <div class="bg-darkblue shadow pt-2 pb-2 pl-3 pr-3 arrowDarkBlueBottomNone mr-lg-2 grow">
                    <p class="text-white"><?=__("Creating a new European-level data-sharing community:");?></p>
                    <p class="data text-white"><?=__("Energy Community");?></p>
                  </div>
                </a>
              </div>
            </div><!-- /.row -->
            <div id="ferias" class="mb-2 pb-2 mb-lg-5 pb-lg-3"></div>
          </div>

          <div class="container mt-5">
            <h5 class="upper-case workDarkBlue col-lg-6 pl-0"><?=__("Planned presence at international fairs and events");?></h5>
            <div class="row">
              <ol class="listNumericBlue listDarkBlue borderDarkBlue col-lg-6">
                <li>
                  <a href="#" class="row align-items-center">
                      <p><?=__("<span class='data'>Wind Europe Copenhagen 2021</span>Group stand at fair.");?></p>
                      <div class="ml-auto">
                        <img src="<?=$URL_ROOT?>assets/img/all/logo-wind-europe.svg" />
                      </div>
                  </a>
                </li>
                <li>
                  <a href="#" class="row align-items-center">
                      <p><?=__("<span class='data'>EES/Intersolar Munich 2021</span>Own stand at fair.");?></p>
                      <div class="ml-auto">
                        <img src="<?=$URL_ROOT?>assets/img/all/logo-intersolar.png" />
                      </div>
                  </a>
                </li>
                <li>
                  <a href="#" class="row align-items-center">
                    <p><?=__("<span class='data'>EnLit Europe 2021</span>Own stand at fair.");?></p>
                    <div class="ml-auto">
                      <img src="<?=$URL_ROOT?>assets/img/all/logo-enlit-europe.png" />
                    </div>
                  </a>
                </li>
              </ol>
              <div class="col-lg-5 mt-lg-3 text-center">
                <img src="<?=$URL_ROOT?>assets/img/all/foto-ferias.png" />
              </div>
            </div>
          <div id="grupo_trabajo" class="mb-2 pb-2 mb-lg-5 pb-lg-3"></div>
          </div>


          <div class="container mt-4">
            <h5 class="upper-case workDarkBlue col-lg-6 pl-0"><?=__("Wind Turbine Data Sharing Work Group");?></h5>
            <h6 class="intro workDarkBlue"><?=__("Aims of Work Group:");?></h6>
              <ol class="listNumericDarkBlue pl-0 col-lg-7">
                <li><?=__("To facilitate wind turbine component manufacturers (segment 3) access to the data required to operate wind farms, so that they can obtain value from them and improve their competitive offer.");?></li>
                <li><?=__("To compile, process and analyse the requirements and specifications of the data requested by wind turbine component manufacturers (Data Users).");?></li>
                <li><?=__("To identify the conditions and requirements of Data Owners (segments 1 & 2) for data sharing (data privacy, security, governance, compensation...) and offer solutions.");?></li>
                <li><?=__("To contribute to the Elkartek DAEKIN project in defining and validating a digital platform for sharing data from wind turbines in operational wind farms, offering confidence in data privacy and security and providing easy, systematic access to quality data to businesses all along the value chain.");?></li>
                <li><?=__("To identify collaborative projects for joint data use and analysis among companies in the wind value chain.");?></li>
              </ol>
          </div>

          <div class="container mt-4">
            <h5 class="upper-case workDarkBlue col-lg-6 pl-0"><?=__("EXPECTED RESULTS");?></h5>
              <ol class="listNumericDarkBlue pl-0 col-lg-7">
                <li><?=__("Specifications for the data requested by component manufacturers (Data users): quantity, frequency, quality, taxonomy, format, etc.");?></li>
                <li><?=__("Agreements with Data owners (wind farm operators, wind turbine manufacturers) for the sharing (cession/sale) of wind turbine data at component and/or subsystem level.");?></li>
                <li><?=__("Comparison and review of the specifications and architecture of the digital platform to be defined and developed as part of the DAEKIN project.");?></li>
                <li><?=__("Validation of the demo / minimum viable product of the digital platform.");?></li>
                <li><?=__("Proposed collaborative pilot projects on using and sharing accessible data.");?></li>
              </ol>

              <div class="row mt-4">
                <div class="col-lg-4 mb-3">
                  <h6 class="data workDarkBlue wp7"><?=__("MANUFACTURERS");?></h6>
                  <div class="logos-wp7">
                    <div class="mt-3">
                      <a href="https://antec-group.com/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-antec.png" alt="Antec" /></a>
                      <a href="https://www.erreka.com/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-erreka.png" alt="Erreka" /></a>
                      <a href="https://www.ingeteam.com/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-ingeteam.png" alt="Ingeteam" /></a>
                      <a href="https://www.glual.com/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-glual-hydraulics.png" alt="Glual" /></a>
                      <a href="https://www.ormazabal.com/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-ormazabal-velatia.png" alt="Ormazabal Velatia" /></a>
                      <a href="https://www.hinegroup.com/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-hine.png" alt="Hine" /></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 mb-3">
                  <h6 class="data workDarkBlue wp7"><?=__("DIGITALISATION<br />COMPANIES");?></h6>
                  <div class="row logos-wp7">
                    <div class="col mt-2">
                        <a href="https://www.ii40services.com//" target="_blank" class="d-block"><img class="grow" src="<?=$URL_ROOT?>assets/img/project/logo-ii40-services.png" alt="ii4.0 Services" /></a>
                        <a href="https://skootik.com/" target="_blank" class="d-block"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-skootik.png" alt="Skootik" /></a>
                        <a href="http://www.wimbitek.com/" target="_blank" ><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-wimbitek.png" alt="Wimbitek" /></a>
                    </div>
                    <div class="col mt-2 pr-lg-4">
                      <p class="data workDarkBlue"><?=__("co-ordinator");?></p>
                      <a href="https://xabet.net/" target="_blank" ><img class="w-60 grow" src="<?=$URL_ROOT?>assets/img/project/logo-xabet.png" alt="Xabet" /></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 mb-3">
                  <h6 class="data workDarkBlue wp7"><?=__("BASQUE NETWORK<br />AGENTS");?></h6>
                  <div class="logos-wp7">
                    <div class="mt-3">
                      <a href="https://www.ikerlan.es/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-ikerlan-2.png" alt="Ikerlan" /></a>
                      <a href="https://www.tekniker.es/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-tekniker-2.png" alt="Tekniker" /></a>
                      <a href="https://www.tecnalia.com/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-tecnalia-2.png" alt="Tecnalia" /></a>
                      <a href="https://www.mondragon.edu/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-mondragon.png" alt="Mondragon Unibertsitatea" /></a>
                      <a href="https://hispavistalabs.com/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/all/logo-hispavista-labs.png" alt="Hispavista Labs" /></a>
                      <a href="https://www.ceit.es/" target="_blank" class="float-left"><img class="w-100 grow" src="<?=$URL_ROOT?>assets/img/project/logo-ceit.png" alt="Ceit" /></a>
                    </div>
                  </div>
                </div>
              </div>
          </div>

          <div class="container actividadesTrabajo mt-4">
            <div class="row">
              <h5 class="upper-case workDarkBlue col-lg-2 pl-0 pt-lg-2"><?=__("WORKING GROUP ACTIVITIES");?></h5>
              <div class="col-lg-3">
                <a hrewf="#">
                  <img class="mb-2" src="<?=$URL_ROOT?>assets/img/all/img-wp7-actividad01.png" />
                  <p class="data small workDarkBlue"><?=__("SEPTIEMBRE 2020");?></p>
                  <p><?=__("Launch meeting of the Wind Turbine Data Sharing Work Group.");?></p>
                </a>
              </div>
              <div class="col-lg-3">
                <a href="#">
                  <img class="mb-2" src="<?=$URL_ROOT?>assets/img/all/img-wp7-actividad02.png" />
                  <p class="data small workDarkBlue"><?=__("NOVIEMBRE 2020");?></p>
                  <p><?=__("Bilateral meetings to compile the specifications of the data demanded by component manufacturers.");?></p>
                </a>
              </div>
              <div class="col-lg-3">
                <a href="#" class="arrow">
                  <img class="mb-2" src="<?=$URL_ROOT?>assets/img/all/img-wp7-actividad01.png" />
                  <p class="data small workDarkBlue"><?=__("NOVIEMBRE 2020");?></p>
                  <p><?=__("Bilateral meetings to compile the specifications of the data demanded by component manufacturers.");?></p>
                </a>
              </div>
            </div>
          </div>
         <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>
    <div class="container pb-2">
      <article class="banner mt-1 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/project/bg-actividades-resultados.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("ACTIVITIES AND OUTCOMES");?></h3>
          <p class="text-white"><?=__("See the latest news and developments surrounding Daekin");?></p>
        </div>
        <a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/" class="btn btn-corporate1 shadow text-left"><?=__("ACTIVITIES AND<br />OUTCOMES");?></a>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>