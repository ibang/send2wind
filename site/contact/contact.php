<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-privacy.php");
require_once("../../site/php/inc-contact.php");
$title=__("Contact | DAEKIN");
$description=__("If you want to know more about DAEKIN get in touch with us.");
$js[]="forms.js";
$js[]="gc.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$js_lang[]="contact.js";
if(CAPTCHA_ACTIVE == '3' OR (CAPTCHA_ACTIVE == '1' AND (!isset($validate["recaptcha"]) OR $validate["recaptcha"]!=1) )){
	$js_lang[]="recaptcha3.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="contact" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<main>
	<div class="full-container">
    <div class="container">
      <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/contacto/hero.jpg" class="">
                 </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("Contact");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
    </div>
  </div>
	<div class="full-container bg-gray ">
      <div class="container">
        <article>
        	 <div class="row">
              <div class="col-md-7 bg-gradient1 pt-2 pt-md-4 pb-2">
              	<div class="formbox p-3">
	              	<?require("{$DOC_ROOT}site/includes/widget-fullform.php")?>
    			</div>
              </div>
               <div class="col-md-5">
               	<div class="headquarters mt-3 mt-md-5 ml-4">
                <p class="person">Eugenio Perea <span class="small">(Tecnalia)</span></p>
        				<p class="email mt-0"><a data-event="link" data-category="mail" data-label="contacto" href="mailto:<?=$txt->footer->emailaddress?>"><?=$txt->footer->emailaddress1?></a></p>
                <p class="person mt-2">José Ignacio Hormaeche <span class="small">(<?=__("Energy Cluster Association");?>)</span></p>
                <p class="email"><a data-event="link" data-category="mail" data-label="contacto" href="mailto:<?=$txt->footer->emailaddress?>"><?=$txt->footer->emailaddress2?></a></p>
	              </div>
	          </div>
            </div>
        </article>
       </div>
    </div>
</main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>