<?php
require_once($DOC_ROOT."site/php/inc-mail".PHPVERSION.".php");
function parsedate($a,$language){
	$month_es=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	$month_en=array("January","February","March","April","May","June","July","August","September","October","November","December");
	$month_fr=array("Janvier","Février","Mars","Avril","Mai","Juin","Julliet","Aôut","Septembre","Octobre","Novembre","Décembre");
	$month_tr=array("Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık");
	$month_eu=array("Urtarrila","Otsaila","Martxoa","Apirila","Maiatza","Ekaina","Uztaila","Abuztua","Iraila","Urria","Azaroa","Abendua");
	$month_ru=array("Январь","февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь");
	$m=intval(substr ($a,5,2)-1);
	if ($language=="es"){$date_ok=substr ($a,8,2)." ".$month_es["$m"]." ".substr ($a,0,4);}
	if ($language=="en"){$date_ok=substr ($a,8,2)." ".$month_en["$m"]." ".substr ($a,0,4);}
	if ($language=="fr"){$date_ok=substr ($a,8,2)." ".$month_fr["$m"]." ".substr ($a,0,4);}
	if ($language=="tr"){$date_ok=substr ($a,8,2)." ".$month_tr["$m"]." ".substr ($a,0,4);}
	if ($language=="eu"){$date_ok=substr ($a,8,2)." ".$month_eu["$m"]." ".substr ($a,0,4);}
	if ($language=="ru"){$date_ok=substr ($a,8,2)." ".$month_ru["$m"]." ".substr ($a,0,4);}
	return $date_ok;
}

// take a absolute international language URL, and transform to actual localized URL
function transformInLocalizedURL($absolute_url){
	global $language, $langURL, $langURLReal,$URL_ROOT; 
	
	$host = parse_url($absolute_url, PHP_URL_HOST);
	$host = Constants::getProtocol($host).'://'.$host;
	$port = parse_url($absolute_url, PHP_URL_PORT);
	// faltaría comprobar el dominio para no aplicarlo a urls externas, solo a las de los dominios existentes
	// no meto los dominios sin especificación de idioma ya que estos son como son. 'htt..://www.send2wind.es.mx'
	$domainshostsArray = array('http://send2wind.promueve3.com', 'http://send2wind.localhost',  'https://www.send2windproject.com', 'https://send2windproject.com', 'http://localhost');
	$domainsHostsUrlRootArray = array('http://send2wind.promueve3.com' => '/', 'http://send2wind.localhost' => '/send2wind/', 'https://www.send2windproject.com' => '/', 'https://send2windproject.com' => '/', 'http://localhost' => '/send2wind/');
	$domainsHostsPortArray = array('http://send2wind.promueve3.com' => '', 'http://send2wind.localhost' => '', 'https://www.send2windproject.com' => '', 'https://send2windproject.com' => '', 'http://localhost' => LOCALHOSTPORT);
	$localizedURL = $absolute_url;
	if (in_array($host, $domainshostsArray)){
		// sustituyo el idioma actual por el mismo idioma localizado en la url en el enlace
		
		if ($langURL AND $langURLReal){
			//url con especificación de idioma send2wind.es/es/
			$localizedURL = str_replace('/'.$language.'/', '/'.$langURL.'/', $localizedURL);
		}
		else{
			//ur sin especificación del idioma, por ejemplo send2wind.es.mx/
			$localizedURL = str_replace('/'.$language.'/', '/', $absolute_url);
		}
		if ($host!=HTTPHOST){
			//addToLog($localizedURL.' , '.$host.$domainsHostsPortArray[$host].$domainsHostsUrlRootArray[$host].' , '.HTTPHOST.$URL_ROOT);
			$localizedURLAux = str_replace($host.$domainsHostsPortArray[$host].$domainsHostsUrlRootArray[$host], HTTPHOST.$URL_ROOT, $localizedURL);
			if ($localizedURLAux==$localizedURL){
				$localizedURL = str_replace($host.$domainsHostsPortArray[$host].'/', HTTPHOST.$URL_ROOT, $localizedURL);
			}
			else{
				$localizedURL =	$localizedURLAux;
			}
		}
	}
	return $localizedURL;
}

// take a absolute mainhost uploads real URL, and transform to relative URL
function transformInRelativeURL($absolute_url){
	global $URL_ROOT;
	//addToLog($absolute_url);
	$relative_url = $absolute_url;
	if ($absolute_url){
		$relative_url = str_replace( MAINHOST, '', $absolute_url);
		$relative_url = preg_replace( '/^\/uploads/', $URL_ROOT.'uploads', $relative_url); // para local
		//addToLog("Relative: ".$relative_url);
	}
	return $relative_url;
}

function transformVideoURL($iframe_url){
	// handle youtube videos
	if (strpos($iframe_url, 'youtu') !== FALSE) {
		if (strpos($iframe_url, 'rel=0') === FALSE) {
			if (strpos($iframe_url, '?') === FALSE) {
				$iframe_url.='?rel=0';
			}
			else{
				$iframe_url.='&rel=0';
			}
		}
	}
	// handle vimeo videos
	else if (strpos($iframe_url, 'vimeo') !== FALSE) {
	}
	return $iframe_url;
}

// transform all Links URLs from HTML code string in LocalizedURLS and IMAGE src transform absolute path in Relative paths
function transformAllInLocalizedURL($html_code){
	$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
	$localizedHtmlCode = $html_code;
	if (!empty($html_code)){
		if(preg_match_all("/$regexp/siU", $html_code, $matches, PREG_SET_ORDER)) {
			if (!empty($matches)){
				foreach($matches as $match) {
					// $match[2] = link address
					// $match[3] = link text
					$localizedURL = transformInLocalizedURL($match[2]);
					$localizedHtmlCode = str_replace($match[2], $localizedURL, $localizedHtmlCode);
				}
			}
		}
	}
	
	// transform IMAGE src from absolute urls to relative urls
	$regexp = "<img\s[^>]*src=(\"??)([^\" >]*?)\\1[^>]*\/?>";
	if (!empty($html_code)){
		if(preg_match_all("/$regexp/siU", $html_code, $matches, PREG_SET_ORDER)) {
			if (!empty($matches)){
				//addToLog (print_r($matches, true));
				foreach($matches as $match) {
					// $match[2] = link address
					// $match[3] = link text
					$relativeURL = transformInRelativeURL($match[2]);
					$localizedHtmlCode = str_replace($match[2], $relativeURL, $localizedHtmlCode);
				}
			}
		}
	}
	
	// transform embeded VIDEOS urls to no show related videos
	$regexp = "<iframe\s[^>]*src=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/iframe>";
	if (!empty($html_code)){
		if(preg_match_all("/$regexp/siU", $html_code, $matches, PREG_SET_ORDER)) {
			if (!empty($matches)){
				//addToLog (print_r($matches, true));
				foreach($matches as $match) {
					// $match[2] = link address
					// $match[3] = link text
					$videoURL = transformVideoURL($match[2]);
					$localizedHtmlCode = str_replace($match[2], $videoURL, $localizedHtmlCode);
				}
			}
		}
	}
	
	return $localizedHtmlCode;
}

// get current location unit format of a data in default units (m to inches, kg to pounds, etc.)
function getCurrentLocationUnitsFormat($data, $unit='', $to_data='', $fromtotxt = '', $returnString=false, $emptyisvariable=false){
	global $language, $langURL, $db, $location_format, $txt;
	$lang_location = $_SESSION['lang-location'];
	$format = '';
	if (empty($fromtotxt)){
		$fromtotxt = $txt->products->fromto;
	}
	//addToLog('INICIO UNITS FORMAT');
	settype($data, "float");
	if ($to_data!==''){
		settype($to_data, "float");
	}
	
	// var_dump
	ob_start();
	var_dump($data);
	$debug_dump = ob_get_clean();
	ob_start();
	var_dump($to_data);
	$debugto_dump = ob_get_clean();
	//addToLog('VARDUMP DATA: '.$debug_dump);
	//addToLog('VARDUMP DATA TO: '.$debugto_dump);
	
	if (empty($data) and !empty($to_data)){
		$data = $to_data;
		$to_data = '';
	}
	
	if (empty($data) AND $emptyisvariable){
		$fromtotxt = $txt->products->variable;
	}
	//addToLog('data: '.$data.', todata: '.$to_data.', Unit:' .$unit. ", lang-location: ".$lang_location.", language: ".$language.", langURL:".$langURL.", Location_format:".print_r($location_format, true));
	if (empty($unit)){ //sin unidad
		if (!$data and !$to_data and $emptyisvariable){
			if ($returnString){
				//
				$format = $fromtotxt;
			}
			else{
				//
				$format[] = $fromtotxt;
			}
		}
		else{
			if (!$to_data){
				$value = (number_format(round($data,0)+0,0,'.','')+0);
				//addToLog('value '.$value);
				if (!$format){
					$format = $value;
				}
				else{
					$format.=' ('.$value.')';
				}
			}
			else{
				$value = (number_format(round($data,0)+0,0,'.','')+0);
				$value_to = (number_format(round($to_data,0)+0,0,'.','')+0);
				//addToLog('value '.$value);
				if ($value_to>$value){
					$format[] = sprintf($fromtotxt, $value, $value_to);
				}
				else{
					$format[] = $value;
				}
			}
		}
	}
	else{
		//addToLog('Location_format INICIO: '.print_r($location_format, true));
		if (empty($location_format[$unit])){
			$location_format[$unit] = $db->getLocationUnits_from_url($langURL, $language, 'units_'.$unit);
			//addToLog('get location format Location_format:'.$location_format[$unit]);
		}
		if (!empty($location_format[$unit])){
			$location_format_array = explode(',', $location_format[$unit]);
			//addToLog('location format array:'.print_r($location_format_array, true));
			foreach ($location_format_array as $ind => $unit){
				if (empty($location_format[$unit]['unit_data'][$unit])){
					$unit_data = $db->getUnitData($unit);
					//addToLog('1- unit data for '.$unit.':'.print_r($unit_data, true));
					$location_format[$unit]['unit_data'][$unit] = $unit_data;
				}else{
					$unit_data = $location_format[$unit]['unit_data'][$unit];
					//addToLog('2- unit data for '.$unit.':'.print_r($unit_data, true));
				}
				if (!empty($unit_data)){
					if (!$data and !$to_data and $emptyisvariable){
						if ($returnString){
							//
							$format = $fromtotxt;
						}
						else{
							//
							$format[] = $fromtotxt;
						}
					}
					else{
						if (!$to_data){
							$value = (number_format(round($data*$unit_data['conversion'],(int)$unit_data['decimals'])+0,(int)$unit_data['decimals'],'.','')+0).' '.$unit_data['unit'];
							//addToLog('value '.$value);
							if (!$format){
								$format = $value;
							}
							else{
								$format.=' ('.$value.')';
							}
						}
						else{
							$value = (number_format(round($data*$unit_data['conversion'],(int)$unit_data['decimals'])+0,(int)$unit_data['decimals'],'.','')+0);
							$value_to = (number_format(round($to_data*$unit_data['conversion'],(int)$unit_data['decimals'])+0,(int)$unit_data['decimals'],'.','')+0);
							//addToLog('value '.$value);
							if ($value_to>$value){
								$format[] = sprintf($fromtotxt, $value, $value_to).' '.$unit_data['unit'];
							}
							else{
								$format[] = $value.' '.$unit_data['unit'];
							}
						}
					}
					//addToLog('format '.$format);
				}
			}
		}
	}
	//addToLog('FIN UNITS FORMAT');
	
	//miro si tengo que devolver directamente un string o puedo devolver el array
	if (!empty($to_data) AND $returnString){
		$formatString = '';
		if(!empty($format)){
			foreach($format as $ind => $value){
				if (!$ind){
					$formatString.="".$value."";
				}else{
					$formatString.="(".$value.")";
				}
			}
		}
		return $formatString;
	}
	return $format;	
}

//get random of 3 testimonials from a html code with 8 testimonials code
function getRandomTestimonials($buffer){
	$rand = range(1, 8); 
	shuffle($rand); 
	$selected = array();
	foreach ($rand as $ind=>$val) { 
		if ($ind<3){
			$selected[] = $val;
			unset($rand[$ind]);
		}
		else{
			break;
		}
	}
	sort($selected);
	$active = reset($selected);
	
	// load our document into a DOM object
	$dom = @DOMDocument::loadHTML('<?xml encoding="UTF-8">' .$buffer);
	// we want nice output
	$dom->formatOutput = true;
	
	$finder = new DomXPath($dom);
	foreach ($rand as $val){
		$classname="item item".$val;
		//*[contains(concat(' ', normalize-space(@class), ' '), ' classname ')]
		//$nodes = $finder->query("//*[contains(@class, '$classname')]");
		$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
		foreach ($nodes as $node) {
			$node->parentNode->removeChild($node);
		}
		
	}
	$classname="item item".$active;
	$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
	foreach ($nodes as $node) {
		$node->setAttribute('class', 'item item'.$active.' active');
	}
	$dom->encoding = 'UTF-8'; // insert proper
	//remove doctype, html and body added automatically by dom
	$html_fragment = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<?xml encoding="UTF-8">', '<html>', '</html>', '<body>', '</body>'), array('', '', '', '', ''), $dom->saveHTML()));
	
	return($html_fragment);
}

if (!function_exists('isTime')){
function isTime($time){
	if (!empty($time)){
		if (preg_match("/^(0[0-9]|1[0-9]|2[0-3]):(0[0-9]|[1-5][0-9])$/",$time)){
			return true;
		}
	}
	return false;
}
}

if (!function_exists('randVal')){
	function randVal(){
		//return rand();
		return uniqid('', true);
	}
}


// checknumber.php - V 0.2
// (C) 2014 James Nonnemaker (james[at]ustelcom[dot]net) - Feel free to redistribute/modify, but please give me credit.
// Checks that a telephone number's length is appropriate for the selected country (country code).
// It is best to strip out punctuation before passing a number to this function.
// This function will return true if the number would be valid for the region, false if not.
//
// Example: if (checknumber($country,$number)) { true action; } else { false action; }
// You can either paste this function in your script, or "include('checknumber.php');"
//
// This, and other goodies I've written can be found at www.moo.net/code

function checknumber($countrycode,$phonenumber) {
	if (($countrycode == '93') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Afghanistan
	if (($countrycode == '355') and (strlen($phonenumber) >= '3') and (strlen($phonenumber) <= '9')) { return true; } //Albania
	if (($countrycode == '213') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Algeria
	if (($countrycode == '376') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '9')) { return true; } //Andorra
	if (($countrycode == '244') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Angola
	if (($countrycode == '54') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //Argentina
	if (($countrycode == '374') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Armenia
	if (($countrycode == '297') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Aruba
	if (($countrycode == '61') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '15')) { return true; } //Australia
	if (($countrycode == '672') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '6')) { return true; } //Australian External Territories
	if (($countrycode == '43') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '13')) { return true; } //Austria
	if (($countrycode == '994') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Azerbaijan
	if (($countrycode == '973') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Bahrain
	if (($countrycode == '880') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '10')) { return true; } //Bangladesh
	if (($countrycode == '375') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '10')) { return true; } //Belarus
	if (($countrycode == '32') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Belgium
	if (($countrycode == '501') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Belize
	if (($countrycode == '229') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Benin
	if (($countrycode == '975') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Bhutan
	if (($countrycode == '591') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Bolivia (Plurinational State of)
	if (($countrycode == '599') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Bonaire Sint Eustatius and Saba
	if (($countrycode == '387') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Bosnia and Herzegovina
	if (($countrycode == '267') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Botswana
	if (($countrycode == '55') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //Brazil
	if (($countrycode == '673') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Brunei Darussalam
	if (($countrycode == '359') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '9')) { return true; } //Bulgaria
	if (($countrycode == '226') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Burkina Faso
	if (($countrycode == '257') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Burundi
	if (($countrycode == '855') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Cambodia
	if (($countrycode == '237') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Cameroon
	if (($countrycode == '238') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Cape Verde
	if (($countrycode == '236') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Central African Rep.
	if (($countrycode == '235') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Chad
	if (($countrycode == '56') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Chile
	if (($countrycode == '86') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '12')) { return true; } //China
	if (($countrycode == '57') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '10')) { return true; } //Colombia
	if (($countrycode == '269') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Comoros
	if (($countrycode == '242') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Congo
	if (($countrycode == '682') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '5')) { return true; } //Cook Islands
	if (($countrycode == '506') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Costa Rica
	if (($countrycode == '225') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //C?te d'Ivoire
	if (($countrycode == '385') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '12')) { return true; } //Croatia
	if (($countrycode == '53') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '8')) { return true; } //Cuba
	if (($countrycode == '599') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Cura?ao
	if (($countrycode == '357') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '11')) { return true; } //Cyprus
	if (($countrycode == '420') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '12')) { return true; } //Czech Rep.
	if (($countrycode == '850') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '17')) { return true; } //Dem. People's Rep. of Korea
	if (($countrycode == '243') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '9')) { return true; } //Dem. Rep. of the Congo
	if (($countrycode == '45') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Denmark
	if (($countrycode == '246') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Diego Garcia
	if (($countrycode == '253') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '6')) { return true; } //Djibouti
	if (($countrycode == '593') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Ecuador
	if (($countrycode == '20') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '9')) { return true; } //Egypt
	if (($countrycode == '503') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '11')) { return true; } //El Salvador
	if (($countrycode == '240') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Equatorial Guinea
	if (($countrycode == '291') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Eritrea
	if (($countrycode == '372') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '10')) { return true; } //Estonia
	if (($countrycode == '251') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Ethiopia
	if (($countrycode == '500') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '5')) { return true; } //Falkland Islands (Malvinas)
	if (($countrycode == '298') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '6')) { return true; } //Faroe Islands
	if (($countrycode == '679') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Fiji
	if (($countrycode == '358') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '12')) { return true; } //Finland
	if (($countrycode == '33') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //France
	if (($countrycode == '262') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //French Departments and Territories in the Indian Ocean
	if (($countrycode == '594') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //French Guiana
	if (($countrycode == '689') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '6')) { return true; } //French Polynesia
	if (($countrycode == '241') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '7')) { return true; } //Gabon
	if (($countrycode == '220') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Gambia
	if (($countrycode == '995') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Georgia
	if (($countrycode == '49') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '13')) { return true; } //Germany
	if (($countrycode == '233') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '9')) { return true; } //Ghana
	if (($countrycode == '350') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Gibraltar
	if (($countrycode == '30') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //Greece
	if (($countrycode == '299') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '6')) { return true; } //Greenland
	if (($countrycode == '590') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Guadeloupe
	if (($countrycode == '502') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Guatemala
	if (($countrycode == '224') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Guinea
	if (($countrycode == '245') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Guinea-Bissau
	if (($countrycode == '592') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Guyana
	if (($countrycode == '509') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Haiti
	if (($countrycode == '504') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Honduras
	if (($countrycode == '852') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '9')) { return true; } //Hong Kong China
	if (($countrycode == '36') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Hungary
	if (($countrycode == '354') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '9')) { return true; } //Iceland
	if (($countrycode == '91') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '10')) { return true; } //India
	if (($countrycode == '62') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '10')) { return true; } //Indonesia
	if (($countrycode == '870') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Inmarsat SNAC
	if (($countrycode == '800') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //International Freephone Service
	if (($countrycode == '882') and (strlen($phonenumber) >= '0') and (strlen($phonenumber) <= '0')) { return true; } //International Networks shared code
	if (($countrycode == '883') and (strlen($phonenumber) >= '0') and (strlen($phonenumber) <= '0')) { return true; } //International Networks shared code
	if (($countrycode == '979') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //International Premium Rate Service (IPRS)
	if (($countrycode == '808') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //International Shared Cost Service (ISCS)
	if (($countrycode == '98') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '10')) { return true; } //Iran (Islamic Republic of)
	if (($countrycode == '964') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '10')) { return true; } //Iraq
	if (($countrycode == '353') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '11')) { return true; } //Ireland
	if (($countrycode == '972') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Israel
	if (($countrycode == '39') and (strlen($phonenumber) >= '1') and (strlen($phonenumber) <= '11')) { return true; } //Italy
	if (($countrycode == '81') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '13')) { return true; } //Japan
	if (($countrycode == '962') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '9')) { return true; } //Jordan
	if (($countrycode == '7') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //Kazakhstan
	if (($countrycode == '254') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '10')) { return true; } //Kenya
	if (($countrycode == '686') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '5')) { return true; } //Kiribati
	if (($countrycode == '82') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '11')) { return true; } //Korea (Rep. of)
	if (($countrycode == '965') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Kuwait
	if (($countrycode == '996') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Kyrgyzstan
	if (($countrycode == '856') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '10')) { return true; } //Lao P.D.R.
	if (($countrycode == '371') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Latvia
	if (($countrycode == '961') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Lebanon
	if (($countrycode == '266') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Lesotho
	if (($countrycode == '231') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Liberia
	if (($countrycode == '218') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Libya
	if (($countrycode == '423') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '9')) { return true; } //Liechtenstein
	if (($countrycode == '370') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Lithuania
	if (($countrycode == '352') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '11')) { return true; } //Luxembourg
	if (($countrycode == '853') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Macao China
	if (($countrycode == '261') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '10')) { return true; } //Madagascar
	if (($countrycode == '265') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Malawi
	if (($countrycode == '60') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '9')) { return true; } //Malaysia
	if (($countrycode == '960') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Maldives
	if (($countrycode == '223') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Mali
	if (($countrycode == '356') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Malta
	if (($countrycode == '692') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Marshall Islands
	if (($countrycode == '596') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Martinique
	if (($countrycode == '222') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Mauritania
	if (($countrycode == '230') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Mauritius
	if (($countrycode == '52') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //Mexico
	if (($countrycode == '691') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Micronesia
	if (($countrycode == '373') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Moldova (Republic of)
	if (($countrycode == '377') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '9')) { return true; } //Monaco
	if (($countrycode == '976') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Mongolia
	if (($countrycode == '382') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '12')) { return true; } //Montenegro
	if (($countrycode == '212') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Morocco
	if (($countrycode == '258') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Mozambique
	if (($countrycode == '95') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '9')) { return true; } //Myanmar
	if (($countrycode == '264') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '10')) { return true; } //Namibia
	if (($countrycode == '674') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '7')) { return true; } //Nauru
	if (($countrycode == '977') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Nepal
	if (($countrycode == '31') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Netherlands
	if (($countrycode == '687') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '6')) { return true; } //New Caledonia
	if (($countrycode == '64') and (strlen($phonenumber) >= '3') and (strlen($phonenumber) <= '10')) { return true; } //New Zealand
	if (($countrycode == '505') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Nicaragua
	if (($countrycode == '227') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Niger
	if (($countrycode == '234') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '10')) { return true; } //Nigeria
	if (($countrycode == '683') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '4')) { return true; } //Niue
	if (($countrycode == '47') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '8')) { return true; } //Norway
	if (($countrycode == '968') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Oman
	if (($countrycode == '92') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '11')) { return true; } //Pakistan
	if (($countrycode == '680') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Palau
	if (($countrycode == '507') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Panama
	if (($countrycode == '675') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '11')) { return true; } //Papua New Guinea
	if (($countrycode == '595') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '9')) { return true; } //Paraguay
	if (($countrycode == '51') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '11')) { return true; } //Peru
	if (($countrycode == '63') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '10')) { return true; } //Philippines
	if (($countrycode == '48') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '9')) { return true; } //Poland
	if (($countrycode == '351') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '11')) { return true; } //Portugal
	if (($countrycode == '974') and (strlen($phonenumber) >= '3') and (strlen($phonenumber) <= '8')) { return true; } //Qatar
	if (($countrycode == '40') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Romania
	if (($countrycode == '7') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //Russian Federation
	if (($countrycode == '250') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Rwanda
	if (($countrycode == '247') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '4')) { return true; } //Saint Helena Ascension and Tristan da Cunha
	if (($countrycode == '290') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '4')) { return true; } //Saint Helena Ascension and Tristan da Cunha
	if (($countrycode == '508') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '6')) { return true; } //Saint Pierre and Miquelon
	if (($countrycode == '685') and (strlen($phonenumber) >= '3') and (strlen($phonenumber) <= '7')) { return true; } //Samoa
	if (($countrycode == '378') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '10')) { return true; } //San Marino
	if (($countrycode == '239') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Sao Tome and Principe
	if (($countrycode == '966') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Saudi Arabia
	if (($countrycode == '221') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Senegal
	if (($countrycode == '381') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '12')) { return true; } //Serbia
	if (($countrycode == '248') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Seychelles
	if (($countrycode == '232') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Sierra Leone
	if (($countrycode == '65') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '12')) { return true; } //Singapore
	if (($countrycode == '421') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '9')) { return true; } //Slovakia
	if (($countrycode == '386') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Slovenia
	if (($countrycode == '677') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '5')) { return true; } //Solomon Islands
	if (($countrycode == '252') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '8')) { return true; } //Somalia
	if (($countrycode == '27') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //South Africa
	if (($countrycode == '211') and (strlen($phonenumber) >= '1') and (strlen($phonenumber) <= '15')) { return true; } //South Sudan
	if (($countrycode == '34') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Spain
	if (($countrycode == '94') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Sri Lanka
	if (($countrycode == '249') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Sudan
	if (($countrycode == '597') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '7')) { return true; } //Suriname
	if (($countrycode == '268') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '8')) { return true; } //Swaziland
	if (($countrycode == '46') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '13')) { return true; } //Sweden
	if (($countrycode == '41') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '12')) { return true; } //Switzerland
	if (($countrycode == '963') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '10')) { return true; } //Syrian Arab Republic
	if (($countrycode == '886') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Taiwan China
	if (($countrycode == '992') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Tajikistan
	if (($countrycode == '255') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Tanzania
	if (($countrycode == '888') and (strlen($phonenumber) >= '1') and (strlen($phonenumber) <= '15')) { return true; } //Telecommunications for Disaster Relief (TDR)
	if (($countrycode == '66') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //Thailand
	if (($countrycode == '389') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //The Former Yugoslav Republic of Macedonia
	if (($countrycode == '670') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '7')) { return true; } //Timor-Leste
	if (($countrycode == '228') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Togo
	if (($countrycode == '690') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '4')) { return true; } //Tokelau
	if (($countrycode == '676') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '7')) { return true; } //Tonga
	if (($countrycode == '991') and (strlen($phonenumber) >= '1') and (strlen($phonenumber) <= '15')) { return true; } //Trial of a proposed new international service shared code
	if (($countrycode == '216') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Tunisia
	if (($countrycode == '90') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //Turkey
	if (($countrycode == '993') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '8')) { return true; } //Turkmenistan
	if (($countrycode == '688') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '6')) { return true; } //Tuvalu
	if (($countrycode == '256') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Uganda
	if (($countrycode == '380') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Ukraine
	if (($countrycode == '971') and (strlen($phonenumber) >= '8') and (strlen($phonenumber) <= '9')) { return true; } //United Arab Emirates
	if (($countrycode == '44') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '10')) { return true; } //United Kingdom
	if (($countrycode == '1') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //United States / Canada / Many Island Nations
	if (($countrycode == '878') and (strlen($phonenumber) >= '1') and (strlen($phonenumber) <= '15')) { return true; } //Universal Personal Telecommunication (UPT)
	if (($countrycode == '598') and (strlen($phonenumber) >= '4') and (strlen($phonenumber) <= '11')) { return true; } //Uruguay
	if (($countrycode == '998') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Uzbekistan
	if (($countrycode == '678') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '7')) { return true; } //Vanuatu
	if (($countrycode == '39') and (strlen($phonenumber) >= '1') and (strlen($phonenumber) <= '11')) { return true; } //Vatican
	if (($countrycode == '379') and (strlen($phonenumber) >= '1') and (strlen($phonenumber) <= '11')) { return true; } //Vatican
	if (($countrycode == '58') and (strlen($phonenumber) >= '10') and (strlen($phonenumber) <= '10')) { return true; } //Venezuela (Bolivarian Republic of)
	if (($countrycode == '84') and (strlen($phonenumber) >= '7') and (strlen($phonenumber) <= '10')) { return true; } //Viet Nam
	if (($countrycode == '681') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '6')) { return true; } //Wallis and Futuna
	if (($countrycode == '967') and (strlen($phonenumber) >= '6') and (strlen($phonenumber) <= '9')) { return true; } //Yemen
	if (($countrycode == '260') and (strlen($phonenumber) >= '9') and (strlen($phonenumber) <= '9')) { return true; } //Zambia
	if (($countrycode == '263') and (strlen($phonenumber) >= '5') and (strlen($phonenumber) <= '10')) { return true; } //Zimbabwe

	return false;
}

/** GET CLIENT IP DATA **/
function get_ip_address() {
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }

    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip))
                    return $ip;
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];

    // return unreliable ip since all else failed
    return $_SERVER['REMOTE_ADDR'];
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip) {
    if (strtolower($ip) === 'unknown')
        return false;

    // generate ipv4 network address
    $ip = ip2long($ip);

    // if the ip is set and not equivalent to 255.255.255.255
    if ($ip !== false && $ip !== -1) {
        // make sure to get unsigned long representation of ip
        // due to discrepancies between 32 and 64 bit OSes and
        // signed numbers (ints default to signed in PHP)
        $ip = sprintf('%u', $ip);
        // do private network range checking
        if ($ip >= 0 && $ip <= 50331647) return false;
        if ($ip >= 167772160 && $ip <= 184549375) return false;
        if ($ip >= 2130706432 && $ip <= 2147483647) return false;
        if ($ip >= 2851995648 && $ip <= 2852061183) return false;
        if ($ip >= 2886729728 && $ip <= 2887778303) return false;
        if ($ip >= 3221225984 && $ip <= 3221226239) return false;
        if ($ip >= 3232235520 && $ip <= 3232301055) return false;
        if ($ip >= 4294967040) return false;
    }
    return true;
}

## Cut string to max chars without cutting last word 
function shortStringWord($str='', $max=200, $ellipse=''){
	$str = trim($str, ". \t\n\r\0\x0B");
	$len = strlen($str);
	if (!empty($str) AND $max>0){
		$str = wordwrap($str, $max);
		$str = explode("\n", $str);
		$str = $str[0];
		if (strlen($str)<$len){
			$str .= $ellipse;
		}
	}
	return $str;
}

## Bytes to KB, MB, etc.
function formatSizeUnits($bytes){
        if ($bytes >= 1073741824){
            $bytes = number_format($bytes / 1073741824, 2) . 'GB';
        }
        elseif ($bytes >= 1048576){
            $bytes = number_format($bytes / 1048576, 2) . 'MB';
        }
        elseif ($bytes >= 1024){
            $bytes = number_format($bytes / 1024, 2) . 'KB';
        }
        elseif ($bytes > 1){
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1){
            $bytes = $bytes . ' byte';
        }
        else{
            $bytes = '0 bytes';
        }
        return $bytes;
}
?>