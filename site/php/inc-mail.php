<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// PHPMailer
require($DOC_WEB_ROOT."php/PHPMailer/src/Exception.php");
require($DOC_WEB_ROOT."php/PHPMailer/src/PHPMailer.php");
require($DOC_WEB_ROOT."php/PHPMailer/src/SMTP.php");
//

function sendEmail($to='', $to_name='', $from='', $from_name='', $subject='', $body='', $trySMTP=true, $reply_to='', $reply_to_name='', $cc=false){
	$ret = true;
	if (EMAILING_ACTIVE=='1'){
		$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
		if (SMTP_ACTIVE=='1' AND $trySMTP){
			$mail->IsSMTP(); // telling the class to use SMTP transport
			$mail->SMTPAuth = true;
			if (SMTP_SECURE){
			$mail->SMTPSecure = SMTP_SECURE;
			}
			$mail->Host       = SMTP_HOST;
			$mail->Port       = SMTP_PORT;
			$mail->Username = SMTP_USERNAME;
			$mail->Password = SMTP_PASSWORD;
			$admin_from = SMTP_ADDRESS;
		}
		else{
			if (SENDMAIL_ACTIVE=='1'){
				$mail->IsSendmail(); // telling the class to use SendMail transport
			}
			$admin_from = $from;
		}
		$to_email = $to;
		try {
		  $mail->CharSet = 'UTF-8';
		  $mail->AddAddress($to_email);
		  //copias
		  if (!empty($cc)){
			foreach($cc as $current){
				$mail->addCC($current);
			}
		  }
		  //copias ocultas
		  for ($adn=2;$adn<=8;$adn++){
			  if (defined('ADMIN_EMAIL_ADDRESS'.$adn) AND constant('ADMIN_EMAIL_ADDRESS'.$adn)!=""){
				  $mail->addBCC(constant('ADMIN_EMAIL_ADDRESS'.$adn));
			  }
		  }
		  if ($reply_to){
			$mail->AddReplyTo($reply_to, $reply_to_name);
		  }
		  $mail->SetFrom($admin_from, $from_name);
		  $mail->Subject = $subject;
		  $mail->MsgHTML($body);
		  $mail->Send();
		  //echo "Message Sent OK<p></p>\n";
		  $return = true;
		} catch (phpmailerException $e) {
			$ret = false;
			addToLog($e->errorMessage()); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			$ret = false;
			addToLog($e->getMessage()); //Boring error messages from anything else!
		}
	}
	else{
		addToLog('NEW EMAIL'."\n".
			'To: '.$to."\n".
			'To Name:'. $to_name."\n".
			'Subject:'. $subject."\n".
			'Body:'. $body."\n".
			'From email:'. $from."\n".
			'From name:'. $from_name."\n".
			(!empty($adjuntos)?'adjuntos:'.print_r($adjuntos, true):'')
		);
	}
	return $ret;
}
?>