<?php
/* INICIALIZO VARIABLES */
$_GET['page'] = isset($_GET['page'])?(int)$_GET['page']:0;
$total_pages = 0;
$actual_page=0;
//

//------------------ sidebar en news y category pero no en detalle noticia------------------
	if ($s2=="new" OR $s2=="category"){
		$categories=$db->get_news_categories($language, $s1);
		//$latest_news=$db->get_latest_news($language);
		
	}

//------------------ consultas para cada subseccion ------------------	
	if ($s2=="category"){
		$category=$_GET["category"];
		$newspage=6;
		$newscount=count($db->count_category_news_list($language, $category, $s1));
		$total_pages=ceil($newscount/$newspage);
		if ($_GET["page"]){$actual_page=$_GET["page"];}else{$actual_page=1;}
		$limit="LIMIT ".$newspage*($actual_page-1)." , ".$newspage;
		$category_news = $db->get_category_news_list($language, $category, $limit, $s1);	
		if (!empty($category_news)){
			$category_news = $db->get_news_images_list($category_news);
			$category_news = $db->get_news_files_list($category_news);
			$lastArticleIndex = count($category_news)-1;
		}
		else{
			header("HTTP/1.0 404 Not Found");
			$PRE = '../../';
			unset($_GET);
			$_GET['s1'] = 'error';
			$s1='error';
			$_GET['error'] = '404';
			$error = '404';
			include("../error.php");
			exit();
		}
	}

//----------------- news home --------------------------------	
	if(!$s2 OR $s2=='past'){
		$newspage=6;
		$newscount = count($db->count_front_news_list($language, $s1, $s2));
		$total_pages=ceil($newscount/$newspage);
		if ($_GET["page"]){$actual_page=$_GET["page"];}else{$actual_page=1;}
		$limit="LIMIT ".$newspage*($actual_page-1)." , ".$newspage;
		$front_news=$db->get_front_news_list($language, $limit, $s1, $s2);
		$front_news = $db->get_news_images_list($front_news);
		$front_news = $db->get_news_files_list($front_news);
		$lastArticleIndex = count($front_news)-1;
	}
//----------------- news article sheet -----------------------	
	if($s2=="new"){
		$slug=$_GET["slug"];
		$article=$db->get_news_article($slug, $language, $s1);
		if (!empty($article)){
			$article_images_array = $db->get_news_images_list(array($article));
			$article = reset($article_images_array);
			$article_files_array = $db->get_news_files_list(array($article));
			$article = reset($article_files_array);
		}
		else{
			header("HTTP/1.0 404 Not Found");
			$PRE = '../../';
			unset($_GET);
			$_GET['s1'] = 'error';
			$s1='error';
			$_GET['error'] = '404';
			$error = '404';
			include("../error.php");
			exit();
		}
	}
?>

