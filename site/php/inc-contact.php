<?php
/* INICIALIZO VARIABLES */
$_GET['status'] = isset($_GET['status'])?$_GET['status']:'';
$contact = array_fill_keys(array('firstname', 'lastname', 'email', 'phone', 'phonecode', 'message', 'spamBotHoneyPot', 'company', 'country', 'country_name', 'province', 'province_name', 'about', 'dataid', 'consent', 'id', 'address', 'serial', 'requesttype', 'sector'),'');
$validate = array_fill_keys(array('developerTest', 'firstname', 'lastname', 'company', 'country', 'province', 'email', 'phone', 'phonecode', 'message', 'consent', 'spamBotHoneyPot', 'alreadysent', 'recaptcha', 'sector'),0);
$error = 0;
$sent='';

if (!empty($_POST["email"]) AND $_POST["email"]==DEVELOPER_EMAIL_ADDRESS){
	$developerTest = true;
}
// ------------- AJAX --------------
$functionName=isset($_GET['fn'])?$_GET['fn']:'';
if ($functionName){
	echo call_user_func('_'.$functionName);
	exit();
}

// Función que obtiene las provincias de un país dado
function _getProvinces(){
	global $txt, $db;
	$message='';
	$ok=false;
	$error=false;
	$error_arr='';
	$provinces = array();
	$result['provinces'] = array();
	$country = isset($_GET['selected'])?$_GET['selected']:'';
	if ($country=='ESP'){
		$provinces[] = array('value'=>'', 'title'=>(string)$txt->form->province->holder);
		if ($country=='ESP'){
			$province_list = $db->get_country_provinces($country);
			if(!empty($province_list)){
				foreach ($province_list as $province){
					$provinces[] = array('value'=>(string)$province['id'], 'title'=>(string)$province['province']);
				}
			}
		}
		$result['provinces'] = $provinces;
		unset($_GET['selected']);
	}
	else{
		$error=true;
		$error_arr=(string)$txt->form->alert->error->country;
	}
	
	if (!empty($error_arr)){
		$message=$error_arr;
	}
	
	if (!$error){
		$ok=true;
		$resultado['ok']=$message;
	}
	else{
		$result['error']=$message;
	}
	$result = json_encode($result);

	return $result;
}
//----------------------------------
//$country_list = $db->get_country_list();

if (!empty($_POST["send"])){
	//addToLog(' INICIO SEND CONTACT ::', 'SEND');
	$contact=array(
				"firstname"			=>	(isset($_POST["firstname"])?$_POST["firstname"]:''),
				"lastname"			=>	(isset($_POST["lastname"])?$_POST["lastname"]:''),
				"email"				=>	(isset($_POST["email"])?str_replace(' ', '', trim($_POST["email"])):''),
				//"phone"				=>	(isset($_POST["phone"])?str_replace(' ', '', trim($_POST["phone"])):''),
				//"phonecode"			=>	(isset($_POST["phonecode"])?str_replace(' ', '', trim($_POST["phonecode"])):''),
				"message"			=>	(isset($_POST["message"])?$_POST["message"]:''),
				"company" 			=>	(isset($_POST["company"])?$_POST["company"]:''),
				"sector" 			=>	(isset($_POST["sector"])?$_POST["sector"]:''),
				//"country_name" 		=>	"",
				//"country" 			=>	(isset($_POST["country"])?$_POST["country"]:''),
				//"province"			=>	(isset($_POST["province"])?$_POST["province"]:''),
				//"province_name"		=>	"",
				"about"				=>	(isset($_POST["about"])?$_POST["about"]:''),
				"dataid"			=>	(isset($_POST["dataid"])?$_POST["dataid"]:''),
				"googlecookie"		=>	(isset($_POST["googlecookie"])?$_POST["googlecookie"]:''),
				"gclid"				=>	(isset($_POST["gclid"])?$_POST["gclid"]:''),
				"gmode"				=>	(isset($_POST["gmode"])?$_POST["gmode"]:''),
				"request"			=>	(isset($_POST["request"])?$_POST["request"]:''),
				"requesttype"		=>	(isset($_POST["requesttype"])?$_POST["requesttype"]:''),
				"consent"			=>	(isset($_POST["consent"])?$_POST["consent"]:''),
				"id"				=>	"",
				"address"			=>	"",
				"serial"			=>	""
	);
	
	//---------- validaciones ----------------------
	$error=0;
	
	//-- recaptcha --
	/*
	When your users submit the form where you integrated reCAPTCHA, you'll get as part of the payload a string with the name "g-recaptcha-response". 
	In order to check whether Google has verified that user, send a POST request with these parameters:
	URL: https://www.google.com/recaptcha/api/siteverify
	secret (required)	6LfqYzEUAAAAAJHhRnXBqKOsfSGwEtIWKxK10w3w
	response (required)	El valor de "g-recaptcha-response".
	remoteip	The end user's ip address.
	*/
	if (empty($contact['requesttype']) AND (CAPTCHA_ACTIVE == '1' || CAPTCHA_ACTIVE == '3')){
	//addToLog("prueba recaptcha 3:".print_r($_POST, true));
		if (isset($_POST['token'])){
			$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.CAPTCHA3_SECRET_KEY.'&response='.$_POST['token'].'&remoteip='.$_SERVER['REMOTE_ADDR']);
			$responseDecoded  = json_decode($response);
			addToLog("prueba recaptcha 3:".print_r($responseDecoded, true)); //$error++;
			if ( $responseDecoded->success != false ){
				 if($responseDecoded->score <= CAPTCHA3_SCORE){
					 $validate["recaptcha"]=1;$error++;	
				 }
			}else{
				$validate["recaptcha"]=1;$error++;	
			}
		}
		else{
			 $validate["recaptcha"]=1;$error++;	
		}
	}
	if (!empty($contact['requesttype']) AND (CAPTCHA_ACTIVE == '2' || CAPTCHA_ACTIVE == '1') ){
		require_once($DOC_ROOT."site/includes/recaptcha-master/src/autoload.php");
		if (CAPTCHA_MODE!='CURL'){
			$recaptcha = new \ReCaptcha\ReCaptcha(CAPTCHA_SECRET_KEY);
		}
		else{
			$recaptcha = new \ReCaptcha\ReCaptcha(CAPTCHA_SECRET_KEY, new \ReCaptcha\RequestMethod\CurlPost());
		}
		$resp = $recaptcha->verify($_POST["g-recaptcha-response"], $_SERVER['REMOTE_ADDR']);
		if ($resp->isSuccess()) {
			// verified!
			// if Domain Name Validation turned off don't forget to check hostname field
			// if($resp->getHostName() === $_SERVER['SERVER_NAME']) {  }
		} else {
			//$errors = $resp->getErrorCodes();
			$validate["recaptcha"]=1;$error++;
		}
	}
	//-- fin recaptcha --

	if ($contact["firstname"]==""){$validate["firstname"]=1;$error++;}
	elseif (strlen(trim($contact["firstname"])) < 2 ) {$validate["firstname"]=2;$error++;}
	elseif (strlen(trim($contact["firstname"])) > 30 ) {$validate["firstname"]=2;$error++;};
	/*
	if ($contact["lastname"]==""){$validate["lastname"]=1;$error++;}
	elseif (strlen(trim($contact["lastname"])) < 2 ) {$validate["lastname"]=2;$error++;}
	elseif (strlen(trim($contact["lastname"])) > 50 ) {$validate["lastname"]=2;$error++;};
	*/
	if ($contact["email"]==""){$validate["email"]=1;$error++;};
	if(!preg_match("/^([a-zA-Z0-9\._-])+@([a-zA-Z0-9\._-])+\.[a-zA-Z]{2,4}$/", $contact["email"])){$validate["email"]=1;$error++;};
	/*
	if ($contact["phone"]==""){
		//$validate["phone"]=1;$error++;
	}
	elseif (strlen(trim($contact["phone"])) < 6 ) {$validate["phone"]=2;$error++;}
	elseif (strlen(trim($contact["phone"])) > 20 ) {$validate["phone"]=2;$error++;};
	$contact["phonecode"] = trim(str_replace('+', '', $contact["phonecode"]));
	if (empty($contact["phonecode"])){
		if (!empty($contact["phone"])){$validate["phonecode"]=1;$error++;}
	}
	elseif (!checknumber($contact["phonecode"], $contact["phone"])){
		if (!empty($contact["phone"])){$validate["phonecode"]=2;$error++;}
	}
	if (!empty($contact["phonecode"])){$contact["phonecode"]='+'.$contact["phonecode"];}
	*/
	/*
	if ($contact["company"]==""){$validate["company"]=1;$error++;}
	elseif (strlen(trim($contact["company"])) < 2 ) {$validate["company"]=2;$error++;}
	elseif (strlen(trim($contact["company"])) > 40 ) {$validate["company"]=2;$error++;};
	*/
	/*
	if ($contact["country"]==""){
		//$validate["country"]=1;$error++;
	}
	if (($contact["country"]=="ESP") && $contact["province"]==""){$validate["province"]=1;$error++;};
	*/
	if ($contact["message"]==""){$validate["message"]=1;$error++;}
	elseif (strlen(trim($contact["message"])) < 20 ) {$validate["message"]=2;$error++;};
	
	/*
	if ($contact["sector"]==""){ $validate["sector"]=1;$error++;}
	elseif (strlen(trim($contact["sector"])) < 2 ) {$validate["sector"]=2;$error++;}
	elseif (strlen(trim($contact["sector"])) > 60 ) {$validate["sector"]=2;$error++;};
	*/
	//consent
	if ($contact["consent"]!="Yes"){$validate["consent"]=1;$error++;};
		
	
/*
	if ($error==0){
		
		if ($contact["country"]=='ESP'){
			$theProvince = $db->get_province($contact['province']);
			if (!empty($theProvince)){
				$contact["province_name"] = $theProvince['province'];
			}
			else{
				$validate["province"]=1;$error++;
			}
		}
		if(!empty($contact["country"])){
			$theCountry = $db->get_country($contact['country']);
			if (!empty($theCountry)){
				$contact["country_name"] = $theCountry['LocalName'];;
			}
			else{
				$validate["country"]=1;$error++;
			}
		}
		
	}
*/	
	if ($error==0 AND !empty($developerTest)){
		$validate["developerTest"]=1;
		$error++;
		//addToLog(" DEVELOPER TEST: NOT SENDED");
	}
	
	//check if form is already sent
	if ($error == 0){
		if ($db->formContactExists($contact['dataid'])){
			unset($contact["firstname"]);
			unset($contact["lastname"]);
			unset($contact["email"]);
			//unset($contact["phone"]);
			//unset($contact["phonecode"]);
			unset($contact["message"]);
			unset($contact["company"]);
			unset($contact["sector"]);
			//unset($contact["country"]);
			//unset($contact["province"]);
			unset($contact["dataid"]);
			unset($contact["googlecookie"]);
			unset($contact["gclid"]);
			unset($contact["gmode"]);
			//mantengo el about y el request (url inicial)
			
			$contact['dataid'] = randVal(); //nuevo id
			$validate['alreadysent'] = 1;
			$error++;
		}
		else{
			$contact['id'] = $db->formContactCreate($contact['dataid']);
		}
	}
	else{
		$contact['dataid'] = randVal(); //nuevo id
	}
	
	//---------------- email body (client & user)----------------		
	$contact_insert = array();
	$current_location = (!empty($country_data['Name'])?$country_data['Name']:$db->get_country_from_localization_code($_SESSION['lang-location']));
	$language_data = $db->get_lang_by_code($language);
	if ($error==0){
		$fields= array("{firstname}","{lastname}","{email}","{company}","{sector}","{message}","{about}", "{location}", "{consent}", "../../", "{legalinfo}", "{date}");
		$replace_fields= array($contact["firstname"],$contact["lastname"],$contact["email"],$contact["company"],$contact["sector"],$contact["message"], $contact["about"], $language_data['lang_name'].(($language_data['lang_name'] AND $current_location)?' - ':'').$current_location, '<p>V. '.$policies['added'].'</p>'.$policies['body'], HTTPHOST.$URL_ROOT, (string)$txt->form->legalinfo->contact, date("Y-m-d H:i:s"));
		
		$email_client=file_get_contents("$DOC_ROOT"."alerts/client/contact.html");
		$email_client=str_replace($fields, $replace_fields, $email_client);
		
		$email_user=file_get_contents("$DOC_ROOT"."alerts/user/$language.html");
		$email_user=str_replace($fields, $replace_fields, $email_user);
		
		//---------- email client ----------------------
		$to_email = ADMIN_EMAIL_ADDRESS;
		if(sendEmail($to_email, 'Daekin', CONTACT_ADMIN_EMAIL_ADDRESS, (string)$txt->form->from->name, (string)$txt->form->subject->client->contact, $email_client, true, $contact["email"], $contact['firstname'].' '.$contact['lastname'])){
			$sent = "success";
		}
		else{
			addToLog("CLIENT: FALLO AL ENVIAR EL EMAIL");
		}
		$sent = "success";
		if ($sent=="success"){
			$contact_insert = $contact;
			
			//Save Google Cookies if not empty
			if (empty($contact_insert['googlecookie'])){
				unset($contact_insert['googlecookie']);
			}
			if (empty($contact_insert['gclid'])){
				unset($contact_insert['gclid']);
			}
			if (empty($contact_insert['gmode'])){
				unset($contact_insert['gmode']);
			}
			if (empty($contact_insert['request'])){
				unset($contact_insert['request']);
			}
			//
			
			$contact_insert['phone'] = $contact_insert['phonecode'].$contact_insert['phone'];
			$contact_insert['lang_name'] = $language_data['lang_name'];
			$contact_insert['location_name'] = $current_location;
			$contact_insert['type'] = 'contacto';
			$contact_insert['ip'] = get_ip_address();
			$contact_insert['policies_code'] = $policies['code'];
			$contact_insert['policies_revision_added'] = $policies['added'];
			$db->add_contacts(array_map('addslashes', $contact_insert));
		}
		else{
			$db->clearContact($contact['dataid']);
		}

		//---------- email user ----------------------
		if(sendEmail($contact["email"], $contact["firstname"], CONTACT_ADMIN_EMAIL_ADDRESS, (string)$txt->form->from->name, (string)$txt->form->subject->user, $email_user, true, CONTACT_REPLY_TO_EMAIL_ADDRESS, 'Daekin')){
			$sent = "success";
		}
		else{
			addToLog("USER: FALLO AL ENVIAR EL EMAIL");
		}
		unset($contact);
	}
	
	if ($sent=="success"){ // succes, le mando a la página de enviado
		$_SESSION['contact'] = $contact_insert;
		session_write_close();
		header('Location:'.$URL_ROOT_BASE.'/'.$txt->{$s1}->url.'/'.$txt->form->url.'/');
		exit();
	}
}
else{
	$contact['dataid'] = randVal();
}
if ($_GET['status']=='success' AND empty($_SESSION['contact'])){ //Compruebo que han accedido a la página de enviado tras enviar un formulario, sino redirijo a la principal
	session_write_close();
	header('Location:'.$URL_ROOT_BASE.'/'.$txt->{$s1}->url.'/');
	exit();
}
//$province_list = $db->get_country_provinces($contact['country']);
unset($_SESSION['contact']);
session_write_close();

if ($_GET['status']=="success" OR $error>0){
	$js[]="scrolltocontactform.js";
}

// si se ha enviado correctamente, enviamos el evento
if ((strpos($_GET['status'], 'success')!==false) AND defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="sendcontactevent.js";
}
?>