<?php

/* INICIALIZO VARIABLES */
$_GET['page'] = isset($_GET['page'])?$_GET['page']:0;
$total_pages = 0;
$actual_page=0;
$fromPage=0;
$toPage=0;
//
//$downloadscategories_list = $db->get_downloadscategories_subcategories($activeLang['id'],1, false);
$downloadscategories_list = '';

//------------------ download given file ------------------------

if( isset($_GET["s2"]) && $_GET["s2"] == 'download' ){
    $slug=$_GET["slug"];
	$article=$db->get_downloads_sheet($slug, $language);
	
	if (!empty($article["file1"])){
        
		$file_name = $slug.".".$article["fileformat1"];
		$size   = $article["filesize1"];
		
		//$finfo = finfo_open(FILEINFO_MIME_TYPE);
		//$mime = finfo_file($finfo, $DOC_ROOT."uploads/downloads/".$article["file1"]);
		//finfo_close($finfo);
		$mime = "application/pdf";
		
		// IE6 IE7
		header("Pragma: public");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		
		//
		header("Content-type: " . $mime);
		header('Content-Length: ' . $size);
		header("Content-Disposition: attachment; filename=\"$file_name\""); //forze download
		//header("Content-Disposition: inline; filename=\"$file_name\""); //open or download
		readfile($DOC_ROOT."uploads/downloads/".$article["file1"]);
		exit();
	}
	else{
		header('HTTP/1.0 404 Not Found');
		echo "<h1>404 Not Found</h1>";
		echo "The page that you have requested could not be found.";
		exit();
	}
}


//------------------ downloads lists and pages ------------------
$downloadspage = CFGDOWNLOADSBYPAGE;
$maxpageshow=5;
$fromPage=1;
$toPage=($fromPage-1)+$maxpageshow;

$downloadSearch = '';
if (!empty($_POST['downloadSearch'])){
    $downloadSearch = trim($_POST['downloadSearch']);
}
elseif(!empty($_GET['downloadSearch'])){
    $downloadSearch = trim($_GET['downloadSearch']);
}

$downloadscount=count($db->get_front_downloads_list( $language));
$total_pages=ceil($downloadscount/$downloadspage);
if ($_GET["page"]){$actual_page=$_GET["page"];}else{$actual_page=1;}
$limit="LIMIT ".$downloadspage*($actual_page-1)." , ".$downloadspage;
$donwloads_list = $db->get_front_downloads_list($language, $limit, $downloadSearch);	

// Desde y hasta pagina
if ($actual_page AND $total_pages AND $maxpageshow){
	if ($maxpageshow>$total_pages){
		$maxpageshow = $total_pages;
		$fromPage=1;
		$toPage=($fromPage-1)+$maxpageshow;
	}
	else{
		$espar = true;
		$desviacion = 0;
		if ($maxpageshow%2){ //impar
			$espar = false;
			$desviacion = (($maxpageshow-1)/2);
		}
		else{
			$desviacion = ($maxpageshow/2);
		}
		//derecha
		$toPage = ($actual_page + $desviacion);
		$ajusteDerecha = 0;
		if ( $toPage > $total_pages){
			$ajusteDerecha = $toPage - $total_pages;
			$toPage = $total_pages;
		}
		//izquierda
		$fromPage = ($actual_page - $desviacion);
		$ajusteIzquierda = 0;
		if ($fromPage < 1){
			$ajusteIzquierda = 1 - $fromPage;
			$fromPage = 1;
		}
		//resto (hay que ajustar una página más por izquierda o por la derecha)
		if ($espar){
			if ($fromPage > 1){
				$fromPage = $fromPage - 1;
			}
			elseif($toPage < $total_pages){
				$toPage = $toPage + 1;
			}	
		}
		
		//ajustes derecha y izquierda
		if ($ajusteDerecha AND $fromPage>1){
			$fromPage = $fromPage - $ajusteDerecha;
			if ($fromPage<1){
				$ajusteIzquierda = 1 - $fromPage;
				$fromPage = 1;
			}
		}
		if ($ajusteIzquierda AND $toPage < $total_pages){
			$toPage = $toPage + $ajusteIzquierda;
			if ($toPage > $total_pages){
				$ajusteDerecha = $toPage - $total_pages;
				$toPage = $total_pages;
			}
		}
		
		if ($toPage < $fromPage){
			$toPage = $fromPage;
		}
	}
}
// Fin Desde y Hasta página	
?>