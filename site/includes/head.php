<!DOCTYPE html>
<html class="no-js" lang="<?=$language?>">
<head>	
<?require("{$DOC_ROOT}site/includes/widget-head-tag-manager.php")?>
<script>
  window['GoogleAnalyticsObject'] = 'ga';
  window['ga'] = window['ga'] || function() {
    (window['ga'].q = window['ga'].q || []).push(arguments)
  };
</script>
<meta charset="utf-8" />
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="apple-mobile-web-app-title" content="<?=$txt->logo?>" />

<meta name="google-site-verification" content="" />
<meta name='yandex-verification' content='' />
<meta name="msvalidate.01" content="" />

<?if ((isset($_GET['status']) AND $_GET['status']=="success" AND !empty($contact['u'])) OR (!empty($_GET['u']))){?>
<meta name="robots" content="noindex,nofollow" />
<?}?>
	
<title><?=$title?></title>
<meta name="description" content="<?=$description?>" />
<meta name="keywords" content="<?=$keywords?>" />

<meta property="og:type" content="website" />
<meta property="og:title" content="<?=$title?>" />
<meta property="og:description" content="<?=$description?>" />
<meta property="og:url" content="<?=HTTPHOST;?><?=inLanguageDomain()?str_replace('/'.$langURL.'/', '/', $alternate[$langURL]):$alternate[$langURL];?>" />
<meta property="og:image" content="<?=HTTPHOST;?><?=$URL_ROOT?><?if($shareimage){?><?=$shareimage?><?}else{?>assets/img/all/share.png<?}?>" />
<meta property="og:site_name" content="<?=$txt->logo?>" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@Daekinproject" />
<meta name="twitter:title" content="<?=$title?>" />
<meta name="twitter:description" content="<?=$description?>" />
<meta name="twitter:image" content="<?=HTTPHOST;?><?=$URL_ROOT?><?if($shareimage){?><?=$shareimage?><?}else{?>assets/img/all/share.png<?}?>" />

<link rel="shortcut icon" href="<?=$URL_ROOT?>assets/ico/favicon.ico" />
<link rel="apple-touch-icon" href="<?=$URL_ROOT?>assets/ico/apple-touch-icon.png" />

<link rel="author" href="<?=$URL_ROOT?>humans.txt" />

<?if (!empty($actual_page) AND !empty($total_pages)){?>
	<?if ($actual_page>1){?>
<link rel="prev" href="<?=HTTPHOST;?><?=inLanguageDomain()?str_replace(array('/'.$langURL.'/', (string)($actual_page).'/'), array('/', ($actual_page>2?(string)($actual_page-1).'/':'') ), $alternate[$langURL]):str_replace((string)($actual_page).'/', ($actual_page>2?(string)($actual_page-1).'/':''), $alternate[$langURL]);?>" />
	<?}?>
	<?if ($actual_page<$total_pages){?>
<link rel="next" href="<?=HTTPHOST;?><?=inLanguageDomain()?str_replace(array('/'.$langURL.'/', (string)($actual_page).'/'), array('/', ''), $alternate[$langURL]).(string)($actual_page+1).'/':str_replace((string)($actual_page).'/', '', $alternate[$langURL]).(string)($actual_page+1).'/';?>" />
	<?}?>
<?}?>

<link rel="canonical" href="<?=HTTPHOST;?><?=inLanguageDomain()?str_replace('/'.$language.'/', '/', $alternate[$language]):$alternate[$language];?>" />

<?if ($alternate[$langURL] == $URL_ROOT.$langURL."/"){?>
<link rel="alternate" href="<?=HTTPHOST.$URL_ROOT;?>" hreflang="x-default" />
<?}?>

<?foreach ($available_localizations_urls as $currentLangUrl){?>
	<?if (!empty($alternate[$currentLangUrl])){?>
		<?if ($domainLanguage = Constants::getDomainLanguage($currentLangUrl)){?>
			<?$domainProtocol = Constants::getProtocol($domainLanguage);?>
<link rel="alternate" hreflang="<?=$currentLangUrl;?>" href="<?=$domainProtocol;?>://<?=$domainLanguage;?><?=str_replace('/'.$currentLangUrl.'/', '/', $alternate[$currentLangUrl]);?>" />
		<?}else{?>
<link rel="alternate" hreflang="<?=$currentLangUrl;?>" href="<?=MAINHOST;?><?=$alternate[$currentLangUrl];?>" />
		<?}?>
	<?}?>
<?}?>



<!-- SLICK SLIDER -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" />


<!-- OUR CSS -->
<?if (!empty($css)){?>
	<?foreach($css as $ccss){?>
		<link href="<?=$URL_ROOT?>assets/css/<?=$ccss?>?v=<?=CFGCURRENTVERSION;?>" rel="stylesheet">
	<?}?>
<?}?>
<? if (SRCORCSS=='CSS'){?>
<link href="<?=$URL_ROOT?>assets/css/screen.css?v=<?=CFGCURRENTVERSION;?>" rel="stylesheet">
<? }?>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<script src="<?=$URL_ROOT?>assets/js/modernizr.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,700,800,900&display=swap" rel="stylesheet"><?/* Local */?>
<?if (SRCORCSS!='CSS' AND SASSLESS=='SASS'){?>
<link href="<?=$URL_ROOT?>assets/scss/bootstrap.scss" type="text/scss" />
<!--<script src="<?=$URL_ROOT?>assets/dist/sass.link.min.js"></script>-->
<script src="<?=$URL_ROOT?>assets/dist/sass.link.src.js"></script>
<?}?>
<?if (CAPTCHA_ACTIVE == '2' OR (CAPTCHA_ACTIVE == '1' AND isset($validate["recaptcha"]) AND $validate["recaptcha"]==1)){?>
<script src="https://www.google.com/recaptcha/api.js?hl=<?php echo $language; ?>"></script>
<?}elseif(CAPTCHA_ACTIVE == '3' OR CAPTCHA_ACTIVE == '1'){?>
<!-- CAPTCHA V3-->
<script src="https://www.google.com/recaptcha/api.js?render=<?=CAPTCHA3_PUBLIC_SITE_KEY;?>"></script>
<?};?>

</head>