<article class="contacto" id="contactform">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="content">
				<? if ($_GET['status']=="success"){?>
				<div class="alert alert-success">
					<p class="title"><?=$txt->form->alert->success->title?></p>
					<p><?=$txt->form->alert->success->text?></p>
				</div>
				<?};?>
				<?if ($validate["developerTest"]==1){?>
				<div class="alert alert-warning">
					<p class="title"><?=$txt->form->alert->error->title?></p>
					<p>DEVELOPER TEST :: NOT SENDED TO THAT EMAIL ADDRESS</p>
				</div>
				<?}elseif ($error>0 ){?>
				<div class="alert alert-danger">
					<p class="title"><?=$txt->form->alert->error->title?></p>
					<p><?=$txt->form->alert->error->text?></p>
					<ol>
						<?if ($validate["firstname"]==1){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->firstname->title?></strong> <?=$txt->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["firstname"]==2){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->firstname->title?></strong> <?=$txt->form->alert->error->size?></li>
						<?}?>
						<?if ($validate["lastname"]==1){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->lastname->title?></strong> <?=$txt->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["lastname"]==2){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->lastname->title?></strong> <?=$txt->form->alert->error->size?></li>
						<?}?>
						<?if ($validate["email"]==1){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->email->title?></strong> <?=$txt->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["email"]==2){?>
								<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->email->title?></strong> <?=$txt->form->alert->error->emailformat?></li>
							<?}?>
						<?if ($validate["phone"]==1){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->phone->title?></strong> <?=$txt->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["phone"]==2){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->phone->title?></strong> <?=$txt->form->alert->error->size?></li>
						<?}?>
						<?if ($validate["phonecode"]==1){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->phonecode->title?></strong> <?=$txt->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["phonecode"]==2){?>
							<li><?=$txt->form->alert->error->labels?> <strong><?=$txt->form->phonecode->title?>, <?=$txt->form->phone->title?></strong> <?=$txt->form->alert->error->phonecode?></li>
						<?}?>
						<?if ($validate["alreadysent"]==1){?>
							<li><?=$txt->form->alert->error->alreadysent?></li>
						<?}?>
						<?if ($validate["consent"]==1){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->consent->title?></strong> <?=$txt->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["recaptcha"]==1){?>
							<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->recaptcha->title?></strong> <?=$txt->form->alert->error->required?></li>
						<?}?>
					</ol>
				</div>
				<?}?>
				<form action="<?=rtrim(preg_replace(array('/\?.*/', '/'.preg_quote($txt->form->url, '/').'\/$/'), '',  $_SERVER["REQUEST_URI"]), '/').'/'?>" method="post" id="form-contact" name="form-contact"  data-gc="form" class="recapcha3form" >
			<?if (CAPTCHA_ACTIVE == '2' OR (CAPTCHA_ACTIVE == '1' AND $validate["recaptcha"]==1)){?>
			<input type="hidden" name="requesttype" id="requesttype" value="1"  />
			<?}?>
			<?if(!empty($contact["about"])){?>
			<input class="hidden" type="hidden" name="about" id="about" value="<?=htmlspecialchars($contact["about"])?>"  />
			<?}?>
			<?if(!empty($contact["type"])){?>
			<input class="hidden" type="hidden" name="type" id="type" value="<?=htmlspecialchars($contact["type"])?>"  />
			<?}?>
			<?if ($_GET['status']=="success"){?>
			<input type="hidden" name="sended" id="sended" value="<?=$s1.(!empty($s2)?'-'.$s2:'');?>"  />
			<?}?>
			<input class="hidden" type="hidden" name="send" id="send" value="send"  />
			<input class="hidden" type="hidden" name="dataid" id="dataid" value="<?=$contact["dataid"];?>"  />
			<input type="hidden" name="googlecookie" id="googlecookie" value="<?php echo !empty($contact['googlecookie'])?$contact['googlecookie']:'';?>" />
			<input type="hidden" name="gclid" id="gclid" value="<?php echo !empty($contact['gclid'])?$contact['gclid']:'';?>" />
			<input type="hidden" name="gmode" id="gmode" value="<?php echo !empty($contact['gmode'])?$contact['gmode']:'';?>" />
			<input type="hidden" name="request" id="request" value="<?php echo !empty($contact['request'])?$contact['request']:$_SERVER["REQUEST_URI"];?>" />
			<div class="row">
				<div class="col-md-5">
					<div class="form-group<?if ($error AND !empty($validate["firstname"]) ){?> has-error<?}?>">
					  <label for="firstname" class="sr-only"><?=$txt->form->firstname->title?> <span>*</span></label>
					  <input type="text" class="form-control input-lg<?if ($error AND !empty($validate["firstname"]) ){?> has-error<?}?>" name="firstname" id="firstname" placeholder="<?=$txt->form->firstname->holder?> *" title="<?=$txt->form->firstname->title?>" value="<?=htmlspecialchars($contact["firstname"])?>" required />				    
					</div>
				</div>
				<div class="col-md-7">
					<div class="form-group<?if ($error AND !empty($validate["lastname"]) ){?> has-error<?}?>">
					  <label for="lastname" class="sr-only"><?=$txt->form->lastname->title?> <span>*</span></label>
					  <input type="text" class="form-control input-lg<?if ($error AND !empty($validate["lastname"]) ){?> has-error<?}?>" name="lastname" id="lastname" placeholder="<?=$txt->form->lastname->holder?> *" title="<?=$txt->form->lastname->title?>" value="<?=htmlspecialchars($contact["lastname"])?>" required />				    
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="form-group<?if ($error AND !empty($validate["email"]) ){?> has-error<?}?>">
					  <label for="email" class="sr-only"><?=$txt->form->email->title?> <span>*</span></label>
					  <input type="email" class="form-control input-lg<?if ($error AND !empty($validate["email"]) ){?> has-error<?}?>" name="email" id="email" placeholder="<?=$txt->form->email->holder?> *" title="<?=$txt->form->email->title?>" value="<?=htmlspecialchars($contact["email"])?>" required />				    
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group<?if ($error AND !empty($validate["phonecode"]) ){?> has-error<?}?>">
					  <label for="phonecode" class="sr-only"><?=$txt->form->phonecode->title?> </label>
					  <input type="tel" class="form-control input-lg<?if ($error AND !empty($validate["phonecode"])){?> has-error<?}?>" name="phonecode" id="phonecode" placeholder="<?=$txt->form->phonecode->holder?>" title="<?=$txt->form->phonecode->title?>" value="<?=htmlspecialchars($contact["phonecode"])?>"  />				    
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group<?if ($error AND (!empty($validate["phone"]) OR (!empty($validate["phonecode"]) AND $validate["phonecode"]==2) ) ){?> has-error<?}?>">
					  <label for="phone" class="sr-only"><?=$txt->form->phone->title?> <span>*</span></label>
					  <input type="tel" class="form-control input-lg<?if ($error AND (!empty($validate["phone"]) OR (!empty($validate["phonecode"]) AND $validate["phonecode"]==2) ) ){?> has-error<?}?>" name="phone" id="phone" placeholder="<?=$txt->form->phone->holder?>" title="<?=$txt->form->phone->title?>" value="<?=htmlspecialchars($contact["phone"])?>" />				    
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="btn-group" data-toggle="buttons"> <label class="btn opt-corporate1 active"> <input type="radio" name="persona" id="option1" class="mr-1" checked=""><?=__("Particular");?></label> <label class="btn opt-corporate1"> <input type="radio" name="persona" id="option2" class="mr-1"><?=__("Empresa");?></label> </div>
				</div>
				<div class="col-md-6">
					<div class="form-group<?if ($error AND !empty($validate["cif"]) ){?> has-error<?}?>">
					  <label for="cif" class="sr-only"><?=$txt->form->cif->title?> <span>*</span></label>
					  <input type="text" class="form-control input-lg<?if ($error AND !empty($validate["cif"]) ){?> has-error<?}?>" name="cif" id="cif" placeholder="<?=$txt->form->cif->holder?> *" title="<?=$txt->form->cif->title?>" value="<?=htmlspecialchars($contact["cif"])?>" required />				    
					</div>
				</div>
			</div>
			<div class="row" data-show="#consentinfo">
				<div class="col-md-12">
					<div class="form-group<?if ($error AND !empty($validate["consent"])){?> has-error<?}?>">
						<div class="checkbox form-control bg-white p-0">
							<label class="checkbox small"><input type="checkbox" class="mr-1" name="consent" id="checkbox" title="" value="Yes" required /><?=sprintf($txt->form->consent->text, $URL_ROOT_BASE.'/'.$txt->privacy->url.'/');?></label>
						</div>
						<div class="consentinfo starthide" id="consentinfo">
							<?=sprintf($txt->form->consent->contact, $URL_ROOT_BASE.'/'.$txt->privacy->url.'/');?>
						</div>
					</div>
					<p class="required small "><span><?=$txt->form->required?></span></p>
				</div>
			</div>
			<?if (CAPTCHA_ACTIVE == '2' OR (CAPTCHA_ACTIVE == '1' AND $validate["recaptcha"]==1)){?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group<?if ($error AND !empty($validate["recaptcha"])){?> has-error<?}?>">
						<label for="recaptcha" class="sr-only"><?=$txt->form->recaptcha->title?> <span>*</span></label>
						<div class="recaptcha form-control<?if ($error AND !empty($validate["recaptcha"])){?> has-error<?}?>">
							<div class="g-recaptcha" data-sitekey="<?=CAPTCHA_PUBLIC_SITE_KEY;?>"></div>
						</div>		    
					</div>
				</div>
			</div>
			<?}?>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-corporate1"><span><?=__("Regístrate");?></span></button>
				</div>
			</div>
		</form>
			</div>
		</div>
	</div>
</article>
