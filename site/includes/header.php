<?require("{$DOC_ROOT}site/includes/widget-body-tag-manager.php")?>	
<!--[if lt IE 8]>
<div id="browser-bar">
	<p><?=$txt->browser->alert->title?> <a class="cb-enable" href="http://outdatedbrowser.com/<?=$language?>" rel="nofollow" target="_blank"><?=$txt->browser->alert->link?></a></p>
</div>
<![endif]-->
<header>

  <nav class="fixed-top navbar-expand-lg navbar-dark bg-white">

  <div class="full-container bg-corporate1 d-none d-lg-block">
    <div class="container">
      <div class="row">
        <div class="col-sm power"><img src="<?=$URL_ROOT?>assets/img/all/logo-powered-cluster.png" alt="Powered by Cluster Energía" /></div>
        <div class="col-sm">
          <ul class="navbar-aux text-right list-unstyled">
            <li class="nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->downloads->url?>/"><?=$txt->downloads->title?></a></li>
              <li class="<? if($s1=="contacto"){?> active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->contacto->url?>/"><?=__("Contact")?></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <? if ($s1=="home") {?><h1 id="logo" class=""><a href="<?=$URL_ROOT_BASE?>/" class=""><span><?=__("SEND2WIND")?></span></a></h1><? }
        else {?><p id="logo" class=""><a href="<?=$URL_ROOT_BASE?>/" class=""><span><?=__("SEND2WIND")?></span></a></p><? }?>
          <button class="navbar-toggler d-inline-block d-lg-none" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
      </div>
      <div class="col-lg-9">
        <div class="collapse navbar-collapse flex-row" id="navbarCollapse">
          <ul class="navbar-nav">
            <li class="<? if($s1=="challenge"){?> active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->challenge->url?>/"><?=__("THE CHALLENGE")?></a></li>
            <li class="<? if($s1=="project"){?> active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/" class=""><?=__("THE PROJECT")?></a></li>
            <li class="<? if($s1=="context"){?> active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->consortium->url?>/" class=""><?=__("CONSORTIUM")?></a></li>
            <li class="<? if($s1=="news"){?> active<? }?> nav-item pb-lg-1"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/" class=""><?=__("ACTIVITIES")?></a></li>
            <li class="<? if($s1=="scoopitnews"){?> active<? }?> nav-item pb-lg-1"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->scoopitnews->url?>/" class=""><?=__("NEWS")?></a></li>
          </ul>
        
          <?if (!empty($country_data['available_languages']) and count($country_data['available_languages'])>1){?>
          <ul class="lang list-unstyled">
            <?foreach($country_data['available_languages'] as $current_lang){?>
            <li class="list-inline-item"><a <?if($current_lang['lang_code'] == $language){?> class="active"<?}?> href="<?=(!empty($alternate[$current_lang['localization_url']])?$alternate[$current_lang['localization_url']]:$URL_ROOT.$current_lang['localization_url'].'/');?>"><?=$txt->language->{$current_lang['lang_code']}?></a></li>
            <?}?>
          </ul>
          <?}?>
         
          <ul class="navbar-aux d-lg-none list-unstyled bg-corporate1">
            <li class="nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->contacto->url?>/"><?=__("Contact")?></a></li>
        </ul>
        </div>
      </div>
    </div>
</div>
  </nav>
</header>