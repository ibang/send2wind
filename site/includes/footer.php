<footer>
    <div class="container">
  		<div class="row no-gutters">
   			<div class="col-md-3">
          <p class="projectName"><img src="<?=$URL_ROOT?>assets/img/all/eu.png" class=""></p>
          <p class="small"><?=__("Project funded by the Department of Economic Development and Infrastructure of the Basque Government (HAZITEK programme) and the European Regional Development Fund (ERDF)")?></p>
        </div>
        <div class="col-md-8 ml-md-4">
          <div class="row">
              <div class="col-md-9">
                <span class="float-md-right mt-md-3 mr-md-2"><a data-event="link" data-category="twitter" data-label="pie" href="https://twitter.com/cluster_energia" target="_blank" class="twitter"><span><?=__("Twitter")?></span></a> <a data-event="link" data-category="linkedin" data-label="pie" href="https://www.linkedin.com/company/cluster-de-energia-basque-energy-cluster/" target="_blank" class="linkedin"><span><?=__("Linkedin")?></span></a></span>
                <p class="mt-2"><span class="person">Eugenio Perea (Tecnalia)</span><br /><a data-event="link" data-category="mail" class="mail" data-label="pie" href="mailto:<?=$txt->footer->emailaddress1?>"><?=$txt->footer->emailaddress1?></a></p>
                <p class="mt-2"><span class="person">José Ignacio Hormaeche (<?=__("Energy Cluster Association");?>)</span><br /><a data-event="link" data-category="mail" class="mail" data-label="pie" href="mailto:<?=$txt->footer->emailaddress2?>"><?=$txt->footer->emailaddress2?></a></p>
                <ul class="list-unstyled list-inline small mt-2 mt-md-2">
                  <li class="list-inline-item"><a <? if($s1=="legal"){?> class="active"<? }?> href="<?=$URL_ROOT_BASE?>/<?=$txt->legal->url?>/" rel="nofollow"><?=$txt->nav->footer->legal?></a></li>
                  <li class="list-inline-item"><a <? if($s1=="cookies"){?> class="active"<? }?> href="<?=$URL_ROOT_BASE?>/<?=$txt->cookies->url?>/" rel="nofollow"><?=$txt->nav->footer->cookies?></a></li>
                  <li class="list-inline-item"><a <? if($s1=="privacy"){?> class="active"<? }?> href="<?=$URL_ROOT_BASE?>/<?=$txt->privacy->url?>/" rel="nofollow"><?=$txt->nav->footer->privacy?></a></li>
                  <li class="list-inline-item"><?=sprintf($txt->footer->copy, date('Y'));?></li>
                </ul>
              </div>
              <div class="col-md-3">
                <img src="<?=$URL_ROOT?>assets/img/all/logo-daekin.svg" class="m-0 logo-daekin">
                <img src="<?=$URL_ROOT?>assets/img/all/logo-cluster-energia.svg" class="mt-md-0">
              </div>
            </div>
          </div>
  		</div>
    </div>
    </div>
</footer>
<?if (SASSLESS=='LESS'){?>
<script src="<?=$URL_ROOT?>assets/js/jquery-2.1.3.min.js"></script>
<script src="<?=$URL_ROOT?>assets/js/bootstrap.min.js"></script>
<?}elseif(SASSLESS=='SASS'){?>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?}?>
<script src="<?=$URL_ROOT?>assets/js/jquery.cookiebar.js?v=<?=CFGCURRENTVERSION;?>"></script>
<script src="<?=$URL_ROOT?>assets/js/nav-main.js?v=<?=CFGCURRENTVERSION;?>"></script>
<script src="<?=$URL_ROOT?>assets/js/scroll.js"></script>


<!-- SLICK SLIDER -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A==" crossorigin="anonymous"></script>
<script type="text/javascript">
  $('.slider').slick({
    dots: true,
    arrows: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>

<script>
  // Detect request animation frame
var scroll = window.requestAnimationFrame ||
             // IE Fallback
             function(callback){ window.setTimeout(callback, 1000/60)};
var elementsToShow = document.querySelectorAll('.show-on-scroll'); 

function loop() {

    Array.prototype.forEach.call(elementsToShow, function(element){
      if (isElementInViewport(element)) {
        element.classList.add('is-visible');
      } else {
        element.classList.remove('is-visible');
      }
    });

    scroll(loop);
}

// Call the loop for the first time
loop();

// Helper function from: http://stackoverflow.com/a/7557433/274826
function isElementInViewport(el) {
  // special bonus for those using jQuery
  if (typeof jQuery === "function" && el instanceof jQuery) {
    el = el[0];
  }
  var rect = el.getBoundingClientRect();
  return (
    (rect.top <= 0
      && rect.bottom >= 0)
    ||
    (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.top <= (window.innerHeight || document.documentElement.clientHeight))
    ||
    (rect.top >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
  );
}
</script>
<?if (!empty($js)){?>
	<?foreach($js as $cjs){?>
<script src="<?=$URL_ROOT?>assets/js/<?=$cjs?>?v=<?=CFGCURRENTVERSION;?>"></script>
	<?}?>
<?}?>
<?if (!empty($js_lang)){?>
	<?foreach($js_lang as $cjs_lang){?>
		<?if(!is_array($cjs_lang)){$cjs_lang = array('src' => $cjs_lang, 'params' => '');}?>
<script src="<?=$URL_ROOT_BASE?>/assets/js/<?=$cjs_lang['src']?>?v=<?=CFGCURRENTVERSION;?><?=$cjs_lang['params']?>"></script>
	<?}?>
<?}?>
<script>
$(document).ready(function(){
$.cookieBar({
message: '<?=addslashes($txt->cookies->alert->title)?> <a href="<?=$URL_ROOT_BASE?>/<?=$txt->cookies->url?>/"><?=addslashes($txt->cookies->alert->infolink)?></a>',
acceptText: "<?=addslashes($txt->cookies->alert->link)?>"
});
$(".scroll").on("click", function(e){
e.preventDefault();
var isWebkit = 'WebkitAppearance' in document.documentElement.style
if (isWebkit)
bodyelem = $("body");
else
bodyelem = $("html,body");	
bodyelem.animate({
scrollTop: $($(this).attr('href')).offset().top
}, 400); 
return false;
});
});
</script>

<script type='application/ld+json'>
{
  "@context" : "http://schema.org", 
  "@type" : "LocalBusiness",
  "url" : "<?=MAINHOST;?>",
  "name" : "Daekin",
  "logo" : "<?=MAINHOST;?>/assets/img/all/logo-daekin.png",
  "sameAs" : [ "https://www.linkedin.com/company/xxxx",
      "https://www.youtube.com/channel/xxxx"],
  "telephone" : "(+34) XXX XXX XXX",
  "email" : "estudio@daekin.com",
  "address" :  { 
    "@type" : "PostalAddress",
    "streetAddress" : "Ctra. Madrid-Irún, Km 415",
    "addressLocality" : "Idiazabal",
    "addressRegion" : "Gipuzkoa",
    "postalCode" : "20213",
    "addressCountry" : "Spain"
  } 
}
</script>
<? // addToLog('FIN FOOTER S2, TYPE: '.$_SESSION['s2'].', '.$_SESSION['type']); ?>