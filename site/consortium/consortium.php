<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Context - DAEKIN");
$description=__("Basque engineering, manufacturing, development and research centers collaborate to develop this project");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="context" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure mb-sm-0">
                    <img src="<?=$URL_ROOT?>assets/img/context/hero.jpg" class="">
                 </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("Background");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
            <div class="banda-hero mt-0 pt-2 pb-2 pl-2 pr-2 pl-md-4 pr-md-4">
              <div class="row">
                <div class="col-lg-6 align-self-center">
                  <p><?=__("DAEKIN (Datuak Konpartitzeko Ekimena) is a Type I ELKARTEK project, funded by the Basque Government's 2020 call.");?></p>
                </div>
                <div class="col-lg-2 text-center">
                  <img src="<?=$URL_ROOT?>assets/img/all/logo-gobierno-vasco.png" alt="<?=__("Eusko Jaurlaritza - Gobierno Vasco");?>">
                </div>
                <div class="col-lg-3 align-self-center">
                  <p class="small"><?=__("Project funded by the Basque Government's Department for  Economic Development, Sustainability and the Environment (ELKARTEK Programme).");?></p>
                </div>
              </div>
            </div>
          </article>
      </div>
    </div>


    <div class="full-container">
      <div class="container pt-3 pd-sm-3 pb-md-0">
        <article class="pl-1 pr-1 pl-md-4 pr-md-3">
          <div class="row bloques show-on-scroll reverse">
            <div class="col-lg-4 text-center">
              <div>
                <svg version="1.1" id="gaia" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="300px" viewBox="0 0 197.449 337.755" enable-background="new 0 0 197.449 337.755" xml:space="preserve">
                <metadata>
                  <sfw  xmlns="&ns_sfw;">
                    <slices></slices>
                    <sliceSourceBounds  width="-32766" height="-32766" y="24970" x="8498" bottomLeftOrigin="true"></sliceSourceBounds>
                  </sfw>
                </metadata>
                <g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      32.724,227.475 88.686,204.871 88.686,142.105 52.475,108.09 49.402,108.09"/>
                      <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="45.963" cy="108.09" r="3.004"/>
                      <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="28.156" cy="229.518" r="4.568"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      55.053,226.047 71.625,211.761 88.686,204.871 88.686,142.105 62.089,117.121 42.959,117.121"/>
                      <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="51.706" cy="228.942" r="3.498"/>
                      <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="38.635" cy="117.121" r="4.324"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-miterlimit="10" x1="70.876" y1="125.376" x2="60.156" y2="125.376"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-miterlimit="10" cx="56.576" cy="125.44" r="2.757"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-miterlimit="10" x1="79.062" y1="133.065" x2="79.062" y2="126.14"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-miterlimit="10" cx="79.166" cy="122.601" r="2.469"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      88.686,142.105 88.686,123.123 58.621,92.506 46.769,80.438 37.771,80.438"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="32.724" cy="80.603" r="4.554"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="45.452" y1="92.179" x2="57.824" y2="92.179"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="41.749" cy="91.794" r="3.703"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      59.806,242.497 95.687,206.122 95.687,113.455 52.111,68.521 52.111,42.021    "/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="52.111" cy="37.084" r="4.938"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="55.677" cy="246.694" r="5.372"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      38.045,62.771 51.638,62.771 60.649,54.542 60.649,50.262     "/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="34.864" cy="62.442" r="3.127"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="60.67" cy="47.238" r="3.024"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="67.069" y1="83.84" x2="67.069" y2="67.297"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="67.069" cy="62.771" r="4.362"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="76.898" y1="93.845" x2="76.898" y2="83.886"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="76.898" cy="79.843" r="3.374"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="86.115" y1="103.722" x2="86.115" y2="95.387"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="86.115" cy="92.002" r="2.824"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="80.867" y1="221.135" x2="84.15" y2="236.298"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="84.767" cy="240.036" r="3.709"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="95.687" y1="113.905" x2="95.687" y2="22.171"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="95.687" cy="16.466" r="4.387"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      95.687,83.217 78.371,65.569 78.371,35.997     "/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="78.371" cy="30.73" r="4.297"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="87.029" y1="74.393" x2="87.029" y2="44.213"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="87.03" cy="39.509" r="4.361"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="78.371" y1="49.823" x2="72.226" y2="44.213"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="69.812" cy="42.021" r="2.634"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      95.687,51.689 106.791,42.021 106.791,37.084     "/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="106.791" cy="31.608" r="4.389"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      95.687,72.099 106.351,61.498 106.351,57.108     "/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="106.351" cy="54.103" r="2.305"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      105.583,245.963 100.902,208.93 100.902,108.857 128.956,82.516 128.956,36.091    "/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="106.077" cy="250.572" r="4.28"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="128.956" cy="30.73" r="4.297"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="109.93" y1="100.381" x2="109.93" y2="85.157"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="109.93" cy="80.603" r="3.719"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="120.068" y1="50.262" x2="120.068" y2="90.861"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="120.067" cy="44.656" r="4.29"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="128.956" y1="62.771" x2="134.058" y2="59.303"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="136.198" cy="57.939" r="2.498"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="128.956" y1="73.305" x2="134.058" y2="69.837"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="136.198" cy="68.473" r="2.498"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="101.797" y1="214.699" x2="110.792" y2="225.008"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="112.99" cy="227.475" r="2.962"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      140.257,241.849 125.216,225.449 107.45,206.078 107.45,115.221 160.449,68.038    "/>
                      <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="163.52" cy="64.604" r="4.718"/>
                      <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="142.893" cy="245.443" r="4.197"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="125.39" y1="240.036" x2="125.39" y2="226.047"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="125.39" cy="242.836" r="2.854"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="144.098" y1="82.373" x2="144.098" y2="51.894"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="144.099" cy="46.477" r="4.691"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="151.311" y1="76.173" x2="151.311" y2="61.455"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="151.311" cy="57.939" r="2.416"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      165.057,227.475 112.99,204.871 112.99,124.823 147.089,95.162    "/>
                      <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="169.556" cy="230.438" r="4.499"/>
                       <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="151.311" cy="91.794" r="4.718"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="130.542" y1="109.556" x2="146.593" y2="109.556"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="151.311" cy="109.556" r="3.871"/>
                  </g>
                  <g>
                    <polyline fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                      112.99,138.595 133.253,121.445 153.727,121.445    "/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="158.802" cy="121.445" r="4.026"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="119.524" y1="133.065" x2="137.624" y2="133.065"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="141.355" cy="133.065" r="3.73"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="132.862" y1="213.664" x2="144.888" y2="226.514"/>
                    <circle fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" cx="147.692" cy="229.275" r="3.241"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="88.686" y1="191.703" x2="95.687" y2="185.778"/>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="100.902" y1="133.065" x2="107.45" y2="127.073"/>
                  </g>
                  <g>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="112.99" y1="178.096" x2="107.45" y2="172.17"/>
                    <line fill="none" stroke="#2D78BD" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="100.902" y1="158.455" x2="95.687" y2="152.858"/>
                  </g>
                  <path class="marco" fill="none" stroke="#2D78BD" stroke-width="1" stroke-miterlimit="10" d="M194.463,249.859
                    c0,8.726-7.074,15.801-15.801,15.801H19.776c-8.727,0-15.801-7.075-15.801-15.801V18.55c0-8.727,7.075-15.801,15.801-15.801
                    h158.887c8.727,0,15.801,7.074,15.801,15.801V249.859z"/>
                </g>
                </svg>
              </div>
           </div>
            <div class="col-lg-8">
              <h2 class=""><?=__("GAIA-X AND THE EU DATA SPACE ENERGY");?></h2>
              <div class="columnas mb-2 mb-sm-0 pt-2 blue">
                <p><?=__("In late 2019, the German government launched the GAIA-X project, aimed at creating <strong>an open, federated data infrastructure</strong>, based on European values and consisting of components and services to access, store, exchange and use data, following a set of predefined rules. Seven European governments and 22 businesses from France and Germany have joined the project, which aims to create an ecosystem of developers, suppliers and users of digital products and services, based on digital sovereignty and used as a key foundation for growth in Europe, using digital innovation and developing new business models.");?></p>
                <p><?=__("The EU Energy Data Space, headed by leading European power companies, was launched at the 2020 GAIA-X Summit.");?></p>
              </div>
           </div>
          </div>
       </article>
      </div>
    </div>

    <div class="container pt-3 pb-3 pb-md-3">
        <article class="pl-1 pr-1 pl-md-4 pr-md-3 bloques show-on-scroll">
          <div class="row reverse">
            <div class="col-lg-11 align-self-center">
              <h2 class=""><?=__("IDSA");?></h2>
              <div class="columnas">
                <p><?=__("IDSA is a European organisation that defines and promotes a <strong>digital architecture for the secure exchange of data between businesses</strong>, allowing data suppliers to maintain control and sovereignty over shared data at all times.");?></p>
                <p><?=__("IDSA architecture is intended to <strong>establish a global standard of basic conditions for data and interface governance</strong> to be used as the basis for the promotion and certification of a wide array of software solutions, digital services and new business models. IDSA has more than 100 active members from 20 countries, including the Technology Centres involved in DAEKIN (Tecnalia, Ikerlan, Tekniker).");?></p>
              </div>
            </div>
          </div>
          <div class="row">
            <h3 class="data mt-2 mb-0 pl-1 w-100"><?=__("REFERENCE MODEL");?></h3>
            <div class="col-lg-5 bg-white shadow p-2 mb-2 mt-2 pt-2">
              <ul class="list-arrow pl-0">
                <li><?=__("Establishes a global standard.");?></li>
                <li><?=__("Guarantees basic data and interface management conditions.");?></li>
                <li><?=__("100+ active members in 20+ countries.");?></li>
              </ul>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5 bg-white shadow p-2 mb-2 mt-2 pt-2">
              <ul class="list-arrow pl-0">
                <li><?=__("Promotes and certifies software solutions, digital services and new business models.");?></li>
                <li><?=__("Participation of Tecnalia, Tekniker and Ikerlan.");?></li>
              </ul>
            </div>
          </div>
          <div class="row p-2 mr-4 mt-3 pb-4">
            <div class="col-lg-12 text-center">
              <div class="bg-white-round shadow">
                <img src="<?=$URL_ROOT?>assets/img/context/logo-idsa.png" />
              </div>
            </div>
            <div class="col-lg-5 align-self-center lista-flotada">
              <ul class="margin-minus-6">
                <li class="icon-right"><?=__("RESEARCH KEYS");?></li>
                <li class="icon-right"><?=__("ONLINE EVENTS");?></li>
                <li class="icon-right"><?=__("CERTIFICATION MEASURES");?></li>
                <li class="icon-right"><?=__("EFINING AND IMPLEMENTING STANDARDS");?></li>
              </ul>
            </div>
            <div class="col-2 text-center"></div>
            <div class="col-lg-5 text-right align-self-center lista-flotada">
              <ul class="margin-minus-6">
                <li class="icon-left"><?=__("USER-DRIVEN PROJECTS");?></li>
                <li class="icon-left"><?=__("NEW BUSINESS MODELS");?></li>
                <li class="icon-left"><?=__("DEVELOPING ARCHITECTURES");?></li>
                <li class="icon-left"><?=__("RESEARCH-INDUSTRY EXCHANGE");?></li>
              </ul>
            </div>
          </div>
        </article>
    </div>

    <div class="container pt-3 pb-3 pb-md-5">
        <article class="pl-1 pr-1 pl-md-4 pr-md-3 bloques show-on-scroll">
          <div class="row">
            <div class="col-lg-6 align-self-center">
              <h2 class=""><?=__('“SENSING & REMOTE MONITORING IN MARINE RENEWABLE ENERGY” INTERREGIONAL PILOT ACTION');?></h2>
            </div>
            <div class="col-lg-6 text-center">
              <img src="<?=$URL_ROOT?>assets/img/context/foto-eolica-offshore.jpg" class="" />
            </div>
            <div class="col-lg-12 mt-3 columnas">
              <p><?=__("Over 2018 and 2019, the Basque Energy Cluster headed the interregional pilot data-sharing action <strong>“Sensing & Remote Monitoring in Marine Renewable Energy facilities”</strong> (S&RM in MRE), by means of a contact with DG REGIO, as part of the <strong>Marine Renewable Energy (MRE) partnership</strong>, a network of 16 regions led by the Basque Country and Scotland.");?></p>
              <p><?=__("The partnership defined the “business case” to develop <strong>a digital platform that would enable the owners of active wind farm data</strong> (wind farm operators and wind turbine manufacturers) <strong>to share data with businesses along the wind power value chain</strong>, including component manufacturers, digital businesses, research organisations, etc.");?></p>
            </div>
          </div>
          <div class="row bg-magenta shadow mt-2 p-sm-2 pl-lg-3 pt-lg-3 pb-lg-2">
            <div class="col-lg-6 pl-lg-2 pr-sm-2 pr-md-2">
              <h3 class="introDestacado"><?=__("OFFSHORE WIND POWER DATA DIGITAL PLATFORM");?></h3>
            </div>
            <div class="col-lg-6 pl-md-1 pl-lg-2 text-right">
              <p class="text-left"><?=__("The proposed platform responded to a series of data governance and sovereignty challenges, guaranteeing data privacy and security by means of implementing technology solutions based on <strong>IDSA reference architecture</strong>.");?></p>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col-lg-5 bg-white shadow p-2 mb-2 mt-2 pt-2">
              <h3 class="data"><?=__("BEGINNINGS (2018)");?></h3>
              <ul class="list-arrow pl-0">
                <li><?=__("Initiative led by the Basque Energy Cluster.");?></li>
                <li><?=__("Detection and remote monitoring in renewable marine energy.");?></li>
                <li><?=__("16 regions, led by the Basque Country and Scotland.");?></li>
              </ul>
            </div>
            <div class="col-lg-5 bg-white shadow p-2 mb-2 mt-2 pt-2 ml-md-5">
              <h3 class="data"><?=__("PILOT ACTION");?></h3>
              <ul class="list-arrow pl-0">
                <li><?=__("Defining a business case.");?></li>
                <li><?=__("Responds to data governance and sovereignty challenges, guaranteeing privacy and security.");?></li>
                <li><?=__("Implements technology solutions based on IDSA reference architecture.");?></li>
              </ul>
            </div>
          </div>
        </article>
    </div>

    <div class="container">
      <article class="banner mt-2 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/context/bg-reto.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("LEARN MORE ABOUT THE CHALLENGE");?></h3>
          <p class="text-white"><?=__("Elkartek faces a range of challenges surrounding data, access, security, sovereignty and more.");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->challenge->url?>/" class="btn btn-corporate1 shadow"><?=__("The Challenge");?></a>
      </article>
    </div>
  </main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>