<?
use Gettext\GettextTranslator;
use Gettext\Translations;
use Gettext\Translator;

date_default_timezone_set('Etc/UTC'); // for phpmailer
// Uncomment this lines on production enviroment
// ini_set("display_errors", 0);
// ini_set("log_errors", 1);
//error_reporting(E_ALL ^ E_NOTICE);
//error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
error_reporting(E_ALL);

// INITIALIZE VARIABLES
$error_page = isset($error_page)?$error_page:'';
$error = isset($error)?$error:'';
$shareimage = isset($shareimage)?$shareimage:'';
$title = isset($title)?$title:'';
$description = isset($description)?$description:'';
$keywords = isset($keywords)?$keywords:'';

/*
if (!function_exists('microtime_dif')){
	function microtime_dif(&$startTime='', $endTime=''){
		$time_start = explode(' ', $startTime);
		$endTime = ($endTime?$endTime:microtime());
		$time_end = explode(' ', $endTime );
		$parse_time = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);
		if (!$startTime){
			$parse_time = '0';
		}
		$startTime = $endTime;
		return $parse_time;
	}
}
if (!function_exists('addToLog')){
	function addToLog($errorStr='', $masInfoStr='', $errorNumber=3){
		error_log( date('Y-m-d H:i:s').' - '.$errorStr . " - ".$masInfoStr."\n", $errorNumber, rtrim($_SERVER['DOCUMENT_ROOT'],"/").'/daekin/cms/logs/debug.log');
		//echo '<pre>'.$errorStr.'</pre>';
	}
}
*/

// server location
if (!function_exists('serverLocation')){
	function serverLocation($thisFileRelativePath=''){
		// Load the absolute server path to the directory the script is running in
		$fileDir = str_replace('\\','/',dirname(__FILE__)); //si es windows, sustituyo los \ por /
		
		// Make sure we end with a slash
		$fileDir = rtrim($fileDir, '/').'/';
		
		// Load the absolute server path to the document root
		$docRoot = $_SERVER['DOCUMENT_ROOT'];

		// Make sure we end with a slash
		$docRoot= rtrim($_SERVER['DOCUMENT_ROOT'],"/").'/';

		// find part before docRoot in fileDir (in case fileDir is a absolute Path and docRoot an equivalent Paht than no start with de same path)
		$beforeStr = strstr($fileDir, $docRoot, true); 
		$docRoot = $beforeStr.$docRoot;

		// Remove docRoot string from fileDir string as subPath string
		$subPath = preg_replace('~' . $docRoot . '~i', '', $fileDir);

		// Add a slash to the beginning of subPath string
		$subPath = '/' . $subPath;          

		// Test subPath string to determine if we are in the web root or not
		if ($subPath == '/') {
			// if subPath = single slash, docRoot and fileDir strings were the same
            //echo "We are running in the web foot folder of htt..://" . $_SERVER['SERVER_NAME'];
			return $subPath; // '/'
		} else {
			// Anyting else means the file is running in a subdirectory
			//echo "We are running in the '" . $subPath . "' subdirectory of htt..://" . $_SERVER['SERVER_NAME'];
			if ($thisFileRelativePath){
				$thisFileRelativePath = '/'.trim($thisFileRelativePath, '/').'/';
				$subPath = preg_replace('~' . $thisFileRelativePath . '~i', '', $subPath);
				$subPath = $subPath.'/';
            }
			return $subPath;
		}
	}
}
$URL_ROOT = serverLocation('php');

// roots
$DOC_ROOT = rtrim($_SERVER['DOCUMENT_ROOT'],"/").$URL_ROOT;
$DOC_WEB_ROOT = $DOC_ROOT; //For in-mail.php

// configuration file 
require_once($DOC_ROOT."php/config.php");
$URL_CMS = CMSDIRNAME."/";
$DOC_ROOT_CMS = $DOC_ROOT.$URL_CMS;

// Gettext library
include_once($DOC_ROOT."php/gettext/src/autoloader.php");
include_once($DOC_ROOT."php/cldr-to-gettext-plural-rules/src/autoloader.php");
	
// General methods
require_once($DOC_ROOT.$URL_CMS."php/general.php");

//addToLog('INIT: '.print_r($_SERVER, true));
// DB methods
require_once($DOC_ROOT.$URL_CMS."php/db.php");

//$ipCountryIsoCode = getIPcountryIsoCode();
// languages

if (inLanguageDomain()){
	$langURL = getLanguageDomain();
	//addToLog('LANGURL: '.$langURL);
	$langURLReal = '';
}
elseif (empty($langURL)){
	$urlparts = explode('/', strtolower(substr($_SERVER['REQUEST_URI'],strlen($URL_ROOT))), 2);
	$langURL = reset($urlparts);
	$langURLReal = $langURL;
}
//$language=strtolower(substr($_SERVER['REQUEST_URI'],strlen($URL_ROOT),2));
$languages_array = $db->get_active_langs_array();
//addToLog('LANG ARRAY: '.print_r($languages_array, true));
// COMPRUEBO LOS IDIOMAS ACTIVOS
if (!empty($languages_array) and count($languages_array)==1){ //solo un idioma activo
	// SI SOLO HAY UNO ACTIVO Y EL ELEGIDO ES DISTINTO, LE MANDO AL ACTIVO SIEMPRE Y NO PERMITO IR A LA PREHOME DE SELECCION DE PAIS INTERNACIONAL POR DEFECTO, SIN PAIS
	if (!in_array($langURL,$languages_array)){
		//addToLog('NO IN ARRAY');
		$langDefault = reset($languages_array);
		redirectTo(MAINHOST.$URL_ROOT . $langDefault.'/');
	}
}
//
if (!in_array($langURL,$languages_array)){
	if (!$language = $db->get_lang_from_localization_url($langURL) OR !in_array($language,$languages_array)){
		$language = $db->get_default_lang();
		$langURL = $language;
		$langURLReal = $langURL;
		if ($error!='404' AND $error!='prehome' AND $error!='js' AND $error!='404fired'){ //si no estoy en la página de error 404 redirecciono a la home default language
			// AND $error!='404fired'
			redirectTo($URL_ROOT . $langURLReal.'/');
		}
	}
}
else{
	$language = $langURL;
}

// check if session is already active based on php version 
if (version_compare(phpversion(), '5.4.0', '<')) { //menos que php 5.4.0
    // php version isn't high enough
	if(session_id() == '') {
		session_name(COOKIENAME);
		@session_start();
	}
}
else{ // >= 5.4.0
	if (session_status() == PHP_SESSION_NONE) {
		session_name(COOKIENAME);
		@session_start();
	}
}

if (empty($_SESSION['lang-location']) OR empty($_SESSION['lang-url']) ){
	$_SESSION['lang-location'] = $db->get_code_from_localization_url($langURL);
	$_SESSION['lang-url'] = $langURL;
	if ($error == 'prehome'){ //si no está guardada la cookie de localización y estoy en la prehome, la cojo de el navegador y le mando a la home.
		redirectTo($URL_ROOT . $langURLReal.'/');
	}
}
elseif(!empty($_SESSION['lang-url']) AND $_SESSION['lang-url']!=$langURL ){
	$_SESSION['lang-location'] = $db->get_code_from_localization_url($langURL);
	$_SESSION['lang-url'] = $langURL;
}

// País activo
if ($country_data = $db->get_country_from_localization_url($langURL)){
	$country_data['available_languages'] = $db->get_available_languages_for_country($country_data['Code']);
}
else{
	$country_data['available_languages'] = $db->get_active_international_langs_array();
}
$URL_ROOT_BASE = rtrim($URL_ROOT . $langURLReal, "/");

$available_localizations_urls = $db->get_active_localizations_url_list();

// INICIALIZO EL FORMATO UNIDADES
$location_format = array();
//

require_once($DOC_ROOT."php/class.alternate.php");

$activeLang = $db->get_lang_by_code($language);
if (LANGUAGEMODE=='ALL' OR LANGUAGEMODE=='PO'){ // load po translations
	
	$locale = $activeLang['locale'];
	
	$locale = preg_replace_callback('/(-)([a-z]+)/', function($matches) {
	  return '_'.strtoupper($matches[2]);
	}, $locale);
	
    $poname = strtolower($language); // po file name
	
    $locale_dir = $DOC_ROOT.DIR_LOCALE;

	if (POMODE=='ARRAY'){ // Load translations from php array file
		
		if (CANGENERATETRANSLATIONSARRAY=='1'){
			$poModified = false;
			$poModified = isPoModified($locale_dir.'/'.$poname.'/LC_MESSAGES/');
			if ($poModified){ // regenerate php array file
				//import from a .po file:
				$translations = Translations::fromPoFile($locale_dir.'/'.$poname.'/LC_MESSAGES/'.$poname.'.po');
				//export to a php array:
				$translations->toPhpArrayFile($locale_dir.'/'.$poname.'/LC_MESSAGES/'.$poname.'.php');
			}
		}
		//Create the translator instance
		$t = new Translator();

		//Load your translations (exported as PhpArray):
		$t->loadTranslations($locale_dir.'/'.$poname.'/LC_MESSAGES/'.$poname.'.php');
	}
	else{ // Load translations from mo file and use gettext library
		
		//Create the translator instance
		$t = new GettextTranslator();

		//Set the language and load the domain
		$t->setLanguage($locale);
		$t->loadDomain($poname, $locale_dir);
	}
	//If you want use the global functions
	$t->register();
}

// -- Add STRING to LOG File
if (!function_exists('__')){
	function __($string){
		return _($string);
	}
}

// Load xml
if (LANGUAGEMODE=='ALL' OR LANGUAGEMODE=='XML'){
	$txt=simplexml_load_file($DOC_ROOT."lang/".strtolower($language).".xml") or die("Error loading language");
}

// load alternate s1..s10 variables por section, subsection, subsubsection 
for($i=1;$i<=10;$i++){
	if (!empty(${"s$i"}) OR !empty($_GET["s$i"])){
		${"s$i"} = empty(${"s$i"}) ? $_GET["s$i"] : ${"s$i"};
	}
}
// Initializa Section Array From S1..S10 variables
$section_array = initSectionArray();
//addToLog('INIT POST INIT SECTION ARRAY - '.microtime_dif($startTime));
// Create Alternate URLS from section_array
if ($error != 'js' and $error!='prehome' and !isset($_GET['fn'])){
	$alternateOBJ = new AlternateURLS($language, $section_array);
	//addToLog('INIT POST INIT ALTERNATE URLS - '.microtime_dif($startTime));
	$alternate = $alternateOBJ->getAlternateUrls();
	//addToLog('INIT POST INIT GET ALTERNATE URLS - '.microtime_dif($startTime));
	
}


//$projectscount=count($db->count_front_projects_list($language));
$headernewscount=count($db->count_front_news_list($language));

// -- news count by type
$newscount_news = count($db->count_front_news_list($language, 'news', ''));
$newscount_events = count($db->count_front_news_list($language, 'eventos', ''));
$newscount_past = count($db->count_front_news_list($language, 'eventos', 'past'));
?>