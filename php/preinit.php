<?
set_time_limit(0);
// Uncomment this lines on production enviroment
// ini_set("display_errors", 0);
// ini_set("log_errors", 1);
//error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL);
// parse list of comma separated language tags and sort it by the quality value
function parseLanguageList($languageList, $es_chrome=0) {
    if (is_null($languageList) OR empty($languageList)) {
        if (empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            return array();
        }
        $languageList = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    }
    $languages = array();
    $languageRanges = explode(',', trim($languageList));
	$n = 1;
    foreach ($languageRanges as $languageRange) {
        if (preg_match('/(\*|[a-zA-Z0-9]{1,8}(?:-[a-zA-Z0-9]{1,8})*)(?:\s*;\s*q\s*=\s*(0(?:\.\d{0,3})|1(?:\.0{0,3})))?/', trim($languageRange), $match)) {
            if (!isset($match[2])) {
                $match[2] = '1.0';
            } else {
                $match[2] = (string) floatval($match[2]);
            }
            if (!isset($languages[$match[2]])) {
                $languages[$match[2]] = array();
            }
			####  Chrome exceptions
			if ($es_chrome and $n==1){
				if ($match[1]=='es'){
					$match[1] = 'es-es';	
				}
				if ($match[1]=='fr'){
					$match[1] = 'fr-fr';	
				}
			}
			####
            $languages[$match[2]][] = strtolower($match[1]);
        }
		$n++;
    }
    krsort($languages);
    return $languages;
}

// compare two parsed arrays of language tags and find the matches
function findMatches($accepted, $available) {
    $matches = array();
    $any = false;
    foreach ($accepted as $acceptedQuality => $acceptedValues) {
        $acceptedQuality = floatval($acceptedQuality);
        if ($acceptedQuality === 0.0) continue;
        foreach ($available as $availableQuality => $availableValues) {
            $availableQuality = floatval($availableQuality);
            if ($availableQuality === 0.0) continue;
            foreach ($acceptedValues as $acceptedValue) {
                if ($acceptedValue === '*') {
                    $any = true;
                }
                foreach ($availableValues as $availableValue) {
                    $matchingGrade = matchLanguage($acceptedValue, $availableValue);
                    if ($matchingGrade > 0) {
                        $q = (string) ($acceptedQuality * $availableQuality * $matchingGrade);
                        if (!isset($matches[$q])) {
                            $matches[$q] = array();
                        }
                        if (!in_array($availableValue, $matches[$q])) {
                            $matches[$q][] = $availableValue;
                        }
                    }
                }
            }
        }
    }
    if (count($matches) === 0 && $any) {
        $matches = $available;
    }
    krsort($matches);
    return $matches;
}

// compare two language tags and distinguish the degree of matching
function matchLanguage($a, $b) {
    $a = explode('-', $a);
    $b = explode('-', $b);
    for ($i=0, $n=min(count($a), count($b)); $i<$n; $i++) {
        if ($a[$i] !== $b[$i]) break;
    }
    return $i === 0 ? 0 : (float) $i / count($a);
}

// server location
function serverLocation($thisFileRelativePath=''){
	// Load the absolute server path to the directory the script is running in
	$fileDir = str_replace('\\','/',dirname(__FILE__)); //si es windows, sustituyo los \ por /
	
	// Make sure we end with a slash
	$fileDir = rtrim($fileDir, '/').'/';
	
	// Load the absolute server path to the document root
	$docRoot = $_SERVER['DOCUMENT_ROOT'];

	// Make sure we end with a slash
	$docRoot= rtrim($_SERVER['DOCUMENT_ROOT'],"/").'/';

    // find part before docRoot in fileDir (in case fileDir is a absolute Path and docRoot an equivalent Paht than no start with de same path)
    $beforeStr = strstr($fileDir, $docRoot, true); 
    $docRoot = $beforeStr.$docRoot;

	// Remove docRoot string from fileDir string as subPath string
	$subPath = preg_replace('~' . $docRoot . '~i', '', $fileDir);

	// Add a slash to the beginning of subPath string
	$subPath = '/' . $subPath;          

	// Test subPath string to determine if we are in the web root or not
	if ($subPath == '/') {
		// if subPath = single slash, docRoot and fileDir strings were the same
		//echo "We are running in the web foot folder of htt..://" . $_SERVER['SERVER_NAME'];
		return $subPath; // '/'
	} else {
		// Anyting else means the file is running in a subdirectory
		//echo "We are running in the '" . $subPath . "' subdirectory of htt..://" . $_SERVER['SERVER_NAME'];
		if ($thisFileRelativePath){
			$thisFileRelativePath = '/'.trim($thisFileRelativePath, '/').'/';
			$subPath = preg_replace('~' . $thisFileRelativePath . '~i', '', $subPath);
			$subPath = $subPath.'/';
		}
		return $subPath;
	}
}
$URL_ROOT = serverLocation('php');

// roots
$DOC_ROOT = rtrim($_SERVER['DOCUMENT_ROOT'],"/").$URL_ROOT;
$DOC_WEB_ROOT = $DOC_ROOT; //For in-mail.php

// configuration file 
require_once($DOC_ROOT."php/config.php");
$URL_CMS = CMSDIRNAME."/";
$DOC_ROOT_CMS = $DOC_ROOT.$URL_CMS;

// General methods
require_once($DOC_ROOT.$URL_CMS."php/general.php");

// DB methods
require_once($DOC_ROOT.$URL_CMS."php/db.php");
?>