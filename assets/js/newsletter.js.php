<?php
$error = 'js';
require_once("../../php/init.php");
header("Content-type: application/javascript");
?>
$(function(){
	//bootbox warning
	$('#unsubscribebtn').click(function(e){
		e.preventDefault();
		var tolink= $(this).attr('href');
 		bootbox.confirm({
			title: "<?=$txt->form->alert->warning->unsubscribe->yes;?>",
			message: "<?=addslashes($txt->form->alert->warning->unsubscribe->text);?>",
			buttons: {
				confirm: {
					label: '<?=$txt->form->alert->warning->unsubscribe->yes;?>',
					className: 'btn-primary'
				},
				cancel: {
					label: '<?=$txt->form->alert->warning->unsubscribe->no;?>',
					className: 'btn-primary'
				}
			},
			callback: function (result) {
				//console.log('This was logged in the callback: ' + result);
				if (result){
					location.href = tolink;
				}
			}
		});
		return false;
	});
	$('#deletebtn').click(function(e){
		e.preventDefault();
		var tolink= $(this).attr('href');
 		bootbox.confirm({
			title: "<?=$txt->form->alert->warning->delete->yes;?>",
			message: "<?=addslashes($txt->form->alert->warning->delete->text);?>",
			buttons: {
				confirm: {
					label: '<?=$txt->form->alert->warning->delete->yes;?>',
					className: 'btn-primary'
				},
				cancel: {
					label: '<?=$txt->form->alert->warning->delete->no;?>',
					className: 'btn-primary'
				}
			},
			callback: function (result) {
				//console.log('This was logged in the callback: ' + result);
				if (result){
					location.href = tolink;
				}
			}
		});
		return false;
	});
});