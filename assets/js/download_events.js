$(function() {
	$("a[data-download]").click(function() {
		var action = $(this).data('download');
		var label = $(this).data('label');
		if (action){
			ga("basecmspromueveTracker.send","event", "download", action, label+' downloaded', 1);
		}
	});
});	