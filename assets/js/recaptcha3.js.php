<?php
$error = 'js';
require_once("../../php/init.php");
header("Content-type: application/javascript");
?>
$('.recapcha3form').submit(function() { 
	var theform = this;
	// we stoped it
	event.preventDefault();
	// needs for recaptacha ready
	grecaptcha.ready(function() {
		// do request for recaptcha token
		// response is promise with passed token
		grecaptcha.execute('<?=CAPTCHA3_PUBLIC_SITE_KEY;?>', {action: 'enviar_form'}).then(function(token) {
			// add token to form
			$(theform).prepend('<input type="hidden" name="token" value="' + token + '">');
			$(theform).prepend('<input type="hidden" name="action" value="enviar_form">');
			// submit form now
			$(theform).unbind('submit').submit();
		});;
	});
});
