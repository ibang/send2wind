
function selectindustries(select){
  var $ul = $(select).prev('ul');
   
  if ($ul.find('input[value=' + $(select).val() + ']').length == 0 && $(select).val()!='0')
    $ul.append('<li onclick="$(this).remove();">' +
      '<input type="hidden" name="industries[]" value="' + 
      $(select).val() + '" /> ' +
      $(select).find('option:selected').text() + '</li>');
}


$(document).ready(function(){
	$(":checkbox#variable").change(function(){
		var theForm = $(this).closest('form');
		if($(this).is(":checked")) {
			  //'checked' event code
			  theForm.find("#speed, #speed_min").val("0").prop('required', false);
			  return;
		}
		else{
			 theForm.find("#speed").prop('required',true);
			 return;
		}
	});
	
	$("#speed, #speed_min").on('input',function(e){
		var theForm = $(this).closest('form');
		if (theForm.find("#speed").val()>0 || theForm.find("#speed_min").val()>0){
			//console.log('speed:' + theForm.find("#speed").val());
			theForm.find("#speed").prop('required',true);
			theForm.find(":checkbox#variable").prop('checked', false);
			return;
		}
		else{
			theForm.find("#speed, #speed_min").prop('required', false);
			theForm.find(":checkbox#variable").prop('checked', true);
			return;
		}
	});
	
	//$(".remove-list li:before").on("click", function(e){
	$(document).on("click", ".remove-list li", function(e){
		//console.log('click remove');
		if( $(e.target).closest('.remove-list-child').length==0 ){
			//console.log('removed');
			$(this)[0].parentNode.removeChild($(this)[0]);
		}
		else{
			//console.log('NOT removed');
		}
	});
});