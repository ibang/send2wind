jQuery.fn.updateCountdown = function () {
	var countdown = $(this).data("countdown");
	if (typeof countdown !== typeof undefined && countdown !== false) {
		var countdownObj = $( this ).next('p.countdown');
		if (!countdownObj.length ) {
			countdownObj = $('<p class="info countdown" ></p>').insertAfter($(this));
		}
		var remaining = countdown - $(this).val().length;
		countdownObj.text(remaining);
	}
}
$(document).ready(function(){
	$('[data-countdown]').each(function(){
		$(this).updateCountdown();
		$(this).change($(this).updateCountdown);
		$(this).keyup($(this).updateCountdown);
	});
});
