$(document).ready(function(){
	$(".checkbox.hidden label").click(function(){
		var theForm = $(this).closest('form');
		if ($(":hidden#"+$(this).data('product')).val()){
			$(":hidden#"+$(this).data('product')).val("");
		}
		else{
			$(":hidden#"+$(this).data('product')).val($(this).data('product'));
		}
		if($(":hidden#"+$(this).data('product')).val()) {
		  //'checked' event code
		  theForm.find(":checkbox.checkserie[data-product='" +$(this).data('product') + "']").prop('checked', true);
		  return;
	   }
	   //'unchecked' event code
	    theForm.find(":checkbox.checkserie[data-product='" +$(this).data('product') + "']").prop('checked', false);
	});
	
	$(":checkbox.checkserie").change(function(){
		var theForm = $(this).closest('form');
		if($(this).is(":checked")) {
		  //'checked' event code
		  theForm.find(":hidden#"+$(this).data('product')).val($(this).data('product'));
		  return;
		}
		//'unchecked' event code
		var somechecked = false;
	    theForm.find(":checkbox.checkserie[data-product='" +$(this).data('product') + "']").each(function(){
			if($(this).is(":checked")) {
				somechecked = true;
			}
		});
		if (!somechecked){
			theForm.find(":hidden#"+$(this).data('product')).val("");
		}
	});
	
	$(":checkbox#variable").change(function(){
		var theForm = $(this).closest('form');
		if($(this).is(":checked")) {
			  //'checked' event code
			  theForm.find("#speed, #speed_min").val("0").prop('required', false);
			  return;
		}
		else{
			 theForm.find("#speed").prop('required',true);
			 return;
		}
	});
	
	$("#speed, #speed_min").on('input',function(e){
		var theForm = $(this).closest('form');
		if (theForm.find("#speed").val()>0 || theForm.find("#speed_min").val()>0){
			//console.log('speed:' + theForm.find("#speed").val());
			theForm.find("#speed").prop('required',true);
			theForm.find(":checkbox#variable").prop('checked', false);
			return;
		}
		else{
			theForm.find("#speed, #speed_min").prop('required', false);
			theForm.find(":checkbox#variable").prop('checked', true);
			return;
		}
	});
});