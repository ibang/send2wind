$(document).ready(function(){
	// initialize with defaults
	$("pre.fileinfo").hide();
	var fileinputoptions = {
		'browseClass': 'selector',
		'browseIcon': '',
		'dropZoneEnabled': false,
		'uploadUrl': 'ajax.php',
		'deleteUrl': 'ajax.php',
		'clearFunction': function(){
			var self = this; 
			bootbox.dialog({
			title: "Borrar Imagen",
			message: "¿Seguro que quieres borrar esta imagen?",
			onEscape: function() {},
			show: true,
			backdrop: true,
			closeButton: false,
			animate: true,
			className: "remove-image-modal",
			buttons: {
				cancel: {
				  label: "Cancelar",
				  className: "btn-default",
				  callback: function() {}
				},
				success: {   
				  label: "Confirmar",
				  className: "btn-success",
				  callback: function() { self.clear();}
				},
			  }
			
			});
		}, 
		'clearAjaxFunction': function(settings){
			bootbox.dialog({
			title: "Borrar Imagen",
			message: "¿Seguro que quieres borrar esta imagen?",
			onEscape: function() {},
			show: true,
			backdrop: true,
			closeButton: false,
			animate: true,
			className: "remove-image-modal",
			buttons: {
				cancel: {
				  label: "Cancelar",
				  className: "btn-default",
				  callback: function() {}
				},
				success: {   
				  label: "Confirmar",
				  className: "btn-success",
				  callback: function() {$.ajax(settings);}
				},
				
				
			  }
			
		});
		},
		'maxFileCount': 1,
		'maxFileSize': $("pre.maximagesize").first().text(),
		'showRemove': false,
		'showUpload': false, 
		'showCaption': false, 
		'showCancel': false, 
		'language':'es', 
		'allowedFileTypes':['image'], 
		'allowedFileExtensions':['jpg', 'png'], 
		'previewSettings':{
			'image': {
				'width': "100%", 
				'height': "auto"
			}
		}, 
		'layoutTemplates':{
			'footer':'<div class="file-thumbnail-footer">\n' +
            '    {actions}\n' +
            '</div>',
			'actions':'<div class="file-actions">\n' +
            '    <div class="file-footer-buttons">\n' +
            '       {delete}{other}' +
            '    </div>\n' +
            '    <div class="clearfix"></div>\n' +
            '</div>',
			'preview': '<p class="info">'+$("pre.infoimage").first().text()+'</p><div class="file-preview {class}">\n' +
            '    <div class="{dropClass}">\n' +
            '    <div class="file-preview-thumbnails">\n' +
            '    </div>\n' +
            '    <div class="clearfix"></div>' +
            '    <div class="file-preview-status text-center text-success"></div>\n' +
            '    <div class="kv-fileinput-error"></div>\n' +
            '    </div>\n' +
            '</div>', 
			'main2':'{browse}\n{preview}\n{upload}\n'
		}, 
		'removeClass': 'btn btn-warning',
		'fileActionSettings':{'removeIcon': 'Borrar imagen','removeClass': 'btn btn-default'}
	};		
	
	// Create a fileinput element with initialized image (preview)
	$("input[type=file].imageauto").not(".noimage").each(function(){
		var theForm = $(this).closest('form');
		var theFile = $(this);
		var theKey = theFile.attr('id');
		var theValue = theFile.attr('value');
		var fileinputoptionsespecifico = jQuery.extend(true, {}, fileinputoptions);
		// existing image data
		
		if (theValue!=''){
			fileinputoptionsespecifico.initialPreview = ["<img src='"+theFile.data('urlwebroot')+"uploads/"+theFile.attr('value')+"' class='file-preview-image' alt='' title='' style=\"width:100%;height:auto\"/>"];

			// options specific for existing image
			fileinputoptionsespecifico.initialPreviewShowDelete = true;
			fileinputoptionsespecifico.initialPreviewConfig = [{'caption': theFile.attr('value'), 'url':'ajax.php', 'width':'100%', 'height':'auto', 'key': theKey, 'extra': function(){
					return {
						code: theForm.find("input#code").val(),
						fn: 'fileDelete',
						session_id: theForm.find("input#session_id").val(),
						type:'image',
						file: theFile.attr('value'),
						lang: theForm.find("input#lang").val(),
						id: theFile.attr('id')
					};
				}
			}];
		}
		fileinputoptionsespecifico.deleteExtraData = function() {
			return {
                code: theForm.find("input#code").val(),
                fn: 'fileDelete',
				session_id: theForm.find("input#session_id").val(),
				type:'image',
				file: theFile.attr('value'),
				lang: theForm.find("input#lang").val(),
				id: theFile.attr('id')
            };
		};
		
		fileinputoptionsespecifico.uploadExtraData = function() {
            return {
                code: theForm.find('input#code').val(),
                fn: 'fileUpload',
				session_id: theForm.find("input#session_id").val(),
				type:'image',
				lang: theForm.find("input#lang").val(),
				id: theFile.attr('id')
            };
        };
		
		//create existing image		
		$(this).fileinput(fileinputoptionsespecifico).on("filebatchselected", function(event, files) {
			// trigger upload method immediately after files are selected
			$(this).fileinput("upload");
		}).on("fileuploaded", function(event, data, previewId, index){ //successfully uploaded
			dirty = true;
			var key = data.extra.id;
			theForm.find("input#alt-"+key).attr("required", "Yes");
			//$('.progress-bar-success').fadeOut(1000);
			theForm.find(".kv-upload-progress").hide();
		}).on("filedeleted", function(event, key){
			theForm.find("input#alt-"+key).val('').removeAttr('required');
			dirty = true; // -- form modified show alert on page exit --
		}).on("fileuploaderror", function(event, data, msg){
			theForm.find("#"+data.id+".file-preview-frame").hide();
			dirty = true; // ha habido una modificación en el formulario, lo indicamos para que se muestre la alerta en caso de que el usuario intente ir a otra página sin pulsar submit
		});
		
	});
	
	// Create fileinput empty(no preview, without image) element 
	$("input[type=file].imageauto.noimage").each(function(){
		var theFile = $(this);
		var theForm = $(this).closest('form');
		// Deep copy (copy object de manera independiente)
		var fileinputoptionsespecifico = jQuery.extend(true, {}, fileinputoptions);
		
		fileinputoptionsespecifico.deleteExtraData = function() {
			return {
				code: theForm.find("input#code").val(),
				fn: 'fileDelete',
				session_id: theForm.find("input#session_id").val(),
				type:'image',
				file: theFile.attr('value'),
				lang: theForm.find("input#lang").val(),
				id: theFile.attr('id')
			};
		};
		
		fileinputoptionsespecifico.uploadExtraData = function() {
			return {
				code: theForm.find('input#code').val(),
				fn: 'fileUpload',
				session_id: theForm.find("input#session_id").val(),
				type:'image',
				lang: theForm.find("input#lang").val(),
				id: theFile.attr('id')
			};
		};
		
		theFile.fileinput(fileinputoptionsespecifico).on("filebatchselected", function(event, files) {
			// trigger upload method immediately after files are selected
			$(this).fileinput("upload");
		}).on("fileuploaded", function(event, data, previewId, index){ //successfully uploaded
			 dirty = true;
			 var key = data.extra.id;
			 theForm.find("input#alt-"+key).attr("required", "Yes");
			//$('.progress-bar-success').fadeOut(1000);
			theForm.find(".kv-upload-progress").hide();
		}).on("filedeleted", function(event, key){
			theForm.find("input#alt-"+key).val('').removeAttr('required');
			dirty = true;
		}).on("fileuploaderror", function(event, data, msg){
				theForm.find("#"+data.id+".file-preview-frame").hide();
				dirty = true; // ha habido una modificación en el formulario, lo indicamos para que se muestre la alerta en caso de que el usuario intente ir a otra página sin pulsar submit
		});
	});
	
		
	// Create new image/file object when add new image button is clicked
	$(".js-addfileupload").click( function (e) {
        e.preventDefault(e);
		
		var theForm = $(this).closest('form');
		var theFormLang = theForm.find("input#lang").val();
		// Create unique ID for the element
		var elid = new Date().getTime();
		// Assign the id and call the plugin
		$("#uploadList"+theFormLang+".js-fileuploads").append("<div class=\"form-group file\" id=\"form-group-file-" + elid + "\"><label for=\"js-file-" + elid + "\" class=\"gallerynextimage\">Imagen</label><input type=\"file\" class=\"imageauto\" id=\"js-file-" + elid + "\" name=\"imageattach[]\" accept=\"image/*\"></div><div class=\"form-group\" id=\"form-group-alt-" + elid + "\"><label for=\"alt-js-file-" + elid + "\">Alt Imagen </label><input type=\"text\" class=\"form-control\" name=\"alt-js-file-" + elid + "\" id=\"alt-js-file-" + elid + "\" placeholder=\"Alt de la imagen\" title=\"Alt de la imagen\" value=\"\" /></div>");
		
		
		
		// Create fileinput element
		$("#js-file-" + elid).each(function(){
			var theFile = $(this);
			// Deep copy (copy object de manera independiente)
			var fileinputoptionsespecifico = jQuery.extend(true, {}, fileinputoptions);
			
			fileinputoptionsespecifico.deleteExtraData = function() {
				return {
					code: theForm.find("input#code").val(),
					fn: 'fileDelete',
					session_id: theForm.find("input#session_id").val(),
					type:'image',
					file: theFile.attr('value'),
					lang: theForm.find("input#lang").val(),
					id: theFile.attr('id')
				};
			};
			
			fileinputoptionsespecifico.uploadExtraData = function() {
				return {
					code: theForm.find('input#code').val(),
					fn: 'fileUpload',
					session_id: theForm.find("input#session_id").val(),
					type:'image',
					lang: theForm.find("input#lang").val(),
					id: theFile.attr('id')
				};
			};
			
			$("#js-file-" + elid).fileinput(fileinputoptionsespecifico).on("filebatchselected", function(event, files) {
				// trigger upload method immediately after files are selected
				$(this).fileinput("upload");
			}).on("fileuploaded", function(event, data, previewId, index){ //successfully uploaded
				 dirty = true;
				 var key = data.extra.id;
				 theForm.find("input#alt-"+key).attr("required", "Yes");
				//$('.progress-bar-success').fadeOut(1000);
				theForm.find(".kv-upload-progress").hide();
			}).on("filedeleted", function(event, key){
				theForm.find("input#alt-"+key).val('').removeAttr('required');
				dirty = true;
			}).on("fileuploaderror", function(event, data, msg){ // tamaño no permitido, y demás anteriores a mandar al ajax
				theForm.find("#"+data.id+".file-preview-frame").hide();
				dirty = true; // ha habido una modificación en el formulario, lo indicamos para que se muestre la alerta en caso de que el usuario intente ir a otra página sin pulsar submit
			});
		});
        return false;
    });
	
});