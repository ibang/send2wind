'use strict';

  $(function() {
	if ($( "#dragzone" ).length){
		$( "#dragzone" ).sortable({update: function( event, ui ) {
				var data = $(this).sortable('serialize');
				var start = $(this).data('start');
				var result = $.getJSON("?fn=reorderDownload", data+'&start='+start, "json").promise();
				result.done(function(data) {
				  // ok
				});
			}
		});
	}
	
	if ($( "#frontdragzone" ).length){
		$( "#frontdragzone" ).sortable({update: function( event, ui ) {
				var data = $(this).sortable('serialize');
				var start = $(this).data('start');
				var result = $.getJSON("?fn=reorderFront", data+'&start='+start, "json").promise();
				result.done(function(data) {
				  // ok
				});
			}
		});
	}
	
	if ($( ".productscategoriesdragzone" ).length){
		$( ".productscategoriesdragzone" ).sortable({update: function( event, ui ) {
				var data = $(this).sortable('serialize');
				var parent = $(this).data('parent');
				var depth = $(this).data('depth');
				
				// reordeno
				  var position = 0;
				  $(".productscategoriesdragzone[data-parent='" + parent + "']").children('tr').each(function(){
						var currentID = $(this).attr('id');
						//console.log(currentID);
						var res = currentID.split(":");
						var newID = res[0]+":"+position.toString();
						//console.log(newID);
						$(this).attr('id', newID);
						position=position+1;
				  });
				  //fin reordenar
				  
				var result = $.getJSON("?fn=reorderproductscategory", data+'&parent='+parent+'&depth='+depth, "json").promise();
				result.done(function(data) {
				  // ok
					// mejor reordeno fuera, por si alguien cambia el orden antes de acabar el ajax
				});
			}
		});
	}
	
	if ($( ".downloadscategoriesdragzone" ).length){
		$( ".downloadscategoriesdragzone" ).sortable({update: function( event, ui ) {
				var data = $(this).sortable('serialize');
				var parent = $(this).data('parent');
				var depth = $(this).data('depth');
				
				// reordeno
				  var position = 0;
				  $(".downloadscategoriesdragzone[data-parent='" + parent + "']").children('tr').each(function(){
						var currentID = $(this).attr('id');
						//console.log(currentID);
						var res = currentID.split(":");
						var newID = res[0]+":"+position.toString();
						//console.log(newID);
						$(this).attr('id', newID);
						position=position+1;
				  });
				  //fin reordenar
				  
				var result = $.getJSON("?fn=reorderdownloadscategory", data+'&parent='+parent+'&depth='+depth, "json").promise();
				result.done(function(data) {
				  // ok
					// mejor reordeno fuera, por si alguien cambia el orden antes de acabar el ajax
				});
			}
		});
	}
	
	if ($( "#useddragzone" ).length){
		$( "#useddragzone" ).sortable({update: function( event, ui ) {
				var data = $(this).sortable('serialize');
				var start = $(this).data('start');
				var result = $.getJSON("?fn=reorderused", data+'&start='+start, "json").promise();
				result.done(function(data) {
				  // ok
				});
			}
		});
	}
	
	if ($( "#downloadsdragzone" ).length){
		$( "#downloadsdragzone" ).sortable({update: function( event, ui ) {
				var data = $(this).sortable('serialize');
				var start = $(this).data('start');
				var result = $.getJSON("?fn=reorderDownload", data+'&start='+start, "json").promise();
				result.done(function(data) {
				  // ok
				});
			}
		});
	}
	
   // $( "#dragzone" ).disableSelection();
  });
