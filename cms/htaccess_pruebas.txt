Options -Indexes
Options +FollowSymlinks

RewriteEngine on

# CONSTANTE PARA EL PROTOCOLO, HTTP OR HTTPS -----
RewriteCond %{HTTPS}s ^(on(s)|offs)$
RewriteRule ^ - [env=s:%2]
# FIN CONSTANTE ----------------------------------

RewriteCond %{HTTP_HOST} !^daekin\.promueve3\.com$ [NC]
RewriteRule ^(.*)$ http%{ENV:s}://daekin.promueve3.com/cms/$1 [R=301,L]

<IfModule mod_expires.c>
ExpiresActive On
ExpiresDefault A0
</IfModule>

# ERROR HANDLING ----------------------------------------------------------
ErrorDocument 400 /cms/site/error.php?error=400&actions=no
ErrorDocument 401 /cms/site/error.php?error=401&actions=no
ErrorDocument 403 /cms/site/error.php?error=403&actions=no
ErrorDocument 404 /cms/site/error.php?error=404&actions=no
ErrorDocument 500 /cms/site/error.php?error=500&actions=no

# CHANGE LANGUAGE

RewriteRule ^(en|es)/login/$ /cms/site/login.php?language=$1 [L]
RewriteRule ^(en|es)/logout/$ /cms/site/login.php?language=$1&action=logout [L]

RewriteRule ^(en|es)/$ /cms/site/?language=$1 [L]