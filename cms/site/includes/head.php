<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>	
<meta charset="utf-8">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-mobile-web-app-title" content="<?=$txt->logo?>">
<meta name="robots" content="noindex" />
	
<title><?=$title?></title>

<link rel="shortcut icon" href="<?=$URL_ROOT?>assets/ico/favicon.ico" />
<link rel="apple-touch-icon" href="<?=$URL_ROOT?>assets/ico/apple-touch-icon.png" />

<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

<!-- OUR CSS -->
<? if (SRCORCSS=='CSS'){?>
<link href="<?=$URL_ROOT?>assets/css/screen.css?v=<?=CFGCURRENTVERSION;?>" rel="stylesheet">
<? }?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link href="<?=$URL_ROOT?>assets/css/fileinput.min.css?v=<?=CFGCURRENTVERSION;?>" rel="stylesheet"/>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="<?=$URL_ROOT?>assets/js/modernizr.min.js"></script>

<?if (SRCORCSS!='CSS' AND SASSLESS=='LESS'){?>
	<link rel="stylesheet/less" href="<?=$URL_ROOT?>assets/less/bootstrap.less" type="text/css" />
	<script>less = { env: 'development' };</script>
	<script src="<?=$URL_ROOT?>assets/js/less.min.js"></script>
<?}elseif(SRCORCSS!='CSS' AND SASSLESS=='SASS'){?>
	<link href="<?=$URL_ROOT?>assets/scss/bootstrap.scss" type="text/scss" />
	<!--<script src="<?=$URL_ROOT?>assets/dist/sass.link.min.js"></script>-->
	<script src="<?=$URL_ROOT?>assets/dist/sass.link.src.js"></script>
<?}?>

<script src="<?=$URL_ROOT?>assets/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
        tinymce.init({
			// No codificar con html_entities de character sets de otros idiomas (ruso)
			entity_encoding : "raw",
			content_css : "<?=$URL_ROOT?>assets/css/custom_tinymce.css?v=<?=CFGCURRENTVERSION;?>",
            selector: ".ckeditor",
			language: "es",
			plugins: [
				 "image lists link",
				 "fullscreen", "paste", "media"
		   ],
			paste_as_text: true,
			paste_auto_cleanup_on_paste : true,
			paste_strip_class_attributes: 'all',
			paste_remove_spans: true,
			paste_remove_styles: true,
			paste_text_linebreaktype: 'p',
		    link_title: false,
			target_list: false,
			default_link_target: "",
//			image_class_list: [
//        {title: 'Responsive', value: 'img-responsive'}],
			image_dimensions: false,
			<?if (!$customToolbar){?>
			toolbar: "bold italic | numlist bullist | alignleft aligncenter alignright alignjustify | outdent indent | link image media | fullscreen",
			<?}else{?>
			<?=$customToolbar;?>
			<?}?>
			media_live_embeds: false, // tinymce > 4.3 
			media_alt_source: false,
			media_poster: false,
			media_dimensions: false,
			video_template_callback: function(data) {
			  //alert('llamando al callback '+data.source1); //solo funciona cuando no se mete una url, es raro.
			  // return '<div class="embed-responsive embed-responsive-16by9">'+"\n"+'<iframe class="embed-responsive-item" src="' + data.source1 + '" ></iframe>'+"\n"+'</div>';
			  return '<p>'+"\n"+'<iframe src="' + data.source1 + '" ></iframe>'+"\n"+'</p>';
			},
			menubar: false,
			relative_urls:false,
			remove_script_host: false,
			/*
			file_picker_callback: function(field_name, url, type, win) {
          
				tinymce.activeEditor.windowManager.close();

				tinymce.activeEditor.windowManager.open({
					title: 'Upload an image',
					file: '<?=$URL_ROOT?>assets/js/tinymce/plugins/jbimages/dialog-v4.htm',
					width: 350,
					height: 135,
					buttons: [{
						text: 'Upload',
						classes: 'widget btn primary first abs-layout-item',
						disabled: true,
						onclick: 'close'
					}, {
						text: 'Close',
						onclick: 'close'
					}]
				});
			}
			*/
			/* without images_upload_url set, Upload tab won't show up*/
			 // images_upload_url: '<?=$URL_ROOT?>site/php/upload-handler.php',
			  
			  images_upload_credentials: true,

			  /* we override default upload handler to simulate successful upload*/
			images_upload_handler: function (blobInfo, success, failure) {
				var xhr, formData;

				xhr = new XMLHttpRequest();
				xhr.withCredentials = true;
				xhr.open('POST', '<?=$URL_ROOT?>site/php/upload-handler.php');
				
				xhr.onerror = function() {
					failure("Image upload failed due to a XHR Transport error. Code: " + xhr.status);
				};
				xhr.onload = function() {
					var json;

					if (xhr.status < 200 || xhr.status >= 300) {
						failure('HTTP Error: ' + xhr.status);
						return;
					}

					json = JSON.parse(xhr.responseText);

					if (!json || typeof json.location != 'string') {
						failure('Invalid JSON: ' + xhr.responseText);
						return;
					}

					success(json.location);
				};

				formData = new FormData();
				formData.append('file', blobInfo.blob(), blobInfo.filename());

				xhr.send(formData);
			}
		});
		<?if (!empty($tinymceaditionaltoolbars)){?>
			<?foreach ($tinymceaditionaltoolbars as $ind => $current){?>
		tinymce.init({
			setup : function(ed){
				//añado al contenedor la clase form-control
				ed.on('init', function(event) {
					ed.getContainer().className += ' form-control';
				});
				
				//botoón de h2
				ed.addButton('formath2', // name to add to toolbar button list
				{
					title : 'Crear h2', // tooltip text seen on mouseover
					//image : 'http://myserver/ma_button_image.png',
					text : 'H2',
					onclick : function()
					{
						ed.execCommand('FormatBlock', false, 'h2');
					}
				});
				
			},
			// No codificar con html_entities de character sets de otros idiomas (ruso)
			entity_encoding : "raw",
			content_css : "<?=$URL_ROOT?>assets/css/custom_tinymce.css?v=<?=CFGCURRENTVERSION;?>",
            selector: "<?=$current['selector']?>",
			language: "es",
			plugins: [
				 "image lists jbimages manu2link",
				 "fullscreen", "paste", "media"
		   ],
			paste_as_text: true,
			paste_auto_cleanup_on_paste : true,
			paste_strip_class_attributes: 'all',
			paste_remove_spans: true,
			paste_remove_styles: true,
			paste_text_linebreaktype: 'p',
		    link_title: false,
			target_list: false,
			default_link_target: "",
//			image_class_list: [
//        {title: 'Responsive', value: 'img-responsive'}],
			image_dimensions: false,
			<?=$current['toolbar']?>
			//media_live_embeds: true, // tinymce > 4.3 
			media_alt_source: false,
			media_poster: false,
			media_dimensions: false,
			video_template_callback: function(data) {
				//alert('llamando al callback '+data.source1); //solo funciona cuando no se mete una url, es raro.
				// return '<div class="embed-responsive embed-responsive-16by9">'+"\n"+'<iframe class="embed-responsive-item" src="' + data.source1 + '" ></iframe>'+"\n"+'</div>';
				return '<p>'+"\n"+'<iframe src="' + data.source1 + '" ></iframe>'+"\n"+'</p>';
			},
			menubar: false,
			relative_urls:false,
			remove_script_host: false,
			/*
			file_picker_callback: function(field_name, url, type, win) {
          
				tinymce.activeEditor.windowManager.close();

				tinymce.activeEditor.windowManager.open({
					title: 'Upload an image',
					file: '<?=$URL_ROOT?>assets/js/tinymce/plugins/jbimages/dialog-v4.htm',
					width: 350,
					height: 135,
					buttons: [{
						text: 'Upload',
						classes: 'widget btn primary first abs-layout-item',
						disabled: true,
						onclick: 'close'
					}, {
						text: 'Close',
						onclick: 'close'
					}]
				});
			}
			*/
			/* without images_upload_url set, Upload tab won't show up*/
			 // images_upload_url: '<?=$URL_ROOT?>site/php/upload-handler.php',
			  
			  images_upload_credentials: true,

			  /* we override default upload handler to simulate successful upload*/
			images_upload_handler: function (blobInfo, success, failure) {
				var xhr, formData;

				xhr = new XMLHttpRequest();
				xhr.withCredentials = true;
				xhr.open('POST', '<?=$URL_ROOT?>site/php/upload-handler.php');
				
				xhr.onerror = function() {
					failure("Image upload failed due to a XHR Transport error. Code: " + xhr.status);
				};
				xhr.onload = function() {
					var json;

					if (xhr.status < 200 || xhr.status >= 300) {
						failure('HTTP Error: ' + xhr.status);
						return;
					}

					json = JSON.parse(xhr.responseText);

					if (!json || typeof json.location != 'string') {
						failure('Invalid JSON: ' + xhr.responseText);
						return;
					}

					success(json.location);
				};

				formData = new FormData();
				formData.append('file', blobInfo.blob(), blobInfo.filename());

				xhr.send(formData);
			}
        });	
			<?}?>
		<?}?>
    </script>
</head>