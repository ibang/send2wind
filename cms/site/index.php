<?
require_once("../php/init.php");
require_once("../site/php/inc-news.php");
require_once("../site/php/inc-contacts.php");
require_once("../site/php/inc-general.php");
$section="home";
$url=$txt->home->url;
$title=$txt->home->title;

// columnas y filas según los elementos activos
$showSectionsArray = array('news', 'contacts'); //lista de sectiones a mostrar
$totalcol = 12; //total de columnas
$maxcolinrow = 3; //máximo de columnas por fila permitido
$ocupiedcol = getGrantedSections($showSectionsArray, $totalcol, $maxcolinrow);
//fin columnas y filas

?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="home">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1><?=$txt->home->h1?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="welcome">
				<h2 class="title"><?=$txt->home->welcome->title?></h2>
				<p class="subtitle"><?=$txt->home->welcome->subtitle?></p>
			</div>
		</div>
	</div>
	<?if (!empty($showSectionsArray)){?>
		<?$rowocupiedcoln = array_shift($ocupiedcol);?>
	<div class="row">
		<?foreach ($showSectionsArray as $nind => $sectionName){?>
		<div class="col-md-<?=$rowocupiedcoln;?>">
			<section>
				<h3><span class="number"><?=${$sectionName.'_cnt'};?></span> <span class="title"><?=$txt->{$sectionName}->h1?></span></h3>
				<p class="link"><a class="btn btn-primary plus" href="<?=$URL_ROOT?>site/<?=$sectionName;?>/add.php"><?=$txt->{$sectionName}->link?></a></p>
			</section>
		</div>
			<?if (!(($nind+1)%$maxcolinrow) AND !empty($showSectionsArray[$nind+1])){?>
				<?$rowocupiedcoln = array_shift($ocupiedcol);?>
	</div>
	<div class="row">
			<?}?>
		<?}?>
	</div>
	<?}?>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>