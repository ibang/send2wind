<?
require_once("../../php/init.php");
require_once("../../site/php/inc-general.php");
require_once("../../php/class.upload.php");
require_once("../../site/php/inc-news-add.php");
$section="news";
checkPermissions($section);
$title=$txt->news->add->title;
$js[]="change.js";
$js[]="countdown.js";
$js[]="add_fileinput.js";
$js[]="add_filepdfinput.js";
//$js[]="typeahead.js";
$js[]="news_categories.js";
//addToLog('ESTADO: '.$error.', datos: '.print_r($news_lang, true));
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="news">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="<?=(!empty($code)?'col-md-8':'col-md-12');?>">
			<h1><?=(empty($code)?$txt->news->add->h1:$txt->news->update->h1)?></h1>
		</div>
		<?if (!empty($code)){?>
		<div class="col-md-4">
			<p class="link link-add"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#erase-all"><?=$txt->form->submit->all->erase?></button></p>
			<div class="modal fade" id="erase-all" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <p class="h2 modal-title"><?=$txt->form->submit->all->erase?></p>
					</div>
					<div class="modal-body">
					  <p><?=$txt->form->submit->all->modal?></p>
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal"><?=$txt->nav->cancel?></button>
					  <a class="btn btn-success" href="?code=<?=$code?>&lang=all&action=delete"><?=$txt->nav->confirm?></a>
					</div>
				  </div>
				</div>
			 </div>
		</div>
		<?}?>
	</div>
	<div class="row">
		<div class="col-md-12">
			<? if ($warning>0){?>
			<div class="alert alert-warning">
				<?if ($attention["active"]["draft"]==1){?>
				<p><?=$txt->form->alert->warning->title?> <strong><?=$txt->form->alert->language->$lang_active?></strong></p>
				<?}?>
			</div>
			<?}?>
			<?if ($update=="ok"){?>
			<div class="alert alert-success">
				<p><?=$txt->form->alert->success->update?> <strong><?=$txt->form->alert->language->$lang_active?></strong></p>
			</div>
			<?};?>
			<?if ($insert=="ok"){?>
			<div class="alert alert-success">
				<p><?=$txt->form->alert->success->title?> <strong><?=$txt->form->alert->language->$lang_active?></strong></p>
			</div>
			<?};?>
		</div>
	</div>
	<? if ($error>0 ){?>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger">
				<p><?=$txt->form->alert->error->title?></p>
				<p><?=$txt->form->alert->error->text?> <strong><?=$txt->form->alert->language->$lang_active?></strong></p>
				<ol>
					<?if ($validate["headline"]["duplicated"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->headline->title?></strong> <?=$txt->form->alert->error->duplicated?></li>
					<?}?>
					<?if ($validate["headline"]["isnumber"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->headline->title?></strong> <?=$txt->form->alert->error->isnumber?></li>
					<?}?>
					<?if ($validate["images"]["hasnoelements"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->images->title?></strong> <?=$txt->form->alert->error->hasnoelements?></li>
					<?}?>
					<?if ($validate["date"]["nodate"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->date->title?></strong> <?=$txt->form->alert->error->nodate?></li>
					<?}?>
					<?if ($validate["images"]["imageerror"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->images->title?></strong> <?=$txt->form->alert->error->required?></li>
					<?}?>
					<?if ($validate["images"]["imageerror"]==2){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->images->title?></strong> <?=$txt->form->alert->error->uploadimage?></li>
					<?}?>
					<?if ($validate["images"]["imageerror"]==3){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->images->title?></strong> <?=$txt->form->alert->error->imageerror?></li>
					<?}?>
					<?if ($validate["file"]["upload"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->file->title?></strong> <?=$txt->form->alert->error->upload?></li>
					<?}?>
					<?if ($validate["file"]["upload"]==2){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->file->title?></strong> <?=$txt->form->alert->error->uploadfile?></li>
					<?}?>
					<?if ($validate["file"]["upload"]==3){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->news->file->title?></strong> <?=$txt->form->alert->error->processfile?></li>
					<?}?>
				</ol>
			</div>
		</div>
	</div>
	<?}?>
	<div class="row">
		<div class="col-md-12">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?foreach ($langs as $current_lang){?>
					<li role="presentation"  class="nav-item" ><a href="#<?=$current_lang['lang']?>" aria-controls="<?=$current_lang['lang']?>" role="tab" data-toggle="tab" class="nav-link<?if (($lang_active==$current_lang['lang'])){?> active<?}?>" ><?=$current_lang['lang']?></a></li>
					<?}?>
				</ul>
				<div class="tab-content">
					<?foreach ($langs as $current_lang){?>
					<div role="tabpanel" class="tab-pane fade<?if ($lang_active==$current_lang['lang']){?> show active<?}?>" id="<?=$current_lang['lang']?>">
						<form  role="form" action="add.php" method="post" id="form-news-<?=$current_lang['lang']?>" enctype="multipart/form-data" autocomplete="off">
							<div class="row">
								<div class="col-md-8">
									<input type="hidden" id="session_id" name="session_id" value="<?=$session_id;?>">
									<input type="hidden" id="lang" name="lang" value="<?=$current_lang['id']?>">
									<?if ($news_lang[$current_lang['lang']]["id"]!=""){?><input type="hidden" name="id" value="<?=$news_lang[$current_lang['lang']]["id"];?>"><?}?>
									<?if ($code!=""){?><input type="hidden" id="code" name="code" value="<?=$code;?>"><?}?>
									<fieldset>
										<legend><?=$txt->form->news->legend?></legend>
										<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["headline"])){?>has-error<?}?>" >
										  <label for="headline"><?=$txt->form->news->headline->title?> <span>*</span></label>
										  <textarea class="form-control" rows="2" name="headline" id="headline" placeholder="<?=$txt->form->news->headline->holder?>" title="<?=$txt->form->news->headline->title?>" required><?=htmlspecialchars(stripslashes($news_lang[$current_lang['lang']]["headline"]));?></textarea>  
										</div>
										<!--
										<div class="form-group">
										  <label for="category"><?=$txt->form->news->category->title?> <span>*</span></label>
										  <input type="text" class="form-control typeahead" name="category" id="category" placeholder="<?=$txt->form->news->category->holder?>" title="<?=$txt->form->news->category->title?>" value="<?=htmlspecialchars(stripslashes($news_lang[$current_lang['lang']]["category"]));?>" required>				    
										</div>
										-->
										<div class="form-group">
										  <label for="intro"><?=$txt->form->news->intro->title?> <span>*</span></label>
										  <textarea class="form-control" rows="5" name="intro" data-countdown="150"  id="intro" placeholder="<?=$txt->form->news->intro->holder?>" title="<?=$txt->form->news->intro->title?>" required><?=stripslashes($news_lang[$current_lang['lang']]["intro"]);?></textarea>			    
										</div>
										<div class="form-group">
										  <label for="body"><?=$txt->form->news->body->title?></label>
										  <textarea class="form-control ckeditor" rows="18" name="body" id="body" placeholder="<?=$txt->form->news->body->holder?>" title="<?=$txt->form->news->body->title?>"><?=stripslashes(cleanEditorHTML($news_lang[$current_lang['lang']]["body"]));?></textarea>		    
										</div>
									</fieldset>

                                    <fieldset>
                                        <legend><?=$txt->form->img->media;?></legend>
										
										<div id="uploadFileList<?=$current_lang['id']?>" class="js-filepdfuploads">
											<?if (!empty($news_lang[$current_lang["lang"]]['files'])){?>
												<?foreach ($news_lang[$current_lang["lang"]]['files'] as $currentFile){?>
											<div class="form-group file<?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["files"])){?> has-error<?}?>" id="form-group-filepdf-<?=$currentFile['id-file'];?>" >
											  <label for="<?=$currentFile['id-file'];?>" class="gallerynextimage"><?=$txt->form->file->title?></label>
											  
												<input type="file" class="fileauto" name="fileattach[]" id="<?=$currentFile['id-file'];?>" title="<?=$txt->form->file->title?>"  data-urlwebroot="<?echo $URL_WEB_ROOT?>" data-name="<?=$currentFile['file'];?>" value="<?=$currentFile['path-file'];?><?=$currentFile['file'];?>" accept="application/pdf" />
											 										  
											</div>
											<div class="form-group file" id="form-group-link-<?=$currentFile['id-file'];?>">
											  <label for="link-<?=$currentFile['id-file'];?>"><?=$txt->form->file->link->title?> <span>*</span></label>
											  <input type="text" class="form-control" name="link-<?=$currentFile['id-file'];?>" id="link-<?=$currentFile['id-file'];?>" placeholder="<?=$txt->form->file->link->holder?>" title="<?=$txt->form->file->link->title?>" value="<?=htmlspecialchars(stripslashes($currentFile['link-file']));?>" required="" />	  	   											   
											</div>
												<?}?>
											<?} else {
												$current_file_id = time();
											?>
											<div class="form-group file<?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["files"])){?> has-error<?}?>" id="form-group-filepdf-<?=$currentFile['id-file'];?>" id="form-group-filepdf-<?=$current_file_id?>">
												<label for="js-filepdf-<?=$current_file_id?>" class="gallerynextimage"><?=$txt->form->file->title?></label>
												<input type="file" class="fileauto nofile" id="js-filepdf-<?=$current_file_id?>" name="fileattach[]" accept="application/pdf" />
											</div>
											<div class="form-group file" id="form-group-link-<?=$currentFile['id-file'];?>">
											  <label for="link-js-filepdf-<?=$current_file_id?>"><?=$txt->form->file->link->title?> </label>
											  <input type="text" class="form-control" name="link-js-filepdf-<?=$current_file_id?>" id="link-js-filepdf-<?=$current_file_id?>" placeholder="<?=$txt->form->file->link->holder?>" title="<?=$txt->form->file->link->title?>" value="" />	  	   											   
											</div>
											<?}?>
											<pre class="filepdfinfo infofile hidden"><?=sprintf($txt->form->news->files->info, '10MB');?></pre>
											<pre class="filepdfinfo maxfilesize hidden">10000</pre>
                                            
                                            <div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["files"])){?>has-error<?}?>">
											<button type="button" class="btn btn-primary plus js-addfilepdfupload newimagebutton" ><?=$txt->form->file->new?></button>
										</div>
										</div>
                                    </fieldset>

									<fieldset>
										<legend><?=$txt->form->seo->legend?></legend>
										<div class="form-group">
										  <label for="meta-title"><?=$txt->form->seo->title->title?> <span>*</span></label>
										  <input type="text" class="form-control" data-countdown="55" name="meta-title" id="meta-title" placeholder="<?=$txt->form->seo->title->holder?>" title="<?=$txt->form->seo->title->title?>" value="<?=htmlspecialchars(stripslashes($news_lang[$current_lang['lang']]["meta-title"]));?>" required>									  
										</div>
										<div class="form-group">
										  <label for="meta-desc"><?=$txt->form->seo->desc->title?> <span>*</span></label>
										  <textarea class="form-control" rows="3" data-countdown="155" name="meta-desc" id="meta-desc" placeholder="<?=$txt->form->seo->desc->holder?>" title="<?=$txt->form->seo->desc->title?>" required><?=htmlspecialchars(stripslashes($news_lang[$current_lang['lang']]["meta-description"]));?></textarea>	
										</div>
										<div class="form-group">
										  <label for="meta-key"><?=$txt->form->seo->key->title?></label>
										  <textarea class="form-control" rows="3" name="meta-key" id="meta-key" placeholder="<?=$txt->form->seo->key->holder?>" title="<?=$txt->form->seo->key->title?>"><?=(empty($news_lang[$current_lang['lang']]["meta-keywords"])?$news_lang[$current_lang['lang']]["default-keywords"]:htmlspecialchars(stripslashes($news_lang[$current_lang['lang']]["meta-keywords"])));?></textarea>	 
										</div>
									</fieldset>
									<fieldset>
										<legend><?=$txt->form->status->legend?></legend>
										<div class="form-group">
											<label for="active" class="d-none"><?=$txt->form->status->legend?></label>
											<select class="form-control" name="active" id="active" required>
												<option value="Yes" <? if ($news_lang[$current_lang['lang']]['active']=='Yes' OR empty($news_lang[$current_lang['lang']]['active'])){?>selected="selected"<?}?> ><?=$txt->form->status->active?></option>
												<option value="Draft" <? if ($news_lang[$current_lang['lang']]['active']=='Draft'){?>selected="selected"<?}?>><?=$txt->form->status->draft?></option>
											</select>
										</div>
									</fieldset>
								</div>
								<div class="col-md-4">
									<fieldset class="general">
										<legend><?=$txt->form->general->title?></legend>
										
										<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["type"])){?>has-error<?}?>">
										  <label for="type"><?=$txt->form->news->type->title?> <span>*</span></label>
										  <select class="form-control" name="type" id="type" placeholder="<?=$txt->form->news->type->holder?>" title="<?=$txt->form->news->type->title?>" required data-change="type">
											<option value="news" <?if(empty($news_lang[$lang_active]["type"]) OR $news_lang[$lang_active]["type"]=='news'){?>selected="selected"<?}?> ><?=$txt->form->news->type->news;?></option>
										  </select>
										</div>
										<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["date"])){?>has-error<?}?>">
										  <label for="date" id="date-news" class="<?if(!empty($news_lang[$lang_active]["type"]) AND $news_lang[$lang_active]["type"]=='eventos'){?>d-none<?}?>"><?=$txt->form->news->{'date'}->title?> <span>*</span></label>
										  <label for="date" id="date-eventos" class="<?if(empty($news_lang[$lang_active]["type"]) OR $news_lang[$lang_active]["type"]=='news'){?>d-none<?}?>"><?=$txt->form->news->{'date'}->eventos?> <span>*</span></label>
										  <input type="date" class="form-control" name="date" id="date" placeholder="<?=$txt->form->news->{'date'}->holder?>" title="<?=$txt->form->news->{'date'}->title?>" value="<?=$news_lang[$lang_active]["date"];?>" required />
										</div>
										<div class="form-group<?if(empty($news_lang[$lang_active]["type"]) OR $news_lang[$lang_active]["type"]=='news'){?> d-none<?}?>" <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["location"])){?>has-error<?}?>">
											<label for="location"><?=$txt->form->news->location->title?> <span>*</span></label>
											<input type="text" class="form-control" name="location" id="location" placeholder="<?=$txt->form->news->location->holder?>" title="<?=$txt->form->news->location->title?>" value="<?=htmlspecialchars(stripslashes($news_lang[$lang_active]["location"]));?>" <?if(!empty($news_lang[$lang_active]["type"]) AND $news_lang[$lang_active]["type"]=='eventos'){?> required <?}?> />	  	   											   
										</div>
										<div class="form-group<?if(empty($news_lang[$lang_active]["type"]) OR $news_lang[$lang_active]["type"]=='news'){?><?}?>" <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["link"])){?>has-error<?}?>">
											<label for="link"><?=$txt->form->news->link->title?></label>
											<input type="text" class="form-control" name="link" id="link" placeholder="http://..." title="<?=$txt->form->news->link->title?>" value="<?=htmlspecialchars(stripslashes($news_lang[$lang_active]["link"]));?>"/>	
											<label for="link-text" style="margin-top: 12px;">Texto del enlace</label>
											<input type="text" class="form-control" name="link-text" id="link-text" placeholder="Texto del enlace" title="" value="<?=htmlspecialchars(stripslashes($news_lang[$lang_active]["link-text"]));?>"/>	  	   											   
										</div>
										
										
									
										
										
										<!--
										<div class="form-group file <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["file"])){?>has-error<?}?>">
											<label for="file"><?=$txt->form->news->file->title?></label>
										  <div class="selector">
										  	<input type="file" name="file" id="file" title="<?=$txt->form->news->file->title?>" accept="application/pdf" />
										  </div>
										  <p class="info"><?=$txt->form->news->file->info?></p>
										  <?if ($news_lang[$lang_active]["file"]!=""){?>
										  <p class="info file"><a href="<?echo $URL_WEB_ROOT?>uploads/news/<?=$news_lang[$lang_active]["file"]?>" target="_blank"><?=stripslashes($news_lang[$lang_active]["file"])?></a></p>
										  <input type="checkbox" value="<?=$news_lang[$lang_active]["file"]?>" name="file-del" id="file-del"  />
											<label for="file-del"><?=$txt->form->news->{'delete-file'}?> </label>
										   <?}?>
										</div>
										<div class="form-group file">
										  <label for="file-linktext"><?=$txt->form->news->{'file-linktext'}->title?> <span>*</span></label>
										  <input type="text" class="form-control" name="file-linktext" id="link-1" placeholder="<?=$txt->form->news->{'file-linktext'}->holder?>" title="<?=$txt->form->news->{'file-linktext'}->title?>" value="<?=htmlspecialchars(stripslashes($news_lang[$lang_active]["file-linktext"]));?>" />	  	   											   
										</div>
										-->
										
										<!--<p class="gallery"><?=$txt->form->img->gallery;?></p>-->
										<!-- image gallery -->
										<div id="uploadList<?=$current_lang['id']?>" class="js-fileuploads">
											<?if (!empty($news_lang[$current_lang["lang"]]['images'])){?>
												<?foreach ($news_lang[$current_lang["lang"]]['images'] as $currentImage){?>
											<div class="form-group file<?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["images"])){?> has-error<?}?>" id="form-group-file-<?=$currentImage['id-image'];?>" >
											  <label for="<?=$currentImage['id-image'];?>" class="gallerynextimage"><?=$txt->form->img->title?> <span>*</span></label>
											  
												<input type="file" class="imageauto" name="imageattach[]" id="<?=$currentImage['id-image'];?>" title="<?=$txt->form->img->title?>"  data-urlwebroot="<?echo $URL_WEB_ROOT?>" value="<?=$currentImage['path-image'];?><?=$currentImage['image'];?>" accept="image/*" />
											 										  
											</div>
											<div class="form-group file" id="form-group-alt-<?=$currentImage['id-image'];?>">
											  <label for="alt-<?=$currentImage['id-image'];?>"><?=$txt->form->img->alt->title?> <span>*</span></label>
											  <input type="text" class="form-control" name="alt-<?=$currentImage['id-image'];?>" id="alt-<?=$currentImage['id-image'];?>" placeholder="<?=$txt->form->img->alt->holder?>" title="<?=$txt->form->img->alt->title?>" value="<?=htmlspecialchars(stripslashes($currentImage['alt-image']));?>" required="" />	  	   											   
											</div>
												<?}?>
											<?} else {
												$current_image_id = time();
											?>
											<div class="form-group file<?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["images"])){?> has-error<?}?>" id="form-group-file-<?=$currentImage['id-image'];?>" id="form-group-file-<?=$current_image_id?>">
												<label for="js-file-<?=$current_image_id?>" class="gallerynextimage"><?=$txt->form->img->title?> <span>*</span></label>
												<input type="file" class="imageauto noimage" id="js-file-<?=$current_image_id?>" name="imageattach[]" accept="image/*" />
											</div>
											<div class="form-group file" id="form-group-alt-<?=$currentImage['id-image'];?>">
											  <label for="alt-js-file-<?=$current_image_id?>"><?=$txt->form->img->alt->title?> <span>*</span></label>
											  <input type="text" class="form-control" name="alt-js-file-<?=$current_image_id?>" id="alt-js-file-<?=$current_image_id?>" placeholder="<?=$txt->form->img->alt->holder?>" title="<?=$txt->form->img->alt->title?>" value="" required="" />	  	   											   
											</div>
											<?}?>
											<pre class="fileinfo infoimage hidden"><?=sprintf($txt->form->news->images->info, NEWSIMAGECROPHORIZONTAL, NEWSIMAGECROPVERTICAL);?></pre>
											<pre class="fileinfo maximagesize hidden">10000</pre>
										</div>
										<!--
										<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["images"])){?>has-error<?}?>">
											<button type="button" class="btn btn-primary plus js-addfileupload newimagebutton" ><?=$txt->form->img->new?></button>
										</div>
										<p class="info"><?=$txt->form->ismain->title?></p>
										-->
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									  <p class="required"><span>*</span> <?=$txt->form->required->title?></p>
									  <p><input type="submit" name="submit" class="btn btn-primary" value="<?=$news_lang[$current_lang['lang']]["id"]!=""?$txt->form->submit->{$current_lang['lang']}->update:$txt->form->submit->{$current_lang['lang']}->publish?>"> 
									  <?if ($news_lang[$current_lang['lang']]["id"]!=""){?><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#erase-<?=$current_lang['lang']?>"><?=$txt->form->submit->{$current_lang['lang']}->erase?></button><?}?></p>
									  <div class="modal fade" id="erase-<?=$current_lang['lang']?>" tabindex="-1" role="dialog" aria-hidden="true">
									    <div class="modal-dialog">
									      <div class="modal-content">
									        <div class="modal-header">
									          <p class="h2 modal-title"><?=$txt->form->submit->{$current_lang['lang']}->erase?></p>
									        </div>
									        <div class="modal-body">
									          <p><?=$txt->form->submit->{$current_lang['lang']}->modal?></p>
									        </div>
									        <div class="modal-footer">
									          <button type="button" class="btn btn-default" data-dismiss="modal"><?=$txt->nav->cancel?></button>
									          <a class="btn btn-success" href="?code=<?=$code?>&lang=<?=$current_lang['id']?>&action=delete"><?=$txt->nav->confirm?></a>
									        </div>
									      </div>
									    </div>
									  </div>
									</div>
								</div>
							</div>
						</form>
					</div>
				<?}?>
				</div>
			</div>
		</div>
	</div>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>