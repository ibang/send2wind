<?
require_once("../../php/init.php");
require_once("../../php/class.upload.php");
require_once("../../site/php/inc-news.php");
$section="news";
checkPermissions($section);
$title=$txt->news->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="news">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<h1><?=$txt->news->h1?></h1>
		</div>
		<div class="col-sm-4 col-md-4">
			<p class="link link-add"><a class="btn btn-primary plus" href="<?=$URL_ROOT?>site/news/add.php"><?=$txt->news->link?></a></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?if (!empty($news)){?>
			<div class="table-responsive">
				<table class="table table-striped">
				     <thead>
				       <tr>
				         <th><?=$txt->news->table->title?></th>
				         <th class="d-none d-sm-table-cell"><?=$txt->news->table->{'date'}?></th>
				         <th class="d-none d-sm-table-cell"><?=$txt->news->table->type;?></th>
				        <!-- <th class="d-none d-sm-table-cell"><?=$txt->news->table->category?></th>-->
				         <th class="language d-none d-sm-table-cell"><?=$txt->news->table->language?></th>
				       </tr>
				     </thead>
				     <tbody>
						<?foreach ($news as $current){?>
				     	<tr>
				     	  <td><a href="<?=$URL_ROOT?>site/news/add.php?code=<?=$current["code"]?>"><?=$current["headline"]?></a></td>
				     	  <td class="d-none d-sm-table-cell"><?=$current["date"]?></td>
						  <td class="d-none d-sm-table-cell"><?=$txt->form->news->type->{$current["type"]};?></td>
				     	 <!-- <td class="d-none d-sm-table-cell"><?=$current["category"];?></td>-->
				     	  <td class="language d-none d-sm-table-cell"><?=$current["lang"]?></td>
				     	</tr>
							<?};?>
				     </tbody>
				</table>
			</div>
			<?};?>
		</div>
	</div>
	<? if ($actual_page>1 or $actual_page<$total_pages){?>
	<div class="row">
		<div class="col-md-12">
			<nav>
			  <ul class="pager list-unstyled justify-content-between pt-2 d-flex flex-row pb-0">
			    <? if ($actual_page<$total_pages){?><li class="previous"><a href="<?=$URL_ROOT?>site/news/<?='?page='.($actual_page+1)?>"><?=$txt->nav->prev?></a></li><?}?>
				 <? if ($actual_page>1){?><li class="next"><a href="<?=$URL_ROOT?>site/news/<?if (($actual_page-1)>1){echo '?page='.($actual_page-1);};?>"><?=$txt->nav->next?></a></li><?}?>
			  </ul>
			</nav>
		</div>
	</div>
	<?}?>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>