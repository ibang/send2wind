<?
require_once("../php/init.php");
$title=$txt->errors->error400->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body>
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div id="error">
	<!-- <?=$error;?> -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<i class="fa fa-frown-o"></i>
				<h1><?=$txt->errors->error400->h1?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="subtitle"><?=$txt->errors->error400->subtitle?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p><a href="<?=$URL_ROOT?>site/" class="btn btn-primary"><?=$txt->errors->error400->link?></a></p>
			</div>
		</div>
	</div>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>