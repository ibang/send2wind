<?
require_once("../../php/init.php");
require_once("../../site/php/inc-general.php");
require_once("../../php/class.upload.php");
require_once("../../site/php/inc-downloads-add.php");
$section="downloads";
checkPermissions($section);
$title=$txt->downloads->add->title;
$js[]="change.js";
$js[]="countdown.js";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="downloads">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1><?=(empty($code)?$txt->downloads->add->h1:$txt->downloads->update->h1)?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<? if ($warning>0){?>
			<div class="alert alert-warning">
				<?if ($attention["active"]["draft"]==1){?>
				<p><?=$txt->form->alert->warning->title?> <strong><?=$txt->form->alert->language->{$lang_active}?></strong></p>
				<?}?>
			</div>
			<?}?>
			<?if ($update=="ok"){?>
			<div class="alert alert-success">
				<p><?=$txt->form->alert->success->update?> <strong><?=$txt->form->alert->language->{$lang_active}?></strong></p>
			</div>
			<?};?>
			<?if ($insert=="ok"){?>
			<div class="alert alert-success">
				<p><?=$txt->form->alert->success->title?> <strong><?=$txt->form->alert->language->{$lang_active}?></strong></p>
			</div>
			<?};?>
		</div>
	</div>
	<? if ($error>0 ){?>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger">
				<p><?=$txt->form->alert->error->title?></p>
				<p><?=$txt->form->alert->error->text?> <strong><?=$txt->form->alert->language->{$lang_active}?></strong></p>
				<ol>
					<?if (!empty($validate["headline"]["duplicated"])){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=sprintf($txt->form->alert->error->twofields, $txt->form->downloads->headline->title, $txt->form->downloads->filelanguage->title)?></strong> <?=$txt->form->alert->error->duplicated?></li>
					<?}?>
					<?if (!empty($validate["headline"]["isnumber"])){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->downloads->headline->title?></strong> <?=$txt->form->alert->error->isnumber?></li>
					<?}?>
					<?if (!empty($validate["file"]["upload"])){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->downloads->file->title?></strong> <?=$txt->form->alert->error->upload?></li>
					<?}?>
					<?if ($validate["category"]["noelement"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->downloads->category->title?></strong> <?=$txt->form->alert->error->required?></li>
					<?}?>
				</ol>
			</div>
		</div>
	</div>
	<?}?>
	<div class="row">
		<div class="col-md-12">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?foreach ($langs as $current_lang){?>
					<li role="presentation" class="nav-item" ><a href="#<?=$current_lang['lang']?>" aria-controls="<?=$current_lang['lang']?>" role="tab" data-toggle="tab" class="nav-link<?if (($lang_active==$current_lang['lang'])){?> active<?}?>"><?=$current_lang['lang']?></a></li>
					<?}?>
				</ul>
				<div class="tab-content">
					<?foreach ($langs as $current_lang){?>
					<div role="tabpanel" class="tab-pane fade<?if ($lang_active==$current_lang['lang']){?> show active<?}?>" id="<?=$current_lang['lang']?>">
						<form  role="form" action="add.php" method="post" id="form-downloads-<?=$current_lang['lang']?>" enctype="multipart/form-data" autocomplete="off">
							<div class="row">
								<div class="col-md-8">
									<input type="hidden" name="lang" value="<?=$current_lang['id']?>" />
									<?if ($downloads_lang[$current_lang['lang']]["id"]!=""){?><input type="hidden" name="id" value="<?=$downloads_lang[$current_lang['lang']]["id"];?>" /><?}?>
									<?if ($code!=""){?><input type="hidden" name="code" value="<?=$code;?>"><?}?>
									<fieldset>
										<legend><?=$txt->form->downloads->legend?></legend>
										<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["headline"])){?>has-error<?}?>" >
										  <label for="headline"><?=$txt->form->downloads->headline->title?> <span>*</span></label>
										  <input type="text" class="form-control" name="headline" id="headline" placeholder="<?=$txt->form->downloads->headline->holder?>" title="<?=$txt->form->downloads->headline->title?>" value="<?=htmlspecialchars(stripslashes($downloads_lang[$current_lang['lang']]["headline"]));?>" required>  
										</div>
										<!--
										<div class="form-group">
										  <label for="subtitle"><?=$txt->form->downloads->subtitle->title?> <span>*</span></label>
										  <input type="text" class="form-control" name="subtitle" id="subtitle" placeholder="<?=$txt->form->downloads->subtitle->holder?>" title="<?=$txt->form->downloads->subtitle->title?>" value="<?=htmlspecialchars(stripslashes($downloads_lang[$current_lang['lang']]["subtitle"]));?>" required>
										</div>
										
										<input type="hidden" name="subtitle" id="subtitle" value="" />
										<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["headline"])){?>has-error<?}?>" >
										  <label for="filelanguage"><?=$txt->form->downloads->filelanguage->title?> <span>*</span></label>
										  <input type="text" class="form-control" name="filelanguage" id="filelanguage" placeholder="<?=$txt->form->downloads->filelanguage->holder?>" title="<?=$txt->form->downloads->filelanguage->title?>" value="<?=htmlspecialchars(stripslashes($downloads_lang[$current_lang['lang']]["filelanguage"]));?>" required>
										</div>
                                        -->
									</fieldset>
									<fieldset>
										<legend><?=$txt->form->status->legend?></legend>
										<div class="form-group">
											<label for="active" class="d-none"><?=$txt->form->status->legend?></label>
											<select class="form-control" name="active" id="active" required>
												<option value="Yes" <? if ($downloads_lang[$current_lang['lang']]['active']=='Yes' OR empty($downloads_lang[$current_lang['lang']]['active'])){?>selected="selected"<?}?> ><?=$txt->form->status->active?></option>
												<option value="Draft" <? if ($downloads_lang[$current_lang['lang']]['active']=='Draft'){?>selected="selected"<?}?>><?=$txt->form->status->draft?></option>
											</select>
										</div>
									</fieldset>
								</div>
								<div class="col-md-4">
									<fieldset class="general">
										<legend><?=$txt->form->general->title?></legend>

                                        <div class="form-group">
										  <label for="category"><?=$txt->form->downloads->category->title?> <span>*</span></label>
										  <input type="text" class="form-control" name="category" id="category" placeholder="<?=$txt->form->news->category->holder?>" title="<?=$txt->form->news->category->title?>" value="<?=htmlspecialchars(stripslashes($downloads_lang[$current_lang['lang']]["category"]));?>" required>				    
										</div>

										<div class="form-group file">
										  <label for="file1"><?=$txt->form->downloads->file->title?> <?=$txt->form->downloads->file->n1?> <span>*</span></label>
										  <div class="selector">
										  	<input type="file" name="file1" id="file1" title="<?=$txt->form->downloads->file->title?>" <?if ($downloads_lang[$lang_active]["file1"]==""){?> required <?}?> >
										  </div>
										  <p class="info"><?=$txt->form->downloads->file->info?></p>
										  <?if ($downloads_lang[$lang_active]["file1"]!=""){?>
										  <p><a href="<?echo $URL_WEB_ROOT?>uploads/downloads/<?echo $downloads_lang[$lang_active]["file1"]?>" target="_blank"><?echo $downloads_lang[$lang_active]["file1"]?></a></p>
											<?if ($downloads_lang[$lang_active]["image1"]!=""){?>
										  <p><img class="img-responsive" src="<?echo $URL_WEB_ROOT?>uploads/downloads/preview/<?echo $downloads_lang[$lang_active]["image1"]?>" alt=""/></p>
											<?}?>
										   <?}?>
										</div>
										<div class="form-group d-none">
											<label for="is_public" ><?=$txt->form->downloads->ispublic->title?></label>
											<select class="form-control" name="is_public" id="is_public">
												<option value="all" selected="selected"><?=$txt->form->downloads->ispublic->{'all'}?></option>
												<option value="public" <? if ($downloads_lang[$lang_active]['is_public']=='public'){?>selected="selected"<?}?>><?=$txt->form->downloads->ispublic->{'public'}?></option>
												<option value="private" <? if ($downloads_lang[$lang_active]['is_public']=='private'){?>selected="selected"<?}?>><?=$txt->form->downloads->ispublic->{'private'}?></option>
											</select>
										</div>
										
										
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									  <p class="required"><span>*</span> <?=$txt->form->required->title?></p>
									  <p><input type="submit" name="submit" class="btn btn-primary btn-lg" value="<?=$downloads_lang[$current_lang['lang']]["id"]!=""?$txt->form->submit->{$current_lang['lang']}->update:$txt->form->submit->{$current_lang['lang']}->publish?>"> 
									  <?if ($downloads_lang[$current_lang['lang']]["id"]!=""){?><button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#erase-<?=$current_lang['lang']?>"><?=$txt->form->submit->{$current_lang['lang']}->erase?></button><?}?></p>
									  <div class="modal fade" id="erase-<?=$current_lang['lang']?>" tabindex="-1" role="dialog" aria-hidden="true">
									    <div class="modal-dialog">
									      <div class="modal-content">
									        <div class="modal-header">
									          <p class="h2 modal-title"><?=$txt->form->submit->{$current_lang['lang']}->erase?></p>
									        </div>
									        <div class="modal-body">
									          <p><?=$txt->form->submit->{$current_lang['lang']}->modal?></p>
									        </div>
									        <div class="modal-footer">
									          <button type="button" class="btn btn-default" data-dismiss="modal"><?=$txt->nav->cancel?></button>
									          <a class="btn btn-success" href="?code=<?=$code?>&lang=<?=$current_lang['id']?>&action=delete"><?=$txt->nav->confirm?></a>
									        </div>
									      </div>
									    </div>
									  </div>
									</div>
								</div>
							</div>
						</form>
					</div>
				<?}?>
				</div>
			</div>
		</div>
	</div>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>