<?php
$action = isset($_GET['action'])?$_GET['action']:'';
$error = 0;
//------------------ Login ------------
	if ($action == 'login'){
		if ( $user = $db->get_login_user($_POST['email'], $_POST['password'])){
			$_SESSION['user'] = $user;
			session_write_close();
			header("Location:".$URL_ROOT.'site/');
			exit();
		}
		else{
			$validate["login"]["failed"]=1;
			$error++;
		}
	}
	elseif($action == 'logout'){
		if (!empty($_SESSION['user'])){
			unset($_SESSION['user']);
		}
		session_write_close();
		header("Location:".$URL_ROOT.'site/login.php');
		exit();
	}
?>