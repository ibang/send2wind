<?php
require_once($DOC_WEB_ROOT."site/php/inc-mail".PHPVERSION.".php");

if ($_GET["type"]){$contact["type_custom"]=strtoupper($_GET["type"]);}

if (!empty($_POST)){
	$contact=array(
				"firstname"=>$_POST["firstname"],
				"lastname"=>$_POST["lastname"],
				"email"=>$_POST["email"],
				"phone"=>$_POST["phone"],
				"message"=>$_POST["message"]
	);
	
	//---------- validaciones ----------------------
	$error=0;
		if ($contact["firstname"]==""){$validate["firstname"]=1;$error++;};
		if ($contact["lastname"]==""){$validate["lastname"]=1;$error++;};
		if ($contact["email"]==""){$validate["email"]=1;$error++;};
		if ($contact["phone"]==""){$validate["phone"]=1;$error++;};
		if ($contact["message"]==""){$validate["message"]=1;$error++;};
		
	if(!preg_match("/^([a-zA-Z0-9\._-])+@([a-zA-Z0-9\._-])+\.[a-zA-Z]{2,4}$/", $contact["email"])){$validate["email"]=1;$error++;};

	//---------------- email body (client & user)----------------		
	if ($error==0){

		$fields= array("{firstname}","{lastname}","{email}","{phone}","{message}");
		$replace_fields= array($contact["firstname"],$contact["lastname"],$contact["email"],$contact["phone"],$contact["message"]);
		
		$email_promueve=file_get_contents("$DOC_ROOT"."alerts/contact/promueve.html");
		$email_promueve=str_replace($fields, $replace_fields, $email_promueve);
		
		$email_user=file_get_contents("$DOC_ROOT"."alerts/contact/es.html");
		$email_user=str_replace($fields, $replace_fields, $email_user);
		
	//---------- mail client ----------------------
		$to_email = 'promueve@promueve3.com';
		if(sendEmail($to_email, 'Promueve HIRU S.L.', CONTACT_ADMIN_EMAIL_ADDRESS,  'Cliente', 'Nueva consulta. Promueve CMS', $email_promueve, true, $contact["email"], "Cliente")){
			$sent = "success";
		}
	//---------- mail user ----------------------
		if(sendEmail($contact["email"], 'Cliente', CONTACT_ADMIN_EMAIL_ADDRESS,  'Cliente', (string)$txt->help->form->thank->subject, $email_user, true, $to_email, "Promueve HIRU S.L.")){
			$sent = "success";
		}
		unset($contact);
	}
	
}
?>