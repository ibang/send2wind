<?php
$type = isset($_GET['type'])?$_GET['type']:'';
//------------------ Export to CSV File ------------
	if ($_GET['action'] and $_GET['action']=='export'){
		$export_contacts = $db->get_contacts_list($type);
		switch ($type){
			case 'contacto':
				$fields = array('Id', 'Firstname', 'Lastname', 'Email', 'Phone', 'Country', 'Province', 'Company', 'Message', 'Date', 'Language', 'Location', 'Type');
				break;
			default:
				$fields = array('Id', 'Firstname', 'Lastname', 'Email', 'Phone', 'Country', 'Province', 'Message', 'Date', 'Language', 'About', 'Company', 'Location', 'Type');
		}
		exportCSV_UTF16LE($fields, $export_contacts, 'contacts.csv' );
	}
//------------------ Contacts List ------------------	
	$contactspage = CMSLISTINGPAGESIZE;
	$contactscount=$db->count_contacts_list($type);
	if ($_GET["page"]){$actual_page=$_GET["page"];}else{$actual_page=1;}
	$total_pages=ceil($contactscount/$contactspage); 
	$limit="LIMIT ".($contactspage*($actual_page-1))." , ".$contactspage;
	$contacts=$db->get_contacts_list($type, $limit);
?>