<?php

function subir_imagen($i,$folder){
	//guardo la imagen original al tamaño correcto, recortando si es necesario
	$thumbwidth = 42;
	$thumbheight = 42;
	$ret = -2;
	$image = new upload($i);
	if ($image->uploaded) {
		$image->allowed	= array('image/*'); // o
		$image->file_max_size = '10M';
		
		$image->image_convert	= 'png';
		$image->image_resize	= true;
		$image->image_ratio_crop	= 'TL';
		$image->image_y	= $thumbheight;
		$image->image_x	= $thumbwidth;
		//$image->png_compression = 1; // [1 - 9]  1-> Menos compresión, 9-> compresión máxima
		$image->process($folder);
		if ($image->processed) {
			$ret	=	$image->file_dst_name;
			$image->clean();
		} else {
			addToLog('FRONT PROCESS Image Error: ' . $image->error);
			return  -3; // proccess error
		}
	}
	else{
		addToLog('FRONT UPLOAD Image Error: ' . $image->error);
	}
	return $ret; // image destination name or -2 (upload error)
}


$langs=$db->get_cms_active_langs();

if ($_GET["id"]){
	$id=$_GET["id"];
}

$error=0;
$warning=0;


if ($_GET["action"]=="delete"){
	//nadie puede borrar al administrador principal
	if($_GET["id"]!=1){
		$id=$_GET["id"];
		$users_del=$db->get_users($id);
		if ($users_del["avatar"]!=""){@unlink ("{$DOC_ROOT}uploads/users/".$users_del["avatar"]);};
		$db->delete_users($id);
	}
	
	header('Location: '.$URL_ROOT.'site/users/');
}

if ($_POST){
	//nadie puede borrar al administrador principal
	if($_SESSION['user']['id']!=1 AND $_GET["id"]==1){
		header('Location: '.$URL_ROOT.'site/users/');
	}
	
	$users_form=array(
			"id"=>$_POST["id"],
			"nickname"=>addslashes ($_POST["nickname"]),
			"name"=>addslashes ($_POST["name"]),
			"lastname"=>addslashes ($_POST["lastname"]),
			"email"=>addslashes ($_POST["email"]),
			"password"=>addslashes ($_POST["password"]),
			"perm_front"=>($_POST["perm_front"]=='Y'?'Y':'N'),
			"perm_products"=>($_POST["perm_products"]=='Y'?'Y':'N'),
			"perm_productscategories"=>($_POST["perm_productscategories"]=='Y'?'Y':'N'),
			"perm_downloads"=>($_POST["perm_downloads"]=='Y'?'Y':'N'),
			"perm_downloadscategories"=>($_POST["perm_downloadscategories"]=='Y'?'Y':'N'),
			"perm_projects"=>($_POST["perm_projects"]=='Y'?'Y':'N'),
			"perm_news"=>($_POST["perm_news"]=='Y'?'Y':'N'),
			"perm_contact"=>($_POST["perm_contact"]=='Y'?'Y':'N'),
			"perm_users"=>($_POST["perm_users"]=='Y'?'Y':'N'),
			"active"=>$_POST["active"]
			);

	if ($_POST["id"]!=""){
		
		//------update noticia------//
		$users_org = $db->get_users($users_form['id']);
		
		 //--una imagen--//
		if (!empty($_FILES["avatar"]) AND $_FILES["avatar"]["name"]!=NULL){  // se ha adjuntado una imagen
			$uploaded_file = subir_imagen($_FILES["avatar"],"{$DOC_ROOT}uploads/users/");
			if (!is_numeric($uploaded_file)){
				$users_form["avatar"] = $uploaded_file;
				if ($users_org["avatar"]!=""){
					@unlink ("{$DOC_ROOT}uploads/users/".$users_org["avatar"]);
				};
			}
			else{
				$users_form["avatar"] = $users_org["avatar"];
				$validate["avatar"]["upload"]= abs($uploaded_file); // 2 => upload error or 3 => proccess error
				$error++;
			}
		}else{ // si había y no se ha pasado ninguna, no cambia y cogemos la que había
			$users_form["avatar"] = $users_org["avatar"];
		}

		//--- check used headline --//
		if ((strtolower(addslashes($users_org["nickname"]))!=strtolower($users_form["nickname"])) or ($users_org["email"]!=$users_form["email"])){
			if ($db->headline_users_exist($users_form["nickname"], $users_form["email"], $users_form["id"])){
				$error++;
				$validate["headline"]["duplicated"]=1;
			}
			
		}
		
		//--- check perms selected --//
		if ($users_form['perm_front']=='N' AND $users_form['perm_products']=='N' AND $users_form['perm_productscategories']=='N' AND $users_form['perm_downloads']=='N' AND $users_form['perm_downloadscategories']=='N' AND $users_form['perm_projects']=='N' AND $users_form['perm_news']=='N' AND $users_form['perm_contact']=='N' AND $users_form['perm_users']=='N' ){
			$validate["perms"]["noselected"]=1;
			$error++;
		}
		
		if ($error AND (!empty($validate["headline"]) OR !empty($validate["perms"])) ){
			$users = $db->get_users($users_form["id"]);
			$users = $users_form;
			
			if (!empty($validate["headline"])){
				unset($users_form["nickname"]);
				unset($users_form["email"]);
			}
			
			if (!empty($validate["perms"])){
				unset($users_form["perm_front"]);
				unset($users_form["perm_products"]);
				unset($users_form["perm_productscategories"]);
				unset($users_form["perm_downloads"]);
				unset($users_form["perm_downloadscategories"]);
				unset($users_form["perm_projects"]);
				unset($users_form["perm_news"]);
				unset($users_form["perm_contact"]);
				unset($users_form["perm_users"]);
			}
		}
			
		$db->update_users($users_form);
		
		if ($error and empty($users) and !empty($validate["avatar"])){
			$users = $db->get_users($users_form["id"]);
			$users = $users_form;
		}
		
		if ($users_form['active']=='Draft'){ //borrador
			$warning++;
			$attention['active']['draft']=1;
		}
		$id=$users_form["id"];
		if (!$error){
			if ($warning){
				$warn = '&warning='.$warning.'&attention='.json_encode($attention);
			}
			else{
				$warn = '&update=ok';
			}
			header("Location: add.php?id=$id".$warn);
			exit();
		}
	}else{
		//------nueva usuario------//
		// id vacio
			
		//--- check used headline --//
		if ($db->headline_users_exist($users_form["nickname"], $users_form["email"])){
			$error++;
			$validate["headline"]["duplicated"]=1;
		}
		
		//--- check perms selected --//
		if ($users_form['perm_front']=='N' AND $users_form['perm_products']=='N' AND $users_form['perm_productscategories']=='N' AND $users_form['perm_downloads']=='N' AND $users_form['perm_downloadscategories']=='N' AND $users_form['perm_projects']=='N' AND $users_form['perm_news']=='N' AND $users_form['perm_contact']=='N' AND $users_form['perm_users']=='N' ){
			$validate["perms"]["noselected"]=1;
			$error++;
		}
		
		if ($error AND (!empty($validate["headline"]) OR !empty($validate["perms"]) )){
			$users = $users_form;
		}
		
		if (!$error){
			
			if (!empty($_FILES["avatar"]) AND  $_FILES["avatar"]["name"]!=NULL){
				$uploaded_file = subir_imagen($_FILES["avatar"],"{$DOC_ROOT}uploads/users/");
				if (!is_numeric($uploaded_file)){
					$users_form["avatar"] = $uploaded_file;
				}
				else{
					$users_form["avatar"] = '';
					$validate["avatar"]["upload"]= abs($uploaded_file) ;
					$error++;
				}
			}else{
				$users_form["avatar"] = '';
			}
			
			if (!$error){
				$users_form['id'] = $db->add_users ($users_form);
				
				if ($_POST['active']=='Draft'){ //borrador
					$warning++;
					$attention['active']['draft']=1;
				}
				
				$id=$users_form["id"];
				if ($warning){
					$warn = '&warning='.$warning.'&attention='.json_encode($attention);
				}
				else{
					$warn = '&insert=ok';
				}
				header("Location: add.php?id=$id".$warn);
				exit();
			}
			else{
				$users = $users_form;
				$users_form['avatar'] = '';
			}
		}
	}
}

//------------- extraigo noticia en todos los idiomas en los que esté -----
$users_lang = array();
if ($_GET or ($_POST and $error)){
	if (!$error){
		$users=$db->get_users($id);
		if ($_GET["update"]){
			$update=$_GET["update"];
		
		}
		if ($_GET["insert"]){
			$insert=$_GET["insert"];
		
		}
	}

	//------------- creo un array users_codigoidioma para cada idioma -----
	$users_lang = $users;
}


if (isset($_GET['warning']) AND !empty($_GET['warning'])){
	$warning = $_GET['warning'];
	$attention = json_decode($_GET['attention'], true);
}
?>