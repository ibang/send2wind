<?php
set_time_limit(0);

//------------------ ajax ----------------------------
$functionName=isset($_POST['fn'])?$_POST['fn']:'';
if ($functionName){
	echo call_user_func('_'.$functionName);
	exit();
}

// Función que ordena
function _categorysuggest(){
	global $txt, $db;
	$message='';
	$ok=false;
	$error=false;
	$error_arr='';
	$keyword = isset($_POST['query'])?$_POST['query']:'';
	$lang = isset($_POST['lang'])?$_POST['lang']:'';

	$categoryResults = array();
	$results = $db->get_categories_suggests($keyword, $lang);
	if (!empty($results)) {
		foreach($results as $row) {
			$categoryResults[] = $row["category"];
		}
	}
	return json_encode($categoryResults);
}

//
function subir_archivo($i,$folder){
	$ret = -2;
	$handle = new upload($i);
	if ($handle->uploaded) {
		$handle->allowed	= array('application/pdf'); // only pdf files
		$handle->file_max_size = '10M';
		
		$handle->process($folder);
		if ($handle->processed) {
			$ret	=	$handle->file_dst_name;
			$handle->clean();
		} else {
			addToLog('NEWS File Error: ' . $handle->error);
			return  -3;	// proccess error
		}
	}
	return $ret;	// file destination name or -2 (upload error)
}

function mover_file($originFolderAndFile, $destinationFolder, $newDestinationFileName=''){
	$ret = -2;
	$handle = new upload($originFolderAndFile);
	if ($handle->uploaded) {
		if ($newDestinationFileName){
			$info = pathinfo($newDestinationFileName);
			$file_name =  basename($newDestinationFileName,'.'.$info['extension']);
			$handle->file_new_name_body   = $file_name; //without extension
		}
		$handle->file_auto_rename = true;
		$handle->file_overwrite = false;
		$handle->allowed	= array('application/pdf'); // only pdf files
		$handle->file_max_size = '10M';
		
		$handle->process($destinationFolder);
		if ($handle->processed) {
			$destinationFile	=	$handle->file_dst_name;
			$ret = $destinationFile;
			$handle->clean();
		} else {
			addToLog('NEWS File Error: ' . $handle->error);
			return  -3;	// proccess error
		}
	}
	else{
		addTolog('NEWS File UPLOAD ERROR:'.$handle->error);
		return $ret;
	}
	return $ret;	// file destination name or -2 (upload error)
}

function mover_imagen($originFolderAndImage, $destinationFolder, $newDestinationFileName=''){
	$thumbwidth = NEWSIMAGECROPHORIZONTAL;
	$thumbheight = NEWSIMAGECROPVERTICAL;
	$ret = -2;
	// -- load origin image file
	$image = new upload($originFolderAndImage);
	// -- add edit options like resize, crop etc..
	// -- end edit options 
	// -- save image in destinationFolder
	if ($image->uploaded) {
		if ($newDestinationFileName){
			$info = pathinfo($newDestinationFileName);
			$file_name =  basename($newDestinationFileName,'.'.$info['extension']);
			$image->file_new_name_body   = $file_name; //without extension
		}
		$image->file_auto_rename = true;
		$image->file_overwrite = false;
		$image->allowed	= array('image/*'); // o
		$image->file_max_size = '10M';
		
		$image->image_convert	= 'jpg';
		$image->image_resize	= true;
		$image->image_ratio_crop	= 'TL';
		$image->image_y	= $thumbheight;
		$image->image_x	= $thumbwidth;
		$image->jpeg_quality = 85;
		
		//addTolog('NEWS IMAGE PRE PROCCESS:'.$originFolderAndImage);
		$image->process($destinationFolder);
		//addTolog('NEWS IMAGE POST PROCCESS:'.$file_name);
		if ($image->processed) {
			$destinationFile	=	$image->file_dst_name;
			$ret = $destinationFile;
			$image->clean();
		} else {
			addTolog('NEWS PROCESS ERROR:'.$image->error);
			return -3;
		}
	}
	else{
		addTolog('NEWS UPLOAD ERROR:'.$image->error);
		return $ret;
	}
	return $ret;
}

$news = array();


$langs=$db->get_cms_active_langs();

if ($_GET["code"]){
	$code=$_GET["code"];
}

$lang_active='';
$error=0;
$warning=0;

if ($_GET["action"]=="delete"){
	$code=$_GET["code"];
	$lang=$_GET["lang"];
	$news_del=$db->get_news($code);
	$news_del=$db->get_id_news_images_list($news_del, false);
	$news_del=$db->get_id_news_files_list($news_del, false);
	if ($lang=="all"){
		foreach ($news_del as $img_del){
			if (!empty($img_del['images'])){
				foreach ($img_del['images'] as $current_image){
					@unlink ("{$DOC_WEB_ROOT}uploads/news/".$current_image["image"]);
				}
			}
			if (!empty($img_del['files'])){
				foreach ($img_del['files'] as $current_file){
					@unlink ("{$DOC_WEB_ROOT}uploads/news/".$current_file["file"]);
				}
			}
			/*
			if ($img_del["file"]!=""){
				@unlink ("{$DOC_WEB_ROOT}uploads/news/".$img_del["file"]);
			};
			*/
		}
		$db->delete_all_news_images($code);
		$db->delete_all_news_files($code);
		$db->delete_news($code);
		header('Location: '.$URL_ROOT.'site/news/');
	}else{
		$news_lang_del=$db->get_news_lang($lang,$code);
		$news_lang_del['images'] = $db->get_code_news_images($lang, $code, false);
		$news_lang_del['files'] = $db->get_code_news_files($lang, $code, false);
		if (count($news_del)==1){
			if (!empty($news_lang_del['images'])){
				foreach ($news_lang_del['images'] as $current_image){
					@unlink ("{$DOC_WEB_ROOT}uploads/news/".$current_image["image"]);
				}
			}
			if (!empty($news_lang_del['files'])){
				foreach ($news_lang_del['files'] as $current_file){
					@unlink ("{$DOC_WEB_ROOT}uploads/news/".$current_file["file"]);
				}
			}
			/*
			if ($news_lang_del["file"]!=""){
				@unlink ("{$DOC_WEB_ROOT}uploads/news/".$news_lang_del["file"]);
			};
			*/
			$db->delete_all_news_images($code);
			$db->delete_all_news_files($code);
		}
		$db->delete_news_lang ($code,$lang);
		$db->delete_news_lang_images($code,$lang);
		$db->delete_news_lang_files($code,$lang);
		header('Location: '.$URL_ROOT.'site/news/' );
	}
	
}
if ($_POST){
	$session_id = isset($_POST["session_id"])?$_POST["session_id"]:md5(uniqid()); 
	
	$_POST["headline"] = trim($_POST["headline"]);
	$form_lang_code = $db->get_lang_code($_POST["lang"]); // no transliteramos al alfabeto latino el ruso
	$news_form=array(
			"id"=>$_POST["id"],
			"code"=>$_POST["code"],
			"lang"=>$_POST["lang"],
			"headline"=>addslashes($_POST["headline"]),
			"slug"=> url_slug($_POST["headline"], array('transliterate' => ($form_lang_code!='ru'?true:false) )),
			"date"=>trim($_POST["date"]),
		//	"category"=>addslashes($_POST["category"]),
		//	"category_slug"=> url_slug($_POST["category"], array('transliterate' => ($form_lang_code!='ru'?true:false))),
			"intro"=>addslashes($_POST["intro"]),
			"body"=>addslashes(transformAllToInternationalLangUrl(cleanHTML($_POST["body"]))),
			"meta-title"=>addslashes($_POST["meta-title"]),
			"meta-description"=>addslashes($_POST["meta-desc"]),
			"meta-keywords"=>addslashes($_POST["meta-key"]),
			"location"=>(!empty($_POST["location"])?addslashes($_POST["location"]):''),
			"link"=>addslashes($_POST["link"]),
			"link-text"=>addslashes($_POST["link-text"]),
		//	"file-linktext"=>addslashes($_POST["file-linktext"]),
			"active"=>$_POST["active"],
			"type"	=>	$_POST["type"]
			);
		
	if ($_POST["id"]!=""){
		
		//------update noticia------//
		$news_org = $db->get_news_lang($news_form["lang"],$news_form["code"]);
		$news_org['images'] = $db->get_code_news_images($news_form["lang"], $news_form["code"]);
		$news_org['files'] = $db->get_code_news_files($news_form["lang"], $news_form["code"]);
		
		//--- news images ------//
		$news_form['images'] = $news_org['images'];
		$news_form['files'] = $news_org['files'];
		
		$news_images_cnt = 0;
		if (!empty($_SESSION[$session_id][$news_form['lang']]['images'])){
			$is_main = true;
			foreach ($_SESSION[$session_id][$news_form['lang']]['images'] as $i => $current_image){
				switch ($current_image['status']){
					case 'A':
						$new_image = array(
							'id'	=>	false,
							'code'	=>	false,
							'news_code'	=>	$news_form['code'],
							'lang'	=>	$news_form['lang'],
							'image'	=>	$current_image['image'],
							'path-image' => 'tmp/',
							'alt-image'	=> addslashes($_POST['alt-'.$current_image['id-image']]),
							'added'	=>	false,
							'modified'	=>	false,
							'is-main'	=>	$is_main,
							'status'	=>	$current_image['status'],
							'id-image'	=>	$current_image['id-image'],
							'code-image'	=>	false,
							'name'		=> $current_image['name']
						);
						$news_form['images'][] = $new_image;
						$is_main = false;
						$news_images_cnt++;
						break;
					case 'M':
						$modified_image = $news_org['images'][$current_image['code-image']];
						$modified_image['lang']	= $news_form['lang'];
						$modified_image['image'] = $current_image['image'];
						$modified_image['path-image'] = 'tmp/';
						$modified_image['alt-image'] = addslashes($_POST['alt-'.$current_image['id-image']]);
						$modified_image['is-main'] = $is_main;
						$modified_image['status'] = $current_image['status'];
						$modified_image['id-image'] = $current_image['id-image'];
						$modified_image['name'] = $current_image['name'];
						$news_form['images'][$current_image['code-image']] = $modified_image;
						$is_main = false;
						$news_images_cnt++;
						break;
					case 'D':
						if ($current_image['code-image']){
							$deleted_image = $news_org['images'][$current_image['code-image']];
							$deleted_image['lang']	= $news_form['lang'];
							$deleted_image['alt-image'] = '';
							$deleted_image['image'] = '';
							$deleted_image['path-image'] = '';
							$deleted_image['is-main'] = false;
							$deleted_image['name'] = '';
							$deleted_image['status'] = $current_image['status'];
							$deleted_image['id-image'] = $current_image['id-image'];
							$news_form['images'][$current_image['code-image']] = $deleted_image;
						}
						break;
					default:
						if ($current_image['code-image']){
							$modified_image = $news_org['images'][$current_image['code-image']];
							$modified_image['lang']	= $news_form['lang'];
							$modified_image['alt-image'] = addslashes($_POST['alt-'.$current_image['id-image']]);
							$modified_image['is-main'] = $is_main;
							$news_form['images'][$current_image['code-image']] = $modified_image;
							$is_main = false;
							$news_images_cnt++;
						}
						break;
				}
			}
		}
		if (!$news_images_cnt){
			$error++;
			$validate["images"]["hasnoelements"]=1;
		}
		
		/* FILES */
		$news_files_cnt = 0;
		if (!empty($_SESSION[$session_id][$news_form['lang']]['files'])){
			$is_main = true;
			foreach ($_SESSION[$session_id][$news_form['lang']]['files'] as $i => $current_file){
				switch ($current_file['status']){
					case 'A':
						$new_file = array(
							'id'	=>	false,
							'code'	=>	false,
							'news_code'	=>	$news_form['code'],
							'lang'	=>	$news_form['lang'],
							'file'	=>	$current_file['file'],
							'path-file' => 'tmp/',
							'link-file'	=> addslashes($_POST['link-'.$current_file['id-file']]),
							'added'	=>	false,
							'modified'	=>	false,
							'is-main'	=>	$is_main,
							'status'	=>	$current_file['status'],
							'id-file'	=>	$current_file['id-file'],
							'code-file'	=>	false,
							'name'		=> $current_file['name']
						);
						$news_form['files'][] = $new_file;
						$is_main = false;
						$news_files_cnt++;
						break;
					case 'M':
						$modified_file = $news_org['files'][$current_file['code-file']];
						$modified_file['lang']	= $news_form['lang'];
						$modified_file['file'] = $current_file['file'];
						$modified_file['path-file'] = 'tmp/';
						$modified_file['link-file'] = addslashes($_POST['link-'.$current_file['id-file']]);
						$modified_file['is-main'] = $is_main;
						$modified_file['status'] = $current_file['status'];
						$modified_file['id-file'] = $current_file['id-file'];
						$modified_file['name'] = $current_file['name'];
						$news_form['files'][$current_file['code-file']] = $modified_file;
						$is_main = false;
						$news_files_cnt++;
						break;
					case 'D':
						if ($current_file['code-file']){
							$deleted_file = $news_org['files'][$current_file['code-file']];
							$deleted_file['lang']	= $news_form['lang'];
							$deleted_file['link-file'] = '';
							$deleted_file['file'] = '';
							$deleted_file['path-file'] = '';
							$deleted_file['is-main'] = false;
							$deleted_file['name'] = '';
							$deleted_file['status'] = $current_file['status'];
							$deleted_file['id-file'] = $current_file['id-file'];
							$news_form['files'][$current_file['code-file']] = $deleted_file;
						}
						break;
					default:
						if ($current_file['code-file']){
							$modified_file = $news_org['files'][$current_file['code-file']];
							$modified_file['lang']	= $news_form['lang'];
							$modified_file['link-file'] = addslashes($_POST['link-'.$current_file['id-file']]);
							$modified_file['is-main'] = $is_main;
							$news_form['files'][$current_file['code-file']] = $modified_file;
							$is_main = false;
							$news_files_cnt++;
						}
						break;
				}
			}
		}
		if (!$news_files_cnt){
			//$error++;
			//$validate["files"]["hasnoelements"]=1;
		}
		
		/*
		//--- if file uploads --//
		if (!empty($_FILES["file"]) AND $_FILES["file"]["name"]!=NULL){
            $uploaded_file = subir_archivo($_FILES["file"],"{$DOC_WEB_ROOT}uploads/news/");
            if (!is_numeric($uploaded_file)){
                $news_form["file"] = $uploaded_file;
                if ($news_org["file"]!="" AND ($news_org["file"] != $uploaded_file) ){
                    @unlink ("{$DOC_WEB_ROOT}uploads/news/".$news_org["file"]);
                };
            }
            else{
                $news_form["file"] = $news_org["file"];
                $validate["file"]["upload"] = abs($uploaded_file); // 2 => upload error or 3 => proccess error;
                $error++;
            }
        }elseif(!empty($_FILES["file"]) AND empty($news_org["file"])){ // File name is null
            $news_form["file"] = '';
           // $validate["file"]["upload"]=1; // is required
           // $error++;
        }elseif(!empty($_POST["file-del"]) AND $_POST["file-del"]==$news_org["file"]){
            $news_form["file"] = '';
        }
		else{
			$news_form["file"] = $news_org["file"];
		}
		*/
		
		#--- Checks fields that not avoid update the project if is there an error on any of them.
		
		//--- check used headline --//
		if ((strtolower(addslashes($news_org["headline"]))!=strtolower($news_form["headline"])) or ($news_org["slug"]!=$news_form["slug"])){
			if ($db->headline_news_exist($news_form["headline"], $news_form["slug"], $news_form["lang"], $news_form["code"])){
				$error++;
				$validate["headline"]["duplicated"]=1;
			}
			if (preg_match('/^\d+$/D', $news_form["slug"])){
				$error++;
				$validate["headline"]["isnumber"]=1;
			}
		}
		
		//--- check date
		if (empty($news_form["date"]) OR !isDate($news_form["date"])){
			$error++;
			$validate["date"]["nodate"]=1;
		}
		
		//--- check location
		if (empty($news_form["location"]) AND $news_form['type']=='eventos'){
			$error++;
			$validate["location"]["required"]=1;
		}
		
		//--- check file-linktext
		/*
		if (empty($news_form["file-linktext"]) AND !empty($news_form["file"])){
			$error++;
			$validate["file-linktext"]["required"]=1;
		}
		*/
		
		//--- Is there is a error on any of the fields that not avoid update the project
		if (!empty($validate["headline"]) OR !empty($validate["date"]) OR !empty($validate["location"]) ){
			$news=$db->get_news($news_form["code"]);
			$news=$db->get_id_news_images_list($news);
			$news=$db->get_id_news_files_list($news);
			foreach ($news as $id => $current){
				if ($current['lang']==$news_form['lang']){
					$news[$id] = $news_form; // actualizo los datos con los del del formulario
					$news[$id]['lang_code'] = $current['lang_code'];
					$lang_active = $current['lang_code'];
					break;
				}
			}
			// reset error fields
			if (!empty($validate["headline"])){
				unset($news_form["slug"]);
				unset($news_form["headline"]);
			}
			if (!empty($validate["date"])){
				unset($news_form["date"]);
			}
			if (!empty($validate["location"])){
				unset($news_form["location"]);
			}
		}
		
		$db->update_news($news_form);
		if (!$error){	
			// -- proccess images --
			if (!empty($news_form['images'])){
				foreach ($news_form['images'] as $i => $current_image){
					switch ($current_image['status']){
						case 'A':
							$destinationFile = @mover_imagen($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_image['image'],"{$DOC_WEB_ROOT}uploads/news/", $current_image['name']);
							if (!is_numeric($destinationFile)){
								$news_form['images'][$i]['path-image'] = 'news/';
								$news_form['images'][$i]['image'] = $destinationFile;
							}
							else{
								unset($news_form['images'][$i]);
								$error++;
								$validate["images"]["imageerror"]= abs($destinationFile);
							}
							break;
						case 'M':
							$modified_image = $news_org['images'][$current_image['code-image']];
							$modified_image['lang']	= $news_form['lang'];
							$destinationFile = @mover_imagen($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_image['image'],"{$DOC_WEB_ROOT}uploads/news/", $current_image['name']);
							if (!is_numeric($destinationFile)){
								@unlink ("{$DOC_WEB_ROOT}uploads/news/".$modified_image["image"]);
								
								$news_form['images'][$i]['path-image'] = 'news/';
								$news_form['images'][$i]['image'] = $destinationFile;
							}
							else{
								unset($news_form['images'][$i]);
								$error++;
								$validate["images"]["imageerror"]= abs($destinationFile);
							}
							break;
						case 'D':
							if ($current_image['code-image']){
								$deleted_image = $news_org['images'][$current_image['code-image']];
								$deleted_image['lang']	= $news_form['lang'];
								@unlink ("{$DOC_WEB_ROOT}uploads/news/".$deleted_image["image"]);
							}
							break;
						default:
							break;
					}
				}
			}
			$db->update_news_images($news_form);
			
			// -- proccess files --
			if (!empty($news_form['files'])){
				foreach ($news_form['files'] as $i => $current_file){
					switch ($current_file['status']){
						case 'A':
							$destinationFile = @mover_file($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_file['file'],"{$DOC_WEB_ROOT}uploads/news/", $current_file['name']);
							if (!is_numeric($destinationFile)){
								$news_form['files'][$i]['path-file'] = 'news/';
								$news_form['files'][$i]['file'] = $destinationFile;
							}
							else{
								unset($news_form['files'][$i]);
								$error++;
								$validate["files"]["fileerror"]= abs($destinationFile);
							}
							break;
						case 'M':
							$modified_file = $news_org['files'][$current_file['code-file']];
							$modified_file['lang']	= $news_form['lang'];
							$destinationFile = @mover_file($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_file['file'],"{$DOC_WEB_ROOT}uploads/news/", $current_file['name']);
							if (!is_numeric($destinationFile)){
								@unlink ("{$DOC_WEB_ROOT}uploads/news/".$modified_file["file"]);
								
								$news_form['files'][$i]['path-file'] = 'news/';
								$news_form['files'][$i]['file'] = $destinationFile;
							}
							else{
								unset($news_form['files'][$i]);
								$error++;
								$validate["files"]["fileerror"]= abs($destinationFile);
							}
							break;
						case 'D':
							if ($current_file['code-file']){
								$deleted_file = $news_org['files'][$current_file['code-file']];
								$deleted_file['lang']	= $news_form['lang'];
								@unlink ("{$DOC_WEB_ROOT}uploads/news/".$deleted_file["file"]);
							}
							break;
						default:
							break;
					}
				}
			}
			$db->update_news_files($news_form);
			
			/*
			if(!empty($_POST["file-del"]) AND $_POST["file-del"]==$news_org["file"] AND $news_form["file"]!=$news_org["file"]){
				@unlink ("{$DOC_WEB_ROOT}uploads/news/".$news_org["file"]);
			}
			*/
		}
		elseif(empty($news) and ( !empty($validate["files"]) OR !empty($validate["images"]) ) ){ //-- there is only a 'images' error, initialize $news var
			$news=$db->get_news($news_form["code"]);
			$news=$db->get_id_news_images_list($news);
			$news=$db->get_id_news_files_list($news);
			foreach ($news as $id => $current){
				if ($current['lang']==$news_form['lang']){
					$news[$id] = $news_form; // actualizo los datos con los del del formulario
					$news[$id]['lang_code'] = $current['lang_code'];
					$lang_active = $current['lang_code'];
					break;
				}
			}
		}			
		
		
		if ($news_form['active']=='Draft'){ //borrador
			$warning++;
			$attention['active']['draft']=1;
		}
		$code=$news_form["code"];
		$lang=$news_form["lang"];
		if (!$error){
			if ($warning){
				$warn = '&warning='.$warning.'&attention='.json_encode($attention);
			}
			else{
				$warn = '&update=ok';
			}
			_emptySessions();
			session_write_close();
			header("Location: add.php?code=$code&lang=$lang".$warn);
			exit();
		}
	}else{
		//------nueva noticia------//
		
		//-- news images --//
		if ($news_form["code"]==""){
			$last_code=$db->get_news_last_code ();
			$news_form["code"]=$last_code+1;
		}
		else{
			$code = $news_form["code"];
		}
		
		if ($code){
			$news_org = $db->get_news_lang('general',$news_form["code"]);
			$news_org['images'] = $db->get_code_news_images($news_form['lang'], $news_form["code"]);
			$news_org['files'] = $db->get_code_news_files($news_form['lang'], $news_form["code"]);
		}
		
		// IMAGES
		$news_images_cnt = 0;
		if (!empty($_SESSION[$session_id][$news_form['lang']]['images'])){
			$is_main = true;
			foreach ($_SESSION[$session_id][$news_form['lang']]['images'] as $i => $current_image){
				switch ($current_image['status']){
					case 'A':
						$new_image = array(
							'id'	=>	false,
							'code'	=>	false,
							'news_code'	=>	$news_form['code'],
							'lang'	=>	$news_form['lang'],
							'image'	=>	$current_image['image'],
							'path-image' => 'tmp/',
							'alt-image'	=> addslashes($_POST['alt-'.$current_image['id-image']]),
							'added'	=>	false,
							'modified'	=>	false,
							'is-main'	=>	$is_main,
							'status'	=>	$current_image['status'],
							'id-image'	=>	$current_image['id-image'],
							'code-image'	=>	false,
							'name'		=> $current_image['name']
						);
						$news_form['images'][] = $new_image;
						$is_main = false;
						$news_images_cnt++;
						break;
					case 'M':
						$modified_image = $news_org['images'][$current_image['code-image']];
						$modified_image['lang']	= $news_form['lang'];
						$modified_image['image'] = $current_image['image'];
						$modified_image['path-image'] = 'tmp/';
						$modified_image['alt-image'] = addslashes($_POST['alt-'.$current_image['id-image']]);
						$modified_image['is-main'] = $is_main;
						$modified_image['status'] = $current_image['status'];
						$modified_image['id-image'] = $current_image['id-image'];
						$modified_image['name'] = $current_image['name'];
						$news_form['images'][$current_image['code-image']] = $modified_image;
						$is_main = false;
						$news_images_cnt++;
						break;
					case 'D':
						if ($current_image['code-image']){
							$deleted_image = $news_org['images'][$current_image['code-image']];
							$deleted_image['lang']	= $news_form['lang'];
							$deleted_image['alt-image'] = '';
							$deleted_image['image'] = '';
							$deleted_image['path-image'] = '';
							$deleted_image['name'] = '';
							$deleted_image['is-main'] = false;
							$deleted_image['status'] = $current_image['status'];
							$deleted_image['id-image'] = $current_image['id-image'];
							$news_form['images'][$current_image['code-image']] = $deleted_image;
						}
						break;
					default:
						if ($current_image['code-image']){
							
							$modified_image = $news_org['images'][$current_image['code-image']];
							$modified_image['alt-image'] = addslashes($_POST['alt-'.$current_image['id-image']]);
							$modified_image['is-main'] = $is_main;
							if ($modified_image['lang'] != $news_form['lang']){
								$modified_image['lang'] = $news_form['lang'];
								$modified_image['id'] = '';
								$modified_image['added'] = '';
								$modified_image['modified'] = '';
							}
							$news_form['images'][$current_image['code-image']] = $modified_image;
							$is_main = false;
							$news_images_cnt++;
						}
						break;
				}
			}
		}
		if (!$news_images_cnt){
			$error++;
			$validate["images"]["hasnoelements"]=1;
		}
		
		// FILES
		$news_files_cnt = 0;
		if (!empty($_SESSION[$session_id][$news_form['lang']]['files'])){
			$is_main = true;
			foreach ($_SESSION[$session_id][$news_form['lang']]['files'] as $i => $current_file){
				switch ($current_file['status']){
					case 'A':
						$new_file = array(
							'id'	=>	false,
							'code'	=>	false,
							'news_code'	=>	$news_form['code'],
							'lang'	=>	$news_form['lang'],
							'file'	=>	$current_file['file'],
							'path-file' => 'tmp/',
							'link-file'	=> addslashes($_POST['link-'.$current_file['id-file']]),
							'added'	=>	false,
							'modified'	=>	false,
							'is-main'	=>	$is_main,
							'status'	=>	$current_file['status'],
							'id-file'	=>	$current_file['id-file'],
							'code-file'	=>	false,
							'name'		=> $current_file['name']
						);
						$news_form['files'][] = $new_file;
						$is_main = false;
						$news_files_cnt++;
						break;
					case 'M':
						$modified_file = $news_org['files'][$current_file['code-file']];
						$modified_file['lang']	= $news_form['lang'];
						$modified_file['file'] = $current_file['file'];
						$modified_file['path-file'] = 'tmp/';
						$modified_file['link-file'] = addslashes($_POST['link-'.$current_file['id-file']]);
						$modified_file['is-main'] = $is_main;
						$modified_file['status'] = $current_file['status'];
						$modified_file['id-file'] = $current_file['id-file'];
						$modified_file['name'] = $current_file['name'];
						$news_form['files'][$current_file['code-file']] = $modified_file;
						$is_main = false;
						$news_files_cnt++;
						break;
					case 'D':
						if ($current_file['code-file']){
							$deleted_file = $news_org['files'][$current_file['code-file']];
							$deleted_file['lang']	= $news_form['lang'];
							$deleted_file['link-file'] = '';
							$deleted_file['file'] = '';
							$deleted_file['path-file'] = '';
							$deleted_file['name'] = '';
							$deleted_file['is-main'] = false;
							$deleted_file['status'] = $current_file['status'];
							$deleted_file['id-file'] = $current_file['id-file'];
							$news_form['files'][$current_file['code-file']] = $deleted_file;
						}
						break;
					default:
						if ($current_file['code-file']){
							
							$modified_file = $news_org['files'][$current_file['code-file']];
							$modified_file['link-file'] = addslashes($_POST['link-'.$current_file['id-file']]);
							$modified_file['is-main'] = $is_main;
							if ($modified_file['lang'] != $news_form['lang']){
								$modified_file['lang'] = $news_form['lang'];
								$modified_file['id'] = '';
								$modified_file['added'] = '';
								$modified_file['modified'] = '';
							}
							$news_form['files'][$current_file['code-file']] = $modified_file;
							$is_main = false;
							$news_files_cnt++;
						}
						break;
				}
			}
		}
		if (!$news_files_cnt){
			//$error++;
			//$validate["files"]["hasnoelements"]=1;
		}
		
		//--- check used headline --//
		if ($db->headline_news_exist($news_form["headline"], $news_form["slug"], $news_form["lang"])){
			$error++;
			$validate["headline"]["duplicated"]=1;
		}
		if (preg_match('/^\d+$/D', $news_form["slug"])){
			$error++;
			$validate["headline"]["isnumber"]=1;
		}
		
		//--- check date
		if (empty($news_form["date"]) OR !isDate($news_form["date"])){
			$error++;
			$validate["date"]["nodate"]=1;
		}
		
		//--- check location
		if (empty($news_form["location"]) AND $news_form['type']=='eventos'){
			$error++;
			$validate["location"]["required"]=1;
		}
		
		if ($error AND (!empty($validate["headline"]) OR !empty($validate["images"]) OR !empty($validate["date"]) OR !empty($validate["location"]))){
			$news_form["code"] = false;
			$news[0] = $news_form;
			$news[0]['lang_code'] = $db->get_lang_code($news[0]['lang']);
			$lang_active = $news[0]['lang_code'];
		}
		
		/*
		//die('add: '.print_r($news_form,true));	
		// [IMAGE - FILE] fields proccess (save, move, delete)
		if (!$error){
			if (!empty($_FILES["file"]) AND $_FILES["file"]["name"]!=NULL){
                //$tmp = time();
                $uploaded_file = subir_archivo($_FILES["file"],"{$DOC_WEB_ROOT}uploads/news/");
                if (!is_numeric($uploaded_file)){
                    $news_form["file"] = $uploaded_file;
                    if ($news_org["file"]!="" AND ($news_org["file"]!=$uploaded_file) ){
                        @unlink ("{$DOC_WEB_ROOT}uploads/news/".$news_org["file"]);
                    };
                }
                else{
                    $news_form["file"] = '';
                    $validate["file"]["upload"] = abs($uploaded_file); // 2 => upload error or 3 => proccess error;
                    $error++;
                }
            }else{
                $news_form["file"] = $news_org["file"];
                
                if (empty($news_form["file"])){
                   // $validate["file"]["upload"]=1; // required
                   // $error++;
                }
            }
		}
		*/
		
		if (!$error){
			
			$db->add_news ($news_form);
			
			/*
			//--- check file-linktext
			if (empty($news_form["file-linktext"]) AND !empty($news_form["file"])){
				$error++;
				$validate["file-linktext"]["required"]=1;
			}
			*/
		
			// -- proccess images --
			if (!empty($news_form['images'])){
				foreach ($news_form['images'] as $i => $current_image){
					switch ($current_image['status']){
						case 'A':
							$destinationFile = @mover_imagen($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_image['image'],"{$DOC_WEB_ROOT}uploads/news/", $current_image['name']);
							if(!is_numeric($destinationFile)){
								$news_form['images'][$i]['path-image'] = 'news/';
								$news_form['images'][$i]['image'] = $destinationFile;
							}else{
								unset($news_form['images'][$i]);
								$error++;
								$validate["images"]["imageerror"]= abs($destinationFile);
							}
							break;
						case 'M':
							$modified_image = $news_org['images'][$current_image['code-image']];
							$modified_image['lang']	= $news_form['lang'];
							$destinationFile = @mover_imagen($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_image['image'],"{$DOC_WEB_ROOT}uploads/news/", $current_image['name']);
							
							if (!is_numeric($destinationFile)){
								$news_form['images'][$i]['path-image'] = 'news/';
								$news_form['images'][$i]['image'] = $destinationFile;
								@unlink ("{$DOC_WEB_ROOT}uploads/news/".$modified_image["image"]);
							}
							else{
								unset($news_form['images'][$i]);
								$error++;
								$validate["images"]["imageerror"]= abs($destinationFile);
							}
							break;
						case 'D':
							if ($current_image['code-image']){
								$deleted_image = $news_org['images'][$current_image['code-image']];
								$deleted_image['lang']	= $news_form['lang'];
								@unlink ("{$DOC_WEB_ROOT}uploads/news/".$deleted_image["image"]);
							}
							break;
						default:
							break;
					}
				}
			}
			
			$db->update_news_images($news_form);
			
			// -- proccess files --
			if (!empty($news_form['files'])){
				foreach ($news_form['files'] as $i => $current_file){
					switch ($current_file['status']){
						case 'A':
							$destinationFile = @mover_file($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_file['file'],"{$DOC_WEB_ROOT}uploads/news/", $current_file['name']);
							if(!is_numeric($destinationFile)){
								$news_form['files'][$i]['path-file'] = 'news/';
								$news_form['files'][$i]['file'] = $destinationFile;
							}else{
								unset($news_form['files'][$i]);
								$error++;
								$validate["files"]["fileerror"]= abs($destinationFile);
							}
							break;
						case 'M':
							$modified_file = $news_org['files'][$current_file['code-file']];
							$modified_file['lang']	= $news_form['lang'];
							$destinationFile = @mover_file($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_file['file'],"{$DOC_WEB_ROOT}uploads/news/", $current_file['name']);
							
							if (!is_numeric($destinationFile)){
								$news_form['files'][$i]['path-file'] = 'news/';
								$news_form['files'][$i]['file'] = $destinationFile;
								@unlink ("{$DOC_WEB_ROOT}uploads/news/".$modified_file["file"]);
							}
							else{
								unset($news_form['files'][$i]);
								$error++;
								$validate["files"]["fileerror"]= abs($destinationFile);
							}
							break;
						case 'D':
							if ($current_file['code-file']){
								$deleted_file = $news_org['files'][$current_file['code-file']];
								$deleted_file['lang']	= $news_form['lang'];
								@unlink ("{$DOC_WEB_ROOT}uploads/news/".$deleted_file["file"]);
							}
							break;
						default:
							break;
					}
				}
			}
			
			$db->update_news_files($news_form);
			
			if ($_POST['active']=='Draft'){ //borrador
				$warning++;
				$attention['active']['draft']=1;
			}
			
			$code=$news_form["code"];
			$lang = $news_form["lang"];
			if ($warning){
				$warn = '&warning='.$warning.'&attention='.json_encode($attention);
			}
			else{
				$warn = '&insert=ok';
			}
			_emptySessions();
			session_write_close();
			header("Location: add.php?code=$code&lang=$lang".$warn);
			exit();
		}
		else{
			$news_form["code"] = false;
			
			//reseteamos las imágenes y demás elementos comunes que no habíamos comprobados antes.
			// ????? No tengo muy claro para que hago esto, porque no se usa luego. Si se hace, habría que añadirlo al news[0]['images']
			if (!empty($news_form["images"])){
				foreach ($news_form["images"] as $i	=>	$current_image){
					$news_form["images"][$i]['news_code'] = false;
				}
				
				$news[0]["images"] = $news_form["images"];
			}
			
			//reseteamos los ficheros y demás elementos comunes que no habíamos comprobados antes.
			// ????? No tengo muy claro para que hago esto, porque no se usa luego. Si se hace, habría que añadirlo al news[0]['files']
			if (!empty($news_form["files"])){
				foreach ($news_form["files"] as $i	=>	$current_file){
					$news_form["files"][$i]['news_code'] = false;
				}
				
				$news[0]["files"] = $news_form["files"];
			}
		}
	}
}
else{
	if (isset($_SESSION['id'])){
		_emptySessions();
	}
	// borro las sessiones de usuario
	foreach ($_SESSION as $var => $val){
		if ($var!='user'){
			unset($_SESSION[$var]);
		}
	}
	//
	$session_id = md5(uniqid());
	$_SESSION['id'] = $session_id;
	$_SESSION[$session_id] = array();
	foreach ($langs as $current_lang){
		$_SESSION[$session_id][$current_lang['id']] = array( 
			'code' => (int)$code,
			'images' => array(),
			'files' => array()
		);
	}
}

//------------- extraigo noticia en todos los idiomas en los que esté -----
$news_lang = array();
if ($_GET or ($_POST and $error)){
	if (!$error){
		$news=$db->get_news($code);
		$news=$db->get_id_news_images_list($news);
		$news=$db->get_id_news_files_list($news);
		if ($_GET["update"]){
			$update=$_GET["update"];
		
		}
		if ($_GET["insert"]){
			$insert=$_GET["insert"];
		
		}
	}
	else{ // form data error
		// en caso de error en el formulario no hay que hacer nada en la variable de sessión ya que contiene los datos que tiene que tener y
		// news estará actualizado con los datos del formulario erróneo
	}

	//------------- creo un array news_codigoidioma para cada idioma -----
	
	$default_lang = '';
	foreach ($news as $current){
		$news_lang[$current["lang_code"]]=array(
			"id"=>$current["id"],
			"code"=>$code,
			"lang"=>$current["lang"],
			"headline"=>$current["headline"],
			"date"=>$current["date"],
			"category"=>$current["category"],
			"intro"=>$current["intro"],
			"body"=>$current["body"],
			"meta-title"=>$current["meta-title"],
			"meta-description"=>$current["meta-description"],
			"meta-keywords"=>$current["meta-keywords"],
			"active"=>$current['active'],
			"images"=>$current['images'],
			"files"=>$current['files'],
			"type"=>$current["type"],
			"location"=>$current["location"],
			"link"=>$current["link"],
			"link-text"=>$current["link-text"],
		);
		
		
		
		$_SESSION[$session_id][$current["lang"]] = array(
			'code'=>$code, 
			'images'=> $current['images'],
			'files'=> $current['files']
		);
		$default_lang = $current["lang_code"];
	}
	
	if ($default_lang){
		// -- reset alt-image for the not defined lang --
		$imagesAlt = $news_lang[$default_lang]["images"];
		if (!empty($imagesAlt)){
			$imagesAltReseted = array_map(function ($item) {
				$item['alt-image'] = '';
				$item['id'] = false;
				$item['lang'] = false;
				$item['added'] = false;
				$item['modified'] = false;
				return $item;
			}, $imagesAlt);
		}
		else{
			$imagesAltReseted = $imagesAlt;
		}
		// -- reset link-file for the not defined lang --
		$filesLink = $news_lang[$default_lang]["files"];
		if (!empty($filesLink)){
			$filesLinkReseted = array_map(function ($item) {
				$item['link-file'] = '';
				$item['id'] = false;
				$item['lang'] = false;
				$item['added'] = false;
				$item['modified'] = false;
				return $item;
			}, $filesLink);
		}
		else{
			$filesLinkReseted = $filesLink;
		}
		foreach ($langs as $current_lang){
			if (empty($news_lang[$current_lang["lang"]])){
				$news_lang[$current_lang["lang"]] = $db->get_news_lang($current_lang["id"], $news_lang[$default_lang]["code"]);
				if (empty($news_lang[$current_lang["lang"]])){
					$news_lang[$current_lang["lang"]] = array();
				}
				$news_lang[$current_lang["lang"]]=array_merge($news_lang[$current_lang["lang"]], array(
					"code"=>$code,
					"lang"=>$current_lang["lang"],
					"date"=>$news_lang[$default_lang]["date"],
					"images"=> $imagesAltReseted,
					"files"=> $filesLinkReseted,
					"type"=>$news_lang[$default_lang]["type"],
					"location"=>$news_lang[$default_lang]["type"]
				));
				
				
				$_SESSION[$session_id][$current_lang["id"]] = array(
					'code'=>$code, 
					'images'=> $imagesAltReseted,
					'files'=> $filesLinkReseted
				);
			}
		}
	}
}

//load default keywords
$doc = new DOMDocument();
foreach ($langs as $current_lang){
	if (@$doc->load($DOC_WEB_ROOT."lang/".strtolower($current_lang["lang"]).".xml")){
		 $xPath = new DOMXPath($doc);
		foreach( $xPath->query('/txt/news/keywords') as $current_keywords ) {
			$news_lang[$current_lang["lang"]]['default-keywords'] = $current_keywords->nodeValue; // Will output bar1 and bar2 w\out whitespace
		}
	}
}

if (!$lang_active){
	if (isset($_GET['lang']) AND !empty($_GET['lang'])){
		$lang_active = $db->get_lang_code($_GET['lang']);
	}
	else{
		$lang_active= $db->get_cms_default_lang();;
		if (count($news)>1){
			$lang_active= $db->get_cms_default_lang();;
		}
		elseif (!empty($news[0])){
			$lang_active=$news[0]['lang_code'];
		}
	}
}

if (isset($_GET['warning']) AND !empty($_GET['warning'])){
	$warning = $_GET['warning'];
	$attention = json_decode($_GET['attention'], true);
}
session_write_close();
?>