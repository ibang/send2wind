<?php
function transformToInternationalLangUrl($url){
	global $db;
	$parsed_url = parse_url($url);
	$domainProtocol = Constants::getProtocol($parsed_url['host']);
	if ($parsed_url['scheme']!=$domainProtocol){ // si hemos puesto mal el protocolo http o https, lo corregimos
		$url =  str_replace($parsed_url['scheme'], $domainProtocol, $url);
	}
	$activedHosts = array('daekin.localhost', 'localhost', 'daekin.promueve3.com', 'daekinproject.com', 'www.daekinproject.com');
	$activedHostsUrlRoots = array('daekin.localhost' => '/daekin/', 'localhost' => '/daekin/', 'daekin.promueve3.com' => '/', 'daekinproject.com' => '/', 'www.daekinproject.com' => '/');
	$activedHostsPorts = array('daekin.promueve3.com' => '', 'daekin.localhost' => '', 'daekinproject.com' => '', 'www.daekinproject.com' => '', 'localhost' => LOCALHOSTPORT);
	if (in_array($parsed_url['host'], $activedHosts)){ //url absoluta interna
		$query = str_replace($domainProtocol.'://'.$parsed_url['host'].$activedHostsPorts[$parsed_url['host']].$activedHostsUrlRoots[$parsed_url['host']], '',$url);
		$query_array = explode('/', $query,2);
		if (!empty($query_array)){
			$langOrLocalization = $query_array[0];
			$langData = $db->get_lang_by_code($langOrLocalization);
			if (empty($langData)){
				$active_lang = $db->get_lang_from_localization_url($langOrLocalization);
				if (!$active_lang){
					return $url;
				}
			}
			else{
				$active_lang = $langOrLocalization;
			}
			if (!$active_lang){
				return $url;
			}
			$query = preg_replace("/^(".$langOrLocalization."\/)/", $active_lang.'/', $query);
		}
		$url = $domainProtocol.'://'.$parsed_url['host'].$activedHostsPorts[$parsed_url['host']].$activedHostsUrlRoots[$parsed_url['host']].$query;
	}

	return $url;
}

function transformAllToInternationalLangUrl($html_code){
	// transform all Links URLs from HTML code string in LocalizedURLS
	$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
	$localizedHtmlCode = $html_code;
	if (!empty($html_code)){
		if(preg_match_all("/$regexp/siU", $html_code, $matches, PREG_SET_ORDER)) {
			if (!empty($matches)){
				foreach($matches as $match) {
					// $match[2] = link address
					// $match[3] = link text
					// addToLog($match[2]);
					$localizedURL = transformToInternationalLangUrl($match[2]);
					$localizedHtmlCode = str_replace($match[2], $localizedURL, $localizedHtmlCode);
				}
			}
		}
	}
	return $localizedHtmlCode;
}

function cleanHTML($html_code, $mode='normal'){
	//addToLog('BODY: '.$html_code);
	$html_code = preg_replace('/<p><iframe(.*?)<\/iframe><\/p>/','<mivideo class="embed-responsive embed-responsive-16by9"><iframe$1</iframe></mivideo>',$html_code);
	$html_code = strip_tags($html_code, "<p><br><strong><a><img><ul><ol><li><em><iframe><mivideo>");
	$html_code = preg_replace('/<mivideo class="embed-responsive embed-responsive-16by9">(.*?)<\/mivideo>/','<div class="embed-responsive embed-responsive-16by9">$1</div>',$html_code);
	$dom = new DOMDocument;                 // init new DOMDocument
	@$dom->loadHTML('<?xml encoding="UTF-8">' .$html_code);   // load HTML into it
	// we want nice output
	$dom->formatOutput = true;
	$xpath = new DOMXPath($dom);            // create a new XPath
	$nodes = $xpath->query('//*[@style]');  // Find elements with a style attribute
	foreach ($nodes as $node) {              // Iterate over found elements
		$style = $node->getAttribute('style');
		$class = $node->getAttribute('class');
		
		//$node->removeAttribute('class');	// Remove class attribute
		$node->removeAttribute('style');    // Remove style attribute
		
		$padding_style = '';
		if (preg_match("/^padding-left: (\d+)px;/", $style, $matches)){ //mantengo el indent
			$padding_style .= 'padding-left: '.$matches[1].'px;';
		}
		
		$align_style = '';
		if ($node->nodeName=='img'){
			//$align_class = '';
			
			if(preg_match("/^float: (left|right);/", $style, $matches)){
				if ($matches[1]=='left'){
					//$align_class = 'float-left';
					$align_style .= 'float:left; margin-right: 30px;';
				}
				elseif($matches[1]=='right'){
					//$align_class = 'float-right';
					$align_style .= 'float:right; margin-left: 30px;';
				}
			}
			elseif(preg_match("/^margin-right: auto;/", $style) AND preg_match("/^margin-left: auto;/", $style) AND preg_match("/^display: block;/", $style) ){
				$align_style .= 'display: block; margin-right: auto;margin-left: auto;';
			}
		}
		else{
			$node->removeAttribute('class');
		}
		
		if ($node->nodeName=='p'){
			//$align_class = '';
			$align_style = '';
			if(preg_match("/^text-align: (left|center|right);/", $style, $matches)){
				$align_style .= 'text-align: '.$matches[1].';';
			}
		}
		
		//$node->setAttribute('class', $align_class.(!empty($class)?' '.$class:''));
		$node->setAttribute('style', $align_style.(!empty($padding_style)?' '.$padding_style:''));
	}
	
	$imgs = $dom->getElementsByTagName('img');
	foreach($imgs as $img){
		$img_class = $img->getAttribute('class');
		$img_photo = '';
		$img_align = '';
		if (preg_match("/img-circle/", $img_class)){
			$img_photo = 'img-circle ';
		}
		/*
		if (preg_match("/float-left/", $img_class)){
			$img_align = 'float-left ';
		}
		if (preg_match("/float-right/", $img_class)){
			$img_align = 'float-right ';
		}
		*/
		$img->removeAttribute('class');
		$img->setAttribute('class', $img_photo.$img_align.'img-responsive');
	}
	$iframes = $dom->getElementsByTagName('iframe');
	foreach($iframes as $iframe){
		$iframe->removeAttribute('width');
		$iframe->removeAttribute('height');
		$iframe->removeAttribute('class');
		$iframe->setAttribute('class', 'embed-responsive-item');
	}
	
	$uls = $dom->getElementsByTagName('ul');
	foreach($uls as $ul){
		$ul->removeAttribute('class');
		$ul->setAttribute('class', 'dots-list');
	}
	
	$dom->encoding = 'UTF-8'; // insert proper
	$result = $dom->saveHTML(); 
	//remove doctype, html and body added automatically by dom
	$result = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<?xml encoding="UTF-8">', '<html>', '</html>', '<body>', '</body>'), array('', '', '', '', ''), $result));	
	// sustituyo los html-entities que puedan quedar con la codificación del idioma y sin sustituir al aplicar el mb_convert_encoding
	/*
	["] => &quot;
    [&] => &amp;
    [<] => &lt;
    [>] => &gt;
	*/
	$result = str_replace(array("&quot;", "&amp;", "&lt;", "&gt;"), array(";quot;", ";amp;", ";lt;", ";gt;"), $result);  //hago un paso intermedio para que no reemplace
	$result = mb_convert_encoding($result, "UTF-8", "HTML-ENTITIES");
	// restauramos los entities cambiados
	$result = str_replace(array(";quot;", ";amp;", ";lt;", ";gt;"), array("&quot;", "&amp;", "&lt;", "&gt;"), $result);  //hago un paso intermedio para que no reemplace
	return trim($result);
}

function cleanEditorHTML($html_code=''){
	//clean video code
	$html_code = preg_replace('/<div class="embed-responsive embed-responsive-16by9"><iframe(.*?)<\/iframe><\/div>/','<p><iframe$1</iframe></p>',$html_code);
	
	return $html_code;
}

// obtiene todas las imágenes locales subidas por el tinymce del código html
function getSRCimagesHTML($html_code=''){
	global $URL_WEB_ROOT;
	$images = array();
	$dom = new DOMDocument;                 // init new DOMDocument
	@$dom->loadHTML('<?xml encoding="UTF-8">' .$html_code);   // load HTML into it
	// we want nice output
	$dom->formatOutput = true;
	
	$imgs = $dom->getElementsByTagName('img');
	foreach($imgs as $img){
		$imgSrc = $img->getAttribute('src');
		addToLog('IMAGE SRC: '.$imgSrc);
		addToLog('COMARER: '.preg_quote(HTTPHOST.$URL_WEB_ROOT.'uploads/images/', '/'));
		$imgFile = preg_replace('/'.preg_quote(HTTPHOST.$URL_WEB_ROOT.'uploads/images/', '/').'/', '', $imgSrc);
		if (!empty($imgFile) AND $imgFile!=$imgSrc){
			$images[] = $imgFile;
		}
	}
	return $images;
}

function updateTinymceImages($newImages, $oldImages){
	global $DOC_WEB_ROOT;
	if (!empty($newImages)){
		if(!empty($oldImages)){
			// can be changes
			foreach($newImages as $current){
				$keys = array_keys($oldImages, $current); //cojo todas las coincidencias en old
				if (!empty($keys)){ // si existía, no ha cambiado, no hacemos nada
					$new_keys = array_keys($newImages, $current); //cojo todas las coincidencias en old
					foreach ($new_keys as $new_key){ //borro las coincidencias en old
						unset($newImages[$new_key]);
					}
					foreach ($keys as $key){ //borro las coincidencias en old
						unset($oldImages[$key]);
					}
				}
				else{ // new image
					// do nothing
				}
			}
			if (!empty($oldImages)){ // deleted images, remove from disk
				foreach($oldImages as $current){
					addToLog('BORRO: '.$DOC_WEB_ROOT.'uploads/images/'.$current);
					@unlink($DOC_WEB_ROOT.'uploads/images/'.$current);
				}
			}
		}
		else{
			// All are new images
		}
	}
	elseif(!empty($oldImages)){ //remove all old images
		foreach ($oldImages as $current){
			addToLog('BORRO: '.$DOC_WEB_ROOT.'uploads/images/'.$current);
			@unlink($DOC_WEB_ROOT.'uploads/images/'.$current);
		}
	}
	return true;
}

function isDate($date){
	if (!empty($date)){
		if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)){
			return true;
		}
	}
	return false;
}

function isLocation($location){
	if (!empty($location)){
		if (preg_match("/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/", $location)){
			return true;
		}
	}
	return false;
}

function getGrantedSections(&$showSectionsArray, $totalcol=12, $maxcolinrow=3){
	$ocupiedcol = array();
	$ncol = 0;
	$nrow = 0;
	foreach ($showSectionsArray as $nind => $sectionName){
		if ($_SESSION['user']['perm_'.$sectionName]=='Y'){
			$ncol++;
		}
		else{
			unset($showSectionsArray[$nind]);
		}
	}
	if (!empty($showSectionsArray)){
		$showSectionsArray = array_values($showSectionsArray);
	}
	if ($ncol){
		$nrow = 1;
		$lastrowcol = $ncol;
		if ($ncol>=$maxcolinrow){
			$nrow=intval($ncol/$maxcolinrow);
			$lastrowcol = ($ncol%$maxcolinrow);
			if ($lastrowcol){
				$nrow++;
			}
			else{
				$lastrowcol = $maxcolinrow;
			}
		}
		for ($r=0;$r<$nrow;$r++){
			if ($nrow == ($r+1)){ //last row
				$ocupiedcol[$r] = intval($totalcol/$lastrowcol);
			}
			else{
				$ocupiedcol[$r] = intval($totalcol/$maxcolinrow);
			}
		}
	}
	return $ocupiedcol;
}

// get file name and change extension with given
function newFileExt($filename='', $newExt =''){
	if ($filename and $newExt){
		$info = pathinfo($filename);
		if ($info['extension']!=$newExt){
			$filenamewithoutext =  basename($filename,'.'.$info['extension']);
			return $filenamewithoutext.'.'.$newExt;
		}
	}
	return $filename;
}

function get_filesize($filename=''){
	if (!empty($filename)){	
		clearstatcache();
		if ($size = filesize($filename)){
			return $size;
		}
	}
	return false;
}

function get_fileformat($filename=''){
	if (!empty($filename)){	
		clearstatcache();
		if ($extension = pathinfo($filename, PATHINFO_EXTENSION)){
			return $extension;
		}
	}
	return false;
}
?>