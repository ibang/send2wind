<?php
/**
  ------------------------------------------------------------------
 
  IMPORTANT NOTE! In case, when TinyMCE’s folder is not protected with HTTP Authorisation,
  you should require is_allowed() function to return 
  `TRUE` if user is authorised,
  `FALSE` - otherwise
    is_allowed() can be found in jbimages/is_allowed.php
 
  This is intended to protect upload script, if someone guesses it's url.


  So, here we go...


| -------------------------------------------------------------------
|
| Path to upload target folder, relative to domain name. NO TRAILING SLASH!
| Example: if an image is acessed via http://www.example.com/images/somefolder/image.jpg, you should specify here:
| 
| $config['img_path'] = '/images/somefolder';
| 
| -------------------------------------------------------------------*/

/*-------------------------------------------------------------------
|
| IMPORTANT NOTE! In case, when TinyMCE’s folder is not protected with HTTP Authorisation,
| you should require is_allowed() function to return 
| `TRUE` if user is authorised,
| `FALSE` - otherwise
| 
|  This is intended to protect upload script, if someone guesses it's url.
| 
-------------------------------------------------------------------*/

function is_allowed(){
	global $_SESSION;
	// check if session is already active based on php version 
	if (version_compare(phpversion(), '5.4.0', '<')) { //menos que php 5.4.0
		// php version isn't high enough
		if(session_id() == '') {
			if (defined('ADMINCOOKIENAME')){
				session_name(ADMINCOOKIENAME);
			}
			@session_start();
		}
	}
	else{ // >= 5.4.0
		if (session_status() == PHP_SESSION_NONE) {
			if (defined('ADMINCOOKIENAME')){
				session_name(ADMINCOOKIENAME);
			}
			@session_start();
		}
	}
	if (!empty($_SESSION['user']) AND !empty($_SESSION['user']['perm_news']) AND ($_SESSION['user']['perm_news']=='Y')){
		return TRUE;
	}
	return false;
}

// server location
if (!function_exists('serverLocation')){
	function serverLocation($thisFileRelativePath=''){
		// Load the absolute server path to the directory the script is running in
		$fileDir = str_replace('\\','/',dirname(__FILE__)); //si es windows, sustituyo los \ por /
		
		// Make sure we end with a slash
		$fileDir = rtrim($fileDir, '/').'/';
		
		// Load the absolute server path to the document root
		$docRoot = $_SERVER['DOCUMENT_ROOT'];

		// Make sure we end with a slash
		$docRoot= rtrim($_SERVER['DOCUMENT_ROOT'],"/").'/';

		// find part before docRoot in fileDir (in case fileDir is a absolute Path and docRoot an equivalent Paht than no start with de same path)
		$beforeStr = strstr($fileDir, $docRoot, true); 
		$docRoot = $beforeStr.$docRoot;

		// Remove docRoot string from fileDir string as subPath string
		$subPath = preg_replace('~' . $docRoot . '~i', '', $fileDir);

		// Add a slash to the beginning of subPath string
		$subPath = '/' . $subPath;          

		// Test subPath string to determine if we are in the web root or not
		if ($subPath == '/') {
			// if subPath = single slash, docRoot and fileDir strings were the same
			//echo "We are running in the web foot folder of http://" . $_SERVER['SERVER_NAME'];
			return $subPath; // '/'
		} else {
			// Anyting else means the file is running in a subdirectory
			//echo "We are running in the '" . $subPath . "' subdirectory of http://" . $_SERVER['SERVER_NAME'];
			if ($thisFileRelativePath){
				$thisFileRelativePath = '/'.trim($thisFileRelativePath, '/').'/';
				$subPath = preg_replace('~' . $thisFileRelativePath . '~i', '', $subPath);
				$subPath = $subPath.'/';
			}
			return $subPath;
		}
	}
}
if (!defined("CMSDIRNAME")){
	define("CMSDIRNAME", "cms");
}
define('URL_ROOT', serverLocation(CMSDIRNAME.'/site/php')); //current file location

if (!defined('ADMINCOOKIENAME')){
	// configuration file 
	require_once(rtrim($_SERVER['DOCUMENT_ROOT'],"/").URL_ROOT."php/config.php");
}

require_once(rtrim($_SERVER['DOCUMENT_ROOT'],"/").URL_ROOT.CMSDIRNAME."/"."php/Upload.php");
require_once(rtrim($_SERVER['DOCUMENT_ROOT'],"/").URL_ROOT.CMSDIRNAME."/"."php/Image_lib.php");

if (!function_exists('addToLog')){
	function addToLog($errorStr='', $masInfoStr='', $errorNumber=3){
		error_log( date('Y-m-d H:i:s').' - '.$errorStr . " - ".$masInfoStr."\n", $errorNumber, rtrim($_SERVER['DOCUMENT_ROOT'],"/").URL_ROOT.CMSDIRNAME.'/logs/debug.log');
		//echo '<pre>'.$errorStr.'</pre>';
	}
}

function log_message($type='', $str=''){
	addToLog("MSG ".$type.": ".$str);
}

$mimes = array(	'hqx'	=>	'application/mac-binhex40',
				'cpt'	=>	'application/mac-compactpro',
				'csv'	=>	array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel'),
				'bin'	=>	'application/macbinary',
				'dms'	=>	'application/octet-stream',
				'lha'	=>	'application/octet-stream',
				'lzh'	=>	'application/octet-stream',
				'exe'	=>	array('application/octet-stream', 'application/x-msdownload'),
				'class'	=>	'application/octet-stream',
				'psd'	=>	'application/x-photoshop',
				'so'	=>	'application/octet-stream',
				'sea'	=>	'application/octet-stream',
				'dll'	=>	'application/octet-stream',
				'oda'	=>	'application/oda',
				'pdf'	=>	array('application/pdf', 'application/x-download'),
				'ai'	=>	'application/postscript',
				'eps'	=>	'application/postscript',
				'ps'	=>	'application/postscript',
				'smi'	=>	'application/smil',
				'smil'	=>	'application/smil',
				'mif'	=>	'application/vnd.mif',
				'xls'	=>	array('application/excel', 'application/vnd.ms-excel', 'application/msexcel'),
				'ppt'	=>	array('application/powerpoint', 'application/vnd.ms-powerpoint'),
				'wbxml'	=>	'application/wbxml',
				'wmlc'	=>	'application/wmlc',
				'dcr'	=>	'application/x-director',
				'dir'	=>	'application/x-director',
				'dxr'	=>	'application/x-director',
				'dvi'	=>	'application/x-dvi',
				'gtar'	=>	'application/x-gtar',
				'gz'	=>	'application/x-gzip',
				'php'	=>	'application/x-httpd-php',
				'php4'	=>	'application/x-httpd-php',
				'php3'	=>	'application/x-httpd-php',
				'phtml'	=>	'application/x-httpd-php',
				'phps'	=>	'application/x-httpd-php-source',
				'js'	=>	'application/x-javascript',
				'swf'	=>	'application/x-shockwave-flash',
				'sit'	=>	'application/x-stuffit',
				'tar'	=>	'application/x-tar',
				'tgz'	=>	array('application/x-tar', 'application/x-gzip-compressed'),
				'xhtml'	=>	'application/xhtml+xml',
				'xht'	=>	'application/xhtml+xml',
				'zip'	=>  array('application/x-zip', 'application/zip', 'application/x-zip-compressed'),
				'mid'	=>	'audio/midi',
				'midi'	=>	'audio/midi',
				'mpga'	=>	'audio/mpeg',
				'mp2'	=>	'audio/mpeg',
				'mp3'	=>	array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
				'aif'	=>	'audio/x-aiff',
				'aiff'	=>	'audio/x-aiff',
				'aifc'	=>	'audio/x-aiff',
				'ram'	=>	'audio/x-pn-realaudio',
				'rm'	=>	'audio/x-pn-realaudio',
				'rpm'	=>	'audio/x-pn-realaudio-plugin',
				'ra'	=>	'audio/x-realaudio',
				'rv'	=>	'video/vnd.rn-realvideo',
				'wav'	=>	array('audio/x-wav', 'audio/wave', 'audio/wav'),
				'bmp'	=>	array('image/bmp', 'image/x-windows-bmp'),
				'gif'	=>	'image/gif',
				'jpeg'	=>	array('image/jpeg', 'image/pjpeg'),
				'jpg'	=>	array('image/jpeg', 'image/pjpeg'),
				'jpe'	=>	array('image/jpeg', 'image/pjpeg'),
				'png'	=>	array('image/png',  'image/x-png'),
				'tiff'	=>	'image/tiff',
				'tif'	=>	'image/tiff',
				'css'	=>	'text/css',
				'html'	=>	'text/html',
				'htm'	=>	'text/html',
				'shtml'	=>	'text/html',
				'txt'	=>	'text/plain',
				'text'	=>	'text/plain',
				'log'	=>	array('text/plain', 'text/x-log'),
				'rtx'	=>	'text/richtext',
				'rtf'	=>	'text/rtf',
				'xml'	=>	'text/xml',
				'xsl'	=>	'text/xml',
				'mpeg'	=>	'video/mpeg',
				'mpg'	=>	'video/mpeg',
				'mpe'	=>	'video/mpeg',
				'qt'	=>	'video/quicktime',
				'mov'	=>	'video/quicktime',
				'avi'	=>	'video/x-msvideo',
				'movie'	=>	'video/x-sgi-movie',
				'doc'	=>	'application/msword',
				'docx'	=>	array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip'),
				'xlsx'	=>	array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip'),
				'word'	=>	array('application/msword', 'application/octet-stream'),
				'xl'	=>	'application/excel',
				'eml'	=>	'message/rfc822',
				'json' => array('application/json', 'text/json')
			);
			
/**
 * Tests for file writability
 *
 * is_writable() returns TRUE on Windows servers when you really can't write to
 * the file, based on the read-only attribute.  is_writable() is also unreliable
 * on Unix servers if safe_mode is on.
 *
 * @access	private
 * @return	void
 */
 define('FOPEN_WRITE_CREATE',					'ab');
if ( ! function_exists('is_really_writable')){
	function is_really_writable($file){
		// If we're on a Unix server with safe_mode off we call is_writable
		if (DIRECTORY_SEPARATOR == '/' AND @ini_get("safe_mode") == FALSE){
			//addToLog('is_really_weritable: 1');
			return is_writable($file);
		}

		// For windows servers and safe_mode "on" installations we'll actually
		// write a file then read it.  Bah...
		if (is_dir($file)){
			$file = rtrim($file, '/').'/'.md5(mt_rand(1,100).mt_rand(1,100));

			if (($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE){
				addToLog('is_really_weritable: 2');
				return FALSE;
			}

			fclose($fp);
			@chmod($file, DIR_WRITE_MODE);
			@unlink($file);
			//addToLog('is_really_weritable: 3');
			return TRUE;
		}
		elseif ( ! is_file($file) OR ($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE){
			addToLog('is_really_weritable: 4');
			return FALSE;
		}

		fclose($fp);
		//addToLog('is_really_weritable: 5');
		return TRUE;
	}
}
//addToLog('URL_ROOT: '.$URL_ROOT);
	
$config['img_path'] = URL_ROOT.'uploads/images'; // Relative to domain name
$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . $config['img_path']; // Physical path. [Usually works fine like this]


/*-------------------------------------------------------------------
| 
| Allowed image filetypes. Specifying something other, than image types will result in error. 
| 
| $config['allowed_types'] = 'gif|jpg|png';
| 
| -------------------------------------------------------------------*/

	
$config['allowed_types'] = 'gif|jpg|png';


/*-------------------------------------------------------------------
| 
| Maximum image file size in kilobytes. This value can't exceed value set in php.ini.
| Set to `0` if you want to use php.ini default:
| 
| $config['max_size'] = 0;
| 
| -------------------------------------------------------------------*/

	
$config['max_size'] = 0;


/*-------------------------------------------------------------------
| 
| Maximum image width. Set to `0` for no limit:
| 
| $config['max_width'] = 0;
| 
| -------------------------------------------------------------------*/

	
$config['max_width'] = IMAGECROPHORIZONTAL;


/*-------------------------------------------------------------------
| 
| Maximum image height. Set to `0` for no limit:
| 
| $config['max_height'] = 0;
| 
| -------------------------------------------------------------------*/

	
$config['max_height'] = IMAGECROPVERTICAL;


/*-------------------------------------------------------------------
| 
| Allow script to resize image that exceeds maximum width or maximum height (or both)
| If set to `TRUE`, image will be resized to fit maximum values (proportions are saved)
| If set to `FALSE`, user will recieve an error message.
| 
| $config['allow_resize'] = TRUE;
| 
| -------------------------------------------------------------------*/

	
$config['allow_resize'] = TRUE;


/*-------------------------------------------------------------------
| 
| Allow script to crop image that exceeds maximum width or maximum height (or both) after resize
| If set to `TRUE`, image will be crop to fit maximum values (proportions are saved) 
| If set to `FALSE`, image will be only resized to get inside maximuns width and height container.
| 
| $config['crop_resize'] = TRUE;
| 
| -------------------------------------------------------------------*/

	
	$config['crop_resize'] = FALSE;
	
	
/*-------------------------------------------------------------------
| 
| Image name encryption
| If set to `TRUE`, image file name will be encrypted in something like 7fdd57742f0f7b02288feb62570c7813.jpg
| If set to `FALSE`, original filenames will be preserved
| 
| $config['encrypt_name'] = TRUE;
| 
| -------------------------------------------------------------------*/

	
$config['encrypt_name'] = FALSE;


/*-------------------------------------------------------------------
| 
| How to behave if 2 or more files with the same name are uploaded:
| `TRUE` - the entire file will be overwritten
| `FALSE` - a number will be added to the newly uploaded file name
| 
| -------------------------------------------------------------------*/


$config['overwrite'] = FALSE;
	
	
/*-------------------------------------------------------------------
| 
| Target upload folder relative to document root. Most likely, you will not need to change this setting.
| 
| -------------------------------------------------------------------*/

	
	
	

/*-------------------------------------------------------------------
| 
| THAT IS ALL. HAVE A NICE DAY! )))
| 
| -------------------------------------------------------------------*/

//addToLog("upload-handler - ". " INICIO ");
//addToLog("upload-handler - ". " FILES: ".print_r($_FILES, true));
//addToLog("upload-handler - ". " HTTP_ORIGIN: ".$_SERVER['HTTP_ORIGIN']);

/*************************
* Is allowed to upload? *
*************************/
if (is_allowed() === FALSE){
	header("HTTP/1.1 500 Server Error");
	return;
}

/***************************************************
* Only these origins are allowed to upload images *
***************************************************/
$accepted_origins = array("http://localhost", "http://localhost:8056", "http://localhost:8887", MAINHOST);

/*********************************************
* Change this line to set the upload folder *
*********************************************/
$imageFolder = $config['img_path'];

reset($_FILES);
$temp = current($_FILES);
if (is_uploaded_file($temp['tmp_name'])){
	if (isset($_SERVER['HTTP_ORIGIN'])) {
	  // same-origin requests won't set an origin. If the origin is set, it must be valid.
	  if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
		header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
	  } else {
		header("HTTP/1.1 403 Origin Denied");
		return;
	  }
	}

	/*
	  If your script needs to receive cookies, set images_upload_credentials : true in
	  the configuration and enable the following two headers.
	*/
	// header('Access-Control-Allow-Credentials: true');
	// header('P3P: CP="There is no P3P policy."');

	// Sanitize input
	if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
		addToLog("upload-handler - ". " INVALID FILE NAME: ".$temp['name']);
		header("HTTP/1.1 400 Invalid file name.");
		return;
	}

	// Verify extension

	if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), explode("|", $config['allowed_types']))) {
		addToLog("upload-handler - ". " INVALID EXTENSION: ".strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)));
		header("HTTP/1.1 400 Invalid extension.");
		return;
	}

	/// TAMAÑOS, REDIMENSION Y RECORTE
	if ($config['allow_resize']){
		if (empty($config['max_width']) AND empty($config['max_height'])){
			$config['allow_resize'] = FALSE;
		}
	}

	//////
	$_FILES['userfile'] = $temp;
	$UploadObj = new Upload($config);
	if ($UploadObj->do_upload()){ // Success
		// General result data
		$result = $UploadObj->data();
		//addToLog('File UPLOADED: '.print_r($result, true));
		
		// Shall we resize an image?
		if ($config['allow_resize'] AND $config['crop_resize'] AND !empty($config['max_width']) AND $config['max_width'] > 0 AND !empty($config['max_height']) AND $config['max_height'] > 0 and ( ($result['image_width'] != $config['max_width']) OR ($result['image_height'] != $config['max_height']) ))
		{
			// MANU : vamos a intentar redimensionar para luego cortar
			$max_width = $config['max_width'];
			$max_height = $config['max_height'];
			$ratio = $config['max_width']/$config['max_height'];
			$img_ratio = $result['image_width']/$result['image_height'];
			if ($img_ratio>$ratio){ //more width than height
				$max_width = (int)$max_height*$img_ratio;
				$crop_width = $max_width - $config['max_width'];
				$crop_height = 0;
			}
			if ($img_ratio<$ratio){ //more height than width
				$max_height = (int)$max_width/$img_ratio;
				$crop_height = $max_height - $config['max_height'];
				$crop_width = 0;
			}
			//
			
			// Resizing parameters
			$resizeParams = array(
				'source_image'	=> $result['full_path'],
				'new_image'		=> $result['full_path'],
				'width'			=> $max_width,
				'height'		=> $max_height
			);
			
			// Load resize library
			$ImageObj = new Image_lib($resizeParams);
			
			// Do resize
			$ImageObj->resize();
			
			// MANU : Vamos a cortar lo sobrante
			$cropParams = array(
				'source_image'	=> $result['full_path'],
				'y_axis'		=> $crop_height,
				'x_axis'		=> $crop_width,
				'width'			=> $config['max_width'],
				'height'		=> $config['max_height'],
				//'new_image'		=> $result['full_path'],
				'maintain_ratio'=> false,
				'image_library'	=> 'gd2'
			);
			// cargamos la configuración
			//$this->load->library('image_lib', $cropParams);
			$ImageObj->initialize($cropParams);
			// cortamos
			if ( !$ImageObj->crop()){
				//echo $this->image_lib->display_errors();
				addToLog('IMAGE LIB CROP ERRORS: '.$ImageObj->display_errors());
			}
			//addToLog('Upload Crop: '.$result['full_path']);
			
		}
		elseif ($config['allow_resize'] AND !$config['crop_resize'] AND !empty($config['max_width']) AND $config['max_width'] > 0 AND !empty($config['max_height']) AND $config['max_height'] > 0 and ( ($result['image_width'] != $config['max_width']) OR ($result['image_height'] != $config['max_height']) ))
		{
			// MANU : vamos a intentar redimensionar para luego cortar
			$max_width = $config['max_width'];
			$max_height = $config['max_height'];
			$ratio = $config['max_width']/$config['max_height'];
			$img_ratio = $result['image_width']/$result['image_height'];
			if ($result['image_width']>$max_width){
				$result['image_width'] = $max_width; 
			}
			$result['image_height'] = (int)$result['image_width']/$img_ratio;
			if ($result['image_height']>$max_height){
				$result['image_height'] = $max_height;
			}
			$result['image_width'] = (int)$result['image_height']*$img_ratio;
			
			//
			
			// Resizing parameters
			$resizeParams = array
			(
				'source_image'	=> $result['full_path'],
				'new_image'		=> $result['full_path'],
				'width'			=> $result['image_width'],
				'height'		=> $result['image_height']
			);
			
			// Load resize library
			$ImageObj = new Image_lib($resizeParams);
			
			// Do resize
			$ImageObj->resize();
			
			//addToLog('Upload: '.$result['full_path']);
			
		}
			
		// Add our stuff
		$result['result']		= "file_uploaded";
		$result['resultcode']	= 'ok';
		$result['file_name']	= $config['img_path'] . '/' . $result['file_name'];
		
		//addToLog('File Name: '.$result['file_name']);
		// Output to user $result data
	}
	else // Failure
	{
		// Compile data for output
		$result['result']		= $UploadObj->display_errors(' ', ' ');
		$result['resultcode']	= 'failed';
		
		addToLog('UPLOAD ERRORS: '.$result['result']);
		// Output to user $result error
		$lastError = end($UploadObj->error_msg);
		
		header("HTTP/1.1 400 ".$lastError .'.');
		return;
	}
	//////

	// Accept upload if there was no origin, or if it is an accepted origin
	//addToLog('Folder: '.$imageFolder. ', File: '.$temp['name']);
	if (!empty($result['file_name'])){
		$fileURLtowrite = $result['file_name'];
	}
	else{
		$fileURLtowrite = $imageFolder ."/".$temp['name'];
	}
	//$filetowrite = $config['upload_path'] ."/".$temp['name'];
	//move_uploaded_file($temp['tmp_name'], $filetowrite);

	// Respond to the successful upload with JSON.
	// Use a location key to specify the path to the saved image resource.
	// { location : '/your/uploaded/image/file'}
	addToLog("upload-handler - ". " JSON: ".json_encode(array('location' => $fileURLtowrite)));
	echo json_encode(array('location' => $fileURLtowrite));
} else {
	// Notify editor that the upload failed
	header("HTTP/1.1 500 Server Error");
}
?>