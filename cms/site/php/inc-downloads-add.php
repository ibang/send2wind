<?php
set_time_limit(0);
function subir_archivo($i,$folder){
	$ret = -2;
	addToLog(print_r($i,true).' : '.$folder);
	$handle = new upload($i);
	if ($handle->uploaded) {
		$handle->allowed	= array('application/pdf'); // only pdf files
		$handle->file_max_size = '8M';
		
		$handle->process($folder);
		if ($handle->processed) {
	          $ret	=	$handle->file_dst_name;
	          $handle->clean();
		} else {
			addToLog('DOWNLOADS File Error: ' . $handle->error);
			return  -3;	// proccess error
		}
	}
	addToLog('RET: '.$ret);
	return $ret; // file destination name or -2 (upload error)
}

// creates a jpg image preview of given pdf file
function makePDFpreview($originPdfFile, $originPdfFolder, $destinationFile, $destinationFolder){
		
	
	if($originPdfFile AND pathinfo($originPdfFile, PATHINFO_EXTENSION)=='pdf'){
		
		if (!file_exists($originPdfFolder.$originPdfFile)){
			return false;
		}
		if (!$destinationFile){
			$destinationFile = str_replace('.pdf','.png', $originPdfFile);
		}
		if (!$destinationFolder){
			$destinationFolder = $originPdfFolder.'preview/';
		}	
		
		if (!file_exists(rtrim($destinationFolder,'/'))){
			@mkdir(rtrim($destinationFolder,'/') , 0777, true);
		}
		
		if (isImagickExtensionLoaded()){
			$preview_file_path = extractPDFpreviewImagick($originPdfFile, $originPdfFolder, $destinationFile, $destinationFolder);
		}
		elseif (!isImageMagickInstaled()){
			return false;
		}
		else{
			$preview_file_path = extractPDFpreviewImageMagick($originPdfFile, $originPdfFolder, $destinationFile, $destinationFolder);
		}
		
		if (!$preview_file_path){
			return false;
		}
		
		//guardo la imagen original al tamaño correcto, recortando si es necesario
		$thumbwidth = PDFPREVIEWIMAGECROPHORIZONTAL;
		$thumbheight = PDFPREVIEWIMAGECROPVERTICAL;
	
		$image = new upload($destinationFolder.$destinationFile);
		$image->file_max_size = '8M';
		$image->image_convert	= 'jpg';
		$image->image_resize	= true;
		$image->image_ratio_crop	= 'TL';
		$image->image_y	= $thumbheight;
		$image->image_x	= $thumbwidth;
		$image->jpeg_quality = 85;
		$image->process($destinationFolder);
		if ($image->processed) {
			$destinationFile	=	$image->file_dst_name;
			$image->clean();
		} else {
			echo 'error : ' . $image->error;
		}
		return $destinationFile;
	}
	else{
		return false;
	}
	return false;
}

function extractPDFpreviewImageMagick($originPdfFile, $originPdfFolder, $destinationFile, $destinationFolder){
	//creamos el preview del pdf ;
	//$out = shell_exec(IMAGEMAGICK_COMMAND_PATH."convert -density 288 ".$originPdfFolder.$originPdfFile."[0] -background white -flatten ".$destinationFolder.$destinationFile." 2>&1");
	$out = shell_exec(IMAGEMAGICK_COMMAND_PATH."convert -density 72 ".$originPdfFolder.$originPdfFile."[0] -background white -flatten ".$destinationFolder.$destinationFile." 2>&1");
	if ($out AND !file_exists($destinationFolder.$destinationFile)){
		echo '<pre>Imagen error:'.$out.'</pre>';
		return false;
	}
	return $destinationFolder.$destinationFile;
}

function extractPDFpreviewImagick($originPdfFile, $originPdfFolder, $destinationFile, $destinationFolder){
	
	// Convert this document
	// Each page to single image
	$img = new imagick($originPdfFolder.$originPdfFile);

	// Set background color and flatten
	// Prevents black background on objects with transparency
	$img->setImageBackgroundColor('white');
	$img = $img->flattenImages();

	// Set image resolution
	$img->setResolution(72,72);

	// Compress Image Quality
	//$img->setImageCompressionQuality(100);    

	// Set iterator postion
	$img->setIteratorIndex(0);

	// Set image format
	$img->setImageFormat('png');

	// Write Images to temp 'upload' folder     
	$img->writeImage($destinationFolder.$destinationFile);

	$img->destroy();
	
	return $destinationFolder.$destinationFile;
}


function isImageMagickInstaled(){
	exec(IMAGEMAGICK_COMMAND_PATH."convert -version", $out, $rcode); //Try to get ImageMagick "convert" program version number.
	//echo "Version return code is $rcode <br>"; //Print the return code: 0 if OK, nonzero if error.
	//echo alist($out); //Print the output of "convert -version"
	if ($rcode!==0){
		return false;
	}
	return true;
}
function isImagickExtensionLoaded(){
	if (!extension_loaded('imagick')){
		return false;
	}
	return true;
}
/* INICIALIZO VARIABLES */
$_GET['action'] = isset($_GET['action'])?$_GET['action']:'';
$_GET['update'] = isset($_GET['update'])?$_GET['update']:'';
$_GET['insert'] = isset($_GET['insert'])?$_GET['insert']:'';
$_GET['code'] = isset($_GET['code'])?$_GET['code']:'';
$_GET['lang'] = isset($_GET['lang'])?$_GET['lang']:'';
$_GET['del_file'] = isset($_GET['del_file'])?$_GET['del_file']:'';
$_GET['field'] = isset($_GET['field'])?$_GET['field']:'';
$_GET['id'] = isset($_GET['id'])?$_GET['id']:'';
$_GET['file'] = isset($_GET['file'])?$_GET['file']:'';
$_GET['image'] = isset($_GET['image'])?$_GET['image']:'';
$_GET['imagefield'] = isset($_GET['imagefield'])?$_GET['imagefield']:'';
$update = '';
$insert = '';
$downloads = array();
$code = '';
//

$langs=$db->get_cms_active_langs('downloads');

if ($_GET["code"]){
	$code=$_GET["code"];
}

$lang_active='';
$error=0;
$warning=0;

$validate = array_fill_keys(array(
	"file", "headline"), array());


if ($_GET["action"]=="delete"){
	$code=$_GET["code"];
	$lang=$_GET["lang"];
	$downloads_del=$db->get_downloads($code);

	if ($lang=="all"){
		foreach ($downloads_del as $file_img_del){
			if (!empty($file_img_del["image1"])){@unlink ("{$DOC_WEB_ROOT}uploads/downloads/preview/".$file_img_del["image1"]);};
			if (!empty($file_img_del["file1"])){@unlink ("{$DOC_WEB_ROOT}uploads/downloads/".$file_img_del["file1"]);};
		}

		$db->delete_downloads($code);
		header('Location: '.$URL_ROOT.'site/downloads/');
		exit();
	}else{
		$downloads_lang_del=$db->get_downloads_lang($lang,$code);
		if (count($downloads_del)==1){
			if (!empty($downloads_lang_del["image1"])){@unlink ("{$DOC_WEB_ROOT}uploads/downloads/preview/".$downloads_lang_del["image1"]);};
			if (!empty($downloads_lang_del["file1"])){@unlink ("{$DOC_WEB_ROOT}uploads/downloads/".$downloads_lang_del["file1"]);};
	
		}
		$db->delete_downloads_lang ($code,$lang);
		header('Location: '.$URL_ROOT.'site/downloads/' );
		exit();
	}
	
}

if (!empty($_POST)){
	
	$_POST["headline"] = (isset($_POST["headline"])?trim($_POST["headline"]):'');
	$form_lang_code = $db->get_lang_code($_POST["lang"]); // no transliteramos al alfabeto latino el ruso
	$downloads_form=array(
		"id"				=>	(isset($_POST["id"])?$_POST["id"]:''),
		"code"				=>	(isset($_POST["code"])?$_POST["code"]:''),
		"lang"				=>	(isset($_POST["lang"])?$_POST["lang"]:''),
		"headline"			=>	addslashes($_POST["headline"]),
		"slug"				=>	url_slug($_POST["headline"].(!empty($_POST["filelanguage"])?' '.$_POST["filelanguage"]:''), array('transliterate' => ($form_lang_code!='ru'?true:false))),
		"subtitle"			=>	(isset($_POST["subtitle"])?addslashes($_POST["subtitle"]):''),
		"filelanguage"		=>	(isset($_POST["filelanguage"])?addslashes($_POST["filelanguage"]):''),
		"date"				=>	(isset($_POST["date"])?addslashes($_POST["date"]):''),
		"category"	        =>	(isset($_POST["category"])?$_POST["category"]:''),
		"meta-title"		=>	(isset($_POST["meta-title"])?trim(addslashes($_POST["meta-title"])):''),
		"meta-description"	=>	(isset($_POST["meta-desc"])?trim(addslashes($_POST["meta-desc"])):''),
		"meta-keywords"		=>	(isset($_POST["meta-key"])?trim(addslashes($_POST["meta-key"])):''),
		"active"			=>	(isset($_POST["active"])?$_POST["active"]:'Draft'),
		"is_public"			=>	(isset($_POST["is_public"])?$_POST["is_public"]:'Yes')
        );
        
    $downloadExist['lang'] = '';
    if ( !empty($_POST["code"]) ) {
        $downloadExist = $db->get_downloads_lang('general',$downloads_form["code"]);
    }

	if (!empty($_POST["code"] && $downloads_form["lang"] === $downloadExist['lang'] ) ){
		
		//------update noticia------//
		$downloads_org=$db->get_downloads_lang($downloads_form["lang"],$downloads_form["code"]);
		$downloads_form['order'] = $downloads_org['order'];
		
		//--una imagen--//
		if (!empty($_FILES["file1"]) AND $_FILES["file1"]["name"]!=NULL){
			$uploaded_file = subir_archivo($_FILES["file1"],"{$DOC_WEB_ROOT}uploads/downloads/");
			if (!is_numeric($uploaded_file)){
				$downloads_form["file1"] = $uploaded_file;
				$downloads_form["filesize1"] = get_filesize("{$DOC_WEB_ROOT}uploads/downloads/".$uploaded_file);
				$downloads_form["fileformat1"] = get_fileformat("{$DOC_WEB_ROOT}uploads/downloads/".$uploaded_file);
				$downloads_form["image1"] = makePDFpreview($downloads_form["file1"], "{$DOC_WEB_ROOT}uploads/downloads/", "", "{$DOC_WEB_ROOT}uploads/downloads/preview/");
				if ($downloads_org["file1"]!=""){
					@unlink ("{$DOC_WEB_ROOT}uploads/downloads/".$downloads_org["file1"]);
					@unlink ("{$DOC_WEB_ROOT}uploads/downloads/preview/".$downloads_org["image1"]);
				};
			}
			else{
				$downloads_form["image1"] = $downloads_org["image1"];
				$downloads_form["file1"] = $downloads_org["file1"];
				$downloads_form["filesize1"] = $downloads_org["filesize1"];
				$downloads_form["fileformat1"] = $downloads_org["fileformat1"];
				$validate["file"]["upload"]= abs($uploaded_file); // 2 => upload error or 3 => proccess error
				$error++;
			}
        }elseif(!empty($_FILES["file1"]) AND empty($downloads_org["file1"])){ // File name is null
			$downloads_form["file1"] = '';
			$downloads_form["filesize1"] = 0;
			$downloads_form["fileformat1"] = '';
			$downloads_form["image1"] = '';
			$validate["file"]["upload"]=1; // is required
			$error++;
		}else{
			$downloads_form["file1"] = $downloads_org["file1"];
			$downloads_form["filesize1"] = $downloads_org["filesize1"];
			$downloads_form["fileformat1"] = $downloads_org["fileformat1"];
			$downloads_form["image1"] = $downloads_org["image1"];
			if ($downloads_form["file1"]!="" ){
				if(empty($downloads_form["filesize1"])){
					$downloads_form["filesize1"] = get_filesize("{$DOC_WEB_ROOT}uploads/downloads/".$downloads_form["file1"]);
				}
				if(empty($downloads_form["fileformat1"])){
					$downloads_form["fileformat1"] = get_fileformat("{$DOC_WEB_ROOT}uploads/downloads/".$downloads_form["file1"]);
				}
				if (empty($downloads_form["image1"])){
					$downloads_form["image1"] = makePDFpreview($downloads_form["file1"], "{$DOC_WEB_ROOT}uploads/downloads/", "", "{$DOC_WEB_ROOT}uploads/downloads/preview/");
				}
			}
		}
		
		
        //--- check used headline --//
		if ((strtolower(addslashes($downloads_org["headline"]))!=strtolower($downloads_form["headline"])) or ($downloads_org["slug"]!=$downloads_form["slug"])){
			if ( $db->headline_downloads_exist($downloads_form["headline"], $downloads_form["slug"], $downloads_form["lang"],  $downloads_form["code"])){
				$error++;
				$validate["headline"]["duplicated"]=1;
			}
			if (preg_match('/^\d+$/D', $downloads_form["slug"])){
				$error++;
				$validate["headline"]["isnumber"]=1;
			}
		}
		
		
		//--- Is there is a error on any of the fields that not avoid update the download file
		$downloads_form_update = $downloads_form;
		if (!empty($validate["headline"]) ){
			$downloads=$db->get_downloads($downloads_form["code"]);

			foreach ($downloads as $id => $current){
				if ($current['lang']==$downloads_form['lang']){
					$downloads[$id] = $downloads_form;
					$downloads[$id]['lang_code'] = $current['lang_code'];
					$lang_active = $current['lang_code'];
					break;
				}
			}
			// reset error fields
			if (!empty($validate["headline"])){
				$downloads_form_update['slug'] = '';
				$downloads_form_update["headline"] = '';
			}

			
			
			// no actualizo el description hasta que no haya errores
			$downloads_form_update["description"] = $downloads_org["description"];
		}
		
		$db->update_downloads($downloads_form_update);
		if ($error AND empty($downloads) AND !empty($validate["file"])){
			$downloads = $db->get_downloads($downloads_form["code"]);
			foreach ($downloads as $id => $current){
				if ($current['lang']==$downloads_form['lang']){
					$downloads[$id] = $downloads_form;
					$downloads[$id]['lang_code'] = $current['lang_code'];
					$lang_active = $current['lang_code'];
					break;
				}
			}
		}
		//--- messages to show ----
		if ($downloads_form['active']=='Draft'){ //borrador
			$warning++;
			$attention['active']['draft']=1;
		}
		$code=$downloads_form["code"];
		$lang=$downloads_form["lang"];
		if (!$error){
			if ($warning){
				$warn = '&warning='.$warning.'&attention='.json_encode($attention);
			}
			else{
				$warn = '&update=ok';
			}
			header("Location: add.php?code=$code&lang=$lang".$warn);
			exit();
		}
	}else{
		//------nueva download------//
		if (empty($downloads_form["code"])){
			$last_code=$db->get_downloads_last_code();
			$downloads_form["code"]=$last_code+1;
			
			$last_position=$db->get_downloads_last_position();
			$downloads_form["order"]=$last_position+1;
		}
		else{
			$code = $downloads_form["code"];
		}
		
		if (!empty($code)){
			$downloads_org = $db->get_downloads_lang('general',$downloads_form["code"]);
			if (!empty($downloads_org)){
				$downloads_form["order"] = $downloads_org['order'];
			}
		}
		
        //--- check used headline --//
		if ($db->headline_downloads_exist($downloads_form["headline"], $downloads_form["slug"], $downloads_form["lang"])){
			$error++;
			$validate["headline"]["duplicated"]=1;
		}
		if (preg_match('/^\d+$/D', $downloads_form["slug"])){
			$error++;
			$validate["headline"]["isnumber"]=1;
		}
		
		//--- check related downloadscategories and brands
		if (empty($downloads_form['category'])){
			$validate["category"]["noelement"]=1;
			$error++;
		}
		
		
		if ($error AND (!empty($validate["headline"]) )){
			$downloads_form["code"] = false;
			$downloads[0] = $downloads_form;
			$downloads[0]['lang_code'] = $db->get_lang_code($downloads[0]['lang']);
			$lang_active = $downloads[0]['lang_code'];
		}
		
		if (!$error){
			
			//--una imagen--//
			if (!empty($_FILES["file1"]) AND $_FILES["file1"]["name"]!=NULL){
				$uploaded_file = subir_archivo($_FILES["file1"],"{$DOC_WEB_ROOT}uploads/downloads/");
				if (!is_numeric($uploaded_file)){
					$downloads_form["file1"] = $uploaded_file;
					$downloads_form["filesize1"] = get_filesize("{$DOC_WEB_ROOT}uploads/downloads/".$uploaded_file);
					$downloads_form["fileformat1"] = get_fileformat("{$DOC_WEB_ROOT}uploads/downloads/".$uploaded_file);
					$downloads_form["image1"] = makePDFpreview($downloads_form["file1"], "{$DOC_WEB_ROOT}uploads/downloads/", "", "{$DOC_WEB_ROOT}uploads/downloads/preview/");
					$downloads_form["file1"] = $uploaded_file;
					if ($downloads_org["file1"]!=""){
						@unlink ("{$DOC_WEB_ROOT}uploads/downloads/".$downloads_org["file1"]);
						@unlink ("{$DOC_WEB_ROOT}uploads/downloads/preview/".$downloads_org["image1"]);
					};
				}
				else{
					$downloads_form["file1"] = '';
					$downloads_form["filesize1"] = 0;
					$downloads_form["fileformat1"] = '';
					$downloads_form["image1"] = '';
					$validate["file"]["upload"]= abs($uploaded_file); // 2 => upload error or 3 => proccess error;
					$error++;
				}
			}else{
				$downloads_form["file1"] = $downloads_org["file1"];
				$downloads_form["filesize1"] = $downloads_org["filesize1"];
				$downloads_form["fileformat1"] = $downloads_org["fileformat1"];
				$downloads_form["image1"] = $downloads_org["image1"];
				if (empty($downloads_form["file1"])){
					$validate["file"]["upload"]=1; // required
					$error++;
				}
				else{
					if(empty($downloads_form["filesize1"])){
						$downloads_form["filesize1"] = get_filesize("{$DOC_WEB_ROOT}uploads/downloads/".$downloads_form["file1"]);
					}
					if(empty($downloads_form["fileformat1"])){
						$downloads_form["fileformat1"] = get_fileformat("{$DOC_WEB_ROOT}uploads/downloads/".$downloads_form["file1"]);
					}
					if(empty($downloads_form["image1"])){
						$downloads_form["image1"] = makePDFpreview($downloads_form["file1"], "{$DOC_WEB_ROOT}uploads/downloads/", "", "{$DOC_WEB_ROOT}uploads/downloads/preview/");
					}
				}
			}
			
			
			if (!$error){
				$db->add_downloads ($downloads_form);
				
				if ($_POST['active']=='Draft'){ //borrador
					$warning++;
					$attention['active']['draft']=1;
				}
			
				$code=$downloads_form["code"];
				$lang = $downloads_form["lang"];
				if ($warning){
					$warn = '&warning='.$warning.'&attention='.json_encode($attention);
				}
				else{
					$warn = '&insert=ok';
				}
				header("Location: add.php?code=$code&lang=$lang".$warn);
				exit();
			}
			else{
				$downloads[0] = $downloads_form;
				$code = $downloads_form['code'];
				$downloads[0]['lang_code'] = $db->get_lang_code($downloads[0]['lang']);
				$lang_active = $downloads[0]['lang_code'];
			}
		}
		else{
			$downloads_form["code"] = false;
			//brochure
			$downloads_form["file1"] = $downloads_org["file1"];
			
			//iniciamos brochure
			$downloads[0]["file1"] = $downloads_form["file1"];
		}
	}
}

//------------- extraigo noticia en todos los idiomas en los que esté -----
$downloads_lang = array();
if ($_GET['code'] or (isset($_POST) and $error)){
	if (!$error){
		$downloads=$db->get_downloads($code);
		if ($_GET["update"]){
			$update=$_GET["update"];
		
		}
		if ($_GET["insert"]){
			$insert=$_GET["insert"];
		
		}
	}
	else{ // form data error
		// en caso de error en el formulario no hay que hacer nada en la variable de sessión ya que contiene los datos que tiene que tener y
		// downloads estará actualizado con los datos del formulario erróneo
	}

	//------------- creo un array downloads_codigoidioma para cada idioma -----
	//echo '<!--'.print_r($downloads, true).'-->';
	$default_lang = '';
	foreach ($downloads as $current){
		$downloads_lang[$current["lang_code"]]=array(
			"id"=>$current["id"],
			"code"=>$code,
			"lang"=>$current["lang"],
			"headline"=>$current["headline"],
			"date"=>$current["date"],
			"filelanguage"=>$current["filelanguage"],
			"subtitle"=>$current["subtitle"],
			"image1"=>$current["image1"],
			"file1"=>$current["file1"],
			"filesize1"=>$current["filesize1"],
			"fileformat1"=>$current["fileformat1"],
			"active"=>$current['active'],
			"is_public"=>$current['is_public'],
			"category"=>$current["category"],
			"order"=>$current['order']
		);
		$default_lang = $current["lang_code"];
	}
	
	if ($default_lang){
		foreach ($langs as $current_lang){
			if (empty($downloads_lang[$current_lang["lang"]])){
				$downloads_lang[$current_lang["lang"]] = $db->get_downloads_lang($current_lang["id"], $downloads_lang[$default_lang]["code"]);
				if (empty($downloads_lang[$current_lang["lang"]])){
					$downloads_lang[$current_lang["lang"]] = array_fill_keys(array(
						"id", "code", "lang", "headline", "slug", "filelanguage", "subtitle", "image1", "file1", "filesize1", "fileformat1", "active", "is_public", "date", "order", "downloadscategories_code"),'');
				}
				// GENERAL FIELDS (comun to all languages)
				$downloads_lang[$current_lang["lang"]]=array_merge($downloads_lang[$current_lang["lang"]], array(
					"code"=>$code,
					"lang"=>$current_lang["lang"],
					"date"=>$downloads_lang[$default_lang]["date"],
					"image1"=>$downloads_lang[$default_lang]["image1"],
					"file1"=>$downloads_lang[$default_lang]["file1"],
					"filesize1"=>$downloads_lang[$default_lang]["filesize1"],
					"fileformat1"=>$downloads_lang[$default_lang]["fileformat1"],
					"order"=>$downloads_lang[$default_lang]["order"],
					"category"=>$downloads_lang[$default_lang]["category"]
				));
			}
		}
	}
}
else{ //nueva
	//INICIALIZAMOS
	foreach ($langs as $current_lang){
		$downloads_lang[$current_lang["lang"]] = array_fill_keys(array(
		"id", "code", "lang", "headline", "slug", "filelanguage", "subtitle", "image1", "file1", "filesize1", "fileformat1", "active", "is_public", "date", "order", "category"),'');

	}
}

$activeLangsCode = $db->get_cms_active_langs_array('downloads');
if (!$lang_active){
	if (isset($_GET['lang']) AND !empty($_GET['lang'])){
		$lang_active = $db->get_lang_code($_GET['lang']);
	}
	else{
		$lang_active= $db->get_cms_default_lang();;
		if (count($downloads)>1){
			$lang_active= $db->get_cms_default_lang();;
		}
		elseif (!empty($downloads[0]) AND in_array($downloads[0]['lang_code'], $activeLangsCode)){
			$lang_active=$downloads[0]['lang_code'];
		}
	}
}

if (isset($_GET['warning']) AND !empty($_GET['warning'])){
	$warning = $_GET['warning'];
	$attention = json_decode($_GET['attention'], true);
}
?>